!======================================================================
! A module to segregate Terry's code from the rest of the program,
! and ensure maximum interopamability between gas phase and crystal
! PESs.
! Terry, Terry, quite contrary,
! How does your surface grow?
!======================================================================
module crystPES_mod
use PES_mod, ONLY : tPES, tSplineFrag
use molecule_specs, ONLY : molsysdat
use surface
use interpTrajData

implicit none

!======================================================================
! Contains everything needed for a gas-crystal PES.
!======================================================================
type, extends(tPES) :: tCrystPES
  type(POTobject) :: potfile
  type(POTinstance), dimension(:), allocatable :: mols
  type(molsysdat), pointer :: sys

  ! TOUT purposes:
  logical :: TOUT = .false.
  logical :: degub = .false.
  logical :: bigMem = .false.
  logical :: updateNeighbours = .true.
  real(kind=8) :: maxRMS
  real(kind=8) :: lowest = 0.0d0
  real(kind=8), dimension(:,:,:), allocatable :: TOUTCoords
  character(len=100) :: name
  integer :: nFrag, nSpline
  integer, dimension(:,:), allocatable :: atomIndex
  real(kind=8) :: SPE ! single-point energy
  integer(kind=2), dimension(:,:), allocatable :: nforc
  integer(kind=2), dimension(:,:,:), allocatable :: neighList, aPerm, sgPerm

contains

  procedure, pass(this) :: harvestArgs => harvestArgs
  procedure, pass(PES) :: load => loadPES
  procedure, pass(this) :: calcEnergy => calcEnergy
  procedure, pass(this) :: setupTOUT => setupTOUT
  procedure, pass(this) :: writeTOUT => writeTOUT
  procedure, pass(this) :: LoadXYZ => LoadXYZ
  procedure, pass(this) :: checkNeighs => checkNeighs
  procedure, pass(this) :: allocateIDs => allocateIDs
  procedure, pass(this) :: allocateNeighbours => allocateNeighbours

end type tCrystPES

contains

!======================================================================
! Harvests relevant command-line arguments from the given Arguments
! object. Must be called before 'loadPES' for maximum effectiveness.
!======================================================================
subroutine harvestArgs(this,args)
  use Args_mod, ONLY : tArgs

  class(tCrystPES) :: this
  type (tArgs), intent(in) :: args

  this%absEnergy = args%absEnergy
  this%CPUtime = args%CPUtime
  this%degub = args%degub
  this%pbc = args%pbc
  this%bigMem = args%bigMem
  this%printLowE = args%printLowE

end subroutine harvestArgs

!=======================================================================
! Loads the crystal PES - mostly Terry's code.
!=======================================================================
subroutine LoadPES(PES,sys)

  use molecule_specs, ONLY : molsysdat
  use omp_lib, ONLY : OMP_get_max_threads
  implicit none

  class(tCrystPES) :: PES
  type(molsysdat), target, intent(in) :: sys
  integer :: i, j, k, badness, maxThreads, unit
  integer :: n1, n2, n
  logical :: fileExists
  character(25) :: potFile

  PES%sys => sys

  ! Parallelisation of Terry's code, please work
  maxThreads = OMP_get_max_threads()

  INQUIRE(file='IN_CRYSTPES',exist=fileExists)
  if (.not.fileExists) stop 'IN_CRYSTPES not found!'


  ! Go ahead and load all the crystal details first.

  if (SurfaceCoordsIncludeHeight.and.(PeriodicDOF==3)) stop 'There is no height in a 3D pot'

  ranks=1
  rank=0
  Fragmented=.false.
  i=0

  PES%nAtoms = sys%natom
  call readccm(PES%potfile,'IN_SYSTEM_CRYST',.false.,i,.false.)

  if ((PeriodicDOF==3).and..not.(FrozenMolecule.or.NoForce)) write(*,*)'Jordan, is that you?  Check your flags!'
  nsurfa=0
  nsurfc=0
  CperUC=0
  nc1=0
  nc2=0
  call readz(PES%potfile,'')
  if (PeriodicDOF==2) then
    call readsp(PES%potfile,'IN_SURFPERMS')
  else
    call readsp(PES%potfile,'IN_LATTICEPERMS')
  endif

  call makebp(PES%potfile,'IN_ATOMPERMS',.true.)
  call cntl(PES%potfile,'IN_CNT',.false.)

  if (badness.ne.0) stop 'Error allocating PES%mols array'
  call readlatt(PES%potfile)
  call readt(PES%potfile,'POT')

  ! Expand the space group permutations, and grid up the unit cell into bins
  ! for quicker interpolation.
  call expandPerms(PES%potfile)


  PES%potfile%degub = PES%degub

  ! Load bits and pieces for splines and fragments, etc.
  open(newunit=unit,file='IN_CRYSTPES',status='unknown',action='read')
  write(11,*)
  write(11,*)
  write(11,*) '----------------------------------------'
  write(11,*) '  Reading PES details from IN_CRYSTPES'
  write(11,*) '----------------------------------------'
  ! Comment lines
  read(unit,*)
  read(unit,*)
  read(unit,*)
  ! Name of crystal PES
  read(unit,*) PES%name
  write(11,*) 'Name of crystal/lattice PES:   ', PES%name
  read(unit,*)
  read(unit,*) PES%nFrag
  write(11,*) 'There will be',PES%nFrag, 'fragments of this PES.'
  n = PES%potfile%natom
  allocate(PES%atomIndex(1:PES%nFrag,1:n))
  ! Read atom indexes
  read(unit,*)
  write(11,*) 'Indexes of atoms in fragments, relative to IN_SYSTEM:'
  do i=1,PES%nFrag
    read(unit,*) (PES%atomIndex(i,j),j=1,n)
    write(11,*) i, ':', (PES%atomIndex(i,j),j=1,n)
  enddo
  ! Single-point energy:
  read(unit,*)
  read(unit,*)
  read(unit,*) PES%Vmin, PES%SPE
  write(11,*) 'PES minimum energy to use:', PES%Vmin
  write(11,*) 'Single-point energy to add/subtract:', PES%SPE
  read(unit,*)
  read(unit,*) PES%nSpline
  write(11,*) 'Number of additional cubic spline surfaces:', PES%nSpline

  if (PES%nSpline.gt.0) then
    allocate(PES%FragContainer(1:PES%nSpline))
    read(unit,*)
    ! Read spline details
    do i=1,PES%nSpline
      ! Allocate polymorphic object.
      allocate(tSplineFrag :: PES%FragContainer(i)%fragment)

      select type (frag => PES%FragContainer(i)%fragment)
      type is (tSplineFrag)
        read(unit,*)
        read(unit,*) frag%name
        read(unit,*)
        read(unit,*) frag%nAtoms
        read(unit,*)
        read(unit,*) n1, n2 ! number of atoms in both centre-of-masses
        allocate(frag%CoMs(2))
        frag%CoMs(1)%nAtoms = n1
        frag%CoMs(2)%nAtoms = n2
        allocate(frag%CoMs(1)%indices(1:n1))
        allocate(frag%CoMs(2)%indices(1:n2))
        allocate(frag%CoMs(1)%mass(1:n1))
        allocate(frag%CoMs(2)%mass(1:n2))

        ! Read atom indices for centre-of-masses
        read(unit,*)
        read(unit,*) (frag%CoMs(1)%indices(j),j=1,n1)
        read(unit,*) (frag%CoMs(2)%indices(j),j=1,n2)

        ! Sum the masses:
        do j=1,2
          frag%CoMs(j)%sumMasses = 0.d0
          do k=1,frag%CoMs(j)%nAtoms
            n = frag%CoMs(j)%indices(k)
            frag%CoMs(j)%sumMasses = frag%CoMs(j)%sumMasses + sys%mass(n)
            ! Save the mass of this atom while we're at it.
            frag%CoMs(j)%mass(k) = sys%mass(n)
          enddo
        enddo

        ! Fragment sameness:
        read(unit,*)
        read(unit,*) frag%identicalTo

        if (frag%identicalTo.ne.-1) then
          if (frag%identicalTo.gt.PES%nSpline) then
            print *, 'Spline PES fragment ', trim(frag%name), " is &
              identical to fragment that doesn't exist!"
            call exit(1)
          endif

          ! Skip the next two lines.
          read(unit,*)
          read(unit,*)
        else
          ! Load spline PES:
          read(unit,*)
          read(unit,*) potFile

          call frag%spline%load(trim(potFile))
          frag%identicalTo = i
        endif

        ! Additive or subtractive?
        read(unit,*)
        read(unit,*) frag%multiplier

        read(unit,*)
      end select

    enddo

    ! Loop again to ensure identical fragment pointers point properly.
    write(11,*) '** Pointing pointers properly **'
    do i=1,PES%nSpline
      select type (frag => PES%FragContainer(i)%fragment)
      type is (tSplineFrag)
        j = frag%identicalTo
        write(11,*) 'Spline', i, 'is identical to spline', j
        select type (identFrag => PES%FragContainer(j)%fragment)
        type is (tSplineFrag)
          frag%identFrag => identFrag
        end select
      end select
    enddo
  endif

end subroutine LoadPES

!=====================================================================
! Calculates the potential energy V of the given geometry.
! fragEnergies will not be used here (for now, at least).
!=====================================================================
function calcEnergy(this,coords,ID,step,lowE,fragEnergies) result(V)
  use omp_lib, ONLY : OMP_get_thread_num
  class(tCrystPES) :: this

  real(kind=8), dimension(1:this%nAtoms,1:3), intent(in) :: coords
  integer, dimension(:), intent(in) :: ID
  integer, intent(in) :: step
  logical, optional, intent(out) :: lowE
  real(kind=8), dimension(:), optional, intent(out) :: fragEnergies
  real(kind=8), dimension(1:this%nFrag+this%nSpline) :: fragE, fragRMS
  real(kind=8) :: V, tempV, dist
  real(kind=8) :: bohr2ang = 0.52917720859d0
  real(kind=8), dimension(2,3) :: coms
  real(kind=8), dimension(1:3) :: dx
  integer :: m, j, k, l, i, ind, unit, nAtom, threadID
  logical :: writeTOUT, dLowE

  threadID = OMP_get_thread_num()+1

  writeTOUT = .false.
  V = 0.0d0
  dLowE = .false.

  ! First, loop over lattice PESs
  nAtom = this%potfile%natom
  l = ID(1)
  !m=1
  do i=1,this%nFrag
    ! Copy coordinates for this fragment
    do j=1,nAtom
      k = this%atomIndex(i,j)
      this%mols(threadID)%c(j,:,1) = coords(k,1:3)
    enddo

    ! Start the energy evaluation here
    call intern(this%mols(threadID),this%potfile,1)

    ! Pass this walker's existing neighbourlist to to mols object.
    ! Arrays will be updated if needed.
    if (this%bigMem) then
      j = this%nforc(i,l)
      this%mols(threadID)%nforc(1) = j
      ! Copy arrays
      this%neighList(l,1:j,i) = this%mols(threadID)%mda(1:j,1)
      this%aPerm(l,1:j,i) = this%mols(threadID)%nda(1:j,1)
      this%sgPerm(l,1:j,i) = this%mols(threadID)%n2da(1:j,1)
    endif

    if (this%potfile%ipart==1) then
      ! Check if neighbours need to be updated...
      if (this%updateNeighbours) then
        if (this%degub) then ! call old neighbours routine
         call neighbour(this%mols(threadID),this%potfile,1)
        else
         ! call new neighbours routine
         call neighbourGrid(this%mols(threadID),this%potfile,1)
        endif
        if (this%bigMem) then
          ! Only bother with these if we have a lot of memory.
          j = this%mols(threadID)%nforc(1)
          this%nforc(i,l) = this%mols(threadID)%nforc(1)
          this%neighList(l,1:j,i) = this%mols(threadID)%mda(1:j,1)
          this%aPerm(l,1:j,i) = this%mols(threadID)%nda(1:j,1)
          this%sgPerm(l,1:j,i) = this%mols(threadID)%n2da(1:j,1)
        endif
      endif
      !print *, this%mols(threadID)%nforc(l)
      call dvdi(this%mols(threadID),this%potfile,1)
    else ! two part weight function, yuck
      stop "Two part weight functions not implemented properly"
      if (this%updateNeighbours) then
        call neighbour1(this%mols(threadID),this%potfile,l)
        call neighbour2(this%mols(threadID),this%potfile,l)
        call neighbour3(this%mols(threadID),this%potfile,l)
      endif
      call dvdir(this%mols(threadID),this%potfile,l)
    endif
    ! Check for low energy-ness. Will use a tolerance of 0.5 kJ mol-1
    ! below the minimum; this is a substantial chunk of the 2.84 kJ mol-1
    ! ZPE for a single H2 in MOF5.
    if (this%killLowE.and.(this%potfile%vmin-this%mols(threadID)%en(1).gt.1.9D-4)) then
      lowE = .true.
      if (this%printLowE) then
        !$OMP CRITICAL (lowE1)
        write(11,*) '--------------------------------------------------'
        write(11,*) ' Walker killed: energy more than 0.5 kJmol < Vmin '
        write(11,*) '         : interp%vmin  =',this%potfile%vmin
        write(11,*) '         : energy       =',this%mols(threadID)%en(1)
        write(11,*)

        do j=1, this%potfile%natom
          write(11,*) this%mols(threadID)%c(j,:,1)
        enddo
        !$OMP END CRITICAL (lowE1)
      endif

      ! Abort energy evaluation
      if (.not.this%TOUT) return
    endif

    V = V + this%mols(threadID)%en(1)
    fragE(i) = this%mols(threadID)%en(1)
    fragRMS(i) = this%mols(threadID)%rms(1)
    !print *, 'frag cryst energy:', i, this%mols(threadID)%en(1)
    !print *, fragE(i), fragRMS(i)
  enddo

  ! Calculate spline PES energies
  do i=1,this%nSpline
    coms = 0.0d0
    ! Evaluate spline manually, in case we need to enforce periodic boundary conditions
    select type (frag => this%FragContainer(i)%fragment)
    ! Need to actually tell compiler that this is a spline fragment.
    ! (I know this is dumb, but I've dug my own hole here
    ! with polymorphic objects and I can't be bothered to undig it. Sorry.)
    type is (tSplineFrag)
      ! Calculate centre-of-masses of the splines, convert to fractional
      ! coordinates.
        do j=1,2
          do k=1,frag%CoMs(j)%nAtoms
            ind = frag%CoMs(j)%indices(k)
            do m=1,3
              coms(j,m) = coms(j,m) + coords(ind,m)*frag%CoMs(j)%mass(k)
            enddo
          enddo
          coms(j,:) = coms(j,:)/frag%coms(j)%sumMasses
        enddo
        ! Convert the centre-of-masses to fractionals.
        coms = this%sys%cartToFrac(2,coms)


        dx(:) = ABS(coms(1,:)-coms(2,:))
        !print *, 'before', SQRT(DOT_PRODUCT(dx,dx))*this%sys%length
        if (this%pbc) then
          ! Use the minimum image convention - dx,dy,dz should not be longer than
          ! half the box width; if so, map to smallest distance.
          where (dx > 0.5)
            dx = 1.0d0 - dx
          endwhere
        endif

        ! Calculate new distance
        dist = DSQRT(DOT_PRODUCT(dx,dx))*this%sys%length

        ! Do spline interpolation with this new distance
        tempV = frag%identFrag%spline%splint(dist)

        fragE(this%nFrag+i) = tempV

        !print *, 'spline energy', i, dist*0.529, (tempV+2.3365558659820)*2625.5



        !print *, 'after', SQRT(DOT_PRODUCT(dx,dx))*this%sys%length

        !print *, dx(:)

    end select
      !tempV = this%FragContainer(i)%fragment%calcE(coords,l,step,dLowE)

    !print *, i, tempV

    V = V + tempV*this%FragContainer(i)%fragment%multiplier
  enddo

  ! Add the single-point energy and subtract the global minimum
  V = V + this%SPE - this%Vmin


  if (PRESENT(fragEnergies)) fragEnergies = fragE


  if (.not.this%TOUT) return ! Avoids needless OMP critical section if we're not writing TOUT


  ! Are any of these geometries TOUT-worthy?
  !$OMP CRITICAL (crystTOUT)
  do i=1,this%nFrag
    ! Check for lowE first.
    if (fragE(i).lt.this%potfile%vmin - 1.9d-4) then
      if (fragE(i).lt.this%lowest) then
        this%lowest = fragE(i)

        ! Save this fragment geometry.
        do j=1,nAtom
          k = this%atomIndex(i,j)
          this%TOUTCoords(2,j,1:3) = coords(k,1:3)
        enddo
        writeTOUT = .true.
      endif
    else
      ! Check RMS for this fragment.
      if (fragRMS(i).gt.this%maxRMS) then
        ! Save this rms and geometry.
        this%maxRMS = fragRMS(i)
        do j=1,nAtom
          k = this%atomIndex(i,j)
          this%TOUTCoords(1,j,1:3) = coords(k,1:3)
        enddo
        writeTOUT = .true.
      endif
    endif
  enddo



  if (writeTOUT) then
    open(newunit=unit,file='TOUT',status='replace',access='sequential')

    ! Rewrite the TOUT file.
    write(unit,*) 'rms'
    do j=1, nAtom
      write(unit,*) (this%TOUTCoords(1,j,k)*bohr2ang,k=1,3)
    enddo


    ! Write low energy geometries, if any.
    if (this%lowest < this%potfile%vmin - 1.9d-4) then
      write(unit,*) 'low'
      do j=1, this%nAtoms
        write(unit,*) (this%TOUTCoords(2,j,k)*bohr2ang,k=1,3)
      enddo
    endif

    writeTOUT = .false.

    close(unit)


  endif


!$OMP END CRITICAL (crystTOUT)

end function calcEnergy


!===========================================================
! Doesn't write to TOUT, but turns on TOUT writing.
! For this one, status boolean doesn't matter.
!===========================================================
subroutine writeTOUT(this,status)
  class(tCrystPES) :: this
  logical, intent(in) :: status

  this%TOUT = .true.

end subroutine writeTOUT

!===========================================================
! Sets up TOUT arrays.
!===========================================================
subroutine setupTOUT(this)
  class(tCrystPES) :: this

  allocate(this%TOUTCoords(1:2,1:this%nAtoms,1:3))

  this%maxRMS = 0.0d0
  this%TOUTCoords = 0.0d0
  this%lowest = this%potfile%vmin


end subroutine setupTOUT

!=========================================================================
! Allocates the neighbourlists on the Crystal PES.
!=========================================================================
subroutine allocateNeighbours(this,max)
  use omp_lib
  class (tCrystPES) :: this
  integer, intent(in) :: max
  integer :: i, j, badness, maxThreads

  ! Allocate mols object based on maximum number of threads for OMP parallelisation.
  maxThreads = OMP_get_max_threads()

  allocate(this%mols(1:maxThreads),stat=badness)
  ! Loop over all the fragments on this PES.
  do i=1,maxThreads
    call Allocatemols(this%mols(i),1,InnerListSize,OuterListSize,this%potfile,1)
  enddo
  ! Now allocate local copies of arrays storing number of neighbours, ID of data point, ID of atomic perm
  ! and ID of space group perm for the neighbourlists.
  if (this%bigMem) then ! Only allocate if --bigmem option is used, since these arrays consume gigabytes of memory.
    allocate(this%neighList(1:max,1:InnerListSize,1:this%nFrag))
    allocate(this%aPerm(1:max,1:InnerListSize,1:this%nFrag))
    allocate(this%sgPerm(1:max,1:InnerListSize,1:this%nFrag))
    allocate(this%nforc(1:this%nFrag,1:max))
  endif

  !print *, 'now sleeping'
  !call sleep(60)
  !stop




end subroutine allocateNeighbours


!==========================================================================================
! Allocates and returns an array for storing IDs for a walker on the crystal PESs.
!===========================================================================================
subroutine allocateIDs(this,idPES)
  class (tCrystPES) :: this
  integer, dimension(:), intent(out), allocatable :: idPES

  allocate(idPES(1))

  idPES = 0

end subroutine allocateIDs

!=====================================================================
! Checks if it's time to update the neighbour lists on the Crystal
! PES.
!=====================================================================
subroutine checkNeighs(this,step,check)
  class(tCrystPES) :: this
  integer, intent(in) :: step
  logical, dimension(:), allocatable, intent(out) :: check
  integer :: i

  allocate(check(1:1))
  check = .false.
  this%updateNeighbours = .false.

  if ((MODULO(step,this%potfile%neighci).eq.0).or.step.eq.1.or..not.this%bigMem) then
    check(1) = .true.
    this%updateNeighbours = .true.
  endif


end subroutine checkNeighs


!================================================================
! Loads geometries from a .xyz file and prints their energies.
!================================================================
subroutine LoadXYZ(this,fileName)
  use omp_lib, ONLY : OMP_get_max_threads, OMP_get_thread_num
  use Utilities, ONLY : readXYZ

  class (tCrystPES) :: this
  character(30), intent(in) :: fileName

  real(kind=8), parameter :: au2ang = 0.52917720859d0

  character(20) :: comment
  character(30) :: outFormat

  integer :: geomCount, maxThreads, threadID
  integer :: IOStatus, unit
  integer :: n, i, j, k
  logical :: fileExists

  real(kind=8), dimension(:,:,:), allocatable :: Geoms
  real(kind=8), dimension(:), allocatable :: Energies
  real(kind=8), dimension(:,:), allocatable :: fragEnergies
  integer, dimension(:), allocatable :: IDs
  logical, dimension(:), allocatable :: updateNeighbours

  real :: t1, t2

  geomCount = 0
  unit = 0

  maxThreads = OMP_get_max_threads()

  Geoms = readXYZ(this%nAtoms,fileName,geomCount)

  allocate(Energies(1:geomCount))
  call this%allocateNeighbours(geomCount)
  allocate(IDs(1:1))
  call this%checkNeighs(1,updateNeighbours)

  ! Now we know number of geometries to load - rewind and read them in.
  rewind(unit)
  do i=1, geomCount
      ! Number of atoms
      read(unit,*,IOSTAT=IOStatus) n
      ! comment line
      read(unit,*,IOSTAT=IOStatus) comment
      ! Geometry
      do j=1, this%nAtoms
          read(unit,*,IOSTAT=IOStatus) comment, (Geoms(i,j,k),k=1,3)
      enddo
  enddo
  close(unit)

  if (this%nFrag.gt.1) then
    allocate(fragEnergies(geomCount,this%nFrag+this%nSpline))
    open(newunit=unit,file='FragEnergies.OUT',status='replace')
  endif

  ! Convert to bohr.
  Geoms = Geoms/au2ang
  !call this%checkNeighs(1,updateNeighbours)

  ! Now loop through and calculate the energies
  call CPU_TIME(t1)
!$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(IDs) SCHEDULE(guided)
  do i=1, geomCount
      IDs = i
      if (this%nFrag.gt.1) then
        Energies(i) = this%calcEnergy(Geoms(i,1:this%nAtoms,1:3),IDs,0,fragEnergies=fragEnergies(i,:))
      else
        Energies(i) = this%calcEnergy(Geoms(i,1:this%nAtoms,1:3),IDs,0)
      endif
  if (this%absEnergy) Energies(i) = Energies(i) + this%vmin
  enddo
!$OMP END PARALLEL DO
  call CPU_TIME(t2)

  if (this%CPUtime) then
    print *, t2-t1
    call exit(0)
  endif
  print *, '# geom number |  energy'
  write(outFormat,'(A11,I2,A7)') "(I4,F20.12,",this%nFrag+this%nSpline,"E15.7)"

  do i=1, geomCount
      if (this%nFrag.gt.1) write(unit,outFormat) i, Energies(i), fragEnergies(i,:)
      print *, i, Energies(i)
  enddo


  if (this%nFrag.gt.1) close(unit)


end subroutine LoadXYZ






end module crystPES_mod
