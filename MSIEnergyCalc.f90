!========================================================================
! A module that contains all subroutines related to calculating energies
! of DMC walkers, including the updating of neighbourlists, etc.
!========================================================================

module MSIEnergy_mod

use molecule_specs, ONLY : molsysdat
use interpolation

logical, save, private :: energiesOnly

  contains


  !===================================================================
  ! Updates the given inner neighbour list.
  !===================================================================
  subroutine updateNeighbours(system,POT,interp,neighbours,nonRigidr)
    implicit none
    type(molsysdat), intent(in) :: system
    type(pot_file), intent(in) :: POT
    type(interp_params), intent(in) :: interp
    type(neighbour_list), intent(inout) :: neighbours
    real(kind=8), dimension(1:system%nNRbonds), intent(in) :: nonRigidr
    real(kind=8), dimension(1:interp%ndata) :: weight
    real(kind=8) :: totsum, dsquared
    integer :: i, j

    neighbours%n = 0
    ! Loop over all the data points
    do i=1,interp%ndata
      dsquared = 0.0d0

      ! Loop over non-rigid bonds.
      do j=1,system%nNRbonds
        dsquared = dsquared + &
           (nonRigidr(j) - POT%nonRigidr(j,i))**2
      enddo
      weight(i) = dsquared
    enddo

    ! Check for data points with a very small sum of squared differences - if so we'll just set its primitive weight to 1,
    ! since the geometry is very, very close to a data point.
    if (ANY(weight.lt.1.d-12)) then
      where (weight.lt.1.d-12)
        weight = 1.d0
      elsewhere
        weight = 0.d0
      endwhere
    else
      weight = weight**(-interp%ipow)
    endif

    totsum = SUM(weight)
    weight = weight/totsum

    ! Add data points to neighbourlist, if weight is high enough.
    do i=1,interp%ndata
      if (weight(i).gt.interp%wtol) call neighbours%add(i)
    enddo

  end subroutine updateNeighbours



  function Potential(system,interp,POT,neighbours,r,nonRigidr,rms,totsum) result(V)

    implicit none

    type(molsysdat), intent(in) :: system
    type(interp_params), intent(in) :: interp
    type(pot_file), intent(in) :: POT
    type(neighbour_list), intent(in) :: neighbours
    real(kind=8), dimension(1:system%nbond), intent(in) :: r
    real(kind=8), dimension(1:system%nNRbonds), intent(in) :: nonRigidr
    real(kind=8) :: V
    real(kind=8), intent(inout) :: rms, totsum

    real(kind=8) :: dsquared
    real(kind=8), dimension(1:neighbours%n) :: weight, taylor
    real(kind=8), dimension(1:system%nint) :: zeta, zetaSq

    integer :: i, j, k

    zeta = 0.0d0

    ! Do everything in one loop!

    do i=1,neighbours%n
      k = neighbours%points(i)

      ! Use BLAS matrix multiplication routines.

      ! The zeta coordinates are defined as zeta = U^T x R
      ! where R is an Nbondx1 column vector, U^T an NintxNbond
      ! matrix. Then we need to calculate z - z0 for the Taylor expansions.
      ! For this, I can include this step in DGEMV routine:
      zeta = POT%z(:,k)
      ! The subroutine calculates y = aAx + by. I set b = -1.0d0, and pass in
      ! zeta array with datapoint zetas, that is, z0.
      call DGEMV('N',system%nint,system%nbond,1.0d0,POT%ut(:,:,k),system%nint,r,1,-1.0d0,zeta,1)
      !zeta = MATMUL(datapoint%ut,r) - datapoint%z

      ! Do the Taylor expansion. Will calculate as dot products:
      ! Zeroth-order term: v0
      ! First-order term: dot(zeta,dv/dzeta)
      ! Second-order term: 0.5*dot(zeta**2,d2v/dzeta2)
      zetaSq = zeta**2

      Taylor(i) = POT%v0(k) + DOT_PRODUCT(zeta,POT%v1(:,k)) &
        + 0.5d0*DOT_PRODUCT(zetaSq,POT%v2(:,k))

      !print *, 'new taylor:'
      !print *, Taylor(i)



      ! Evaluate the linear combinations of inverse bong lengths.
      !zeta = MATMUL(datapoint%ut,r)

      ! Evaluate z - z0
      !zeta = zeta - datapoint%z

      ! Start the Taylor expansion.
      !Taylor(i) = datapoint%v0
      !Taylor(i) = Taylor(i) + SUM(zeta*(datapoint%v1 + &
       !   0.5d0*zeta*datapoint%v2))

      !print *, 'old taylor:'
      !print *, Taylor(i)

      ! Note: see further below for more readable version of above code.
      ! Zeta, %v1 and %v2 are all arrays!

      ! Calculate the weight of this data point
      dsquared = 0.0d0
      do j=1,system%nNRbonds
        dsquared = dsquared + &
           (nonRigidr(j) - POT%nonRigidr(j,k))**2
      enddo
      weight(i) = dsquared
    enddo

    ! Check for data points with a very small sum of squared differences - if so we'll just set its primitive weight to 1,
    ! since the geometry is very, very close to a data point.
    if (ANY(weight.lt.1.d-12)) then
      where (weight.lt.1.d-12)
        weight = 1.d0
      elsewhere
        weight = 0.d0
      endwhere
    else
      weight = weight**(-interp%ipow)
    endif

    totsum = SUM(weight)
    weight = weight/totsum

    ! As the interpolated energy is the sum of Ti*Wi, where i=1 ... nNeighbours,
    ! the energy can be calculated as the dot product of the taylor and weight arrays,
    ! i.e. T1*W1 + T2*W2 + ... etc.
    V = DOT_PRODUCT(weight,taylor)

    ! Write some details to screen, if desired:
    if (interp%printDetails) then
      print *, '===== INTERPOLATION DETAILS ====='
      print *, 'Datapoints used:', neighbours%n
      print *, 'Totsum:', totsum
      print *, ''
      print *, '~~~ Datapoints ~~~'
      print *, 'n, ID, perm, Taylor(i), weight'
      do i=1,neighbours%n
        k = neighbours%points(i)
        print *, i, POT%ID(k), POT%perm(k), Taylor(i), weight(i)
      enddo
    endif


    ! Calculate the rms:
    rms = 0.0d0
    taylor = (taylor - V)**2
    rms = dsqrt(DOT_PRODUCT(weight,taylor))
    !do i=1,neighbours%n
      !rms = rms + weight(i)*(taylor(i) - V)**2
    !enddo


    return




    ! Evaluate the linear combinations of inverse bong lengths.
    ! Loop over zeta array in column-major order...
    !zeta = 0.0d0
    !do i=1,neighbours%n
    !  datapoint => neighbours%points(i)%dp
    !  zeta(:,i) = MATMUL(datapoint%ut,r)
    !  do j=1,system%nint
        !do k=1,system%nbond
        !  zeta(j,i) = zeta(j,i) + datapoint%ut(j,k)*r(k)
        !enddo

        ! Now evaluate z - z0 for each data point.
    !    zeta(j,i) = zeta(j,i) - datapoint%z(j)
    !  enddo
    !enddo

    ! Start the Taylor expansion:
    !do i=1,neighbours%n
    !  datapoint => neighbours%points(i)%dp

    !  taylor(i) = datapoint%v0
    !  do j=1,system%nint
    !    taylor(i) = taylor(i) + zeta(j,i)*(datapoint%v1(j) + &
    !         0.5d0*zeta(j,i)*datapoint%v2(j))
    !  enddo
    !enddo





  end function Potential


!==================================================================================================
! Adds the given geometry to the TOUT file.
! Note: in this new version, we won't write geometries based on some probability - we'll write all
! geometries when we need to. This is equivalent to setting qdmc%sample to 1.0.
!==================================================================================================
  subroutine addToTOUT(coords,V,rms,totsum,interp)
    real(kind=8), dimension(:,:), intent(in) :: coords
    real(kind=8), intent(in) :: V, rms, totsum
    type(interp_params), intent(in) :: interp
    integer :: i, j, unit
    character*50 :: fileName
    real(kind=8) :: energy

!$OMP CRITICAL (tout)
    if (trim(interp%name) == 'MSI') then
      fileName = 'TOUT.1'
    else
      fileName = trim(interp%name)//'.TOUT'
    endif

    open(unit,file=trim(fileName),access='append',status='old')

    ! Now write the geometry.
    do i=1, SIZE(coords,DIM=1)
      write(unit,*) (coords(i,j),j=1,3)
    enddo

    ! Energy, rms, totsum
    energy = V - interp%Vmin

    write(unit,*) energy, rms, totsum

    close(unit)

!$OMP END CRITICAL (tout)









  end subroutine addToTOUT






    !************************************************************!





!===========================================
! Calculates bond lengths and inverts them.
!===========================================
subroutine intern_rigid(sys,x,r,nonRigidr)
implicit none

type (molsysdat), intent(in) :: sys
real(kind=8), dimension(:,:), intent(in) :: x
real(kind=8), dimension(:), intent(out) :: r
real(kind=8), dimension(:), intent(out) :: nonRigidr
integer(kind=4) :: i, j, k, l, ind

! First things first: copy rigid bond lengths from system object.
r(:) = sys%bondLengths(:)**2 ! also square them, we'll unsquare them later.

!  bond lengths - only loop over non-rigid bonds.
do k=1, sys%nNRbonds
  i = sys%nonRigidBonds(k)%i
  j = sys%nonRigidBonds(k)%j
  ind = sys%nonRigidBonds(k)%ind

  r(ind) = 0.0d0
  do l=1,3
    r(ind) = r(ind) + (x(i,l)-x(j,l))**2
  enddo

  nonRigidr(k) = r(ind)

enddo

r = 1.0d0/dsqrt(r)
nonRigidr = 1.0d0/dsqrt(nonRigidr)


    return
end subroutine intern_rigid


    !===========================================
    ! Calculates bond lengths and inverts them.
    !===========================================
    subroutine intern(sys,x,r)
      implicit none

      type (molsysdat), intent(in) :: sys
      real(kind=8), dimension(:,:), intent(in) :: x
      real(kind=8), dimension(:), intent(out) :: r
      integer(kind=4) :: i, j, k, l, ind



      r = 0.0d0
      do i=1, sys%nbond
        do j=1,3
            r(i) = r(i) + (x(sys%mb(i),j)-x(sys%nb(i),j))**2
        enddo
    !print *, sys%mb(i), sys%nb(i), r(i)
      enddo

      ! Invert and square root once and for all.
      r = 1.0d0/dsqrt(r)
          return
    end subroutine intern




    !************************************************************!



end module MSIEnergy_mod
