#
#  makefile for diffusion monte carlo on interpolated PES
#
FFLAGS= -O3 -fno-range-check -ffixed-line-length-none -std=gnu -fbacktrace -fopenmp -ffpe-trap=invalid,zero,overflow #-fcheck=all #-finline-functions -O3
#FFLAGS= -O -Wall -g -fcheck=all -fbacktrace -fno-range-check -fopenmp -ffpe-trap=invalid,zero,overflow
#FFLAGS= -ggdb -fno-range-check -fopenmp -ffpe-trap=invalid,zero,overflow -fbacktrace

#LIBS=/Users/jordan/Downloads/BLAS-3.5.0 -lblas
LIBS=/usr/local/lib -lblas -llapack
F90=gfortran
# -align all

MODULES=Args.f90 Geometry.f90 Spline.f90 vec_mod.f90 molspec_mod.f90 mt19937ar.f90 Utilities.f90 interp_mod.f90 frag_mod.f90 MSIEnergyCalc.f90 PES_mod.f90 crystPES_mod.f90 gasPES_mod.f90 qdmc_mod.f90 Walkers.f90  DW.f90 Histograms_mod.f90 DMC.f90


SRC=RunDMC_main.f90

#SRC=Dummy_main.f90

INTS=

qdmc90: $(MODULES) $(SRC)
	$(F90) $(FFLAGS) -o RunDMC $(MODULES) $(SRC) -L$(LIB_DIR) $(LIB_NAME) -L $(LIBS)
#	cp RunDMC ~/bin/RunDMC
clean:
	rm -f  *.mod *.o


# Additional stuff to turn Terry's code into a library:
OBJECTS1= potmodule.o readccm.o cntl.o readsp.o makebp.o readlatt.o readt.o \
          readcon.o intern.o neigh.o neigh1r.o neigh2r.o neigh3r.o dvdi.o \
          dvdir.o dvdx.o expand.o neighbourgrid.o
OBJECTS3= dummy-mpi.o lisym.o cross.o
CRYSTSRC= cross.f90 dvdi.f90 readlatt.f90 dummy-mpi.f90 dvdir.f90 \
	   readsp.f90 cntl.f intern.f makebp.f neigh1r.f neigh3r.f readcon.f \
	   dvdx.f lisym.f neigh.f neigh2r.f readccm.f readt.f expand.f90 neighbourgrid.f90
CRYSTMOD= potmodule.f90
CRYST_SRC= $(addprefix CrystPES/,$(CRYSTSRC))
CRYST_MOD= $(addprefix CrystPES/,$(CRYSTMOD))

LIB_DIR=./
LIB_NAME=CrystPES.lib

OBJS= $(OBJECTS1) $(OBJECTS3)
CRYST_DIR= $(addprefix CrystPES/,$(OBJS))

$(OBJECTS1): potmodule.o

CrystLib: $(CRYST_MOD) $(CRYST_SRC)
	$(F90) -c $(FFLAGS) $(CRYST_MOD) $(CRYST_SRC)
	ar crv $(LIB_NAME) $(OBJS)
	rm *.o
