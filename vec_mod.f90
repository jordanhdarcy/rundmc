module vec_mod

implicit none

contains

FUNCTION cross(a, b)
  real(kind=8), DIMENSION(3) :: cross
  real(kind=8), DIMENSION(3), INTENT(IN) :: a, b

  cross(1) = a(2) * b(3) - a(3) * b(2)
  cross(2) = a(3) * b(1) - a(1) * b(3)
  cross(3) = a(1) * b(2) - a(2) * b(1)

END FUNCTION cross

function dot(a, b)
  real(kind=8) :: dot
  real(kind=8), dimension(3), intent(in) :: a, b

  dot = sum(a(:)*b(:))

end function dot

function normalise(v)
  real(kind=8), dimension(3) :: normalise
  real(kind=8), dimension(3) :: v
  real(kind=8) :: dist

  dist = dsqrt( sum(v(:)**2) )
  normalise(:) = v(:)/dist

end function normalise

!***********************************************************************************************************************************
!  ANGVEC
!
!  This function calculates the separation angle between two cartesian 3-vectors V1 and V2.  The separation angle THETA is
!  returned in radians.
!
!  Thanks NASA! from http://caps.gsfc.nasa.gov/simpson/software.html
!***********************************************************************************************************************************

  FUNCTION ANGVEC (V1, V2) RESULT (THETA)

  IMPLICIT NONE

  DOUBLE PRECISION, DIMENSION(3), INTENT(IN) :: V1, V2
  DOUBLE PRECISION :: THETA


  THETA = ACOS(DOT_PRODUCT(V1,V2)/(SQRT(DOT_PRODUCT(V1,V1))*SQRT(DOT_PRODUCT(V2,V2))))

  RETURN

  END FUNCTION ANGVEC

!*****************************************************************



        subroutine dihedralVec(v1,v2,v3,diheddeg)

      implicit none

      real(kind=8), dimension(3), intent(in) :: v1, v2, v3
      real(kind=8), intent(out) :: diheddeg
 

      real(kind=8) :: norm1x, norm1y, norm1z, norm1len
      real(kind=8) :: norm2x, norm2y, norm2z, norm2len
      real(kind=8) :: n1xn2x, n1xn2y, n1xn2z
      real(kind=8) :: dihed_numerator, dihed
      real(kind=8) :: dot, sindihed, cosdihed
      real(kind=8) :: lenv1, lenv2, lenv3
      real(kind=8), dimension(2) :: alpha

      integer :: j
      real(kind=8) :: pi, tiny

      pi = 2.d0*dacos(0.d0)
      tiny = 1.0d-100


! Then take the cross products: v1 x v2 = norm1, v2 x v3 = norm2
  
      norm1x = v1(2)*v2(3) - v2(2)*v1(3)
      norm1y = v1(1)*v2(3) - v2(1)*v1(3)
      norm1z = v1(1)*v2(2) - v2(1)*v1(2)
 
      norm2x = v2(2)*v3(3) - v3(2)*v2(3)
      norm2y = v2(1)*v3(3) - v3(1)*v2(3)
      norm2z = v2(1)*v3(2) - v3(1)*v2(2)

! Now calculate the dihedral angle between the two planes by taking the
! cosine of the dot product of the two normal vectors and check sign
! is correct. 
      
      norm1len = dsqrt(norm1x**2 + norm1y**2 + norm1z**2)
      norm2len = dsqrt(norm2x**2 + norm2y**2 + norm2z**2)
      dihed_numerator = norm1x*norm2x + norm1y*norm2y + norm1z*norm2z
      
! Guard against zero length vectors. Define the dihedral angle to be zero when
! we have linearly dependent data.

      if (norm1len.lt.tiny) then
        norm1len = tiny
      endif
      if (norm2len.lt.tiny) then
        norm2len = tiny
      endif

      cosdihed = dihed_numerator/(norm1len*norm2len)

      
! Calculate the sine of the dihedral angle (to check sign).
! First calculate norm1 x norm2 = n1xn2
      
      n1xn2x = norm1y*norm2z - norm2y*norm1z
      n1xn2y = norm1x*norm2z - norm2x*norm1z
      n1xn2z = norm1x*norm2y - norm2x*norm1y

! v2 . [n1xn2]

      dot = v2(1)*n1xn2x + v2(2)*n1xn2y + v2(3)*n1xn2z

! calculate lengths of vectors v1, v2 and v3

      lenv1 = dsqrt(DOT_PRODUCT(v1,v1))
      lenv2 = dsqrt(DOT_PRODUCT(v2,v2))
      lenv3 = dsqrt(DOT_PRODUCT(v3,v3))

! calculate angles between abc and bcd

      alpha(1) = ANGVEC(v1,v2)
      alpha(2) = ANGVEC(v2,v3)


! Guard against zero angles. Define the dihedral angle to be zero when
! we have linearly dependent data.

      do j=1,2
        if (alpha(j).lt.tiny.or.abs(alpha(j)-2*pi).lt.tiny) then
          alpha(j) = tiny
        endif
      enddo

      sindihed = dot/(dsin(alpha(1))*dsin(alpha(2))*lenv1*lenv2**3*lenv3)

! Using sine of dihedral (sindihed) and cosine of dihedral (cosdihed) determine
! the quadrant the dihedral angle is in.

      
!      open(unit=99,file='dbg',status='replace')
!      write(99,*) 'sindihed = ',sindihed
!      write(99,*) 'cosdihed = ',cosdihed
!      write(99,*) 'alpha1 = ',alpha(1)
!      write(99,*) 'alpha2 = ',alpha(2)
!      write(99,*) 'a = ',a
!      write(99,*) 'b = ',b
!      write(99,*) 'c = ',c
!      write(99,*) 'd = ',d
!      close(unit=99)

      if (abs(sindihed).gt.1.00) then
        if (sindihed.lt.0.0) then
          sindihed = -0.99999
        else
          sindihed = 0.99999
        endif
      endif
        
        dihed = dasin(sindihed)      

      if (cosdihed.lt.0.0) then
          dihed = pi - dihed
      endif

      if (dihed.lt.0.0) then
          dihed = dihed + 2*pi
      endif

      diheddeg = dihed*180/pi

!      if(anint(diheddeg).eq.360) then
!          diheddeg = diheddeg - 360
!      endif


      end subroutine dihedralVec

end module vec_mod 



