!==========================================================================
! Contains everything related to the maintenance and upkeep of the walkers.
!==========================================================================
module Walkers_mod
implicit none


private

public :: tWalkers, tWalker, tWalkerBox

!==============================================================================
! A doodad for fragments.
!==============================================================================
type dmc_frag
  real(kind=8), dimension(3) :: com  ! centre of mass
  real(kind=8), dimension(3,3) :: rm ! current rotation matrix
  real(kind=8), dimension(3) :: i ! current rotation matrix
end type dmc_frag



!==============================================================================
! A single walker. Contains coordinates, energy, ID of parent and pointer to
! next walker in list.
!==============================================================================
type tWalker
  real(kind=8) :: Vcurr
  real(kind=8), dimension(:,:), allocatable :: x ! Walker coordinates.
  real(kind=8), dimension(:), allocatable :: r ! Bond lengths.
  type (dmc_frag), dimension(:), allocatable :: frag !fragment data
  logical :: lowE
  integer, dimension(:), allocatable :: idPES ! An ID number that's passed to the energy calculation
  ! for neighbour lists. Can be ignored if the PES isn't a modified Shepard
  ! interpolation.
  integer, dimension(:), allocatable :: ID

  type (tWalker), pointer :: next, prev ! Walkers before/after in list.

  contains

  procedure, pass(this) :: create => newWalker
  procedure, pass(this) :: kill => killWalker
  procedure, pass(this) :: addParent => addParent
  procedure, pass(this) :: getParent => getParent


end type tWalker

!==============================================================================
! This is a box so I can create an array of pointers to walkers to parallelise
! the linked list.
!==============================================================================
type tWalkerBox
  type(tWalker), pointer :: w
end type tWalkerBox

!==============================================================================
! A box for all the walkers. This is all the simulation should see, with access
! to subroutines such as 'branch', 'walk', 'calcE', and Vbar, etc.
!==============================================================================
type tWalkers

  private
  !public :: setup, branch, walk, calcE, avgR

  type(tWalker), pointer :: head ! The walker at the start of the list.
  type(tWalkerBox), dimension(:), allocatable :: walkArray
  integer :: nAtoms, nFrags, nWalkers
  logical :: killLowE = .true.
  real(kind=8), dimension(:), allocatable :: g ! Radial distribution function, g(r)
  real(kind=8) :: gwidth, gmax ! bin width and maximum value for g(r).
  integer :: binMax
  integer(kind=4) :: nBins

  contains

  procedure, pass(this) :: new => newWalkers
  procedure, pass(this) :: setup => setupWalkers
  procedure, pass(this) :: setupPES => setupPES
  procedure, pass(this) :: branch => branchWalkers
  procedure, pass(this) :: walk => walkWalkers
  procedure, pass(this) :: calcEnergy => calcEnergy
  procedure, pass(this) :: getWalkers => getWalkers
  procedure, pass(this) :: dump => dumpWalkers
  procedure, pass(this) :: load => loadWalkers
  procedure, pass(this) :: avgR => getR

end type tWalkers


contains

!==============================================================================
! A little function to return an array of walkers.
!==============================================================================
function getWalkers(this) result(Walkers)
  class (tWalkers) :: this
  type (tWalkerBox), dimension(1:this%nWalkers) :: Walkers
  type (tWalker), pointer :: currentWalker
  integer :: count

  count = 0


  currentWalker => this%head%next

  do while (ASSOCIATED(currentWalker))
    count = count+1
    Walkers(count)%w => currentWalker
    currentWalker => currentWalker%next
  enddo

end function



!==============================================================================
! Creates the required number of walkers, allocates them, little more.
!==============================================================================
subroutine newWalkers(this,nWalkers,nAtoms,nFrags)
  class(tWalkers) :: this
  integer, intent(in) :: nWalkers, nAtoms, nFrags

  type(tWalker), pointer :: currentWalker

  integer :: i

  this%nAtoms = nAtoms
  this%nFrags = nFrags

  ! Create the first walker in the list.
  allocate(this%head)
  this%head%prev => NULL()
  currentWalker => this%head
  ! Head walker will be an inactive rock at the start of the list.
  ! Loop through and allocate rest of the walkers.
  do i=1, nWalkers
    ! Allocate the next walker.
    allocate(currentWalker%next)
    ! I probably don't need backwards links, but I want them anyway.
    currentWalker%next%prev => currentWalker
    currentWalker => currentWalker%next

    call currentWalker%create(nAtoms,nFrags)
  enddo
  currentWalker%next => NULL() ! end of the list.

  this%nWalkers = nWalkers


end subroutine newWalkers


!===============================================================================
! Calculates and returns an array containing average atom-atom distances
! for all the walkers.
!===============================================================================
function getR(this,sys) result(R)
  use molecule_specs, ONLY : molsysdat
  use Utilities, ONLY : intern

  class (tWalkers) :: this
  type(molsysdat), intent(in) :: sys

  real(kind=8), dimension(1:sys%nbond) :: R
  real(kind=8), dimension(1:sys%nbond) :: tempR

  type (tWalker), pointer :: currentWalker

  R = 0.0d0

  ! Loop through walkers.
  currentWalker => this%head%next
  do while (ASSOCIATED(currentWalker))
    call intern(sys,currentWalker%x,tempR)

    tempR(:) = 1.0d0/tempR(:)
    R(:) = R(:) + tempR(:)
    currentWalker => currentWalker%next
  enddo

  R(:) = R(:)/real(this%nWalkers,8)

end function





!===============================================================================
! Sets up the starting geometry of all the walkers.
!===============================================================================
subroutine setupWalkers(this,system,QDMC,frags)
  use molecule_specs, ONLY : molsysdat
  use qdmc_structures, ONLY : qdmc_par
  use fragment_mod, ONLY : frag_par
  use Utilities, ONLY : intern
  use mt19937, ONLY : genrand_real3
  use Geometry_mod, ONLY : JACOBI


  class(tWalkers) :: this
  type(molsysdat), intent(in) :: system
  type(qdmc_par), intent(inout) :: QDMC
  type(frag_par), dimension(:), allocatable, intent(inout) :: frags

  type(tWalker), pointer :: currentWalker, startWalker
  ! Note: in the way this subroutine was written, walkers are processed in blocks.
  ! Need to keep a reference to the start of each block of walkers...

  ! Copy/pasted from old setup_frag file.
  real(kind=8), dimension(:,:), allocatable :: x
  real(kind=8), dimension(:), allocatable :: rref,rchk
  real(kind=8), dimension(3,3) :: tmprot
  real(kind=8), dimension(3) :: tmpi
  real(kind=8), dimension(3) :: centre, dx, randX
  real(kind=8), dimension(3,3) :: inertia, rot
  real(kind=8) :: total_mass
  real(kind=8) :: rtot, rand
  integer :: i,j,k,m,n,ii,ind,nrot,ierr,i1,i2,indx

  allocate(x(system%natom,3),stat=ierr)
  if (ierr.ne.0) stop ' Error allocating x in setup_frag '

  allocate(rref(system%nbond),stat=ierr)
  if (ierr.ne.0) stop ' Error allocating rref in setup_frag '
  allocate(rchk(system%nbond),stat=ierr)
  if (ierr.ne.0) stop ' Error allocating rref in setup_frag '

  ! Only do the following if we're not restarting...
  if (.not.QDMC%restart) then
    QDMC%numWalkers = QDMC%NormWalkers

    ! The start of the walker 'blocks'.
    startWalker => this%head%next


    do i1=1,QDMC%NumGeoms

      x = QDMC%EquilibriumGeom(i1,:,:)

      do k=1,system%nfrag

         if (frags(k)%fixed) cycle

         ! Point the first walker back to the start of this block.
         currentWalker => startWalker

         ! calculate centre of mass of fragment
         centre = 0.d0
         total_mass = 0.d0
         do n=1,frags(k)%natom
           ind = frags(k)%indx(n)
           total_mass = total_mass + system%mass(ind)
         enddo

         do j=1,3
         do n=1,frags(k)%natom
           ind = frags(k)%indx(n)
           centre(j) = centre(j) + x(ind,j)*system%mass(ind)
         enddo
         enddo

         ! assign total mass of fragment to frag thingy
         frags(k)%centre = centre/total_mass
         frags(k)%mass = total_mass

         ! translate fragment so centre of mass is at the origin
         do n=1,frags(k)%natom
           ind = frags(k)%indx(n)
           x(ind,:) = x(ind,:) - frags(k)%centre(:)
         enddo

         ! determine the inertia tensor of the fragment and the principal moments of inertia
         ! build the inertia tensor of x

         inertia = 0.0
         do n=1,frags(k)%natom
          j = frags(k)%indx(n)
          inertia(1,1) = inertia(1,1) + system%mass(j)*(x(j,2)**2 + x(j,3)**2)
          inertia(2,2) = inertia(2,2) + system%mass(j)*(x(j,1)**2 + x(j,3)**2)
          inertia(3,3) = inertia(3,3) + system%mass(j)*(x(j,1)**2 + x(j,2)**2)
          inertia(1,2) = inertia(1,2) - system%mass(j)*x(j,1)*x(j,2)
          inertia(1,3) = inertia(1,3) - system%mass(j)*x(j,1)*x(j,3)
          inertia(2,3) = inertia(2,3) - system%mass(j)*x(j,2)*x(j,3)
         enddo
         inertia(2,1) = inertia(1,2)
         inertia(3,1) = inertia(1,3)
         inertia(3,2) = inertia(2,3)

         ! Diagonalise inertia tensor using the Jacobi algorithm.
         ! Eigenvectors are the principal axes of inertia, and are the initial
         ! rotation matrix.
         ! Eigenvalues are the principal moments of inertia.
         ! I use jacobi to diagonalise because it will return unsorted eigenvalues
         ! and eigenvectors.

         call jacobi(inertia,3,3,frags(k)%i,frags(k)%ax,nrot)

         ! I want the transpose of the outputted matrix because it confuses me less
         ! i.e. I like to left multiply by the rotation matricies rather than right multiply
         ! this is a check to make sure we have no negative or zero moment of inertia
         do j=1,3
           if (frags(k)%i(j).lt.1.d-15) then
           frags(k)%i(j) = 1.d-15
           endif
         enddo
        !        write(*,*) 'moments of inertia of frag', k
        !        write(*,*) frags(k)%i
        !      write(*,*) 'rotmat'
        !      write(*,*) frags(k)%ax
        !      write(*,*) 'transposed rotmat'
        !      write(*,*) transpose(frags(k)%ax)
        !      write(*,*) 'rotated to principal axes'
        !      do n=1,frags(k)%natom
        !        ind = frags(k)%indx(n)
        !        write(*,*) system%atom_label(ind),x(ind,:)
        !      enddo
        !
         do i=1, QDMC%WalkersPerGeom
          currentWalker%frag(k)%com = frags(k)%centre
          currentWalker%frag(k)%rm = transpose(frags(k)%ax)
          currentWalker%frag(k)%i = frags(k)%i

          currentWalker => currentWalker%next
         enddo
       enddo ! End the loop over fragments
       ! Now point the start walker to currentWalker - a new block.
       startWalker => currentWalker
     enddo ! End the loop over NumGeoms

        ! check all fragments for all numgeoms are the same

    if (QDMC%NumGeoms > 1 ) then
      ! calculate distances for each frag in first geom
      call intern(system,QDMC%EquilibriumGeom(1,:,:),rref)
      do i=2,QDMC%NumGeoms
      ! calc distances for subsequent geometries
      call intern(system,QDMC%EquilibriumGeom(i,:,:),rchk)
      ! now check that fragment atom-atom distances all match
      do k=1,system%nfrag
        rtot = 0.0
        do j=1,system%nbond
        if (any(system%mb(j).eq.frags(k)%indx(:)).and.any(system%nb(j).eq.frags(k)%indx(:))) then
          rtot = rtot + abs(rref(j) - rchk(j))
        endif
        enddo
        if (rtot > 1.d-5) then
        write(*,*) ' Error in setup_frags(): Rigid fragment geometries do not match '
        write(*,*) ' Fragment ', k, ' of seed geometry ', i, ' does not match first geometry.'
        stop ' Error in setup_frags(): Rigid fragment geometries do not match '
        endif
      enddo
      enddo
    endif






    !------------------------------------------------------------
    ! initialise coordinates for each Walker
    !------------------------------------------------------------

    ! Don't need to loop through walkers in blocks this time. What joy!
    currentWalker => this%head%next

    do i1=1,QDMC%NumGeoms
      do i2=1,QDMC%WalkersPerGeom

      !       write(*,*) 'initial orientation'
      !       do n=1,system%natom
      !         write(*,*) system%atom_label(n), QDMC%EquilibriumGeom(1,n,:)
      !       enddo

         currentWalker%x = QDMC%EquilibriumGeom(i1,:,:)

         ! move the atoms that are not in fragments
         do n=1,frags(0)%natom
         do j=1,3
           ind = frags(0)%indx(n)
           rand=genrand_real3()*2.0 - 1.0
           currentWalker%x(ind,j) = QDMC%EquilibriumGeom(i1,ind,j) + rand*QDMC%StepSize
         enddo
         enddo

         ! move the fragments as rigid bodies
         ! for each fragment ...
         do k=1,system%nfrag


         if (frags(k)%fixed) cycle

         ! Get x,y,z random displacements first!
         do j=1,3
          randX(j) = genrand_real3()*2.0 - 1.0
         enddo

         ! move centre of mass of fragment

         do j=1,3
           rand=genrand_real3()*2.0 - 1.0
           !currentWalker%frag(k)%com(j) = currentWalker%frag(k)%com(j) + rand*QDMC%StepSize

           ! Translate along the three lattice vectors. If this is gas phase, the lattice vectors
           ! are just the x,y,z unit vectors and this should be equivalent to just picking a random x,y,z displacement.
           do m=1,3
           currentWalker%frag(k)%com(j) = currentWalker%frag(k)%com(j) + system%uVectors(m,j)*randX(m)*QDMC%StepSize
           enddo



           do n=1,frags(k)%natom
           ind = frags(k)%indx(n)
           !currentWalker%x(ind,j) = QDMC%EquilibriumGeom(i1,ind,j) + rand*QDMC%StepSize
           currentWalker%x(ind,j) = QDMC%EquilibriumGeom(i1,ind,j)

           do m=1,3
            currentWalker%x(ind,j) = currentWalker%x(ind,j) + system%uVectors(m,j)*randX(m)*QDMC%StepSize
           enddo
           enddo
         enddo

         ! now we want to rotate the fragment

         ! translate com of fragment to origin

      !         write(*,*) 'com translated'
         do n=1,frags(k)%natom
           ind = frags(k)%indx(n)
           currentWalker%x(ind,:) = currentWalker%x(ind,:) - &
                             currentWalker%frag(k)%com(:)

      !           write(*,*) system%atom_label(ind), currentWalker%x(ind,:)
         enddo


         do j=1,3 ! For each rotational axis determine a random rotation step
           rand=genrand_real3()*2.0 - 1.0
           dx(j) = rand*QDMC%StepSize
         enddo

         ! construct rotation matrix. works for any size rotation i think.

          rot(1,1) =  dcos(dx(2))*dcos(dx(3))
          rot(1,2) = -dcos(dx(2))*dsin(dx(3))
          rot(1,3) =  dsin(dx(2))
          rot(2,1) =  dcos(dx(1))*dsin(dx(3)) + dcos(dx(3))*dsin(dx(1))*dsin(dx(2))
          rot(2,2) =  dcos(dx(1))*dcos(dx(3)) - dsin(dx(1))*dsin(dx(2))*dsin(dx(3))
          rot(2,3) = -dcos(dx(2))*dsin(dx(1))
          rot(3,1) =  dsin(dx(1))*dsin(dx(3)) - dcos(dx(1))*dcos(dx(3))*dsin(dx(2))
          rot(3,2) =  dcos(dx(1))*dsin(dx(2))*dsin(dx(3)) + dcos(dx(3))*dsin(dx(1))
          rot(3,3) =  dcos(dx(1))*dcos(dx(2))

         ! rotate principal axis to coincide with cartesian axes with current rotation matrix
      !         if(i.eq.1.and.k.eq.1) write(*,*) 'principal axes'
         do n=1,frags(k)%natom
           ind = frags(k)%indx(n)
           do j=1,3
           x(ind,j) = sum(currentWalker%frag(k)%rm(j,:)*currentWalker%x(ind,:))
           enddo
           currentWalker%x(ind,:) = x(ind,:)
      !           if (i.eq.1.and.k.eq.1) then
      !             write(*,*) system%atom_label(ind), currentWalker%x(ind,:)
      !           endif
         enddo
      !         write(*,*) 'rotmat check'
      !         write(*,*) currentWalker%frag(k)%rm

         ! perform dmc rotation of molecule

      !         write(*,*) 'dmc rotation matrix'
      !         write(*,*) rot


         ! rotate fragment
      !         write(*,*) 'rotated fragment'
         do n=1,frags(k)%natom
           ind = frags(k)%indx(n)
           do j=1,3
           x(ind,j) = sum(rot(j,:)*currentWalker%x(ind,:))
           enddo
           currentWalker%x(ind,:) = x(ind,:)
      !           write(*,*) system%atom_label(ind), currentWalker%x(ind,:)
         enddo

         ! rotate fragment back to it's body fixed frame
      !         write(*,*) 'rotated back to original orientation + rotation step'
         do n=1,frags(k)%natom
           ind = frags(k)%indx(n)
           do j=1,3
           x(ind,j) = sum(currentWalker%frag(k)%rm(:,j)*currentWalker%x(ind,:))
           enddo
           currentWalker%x(ind,:) = x(ind,:)
      !           write(*,*) system%atom_label(ind), currentWalker%x(ind,:)
         enddo

         ! propagate rotation matrix
      !         write(*,*) 'propagated rotation matrix'
         frags(k)%ax = matmul(transpose(rot),currentWalker%frag(k)%rm)
         currentWalker%frag(k)%rm = frags(k)%ax
      !         write(*,*) currentWalker%frag(k)%rm

         ! move fragment back to original location
      !         write(*,*) 'final geometry'
         do n=1,frags(k)%natom
           ind = frags(k)%indx(n)
           currentWalker%x(ind,:) = currentWalker%x(ind,:) + &
                             currentWalker%frag(k)%com(:)
      !           write(*,*) system%atom_label(ind), currentWalker%x(ind,:)
         enddo

         enddo ! End loop over each fragment

      !       write(*,*) 'final orientation'

      !       do n=1,system%natom
      !         write(*,*) system%atom_label(n), currentWalker%x(n,:)
      !       enddo
      !       stop


        ! Get the next walker...
        currentWalker => currentWalker%next

      enddo ! End loop over walkers per geom
    enddo ! End loop over seed geometries




  else
    ! Restarting, will need to set up each rotation matrix individually.
    currentWalker => this%head%next


    do while (ASSOCIATED(currentWalker))
      x = currentWalker%x(:,:)

      do k=1,system%nfrag

      if (frags(k)%fixed) cycle

      ! calculate centre of mass of fragment
      centre = 0.d0
      total_mass = 0.d0
      do n=1,frags(k)%natom
        ind = frags(k)%indx(n)
        total_mass = total_mass + system%mass(ind)
      enddo

      do j=1,3
      do n=1,frags(k)%natom
        ind = frags(k)%indx(n)
        centre(j) = centre(j) + x(ind,j)*system%mass(ind)
      enddo
      enddo

      ! assign total mass of fragment to frag thingy
      currentWalker%frag(k)%com = centre/total_mass
      frags(k)%mass = total_mass

      ! translate fragment so centre of mass is at the origin
      do n=1,frags(k)%natom
        ind = frags(k)%indx(n)
        x(ind,:) = x(ind,:) - currentWalker%frag(k)%com(:)
      enddo

      ! determine the inertia tensor of the fragment and the principal moments of inertia
      ! build the inertia tensor of x

      inertia = 0.0
      do n=1,frags(k)%natom
         j = frags(k)%indx(n)
         inertia(1,1) = inertia(1,1) + system%mass(j)*(x(j,2)**2 + x(j,3)**2)
         inertia(2,2) = inertia(2,2) + system%mass(j)*(x(j,1)**2 + x(j,3)**2)
         inertia(3,3) = inertia(3,3) + system%mass(j)*(x(j,1)**2 + x(j,2)**2)
         inertia(1,2) = inertia(1,2) - system%mass(j)*x(j,1)*x(j,2)
         inertia(1,3) = inertia(1,3) - system%mass(j)*x(j,1)*x(j,3)
         inertia(2,3) = inertia(2,3) - system%mass(j)*x(j,2)*x(j,3)
      enddo
      inertia(2,1) = inertia(1,2)
      inertia(3,1) = inertia(1,3)
      inertia(3,2) = inertia(2,3)

      ! Diagonalise inertia tensor using the Jacobi algorithm.
      ! Eigenvectors are the principal axes of inertia, and are the initial
      ! rotation matrix.
      ! Eigenvalues are the principal moments of inertia.
      ! I use jacobi to diagonalise because it will return unsorted eigenvalues
      ! and eigenvectors.

      call jacobi(inertia,3,3,tmpi,tmprot,nrot)

       currentWalker%frag(k)%rm = transpose(tmprot)
       do j=1,3
         currentWalker%frag(k)%i(j) = tmpi(j)
         if (currentWalker%frag(k)%i(j).lt.1.d-15) then
         currentWalker%frag(k)%i(j) = 1.d-15
         endif
       enddo


      ! I want the transpose of the outputted matrix because it confuses me less
      ! i.e. I like to left multiply by the rotation matricies rather than right multiply
      ! this is a check to make sure we have no negative or zero moment of inertia

  !        currentWalker%frag(k)%rm = transpose(tmprot)
  !        do j=1,3
  !          if (frags(k)%i(j).lt.1.d-15) then
  !            frags(k)%i(j) = 1.d-15
  !          endif
  !        enddo

      enddo ! End the loop over fragments.


      currentWalker => currentWalker%next
    enddo ! End the loop over walkers.

  endif


  !------------------------------------------------------------
  ! translate Equilibrium geoms to centre of mass frame
  ! for later use in qdmc_output
  !------------------------------------------------------------

  do i=1,QDMC%NumGeoms

   centre = 0.0

   do j=1,system%natom
   do k=1,3
    centre(k) = centre(k) +  QDMC%EquilibriumGeom(i,j,k)*system%mass(j)
   enddo
   enddo

   total_mass = sum(system%mass)
   centre = centre/total_mass

   do j=1,system%natom
   do k=1,3
    QDMC%EquilibriumGeom(i,j,k) = QDMC%EquilibriumGeom(i,j,k) - centre(k)
   enddo
   enddo

  enddo
  deallocate(x)



end subroutine setupWalkers

!====================================================================
! One last bit of PES setting up: allocate neighbour lists for
! modified Shepard interpolation, etc.
!====================================================================
subroutine setupPES(this,PES,maxWalkers)
  use qdmc_structures, ONLY : qdmc_par
  use PES_mod, ONLY : tPES
  class(tWalkers) :: this
  class(tPES), intent(inout) :: PES
  integer, intent(in) :: maxWalkers
  type(tWalker), pointer :: currentWalker
  integer :: count

  count = 1

  currentWalker => this%head%next

  do while (ASSOCIATED(currentWalker))
   call PES%allocateIDs(currentWalker%idPES)
   if (SIZE(currentWalker%idPES).gt.0) currentWalker%idPES = count
   currentWalker => currentWalker%next
   count=count+1
  enddo

  ! Now allocate neighbourlists with maximum number of walkers.
  call PES%allocateNeighbours(maxWalkers)
end subroutine setupPES






!====================================================================
! Calculates the branching weight for the walkers, then kills/copies
! as required.
! If imult = 0, kill walker.
!   imult = 1, leave walker alone.
!    imult = 2, create one copy.
!   imult = 3, create two copies.
!====================================================================
subroutine branchWalkers(this,QDMC)
  use qdmc_structures, ONLY : qdmc_par
  use mt19937, ONLY : genrand_real1

  class(tWalkers) :: this
  type(qdmc_par), intent(inout) :: QDMC

  type(tWalker), pointer :: currentWalker, nextWalker, tempWalker

  real(kind=8) :: Pc, u
  integer :: brWeight, i

  ! Get first walker.
  currentWalker => this%head%next

  ! Loop through walker list:
  do while (ASSOCIATED(currentWalker))

    nextWalker => currentWalker%next

    ! Compute the branching weight...
    if (.not.currentWalker%lowE) then
      ! ...only if the walker is not in a hole.

      Pc = (QDMC%Etrial - currentWalker%Vcurr)*QDMC%tau
      if (Pc < 4.605) then
        Pc = exp(Pc)
      else
        Pc = 100.0
      endif

      u = genrand_real1()

      brWeight = min(int(Pc+u),3)

    endif

    ! Now kill/branch as required.
    if (brWeight == 0 .or. currentWalker%lowE) then
      ! Dead walker walking. Link previous and next walkers.
      currentWalker%prev%next => nextWalker

      ! Only relink next walker if it's actually a walker:
      if (ASSOCIATED(nextWalker)) nextWalker%prev => currentWalker%prev


      if (currentWalker%lowE) QDMC%lowe_count = QDMC%lowe_count+1

      ! Bye bye.
      call currentWalker%kill()
      deallocate(currentWalker)

      QDMC%NumWalkers = QDMC%NumWalkers - 1

      !-----------------------------------------------------------------------
      ! check for underflow
      !-----------------------------------------------------------------------
      if (qdmc%NumWalkers < qdmc%MinWalkers) then
        print *, "!!!Ensemble underflow!!!"
        print *, "Try adjusting minWalkers."
        stop
      endif

    else
      ! Otherwise the number of copies is just brWeight - 1
      ! (brWeight = 1 is no copies, 2 is 1 copy, 3 is 2 copies)
      ! check for population overflow - note "-1" so correct number of copies is accounted for.
            if (QDMC%NumWalkers + brWeight - 1 > QDMC%MaxWalkers) then
                 QDMC%NumWalkers = QDMC%NormWalkers
                 print *, "!!! Ensemble overflow !!!"
                 print *, "If this occurs, there's a problem with the PES, IN_QDMC input parameters, or both."
                 print *, 'Try increasing maximum number of walkers.'
                 stop
            endif

      QDMC%NumWalkers = QDMC%NumWalkers + brWeight-1

      do i=1,brWeight-1
        ! Create a new walker.
        allocate(tempWalker)
        call tempWalker%create(this%nAtoms,this%nFrags,currentWalker)
        ! Make links.
        currentWalker%next => tempWalker
        tempWalker%prev => currentWalker
        tempWalker%next => nextWalker

        if (ASSOCIATED(nextWalker)) nextWalker%prev => tempWalker
        ! (only link next walker if it isn't null.)

        ! Point current walker at new next one, in case we loop again.
        currentWalker => currentWalker%next

      enddo


    endif

    currentWalker => nextWalker

  enddo

  this%nWalkers = QDMC%NumWalkers


end subroutine branchWalkers


!====================================================================
! Walks the walkers, using the Box-Muller method to generate random
! numbers with a spherical Gaussian distribution.
!====================================================================
subroutine walkWalkers(this,system,QDMC,frags)
  use molecule_specs, ONLY : molsysdat
  use qdmc_structures, ONLY : qdmc_par
  use fragment_mod, ONLY : frag_par
  use mt19937, ONLY : gauss_dev


  class(tWalkers) :: this
  type(molsysdat), intent(in) :: system
  type(qdmc_par), intent(in) :: QDMC
  type(frag_par), intent(inout), allocatable, dimension(:) :: frags

  type(tWalker), pointer :: currentWalker

  real(kind=8), dimension(3,3) :: rot
  real(kind=8), dimension(3)   :: dx
  real(kind=8), dimension(1,3) :: com1, com2
  real(kind=8), dimension(:,:), allocatable :: x
  integer :: i,j,k,n,ierr,ind, count
  integer, dimension(3) :: shift
  real(kind=8) :: sigma,rho
  !real(kind=8) :: gauss_dev

  if (ALLOCATED(this%walkArray)) deallocate(this%walkArray)

  ! Allocate a walker array - we'll collect the walkers here.
  allocate(this%walkArray(1:this%nWalkers))
  count = 1

  allocate(x(system%natom,3),stat=ierr)
  if (ierr.ne.0) stop 'error allocating x in walk_walkers'

  ! We are going to linked-list this thing.
  currentWalker => this%head%next



  do while (ASSOCIATED(currentWalker))
     do n=1,frags(0)%natom
     do j=1,3
       ind = frags(0)%indx(n)
       rho = gauss_dev()
       sigma = dsqrt(QDMC%tau/system%mass(ind))
       currentWalker%x(ind,j) = currentWalker%x(ind,j) + sigma*rho
     enddo
     enddo

     ! move the fragments as rigid bodies
     ! for each fragment ...
     do k=1,system%nfrag

     if (frags(k)%fixed) cycle

     ! move centre of mass of fragment
     !print *, 'original position'
     !do n=1,frags(k)%natom
     !  ind = frags(k)%indx(n)
       !print *, system%atom_label(ind), currentWalker%x(ind,:)

     !enddo
     do j=1,3
       rho = gauss_dev()
       sigma = dsqrt(QDMC%tau/frags(k)%mass)
       currentWalker%frag(k)%com(j) = currentWalker%frag(k)%com(j) + rho*sigma
       do n=1,frags(k)%natom
       ind = frags(k)%indx(n)
       currentWalker%x(ind,j) = currentWalker%x(ind,j) + rho*sigma
       enddo
     enddo

     ! now we want to rotate the fragment

     ! translate com of fragment to origin

           !write(*,*) 'com translated'
     do n=1,frags(k)%natom
       ind = frags(k)%indx(n)
       currentWalker%x(ind,:) = currentWalker%x(ind,:) - &
                         currentWalker%frag(k)%com(:)

             !write(*,*) system%atom_label(ind), currentWalker%x(ind,:)
     enddo

     do j=1,3 ! For each rotational axis determine a random rotation step
       rho = gauss_dev()
       sigma = dsqrt(QDMC%tau/currentWalker%frag(k)%i(j))
       dx(j) = sigma*rho
     enddo


     ! construct rotation matrix. works for any size rotation i think.

      rot(1,1) =  dcos(dx(2))*dcos(dx(3))
      rot(1,2) = -dcos(dx(2))*dsin(dx(3))
      rot(1,3) =  dsin(dx(2))
      rot(2,1) =  dcos(dx(1))*dsin(dx(3)) + dcos(dx(3))*dsin(dx(1))*dsin(dx(2))
      rot(2,2) =  dcos(dx(1))*dcos(dx(3)) - dsin(dx(1))*dsin(dx(2))*dsin(dx(3))
      rot(2,3) = -dcos(dx(2))*dsin(dx(1))
      rot(3,1) =  dsin(dx(1))*dsin(dx(3)) - dcos(dx(1))*dcos(dx(3))*dsin(dx(2))
      rot(3,2) =  dcos(dx(1))*dsin(dx(2))*dsin(dx(3)) + dcos(dx(3))*dsin(dx(1))
      rot(3,3) =  dcos(dx(1))*dcos(dx(2))

     ! rotate principal axis to coincide with cartesian axes with current rotation matrix
  !         write(11,*) 'rotm'
  !         write(11,*) rot

           !write(*,*) 'rotate to principal axes'
     do n=1,frags(k)%natom
       ind = frags(k)%indx(n)
       do j=1,3
       x(ind,j) = sum(currentWalker%frag(k)%rm(j,:)*currentWalker%x(ind,:))
       enddo
       currentWalker%x(ind,:) = x(ind,:)
             !write(*,*) system%atom_label(ind), currentWalker%x(ind,:)
     enddo

     ! rotate fragment
           !write(*,*) 'rotated fragment'
     do n=1,frags(k)%natom
       ind = frags(k)%indx(n)
       do j=1,3
       x(ind,j) = sum(rot(j,:)*currentWalker%x(ind,:))
       enddo
       currentWalker%x(ind,:) = x(ind,:)
             !write(*,*) system%atom_label(ind), currentWalker%x(ind,:)
     enddo

     ! rotate fragment back to it's body fixed frame
           !write(*,*) 'rotated back to original orientation + rotation step'
     do n=1,frags(k)%natom
       ind = frags(k)%indx(n)
       do j=1,3
       x(ind,j) = sum(currentWalker%frag(k)%rm(:,j)*currentWalker%x(ind,:))
       enddo
       currentWalker%x(ind,:) = x(ind,:)
             !write(*,*) system%atom_label(ind), currentWalker%x(ind,:)
     enddo



     ! propagate rotation matrix
  !         write(*,*) 'propagated rotation matrix'
     frags(k)%ax = matmul(transpose(rot),currentWalker%frag(k)%rm)
     currentWalker%frag(k)%rm = frags(k)%ax
  !         write(*,*) currentWalker%frag(k)%rm

     ! move fragment back to original location
           !write(*,*) 'final geometry'
     do n=1,frags(k)%natom
       ind = frags(k)%indx(n)
       currentWalker%x(ind,:) = currentWalker%x(ind,:) + &
                         currentWalker%frag(k)%com(:)
             !write(*,*) system%atom_label(ind), currentWalker%x(ind,:)
     enddo
     !stop

     enddo ! end the loop over the number of fragments.

    !do n=1,system%natom
      !     write(*,*) system%atom_label(n), currentWalker%x(n,:)
    !enddo

    ! Enforce periodic boundary conditions, if required.
    if (system%pbc) then
      x = system%cartToFrac(system%natom,currentWalker%x)

      ! Loop over free atoms
      do k=1,frags(0)%natom
        ind = frags(0)%indx(k)
        ! Calculate integer shift for atom
        shift = 0
        do n=1,3
          if (x(ind,n).gt.1.0) then
            shift(n) = -int(x(ind,n)) ! Shift back by int cells,
            ! e.g. if fractional coordinate is 1.2, then equivalent position
            ! in unit cell will be 0.2 (shift = -1).
          elseif (x(ind,n).lt.0.0) then
            shift(n) = -int(x(ind,n))+1 ! Shift forward by int+1 cells.
            ! e.g. if fraction coordinate is -1.2, -int(-1.2) = 1 and we need
            ! to shift by +2 to get to equivalent position at 0.8 in unit cell.
          endif
        enddo

        ! Now shift the atom.
        x(ind,:) = x(ind,:) + real(shift(:),8)
      enddo

      ! Loop over rigid fragments
      do k=1,system%nfrag
        ! Convert centre-of-mass to fractional coordinates.
        ! A bit of array reshaping silliness...
        com1(1,:) = currentWalker%frag(k)%com(:)
        com2 = system%cartToFrac(1,com1)

        ! Now calculate integer shift for centre-of-mass:
        shift = 0
        do n=1,3
          if (com2(1,n).gt.1.0) then
            shift(n) = -int(com2(1,n)) ! Shift back by int cells,
            ! e.g. if fractional coordinate is 1.2, then equivalent position
            ! in unit cell will be 0.2 (shift = -1).
          elseif (com2(1,n).lt.0.0) then
            shift(n) = -int(com2(1,n))+1 ! Shift forward by int+1 cells.
            ! e.g. if fraction coordinate is -1.2, -int(-1.2) = 1 and we need
            ! to shift by +2 to get to equivalent position at 0.8 in unit cell.
          endif
        enddo
        !print *, shift(:)
        ! Loop over atoms and shift fractional coordinates
        do n=1,frags(k)%natom
          ind = frags(k)%indx(n)
          !print *, 'x before', x(ind,:)
          x(ind,:) = x(ind,:)+real(shift(:),8)
          !print *, 'x after', x(ind,:)
        enddo

        ! Shift centre-of-mass too
        com2(1,:) = com2(1,:) + real(shift(:),8)
        ! Save shifted centre-of-mass, otherwise everything explodes
        com1 = system%fracToCart(1,com2)
        currentWalker%frag(k)%com = com1(1,:)
      enddo

      ! Finally, convert back to cartesians.
      currentWalker%x(:,:) = system%fracToCart(system%natom,x)


    endif


    ! Add walker to walker array.
     this%walkArray(count)%w => currentWalker
     count = count+1

     currentWalker => currentWalker%next

  !       write(*,*) 'final orientation'


  !       stop

  enddo

  deallocate(x)

end subroutine walkWalkers



!===================================================================
! Calculates the potential energy V of all the walkers, returns the
! average, V_bar.
!===================================================================
function calcEnergy(this,PES,step_count,nKill,nLowE) result(V_bar)
  use PES_mod, ONLY : tPES
  use Geometry_mod, ONLY : distt

  class(tWalkers) :: this
  class(tPES), intent(in) :: PES
  integer, intent(in) :: step_count
  integer, intent(out) :: nKill ! Number of walkers with energy more than 2 kJ/mol below minimum.
  integer, intent(out) :: nLowE ! Number of walkers with energy -2 kJ/mol < x < Vmin; borderline cases.
  real(kind=8) :: V_bar, dist1, dist2
  logical, dimension(:), allocatable :: updateNeighbours
  type(tWalker), pointer :: currentWalker
  integer :: i, j, nWalkers

  V_bar = 0.0d0
  nKill = 0
  nLowE = 0

  ! Check if it's time to update neighbour lists. (Only applicable to modified Shepard PESs.)
  call PES%checkNeighs(step_count,updateNeighbours)

  ! If so, reset PES IDs of walkers.
  if (ALLOCATED(updateNeighbours)) then
  if (any(updateNeighbours)) then
    ! (check that it's actually allocated first)
    !print *, 'update neighbours'
    do i=1,this%nWalkers
	    currentWalker => this%walkArray(i)%w
      ! Quickly loop over PESs too
      do j=1, SIZE(updateNeighbours)
        if (updateNeighbours(j)) currentWalker%idPES(j) = i
      enddo
	  enddo
  endif
  endif

!$OMP PARALLEL DO SHARED(this,PES,V_bar,nKill,step_count) DEFAULT(private) SCHEDULE(dynamic)
  do i=1,this%nWalkers
    currentWalker => this%walkArray(i)%w

    ! Pass walker's cartesian coordinates to PES.
    currentWalker%Vcurr = PES%calcEnergy(currentWalker%x, currentWalker%idPES, step_count, lowE=currentWalker%lowE)

    if (currentWalker%lowE) then
      !$OMP ATOMIC
      nKill = nKill + 1
      cycle
    endif

    ! Check for low energy-ness on whole PES.
    if (currentWalker%Vcurr  < -0.00076) then
      if (PES%PrintLowE) then
!$OMP CRITICAL (lowE2)
        write(11,*) '---------------------------------------------------'
        write(11,*) ' WARNING : interpolated energy too low on full PES '
        write(11,*) '         : walker ',i,'  step ',step_count
        write(11,*) '         : interp%vmin  =',PES%vmin
        write(11,*) '         : energy       =',currentWalker%Vcurr
        write(11,*)

        do j=1, PES%nAtoms
          write(11,*) currentWalker%x(j,:)
        enddo
!$OMP END CRITICAL (lowE2)
      endif
      ! Kill the walker.
      if (PES%killLowE) then
        !$OMP ATOMIC
        nKill = nKill + 1
        currentWalker%lowE = .true.
        cycle
      endif
    else if (currentWalker%Vcurr < -0.0002) then
      !$OMP ATOMIC
      nLowE = nLowE+1 ! Just increment number of low energy walkers for statistical purposes.
    endif

    ! If we've gotten this far, then the walker's energy must be fine.

!$OMP ATOMIC
    V_bar = V_bar + currentWalker%Vcurr
  enddo
!$OMP END PARALLEL DO

  V_bar = V_bar/real(this%nWalkers-nKill,8)

  if (ALLOCATED(updateNeighbours)) deallocate(updateNeighbours)



end function calcEnergy





!====================================================================
! Adds a descendant weighting parent ID to the end of the walker's
! parents list. Declutters code in DW module.
!====================================================================
subroutine addParent(this,ID,activeGens)
  class(tWalker) :: this
  integer, intent(in) :: ID, activeGens
  integer :: error
  integer, dimension(1:activeGens) :: tempArray

  ! Check this%ID allocation, etc.
  if (.not.(ALLOCATED(this%ID))) then
    allocate(this%ID(1:1),stat=error)
    if (error.ne.0) stop 'Error allocating walker parent ID array.'

    this%ID(1) = ID

  else
    tempArray(1:activeGens-1) = this%ID(1:activeGens-1)
    tempArray(activeGens) = ID
    deallocate(this%ID)

    allocate(this%ID(1:activeGens),stat=error)
    if (error.ne.0) stop 'Error allocating walker parent ID array.'
    this%ID(:) = tempArray(:)

  endif
end subroutine addParent


!====================================================================
! Returns the first parent ID from the list, then deletes it.
!====================================================================
function getParent(this,activeGens,delete) result(parent)
  class(tWalker) :: this
  integer, intent(in) :: activeGens
  logical, intent(in) :: delete ! Whether to delete the ID array.
  integer, dimension(1:activeGens-1) :: tempArray
  integer :: parent, error

  if (delete) then
    parent = this%ID(1)

    if (activeGens == 1) then
      deallocate(this%ID)
    else
      tempArray(:) = this%ID(2:activeGens)
      deallocate(this%ID)
      allocate(this%ID(1:activeGens-1),stat=error)
      if (error.ne.0) stop 'Error allocating walker parent ID array.'

      this%ID(:) = tempArray(:)

    endif
  else
    ! Just return the generation ID specified by activeGens
    parent = this%ID(activeGens)

  endif

end function getParent



!====================================================================
! Allocates the necessary bits and pieces for the given walker.
! Note: appears as walker%create(nAtoms,nFrags) !!!
! Note: wIn is optional - the new walker will be copied from wIn if
! it's provided.
!====================================================================
subroutine newWalker(this,nAtoms,nFrags,wIn)
  class(tWalker) :: this
  integer, intent(in) :: nAtoms, nFrags
  type(tWalker), intent(in), optional :: wIn
  integer :: nAtom, nFrag, error, i, j

  this%Vcurr = 0.0d0
  this%lowE = .false.

  nAtom = nAtoms
  if (PRESENT(wIn)) nAtom = SIZE(wIn%x,1)
  nFrag = nFrags
  if (PRESENT(wIn).and.ALLOCATED(wIn%frag)) nFrag = SIZE(wIn%frag)
  ! I'm doing this so we don't need to provide an accurate nAtoms and nFrags
  ! to a walker we're copying.


  allocate(this%x(1:nAtom,1:3),stat=error)
  if (error.ne.0) stop ' Error allocating qdmc_particle%x '

  allocate(this%r(1:nAtom*(nAtom-1)/2),stat=error)
  if (error.ne.0) stop ' Error allocating qdmc_particle%r '

  if (nFrag.ne.0) then ! Only do this if there's no fragments.
    allocate(this%frag(1:nFrag),stat=error)
    if (error.ne.0) stop ' Error allocating QDMC%frag '
  endif

  if (PRESENT(wIn)) then

    this%Vcurr = wIn%Vcurr
    do i=1, nFrag
      this%frag(i) = wIn%frag(i)
    enddo

    do j=1,3
    do i=1,nAtom
      this%x(i,j) = wIn%x(i,j)
    enddo
    enddo

    do i=1,nAtom*(nAtom-1)/2
      this%r(i) = wIn%r(i)
    enddo

    if (ALLOCATED(wIn%ID)) then
      ! Allocate generation ID array and copy.
      allocate(this%ID(1:SIZE(wIn%ID)),stat=error)
      if (error.ne.0) stop 'Error allocating copied generation ID array'
      this%ID(:) = wIn%ID(:)
    endif

    if (ALLOCATED(wIn%idPES)) then ! Do this to avoid segfaulting when running with --DWonly
      if (SIZE(wIn%idPES).gt.0) then
        ! Allocate PES ID array and copy.
        allocate(this%idPES(1:SIZE(wIn%idPES)))
        if (error.ne.0) stop 'Error allocating copied PES ID array'
        this%idPES(:) = wIn%idPES(:)
      endif

    endif

  endif



end subroutine newWalker


!====================================================================
! Saves the current walkers to the Progress.OUT file.
!====================================================================
subroutine dumpWalkers(this)
  class (tWalkers) :: this
  type (tWalker), pointer :: currentWalker
  integer :: unit, nGens


  currentWalker => this%head%next
  ! Get the size of the ID array.
  if (ALLOCATED(currentWalker%ID)) then
    nGens = SIZE(currentWalker%ID)
  else
    nGens = 0
  endif

  ! Open the Progress.OUT file.
  open(newunit=unit,file='Progress.OUT',status='old',access='append')

  ! Number of walkers, number of generations being tracked, plus a copy of the number of atoms + frags
  write(unit,*) this%nWalkers, nGens, this%nAtoms, this%nFrags

  ! Now write the walkers!
  do while (ASSOCIATED(currentWalker))
    write(unit,*) currentWalker%x(:,:)
    ! Write parent IDs, if applicable.
    if (nGens.gt.0) write(unit,*) currentWalker%ID(:)
    currentWalker => currentWalker%next
  enddo

  close(unit)


end subroutine dumpWalkers

!====================================================================
! Loads the current walkers from the Progress.OUT file.
! Returns the total number of walkers.
!====================================================================
function loadWalkers(this) result(nWalkers)
  class (tWalkers) :: this
  type (tWalker), pointer :: currentWalker
  integer :: unit, nGens, nWalkers
  logical :: fileOpened

  ! Get the unit number for Progress.OUT.
  inquire(file='Progress.OUT',OPENED = fileOpened, NUMBER = unit)

  if (.not.fileOpened) stop 'Progress.OUT not open!'



  ! Get number of walkers, etc.
  read(unit,*) nWalkers, nGens, this%nAtoms, this%nFrags

  ! Create new list of walkers.
  call this%new(nWalkers,this%nAtoms,this%nFrags)

  ! Loop over walkers, load coordinates + parent IDs.
  currentWalker => this%head%next
  do while (ASSOCIATED(currentWalker))
    read(unit,*) currentWalker%x(:,:)

    ! Allocate parent ID array.
    if (nGens.gt.0) then
      allocate(currentWalker%ID(1:nGens))
      read(unit,*) currentWalker%ID(:)
    endif
    currentWalker => currentWalker%next
  enddo

  this%nWalkers = nWalkers


end function




!====================================================================
! Erase a walker's existence.
! "Dead, buried and cremated." - (ex) Prime Minister Tony Abbott
!====================================================================
subroutine killWalker(this)
  class(tWalker) :: this

  deallocate(this%x)
  deallocate(this%r)
  if (ALLOCATED(this%frag)) deallocate(this%frag)
  if (ALLOCATED(this%ID)) deallocate(this%ID)
  if (ALLOCATED(this%idPES)) deallocate(this%idPES)


end subroutine killWalker




end module Walkers_mod
