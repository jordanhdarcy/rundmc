!==================================================================
! Various geometrical-related functions for calculating distances,
! angles, dihedrals and other things.
! Note: functions relating to vectors are in vec_mod.f90.
!==================================================================

module Geometry_mod

contains

! dist.f90
!
! A subroutine to calculate bondlengths between two specified
! points (rather than every point as in the subroutine intern)


subroutine distt(a,b,distout)

  implicit none

  real(kind=8), dimension(3), intent(in) :: a, b
  real(kind=8), intent(out) :: distout 

  distout = sqrt( (a(1)-b(1))**2 + &
          (a(2)-b(2))**2 + &
          (a(3)-b(3))**2 )
  return
end subroutine


! dihedral.f90
!
! A program to calculate a dihedral angle given 4 points.
! In particular calculates dihedral angle between the planes formed by
! points ABC and BCD.


subroutine dihedral(a,b,c,d,diheddeg)

  implicit none

  real(kind=8), dimension(3), intent(in) :: a,b,c,d
  real(kind=8), intent(out) :: diheddeg

  real(kind=8), dimension(3) :: v1, v2, v3
  real(kind=8) :: norm1x, norm1y, norm1z, norm1len
  real(kind=8) :: norm2x, norm2y, norm2z, norm2len
  real(kind=8) :: n1xn2x, n1xn2y, n1xn2z
  real(kind=8) :: dihed_numerator, dihed
  real(kind=8) :: dot, sindihed, cosdihed
  real(kind=8) :: lenv1, lenv2, lenv3
  real(kind=8), dimension(2) :: alpha

  integer :: j
  real(kind=8) :: pi, tiny

  pi = 2.d0*dacos(0.d0)
  tiny = 1.0d-100

  ! Firstly calculate cartesian vector coefficients for vectors:
  ! AB (v1), BC (v2), CD (v3)

  do j=1,3    
   v1(j) = b(j) - a(j)
   v2(j) = c(j) - b(j)
   v3(j) = d(j) - c(j)
  enddo 

  ! Then take the cross products: v1 x v2 = norm1, v2 x v3 = norm2

  norm1x = v1(2)*v2(3) - v2(2)*v1(3)
  norm1y = v1(1)*v2(3) - v2(1)*v1(3)
  norm1z = v1(1)*v2(2) - v2(1)*v1(2)

  norm2x = v2(2)*v3(3) - v3(2)*v2(3)
  norm2y = v2(1)*v3(3) - v3(1)*v2(3)
  norm2z = v2(1)*v3(2) - v3(1)*v2(2)

  ! Now calculate the dihedral angle between the two planes by taking the
  ! cosine of the dot product of the two normal vectors and check sign
  ! is correct. 

  norm1len = dsqrt(norm1x**2 + norm1y**2 + norm1z**2)
  norm2len = dsqrt(norm2x**2 + norm2y**2 + norm2z**2)
  dihed_numerator = norm1x*norm2x + norm1y*norm2y + norm1z*norm2z

  ! Guard against zero length vectors. Define the dihedral angle to be zero when
  ! we have linearly dependent data.

  if (norm1len.lt.tiny) then
    norm1len = tiny
  endif
  if (norm2len.lt.tiny) then
    norm2len = tiny
  endif

  cosdihed = dihed_numerator/(norm1len*norm2len)


  ! Calculate the sine of the dihedral angle (to check sign).
  ! First calculate norm1 x norm2 = n1xn2

  n1xn2x = norm1y*norm2z - norm2y*norm1z
  n1xn2y = norm1x*norm2z - norm2x*norm1z
  n1xn2z = norm1x*norm2y - norm2x*norm1y

  ! v2 . [n1xn2]

  dot = v2(1)*n1xn2x + v2(2)*n1xn2y + v2(3)*n1xn2z

  ! calculate lengths of vectors v1, v2 and v3

  call distt(a,b,lenv1)
  call distt(b,c,lenv2)
  call distt(c,d,lenv3)

  ! calculate angles between abc and bcd

  call angled(a,b,c,alpha(1))
  call angled(b,c,d,alpha(2))

  ! Guard against zero angles. Define the dihedral angle to be zero when
  ! we have linearly dependent data.

  do j=1,2
  if (alpha(j).lt.tiny.or.abs(alpha(j)-2*pi).lt.tiny) then
    alpha(j) = tiny
  endif
  enddo

  sindihed = dot/(dsin(alpha(1))*dsin(alpha(2))*lenv1*lenv2**3*lenv3)

  ! Using sine of dihedral (sindihed) and cosine of dihedral (cosdihed) determine
  ! the quadrant the dihedral angle is in.


  !      open(unit=99,file='dbg',status='replace')
  !      write(99,*) 'sindihed = ',sindihed
  !      write(99,*) 'cosdihed = ',cosdihed
  !      write(99,*) 'alpha1 = ',alpha(1)
  !      write(99,*) 'alpha2 = ',alpha(2)
  !      write(99,*) 'a = ',a
  !      write(99,*) 'b = ',b
  !      write(99,*) 'c = ',c
  !      write(99,*) 'd = ',d
  !      close(unit=99)

  if (abs(sindihed).gt.1.00) then
  if (sindihed.lt.0.0) then
    sindihed = -0.99999
  else
    sindihed = 0.99999
  endif
  endif

  dihed = dasin(sindihed)      

  if (cosdihed.lt.0.0) then
    dihed = pi - dihed
  endif

  if (dihed.lt.0.0) then
    dihed = dihed + 2*pi
  endif

  diheddeg = dihed*180/pi

  !      if(anint(diheddeg).eq.360) then
  !          diheddeg = diheddeg - 360
  !      endif


end subroutine

! angled.f90
!
! Calculates the angle between three points. The
! angle is abc, so it will calculate the angle at the
! point of the middle argument using the cosine rule.

subroutine angled(a,b,c,theta)

  implicit none

  real(kind=8), dimension(3), intent(in) :: a, b, c
  real(kind=8), intent(out) :: theta
  real(kind=8) :: pi
  real(kind=8), dimension(3) :: rr

  pi = 2.d0*dacos(0.d0)

  rr(1) = dsqrt((a(1)-b(1))**2 + (a(2)-b(2))**2 + (a(3)-b(3))**2)
  rr(2) = dsqrt((c(1)-b(1))**2 + (c(2)-b(2))**2 + (c(3)-b(3))**2)
  rr(3) = dsqrt((c(1)-a(1))**2 + (c(2)-a(2))**2 + (c(3)-a(3))**2)

  theta=dacos((rr(1)**2+rr(2)**2-rr(3)**2)/(2*rr(1)*rr(2)))

end subroutine

! angle.f90
!
! Calculates 180 minus the angle between three points. The
! angle is abc, so it will calculate the angle at the
! point of the middle argument using the cosine rule.

subroutine angle(a,b,c,angleoutdeg)

  implicit none

  real(kind=8), dimension(3), intent(in) :: a, b, c
  real(kind=8), intent(out) :: angleoutdeg
  real(kind=8) :: theta, pi
  real(kind=8), dimension(3) :: rr

  pi = 2.d0*dacos(0.d0)

  rr(1) = sqrt((a(1)-b(1))**2 + (a(2)-b(2))**2 + (a(3)-b(3))**2)
  rr(2) = sqrt((c(1)-b(1))**2 + (c(2)-b(2))**2 + (c(3)-b(3))**2)
  rr(3) = sqrt((c(1)-a(1))**2 + (c(2)-a(2))**2 + (c(3)-a(3))**2)

  theta=pi-acos((rr(1)**2+rr(2)**2-rr(3)**2)/(2*rr(1)*rr(2)))
  angleoutdeg = theta*180.d0/pi

end subroutine









! Eep.





    SUBROUTINE JACOBI(A,N,NP,D,V,NROT)
    implicit none
    integer*4 :: nmax,np,ip,n,iq,nrot,i,j
    real*8 :: v,b,a,z,sm,tresh,g,h,t,theta,c,s,tau,d
    PARAMETER (NMAX=100)
    DIMENSION A(NP,NP),D(NP),V(NP,NP),B(NMAX),Z(NMAX)
    DO 12 IP=1,N
        DO 11 IQ=1,N
            V(IP,IQ)=0.d0
        11 END DO
        V(IP,IP)=1.d0
    12 END DO
    DO 13 IP=1,N
        B(IP)=A(IP,IP)
        D(IP)=B(IP)
        Z(IP)=0.d0
    13 END DO
    NROT=0
    DO 24 I=1,50
        SM=0.d0
        DO 15 IP=1,N-1
            DO 14 IQ=IP+1,N
                SM=SM+ABS(A(IP,IQ))
            14 END DO
        15 END DO
    ! modified line below to work when program underflows are not allowed.
        IF(SM < 1.d-50)RETURN
        IF(I < 4)THEN
            TRESH=0.2d0*SM/N**2
        ELSE
            TRESH=0.d0
        ENDIF
        DO 22 IP=1,N-1
            DO 21 IQ=IP+1,N
                G=100.d0*ABS(A(IP,IQ))
                IF((I > 4) .AND. (ABS(D(IP))+G == ABS(D(IP))) &
                 .AND. (ABS(D(IQ))+G == ABS(D(IQ))))THEN
                    A(IP,IQ)=0.d0
                ELSE IF(ABS(A(IP,IQ)) > TRESH)THEN
                    H=D(IQ)-D(IP)
                    IF(ABS(H)+G == ABS(H))THEN
                        T=A(IP,IQ)/H
                    ELSE
                        THETA=0.5d0*H/A(IP,IQ)
                        T=1.d0/(ABS(THETA)+SQRT(1.d0+THETA**2))
                        IF(THETA < 0.d0)T=-T
                    ENDIF
                    C=1.d0/SQRT(1.d0+T**2)
                    S=T*C
                    TAU=S/(1.d0+C)
                    H=T*A(IP,IQ)
                    Z(IP)=Z(IP)-H
                    Z(IQ)=Z(IQ)+H
                    D(IP)=D(IP)-H
                    D(IQ)=D(IQ)+H
                    A(IP,IQ)=0.d0
                    DO 16 J=1,IP-1
                        G=A(J,IP)
                        H=A(J,IQ)
                        A(J,IP)=G-S*(H+G*TAU)
                        A(J,IQ)=H+S*(G-H*TAU)
                    16 END DO
                    DO 17 J=IP+1,IQ-1
                        G=A(IP,J)
                        H=A(J,IQ)
                        A(IP,J)=G-S*(H+G*TAU)
                        A(J,IQ)=H+S*(G-H*TAU)
                    17 END DO
                    DO 18 J=IQ+1,N
                        G=A(IP,J)
                        H=A(IQ,J)
                        A(IP,J)=G-S*(H+G*TAU)
                        A(IQ,J)=H+S*(G-H*TAU)
                    18 END DO
                    DO 19 J=1,N
                        G=V(J,IP)
                        H=V(J,IQ)
                        V(J,IP)=G-S*(H+G*TAU)
                        V(J,IQ)=H+S*(G-H*TAU)
                    19 END DO
                    NROT=NROT+1
                ENDIF
            21 END DO
        22 END DO
        DO 23 IP=1,N
            B(IP)=B(IP)+Z(IP)
            D(IP)=B(IP)
            Z(IP)=0.d0
        23 END DO
    24 END DO
    STOP "50 iterations should never happen"
    RETURN
    END SUBROUTINE JACOBI





end module Geometry_mod
