!****************************************************************
! Contains the parent 'PES' type, which can take on either
! a) a gas-phase PES, containing MSI/spline fragments etc.
! b) a crystal surface PES, using Terry's code.
! To make things easier, the tPES type contains dummy subroutines
! for calcEnergy,loadPES, etc. so we don't have to go through
! select type nonsense whenever we need to do anything.
!****************************************************************



module PES_mod
use CubicSplines

implicit none

private

public :: tPES, tFragment, tSplineFrag


!=====================================================================
! Contains everything common to both modified Shepard and spline PESs.
!=====================================================================
type tFragment
    integer :: multiplier ! equal to 1 or -1, depending whether this fragment is added or subtracted in the sum of E(frag).
    integer :: identicalTo ! Index of fragment that this one is functionally identical to, i.e. by symmetry. Equal to the index of fragment in array if fragment is unique.
    integer :: nAtoms
    integer :: indList ! index of fragment in overall list of fragments - may be different to that of fragment in array.
    character(100) :: name

  contains
  procedure, pass(this) :: calcE => calcEnGeneric


end type tFragment


!=====================================================================
! A container for the tFragment type. This exists so I can put the MSI 
! and spline extensions of tFragment into a single 'polymorphic' array.
! I can't seem to have an array of tFragments that's polymorphic, so
! it has to be done this way. So there.
!=====================================================================
type tFragArray
  class(tFragment), pointer :: fragment
end type tFragArray

!=================================================================================
! A type to store all the PES information. This is what will be carried by the
! simulation above. Only a limited number of things will actually be visible.
!================================================================================
type tPES
  real(kind=8) :: Vmin ! Global minimum
  integer :: nAtoms
  type(tFragArray), dimension(:), allocatable :: FragContainer

  logical :: absEnergy = .false.
  logical :: printDetails = .false.
  logical :: CPUtime = .false.
  logical :: killLowE = .true.
  logical :: printLowE = .true.
  logical :: pbc = .false.

  contains
    ! These are dummy procedures that are implemented properly by the inheriting children.
    procedure, pass(this) :: harvestArgs => harvestArgs
    procedure, pass(PES) :: load => loadPES
    procedure, pass(this) :: calcEnergy => calcEnergy
    procedure, pass(this) :: setupTOUT => setupTOUT
    procedure, pass(this) :: writeTOUT => writeTOUT
    procedure, pass(this) :: LoadXYZ => LoadXYZ
    procedure, pass(this) :: Neighbours => UpdateNeighbours
    procedure, pass(this) :: CheckNeighs => CheckNeighs
    procedure, pass(this) :: allocateIDs => allocateIDs
    procedure, pass(this) :: allocateNeighbours => allocateNeighbours

end type tPES

!=====================================================================
! Defines a centre-of-mass used in the spline interpolation.
!=====================================================================
type, private :: CoM
  integer, dimension(:), allocatable :: indices
  real(kind=8), dimension(:), allocatable :: mass
  integer :: nAtoms
  real(kind=8) :: sumMasses

  contains
    final :: destroy_CoM
end type CoM

!=====================================================================
! Doodads for any spline PESs.
!=====================================================================
type, extends(tFragment) :: tSplineFrag
  type(tSpline) :: spline
  type(CoM), dimension(:), allocatable :: CoMs
  type(tSplineFrag), pointer :: identFrag

  contains
    procedure, pass(this) :: calcE => calcSpline
end type tSplineFrag



contains

!======================================================================
! Dummy subroutines ahead.
!======================================================================

subroutine harvestArgs(this,args)
  use Args_mod, ONLY : tArgs

  class(tPES) :: this
  type (tArgs), intent(in) :: args

  stop "harvestArgs: You shouldn't be here, Jordan."

end subroutine harvestArgs


subroutine LoadPES(PES,sys)

    use molecule_specs, ONLY : molsysdat
    implicit none

    class(tPES) :: PES
    type(molsysdat), target, intent(in) :: sys

  stop "LoadPES: You shouldn't be here, Jordan."

end subroutine LoadPES


function calcEnergy(this,coords,ID,step,lowE,fragEnergies) result(V)
  class(tPES) :: this
  real(kind=8), dimension(1:this%nAtoms,1:3), intent(in) :: coords
  integer, dimension(:), intent(in) :: ID
  integer, intent(in) :: step
  logical, optional, intent(out) :: lowE
  real(kind=8), dimension(:), optional, intent(out) :: fragEnergies
  real(kind=8) :: V

  stop "calcEnergy: You shouldn't be here, Jordan."

end function calcEnergy

function calcEnGeneric(this,coords,ID,step,lowE) result(V)
  class(tFragment) :: this
  real(kind=8), dimension(:,:), intent(in) :: coords
  !integer, dimension(:), intent(in) :: ID
  integer, intent(in) :: ID, step
  logical, intent(out) :: lowE
  real(kind=8) :: V
  lowE = .false.
  stop "You shouldn't be here, Jordan."
end function calcEnGeneric


subroutine setupTOUT(this)
  class(tPES) :: this

  stop "setupTOUT: You shouldn't be here, Jordan."
end subroutine setupTOUT

subroutine writeTOUT(this,status)
  class(tPES) :: this
  logical, intent(in) :: status

  stop "writeTOUT: You shouldn't be here, Jordan."
end subroutine


subroutine LoadXYZ(this,fileName)
    class (tPES) :: this
    character*30, intent(in) :: fileName

  stop "LoadXYZ: You shouldn't be here, Jordan."
end subroutine LoadXYZ

subroutine CheckNeighs(this,step,check)
  class(tPES) :: this
  integer, intent(in) :: step
  logical, dimension(:), allocatable, intent(out) :: check

  ! This should be ignored.
end subroutine CheckNeighs

subroutine UpdateNeighbours(this,coords,step)
  class (tPES) :: this
  real(kind=8), dimension(:,:), intent(in) :: coords
  integer, intent(in) :: step

  print *, "UpdateNeighbours: This is a subroutine specific to Mad Mick's Modified Shepard."
  print *, "Either this hasn't been implemented, or you can comment out any calls to it."
  stop


end subroutine UpdateNeighbours

subroutine allocateIDs(this,idPES)
  class (tPES) :: this
  integer, dimension(:), intent(out), allocatable :: idPES
  allocate(idPES(0))

  ! If there's no modified Shepard PES, the array isn't necessary and
  ! to avoid segmentation faults (i.e. by returning an unallocated
  ! array), will just allocate the 0th element.
end subroutine

subroutine allocateNeighbours(this,max)
  class (tPES) :: this
  integer, intent(in) :: max
  ! If there's no modified Shepard PES, this does nothing and can be ignored.
end subroutine allocateNeighbours



!=====================================================================
! Calculates the potential energy of the given geometry on a cubic
! spline PES.
!=====================================================================
function calcSpline(this,coords,ID,step,lowE) result(V)
  class(tSplineFrag) :: this
  real(kind=8), dimension(:,:), intent(in) :: coords
  integer, intent(in) :: ID, step
  logical, intent(out) :: lowE
  real(kind=8) :: V
  real(kind=8) :: dist
  real(kind=8), dimension(1:2,1:3) :: CoMs
  integer :: i, j, k, l
  type(tSplineFrag), pointer :: identSpline

  lowE = .false.

  V = 0.0d0
  dist = 0.0d0
  CoMs = 0.0d0

  ! Calculate the distance between the centre-of-masses for the given spline.

  do i=1,2 ! loop over centre-of-masses
    do j=1, this%CoMs(i)%nAtoms ! Loop over atoms in centre-of-masses
      l = this%CoMs(i)%indices(j)
      do k=1,3 ! Each x,y,z coordinate
        CoMs(i,k) = CoMs(i,k) + coords(l,k)*this%CoMs(i)%mass(j)
      enddo
    enddo
    CoMs(i,:) = CoMs(i,:)/this%CoMs(i)%sumMasses
  enddo

  ! Calculate the distance between them.
  do i=1,3
    dist = dist + (CoMs(1,i)-CoMs(2,i))**2
  enddo

  dist = sqrt(dist)

  identSpline => this%identFrag

  ! Calculate the energy.
  V = identSpline%spline%Splint(dist)

end function calcSpline


!==============================================================
! This is a little workaround for a gfortran compiler bug.
! Basically:
! tSplineFrag contains a fixed-size array of CoM types.
! These CoM types contain two allocatable arrays, which upsets 
! gcc 4.9. Apparently this has been fixed, but until the compiler
! is updated on Raijin this'll have to do.
! ref: http://dannyvanpoucke.be/ca-coders-anonymous/#codesyntax_2
!==============================================================
subroutine destroy_CoM(this)
  type(CoM) :: this
  if (allocated(this%indices)) deallocate(this%indices)
  if (allocated(this%mass)) deallocate(this%mass)
end subroutine destroy_CoM



end module PES_mod
