!======================================================================
! Contains everything relating to the diffusion Monte Carlo simulation.
! TBA
!======================================================================
module DMC_mod
use molecule_specs, ONLY : molsysdat, kJ, au2ang
use qdmc_structures, ONLY : qdmc_par
use fragment_mod, ONLY : frag_par, read_fragment_data
use Walkers_mod, ONLY : tWalkers, tWalkerBox
use PES_mod, ONLY : tPES
use DW_mod, ONLY : tGenerations
use Utilities, ONLY : blocking
use Histograms_mod, ONLY : SetupDW, DescendantWeighting

implicit none


!======================================================================
! Stores simulation progress - step, block, energy arrays, etc.
!======================================================================
type tProgress
  real(kind=8) :: Vbar_avg, Vbar_avg_sq, Eref_avg, Eref_avg_sq
  real(kind=8), dimension(:), allocatable :: V_block, Eref_block, vbar_steps
  integer :: Block, nWalkers

end type tProgress

!======================================================================
! The simulation itself. Will be an object held by the program above.
! In principle, there could be many simulation objects!
!======================================================================
type tDMCSim

  type(qdmc_par) :: QDMC
  type(frag_par), dimension(:), allocatable :: frags
  type(tWalkers) :: Walkers
  type(tGenerations) :: Generations
  type(tProgress) :: Progress

  ! Whether sys has equilibrated. Important for restarting simulations.
  logical :: equilibrated = .false.

  ! Whether the simulation is restarting, and is restartable.
  logical :: restarting = .false.
  logical :: saveProg = .false.

  logical :: printDMCsteps = .false.
  logical :: writeXYZ = .false.
  logical :: writeTOUT = .false.
  logical :: trackTime = .false.
  
  real(kind=4) :: cutTime
  real(kind=4) :: sSec, sMin, sHour ! starting second, minute, hour.
  integer :: sDay, sMon, sYr ! starting day, month, year



  contains
    procedure, pass(this) :: harvestArgs => harvestArgs
    procedure, pass(this) :: setup => setupSim
    procedure, pass(this) :: equilibrate => equilibrate
    procedure, pass(this) :: sample => sample
    procedure, pass(this) :: dump => dumpSimulation
    procedure, pass(this) :: load => loadSimulation
    procedure, pass(this) :: wallTime => calcWallTime
    procedure, pass(this) :: DWOnly => DWOnly
    procedure, pass(this) :: printWalkers => printWalkers



end type tDMCSim

contains

!======================================================================
! Sets up the simulation:
! 1. Reads fragment data.
! 2. Reads QDMC input and sets up walkers.
! 3. Most important: Prints a headline.
!======================================================================
subroutine setupSim(this,sys)
  class(tDMCSim) :: this
  type(molsysdat), intent(inout) :: sys
  integer(kind=4) :: today(3), now(3)

  ! Read the fragment data for RBDMC.
  call read_fragment_data(sys,this%frags,'IN_FRAG')

  ! Read input data for QDMC.
  call this%QDMC%readInput(sys)

  this%QDMC%restart = this%restarting

  ! Set up the descendant weighting object.
  call this%Generations%setup(this%QDMC%NumGenerations,this%QDMC%DelayBetweenGens,this%QDMC%NumDescendantSteps,sys%natom)

  if (.not.this%restarting) then
    ! Create the required walkers.
    call this%Walkers%new(this%QDMC%Normwalkers,sys%natom,sys%nfrag)

    ! Clean up possible DW.OUT, only if the job is restartable (avoid accidently deleting existing file...)
    if (this%saveProg) call execute_command_line('rm -f DW.OUT')
  else
    call this%load()
  endif


  ! Now set up the walkers - initial geometries, etc.
  call this%Walkers%setup(sys,this%QDMC,this%frags)


  ! Load details for descendant weighting histograms.
  call SetupDW()

  call printHeadline(this%QDMC)

  ! Get the start date and time.
  call idate(today) ! today(1)=day, (2)=month, (3)=year
  call itime(now) ! now(1)=hour, (2)=minute, (3)=second
  this%sDay = today(1)
  this%sMon = today(2)
  this%sYr = today(3)
  this%sHour = now(1)
  this%sMin = now(2)
  this%sSec = now(3)


  
end subroutine setupSim

!======================================================================
! Harvests relevant command-line arguments from the given Arguments
! object. Must be called before 'setupSim' for maximum effectiveness.
!======================================================================
subroutine harvestArgs(this,args)
  use Args_mod, ONLY : tArgs

  class(tDMCSim) :: this
  type (tArgs), intent(in) :: args

  this%restarting = args%restarting
  this%saveProg = args%saveProg
  this%trackTime = args%trackTime
  this%cutTime = args%cutTime

  this%printDMCsteps = args%printDMCsteps
  this%writeTOUT = args%writeTOUT
  this%writeXYZ = args%writeXYZ

  this%QDMC%printVectors = args%printVectors
  this%QDMC%printObservables = args%printObs
  this%QDMC%stem = args%suffix
  this%QDMC%multiStep = args%multiStep

  this%Generations%printXYZ = args%DWtoXYZ
  this%Generations%multiStep = args%multiStep
  this%Generations%trackingSteps = args%steps


end subroutine harvestArgs




!=======================================================================
! The equilibration stage of the DMC simulation.
!=======================================================================
subroutine equilibrate(this,sys,PES)
  class(tDMCSim) :: this
  type(molsysdat), intent(in) :: sys
  class(tPES), intent(inout) :: PES


  ! Simulation blocks, steps, etc.
  integer :: startBlock, iblock, istep, step_count
  integer :: nKilled, nLowE
  real(kind=8) :: nKill_avg, nLowE_avg

  ! Energies
  real(kind=8) :: v_bar, ratio
  real(kind=8), dimension(:), allocatable :: v_block, eref_block

  real(kind=8) :: wallTime

  ! One last quick bit of setting up here
  call this%Walkers%setupPES(PES,this%QDMC%MaxWalkers)

  if (this%equilibrated) return



  allocate(v_block(1:this%QDMC%BlocksToEquil))
  v_block = 0.0d0
  allocate(eref_block(1:this%QDMC%BlocksToEquil))
  eref_block = 0.0d0

  if (this%restarting) then
    startBlock = this%Progress%Block+1
    step_count = this%Progress%Block * this%QDMC%StepsPerBlock
    ! Copy arrays.
    v_block(1:this%Progress%Block) = this%Progress%V_block(:)
    eref_block(1:this%Progress%Block) = this%Progress%Eref_block(:)

    ! Switch off restart flags.
    this%restarting = .false.
    this%QDMC%restart = .false.
  else
    startBlock = 1
    step_count = 0
  endif


  do iblock=startBlock, this%QDMC%BlocksToEquil

    this%QDMC%lowe_count = 0

    do istep=1,this%QDMC%StepsPerBlock
      step_count = step_count+1

      ! Walk the walkers.
      call this%Walkers%walk(sys,this%QDMC,this%frags)

      ! Calculate the energy of the walkers, and vbar.
      v_bar = this%Walkers%calcEnergy(PES,step_count,nKilled,nLowE)
      v_block(iblock) = v_block(iblock) + v_bar

      ! Calculate a trial energy.
      ratio = real((this%QDMC%NumWalkers-nKilled),8)/real(this%QDMC%NormWalkers,8) - 1.0d0
      this%QDMC%Etrial = v_bar - ratio * this%QDMC%feedback
      eref_block(iblock) = eref_block(iblock) + this%QDMC%Etrial

      ! Branch walkers.
      call this%Walkers%branch(this%QDMC)

      if (this%printDMCsteps) then
        print *, istep, 'Vbar:', v_bar*kJ, 'Eref:', this%QDMC%Etrial*kJ, '# walkers:', this%QDMC%NumWalkers
      endif

    enddo

    v_block(iblock) = v_block(iblock)/real(this%QDMC%StepsPerBlock,8)
    eref_block(iblock) = eref_block(iblock)/real(this%QDMC%StepsPerBlock,8)
    nKill_avg = real(this%QDMC%lowe_count,8)/real(this%QDMC%StepsPerBlock,8)
    nLowE_avg = real(nLowE,8)/real(this%QDMC%StepsPerBlock,8)

    ! Print block details.
    print 19, iblock, v_block(iblock)*kJ, eref_block(iblock)*kJ, this%QDMC%NumWalkers, nKill_avg, nLowE_avg
19      format(1x,i4,2(5x,f10.6),5x,i5,2x,3(3x,f10.2),2x,f7.4)


        if (this%saveProg) then
      call this%dump(iblock, v_block, eref_block, real(0,8), real(0,8), real(0,8), real(0,8))

      ! Check wall time:
      if (this%trackTime.and.(this%wallTime().ge.this%cutTime)) then
          print *, 'Simulation approaching wall time limit!'
          print *, 'Submitting restart job...'
          call execute_command_line('echo restarting > ./.JobStatus')
          call exit(0)
      endif
    endif


  enddo ! End the loop over blocks.

  print *, '------ equilibration stage complete --------'


  !........!


  this%equilibrated = .true.

  ! Save data again, for good luck.
  if (this%saveProg.and.(step_count.gt.0)) call this%dump(0, v_block, eref_block, real(0,8), real(0,8), real(0,8), real(0,8))
  ! Will segfault if saveProg is called when step_count = 0, as there's nothing to save.

  !call exit(0)


end subroutine Equilibrate

!=======================================================================
! The main part of the simulation, where we actually collect data.
! Almost the same procedure as equilibration, with descendant weighting
! thrown in.
!=======================================================================
subroutine sample(this,sys,PES)
  class(tDMCSim) :: this
  type(molsysdat), intent(in) :: sys
  class(tPES), intent(in) :: PES

  ! Simulation blocks, steps, etc.
  integer :: startBlock, end_Block, iblock, istep, step_count
  integer :: nLowE, nKilled, i
  real(kind=8) :: LowE_avg, nKill_avg

  ! Energies
  real(kind=8) :: v_bar, ratio
  real(kind=8) :: vbar_avg, vbar_avg_sq, eref_avg, eref_avg_sq
  real(kind=8) :: var_vbar, var_eref
  real(kind=8), dimension(:), allocatable :: v_block, eref_block
  real(kind=8), dimension(:), allocatable :: energy_at_each_step

  ! Bond lengths
  real(kind=8), dimension(1:sys%nbond) :: r_curr, r_block, r_avg, r_avg_sq
  real(kind=8) :: var_r

  end_Block = this%QDMC%NumBlocks - this%QDMC%BlocksToEquil

  allocate(v_block(1:end_Block))
  v_block = 0.0d0
  allocate(eref_block(1:end_Block))
  eref_block = 0.0d0
  vbar_avg = 0.0d0
  vbar_avg_sq = 0.0d0
  eref_avg = 0.0d0
  eref_avg_sq = 0.0d0

  r_curr = 0.0d0
  r_block = 0.0d0
  r_avg = 0.0d0
  r_avg_sq = 0.0d0

  allocate(energy_at_each_step(1:this%QDMC%StepsPerBlock*end_Block))


  if (.not.this%equilibrated) then
    stop "Shouldn't be here if we're not equilibrated."
  endif

  if (this%restarting) then
    startBlock = this%Progress%Block+1
    step_count = this%Progress%Block * this%QDMC%StepsPerBlock
    ! Copy arrays.
    v_block(1:this%Progress%Block) = this%Progress%V_block(:)
    eref_block(1:this%Progress%Block) = this%Progress%Eref_block(:)

    if (allocated(this%Progress%vbar_steps)) then
      energy_at_each_step(1:step_count) = this%Progress%vbar_steps(1:step_count)
    endif

    vbar_avg = this%Progress%Vbar_avg
    vbar_avg_sq = this%Progress%Vbar_avg_sq
    eref_avg = this%Progress%Eref_avg
    eref_avg_sq = this%Progress%Eref_avg_sq



    ! Switch off restart flags.
    this%restarting = .false.
    this%QDMC%restart = .false.
  else
    startBlock = 1
    step_count = 0
  endif

  ! Create TOUT files if needed.
  if (this%writeTOUT) then
    call PES%setupTOUT()
    call PES%writeTOUT(.false.)
  endif

  do iblock=startBlock, end_Block
    v_block(iblock) = 0.0d0
    eref_block(iblock) = 0.0d0
    r_block = 0.0d0

    this%QDMC%lowe_count = 0
    do istep=1, this%QDMC%StepsPerBlock
      step_count = step_count+1


      ! Walk the walkers.
      call this%Walkers%walk(sys,this%QDMC,this%frags)

      ! See if we need to write to TOUT...
      if ((istep == this%QDMC%StepsPerBlock).and.this%writeTOUT) call PES%writeTOUT(.true.)

      ! Write walkers to XYZ, if wanted.
      if (this%writeXYZ) call this%printWalkers(sys,step_count)

      ! Calculate the energy of the walkers, and vbar.
      v_bar = this%Walkers%calcEnergy(PES,step_count,nKilled,nLowE)
      v_block(iblock) = v_block(iblock) + v_bar
      energy_at_each_step(step_count) = v_bar

      ! Calculate a trial energy.
      ratio = real((this%QDMC%NumWalkers-nKilled),8)/real(this%QDMC%NormWalkers,8) - 1.0d0
      this%QDMC%Etrial = v_bar - ratio * this%QDMC%feedback
      eref_block(iblock) = eref_block(iblock) + this%QDMC%Etrial

      ! See if we need to stop writing to TOUT...
      if ((istep == this%QDMC%StepsPerBlock).and.this%writeTOUT) call PES%writeTOUT(.false.)

      ! Note: if we wanted every damned walker in TOUT, we could just call PES%writeTOUT(.true) at the start of
      ! the simulation and leave it. I recommend not doing this - just don't.


      ! Branch walkers.
      call this%Walkers%branch(this%QDMC)


      ! Descend and weight.
      call this%Generations%update(this%Walkers,this%QDMC%NumWalkers,step_count)

      ! collect averages of bondlengths over wavefunction, ie, "density"
      r_curr(:) = this%Walkers%avgR(sys)
      r_block(:) = r_block(:) + r_curr(:)


      if (this%printDMCsteps) then
        print *, istep, 'Vbar:', v_bar*kJ, 'Eref:', this%QDMC%Etrial*kJ, '# walkers:', this%QDMC%NumWalkers
      endif




    enddo ! End the loop over QDMC steps

    ! block energy and terms for naive error estimates
    v_block(iblock) = v_block(iblock)/real(this%QDMC%StepsPerBlock,8)
    eref_block(iblock) = eref_block(iblock)/real(this%QDMC%StepsPerBlock,8)
    nKill_avg = real(this%QDMC%lowe_count,8)/real(this%QDMC%StepsPerBlock,8)
    LowE_avg = real(nLowE,8)/real(this%QDMC%StepsPerBlock,8)

    vbar_avg  = vbar_avg  + v_block(iblock)
    vbar_avg_sq  = vbar_avg_sq + v_block(iblock)**2
    eref_avg    = eref_avg   + eref_block(iblock)
    eref_avg_sq  = eref_avg_sq + eref_block(iblock)**2
    r_block = r_block/real(this%QDMC%StepsPerBlock,8)
    r_avg = r_avg + r_block
    r_avg_sq = r_avg_sq + r_block**2

    ! Print block details.
    print 19, iblock, v_block(iblock)*kJ, eref_block(iblock)*kJ, this%QDMC%NumWalkers, nKill_avg, LowE_avg
19      format(1x,i4,2(5x,f10.6),5x,i5,2x,3(3x,f10.2),2x,f7.4)

    if (this%saveProg) then

      call this%dump(iblock, v_block, eref_block, vbar_avg, vbar_avg_sq, eref_avg, eref_avg_sq,energy_at_each_step(1:step_count))

      ! Check wall time:
      if (this%trackTime.and.(this%wallTime().ge.this%cutTime)) then
          print *, 'Simulation approaching wall time limit!'
          print *, 'Submitting restart job...'
          call execute_command_line('echo restarting > ./.JobStatus')
          call exit(0)
      endif
    endif


  enddo ! End the loop over blocks

  ! Calculate and print final results.
  vbar_avg = vbar_avg/real(end_Block,8)
  vbar_avg_sq = vbar_avg_sq/real(end_Block,8)
  eref_avg = eref_avg/real(end_Block,8)
  eref_avg_sq = eref_avg_sq/real(end_Block,8)

  var_vbar = 2.0 * sqrt(abs(vbar_avg**2 - vbar_avg_sq)/real(end_Block-1,8))
  var_eref = 2.0*sqrt(abs(eref_avg**2 - eref_avg_sq)/real(end_Block-1,8))

  print *, ''
  print *, '#  ================= Grand Averages =======================  #'
  print *, '#   error quoted as twice std dev of block averages          #'
  !  print *, '#   Vbar Average                                             #'
  print 71,  vbar_avg, var_vbar
  print 72,  vbar_avg*kJ, var_vbar*kJ
  !  print *, '#   Eref average                                             #'
  print 73,  eref_avg, var_eref
  print 74,  eref_avg*kJ, var_eref*kJ
  print *, ''

71 format(' # Vbar ZPE (a.u.)   ',4x,f12.6,1x,'+/-',1x,f9.6)
72 format(' # Vbar ZPE (kJ/mol) ',4x,f16.6,1x,'+/-',1x,f13.6)
73 format(' # Eref ZPE (a.u.)    ',4x,f12.6,1x,'+/-',1x,f9.6)
74 format(' # Eref ZPE (kJ/mol) ',4x,f16.6,1x,'+/-',1x,f13.6)
75  format(' #  ',i4,2(4x,f9.4,3x,f7.4))

  r_avg = r_avg/real(end_Block,8)
  r_avg_sq = r_avg_sq/real(end_Block,8)


  print *, ''
  print *, '#  ================= Averages of bond lengths (bohr) ================  #'
  print *, '#  bond       <R> +/- 2sigma     '

  do i=1, sys%nbond
    var_r = 2.0*sqrt(abs(r_avg(i)**2 - r_avg_sq(i))/real(end_Block-1,8))
    print 75, i, r_avg(i), var_r
  enddo

  ! Use blocking algorithm to quantify errors in energy.
  print *, ''
  print *, '#  ================= Blocking algorithm (a.u.)  ================  #'

  ! bond stuff here
  call blocking(energy_at_each_step)


  ! Calculate descendant weighting results.
  if (this%QDMC%NumGenerations.ne.0) then
    print *, ''
    print *, '#  ================= Descendant Weighting results  ================  #'
    call DescendantWeighting(sys,this%QDMC,this%Generations%Generation,&
                            this%Generations%nMultiStep,this%Generations%printXYZ)
  endif

  call execute_command_line('echo finished > ./.JobStatus')






end subroutine sample





!=======================================================================
! Prints the starting headline for the simulation. I have preserved it 
! in my renovation, as it's heritage listed at this stage.
!=======================================================================
subroutine printHeadline(QDMC)
  type(qdmc_par), intent(in) :: QDMC

   ! Let's only print the headline if we're starting from scratch...
   if (.not.qdmc%restart) then
     print *, '==============================================================================='
     print *, '  - RunDMC - Interpolated Quantum Diffusion Monte Carlo with Beats by Dre -'
     print *, '==============================================================================='
     print *, ' TimeStep           ',QDMC%tau
     print *, ' DampingFactor      ',QDMC%feedback
     print *, ' NumBlocks          ',QDMC%NumBlocks
     print *, ' BlocksToEquil      ',QDMC%BlocksToEquil
     print *, ' StepsPerBlock      ',QDMC%StepsPerBlock
     print *, ' MinWalkers         ',QDMC%MinWalkers
     print *, ' MaxWalkers         ',QDMC%MaxWalkers
     print *, ' WalkersPerGeom     ',QDMC%WalkersPerGeom
     print *, ' NumGeoms           ',QDMC%NumGeoms
     print *, ' NormWalkers        ',QDMC%NormWalkers
     print *, ' NumGenerations     ',QDMC%NumGenerations
     print *, ' DelayBetweenGens   ',QDMC%DelayBetweenGens
     print *, ' NumDescendantSteps ',QDMC%NumDescendantSteps
     print *, '------------------------------------------------------------------------------'
     print *, ' block  energy_block    energy_ref   NumWalkers  lowE killed/step   lowE/step '
     print *, '------------------------------------------------------------------------------'
   endif


end subroutine printHeadline

!=======================================================================
! Dumps the simulation progress to a file.
!=======================================================================
subroutine dumpSimulation(this,Block,V_block,Eref_block,Vbar_avg,Vbar_avg_sq,Eref_avg,Eref_avg_sq,vbar_steps)
  class (tDMCSim) :: this

  integer, intent(in) :: Block
  real(kind=8), intent(in), dimension(:) :: V_block,Eref_block
  real(kind=8), intent(in) :: Vbar_avg,Vbar_avg_sq,Eref_avg,Eref_avg_sq
  real(kind=8), intent(in), dimension(:), optional :: vbar_steps
  integer :: i

  integer :: unit

  logical :: fileExists

  ! Back up old progress file, if it exists.
  inquire(file='Progress.OUT',exist=fileExists)
  if (fileExists) then
    !call execute_command_line('cp Progress.OUT Progress.Backup')
  endif

  open(newunit=unit,file='Progress.OUT',status='replace',access='sequential')

  ! Write in this order: whether we're equilibrating, the block, step, single averages
  ! and averages in arrays.
  write(unit,*) this%equilibrated, Block
  write(unit,*) Vbar_avg, Vbar_avg_sq, Eref_avg, Eref_avg_sq

  write(unit,*) V_block(1:Block)
  write(unit,*) Eref_block(1:Block)
  close(unit)

  ! Now save the walkers.
  call this%Walkers%dump()

  ! And the descendant weighting generations.
  call this%Generations%dump()

  ! Write the energy at each step, if applicable
  if (PRESENT(vbar_steps)) then
    inquire(file='Vbar.OUT',exist=fileExists)
    !if (fileExists) call execute_command_line('cp Vbar.OUT Vbar.Backup')
    open(newunit=unit,file='Vbar.OUT',status='replace',access='sequential')

    write(unit,*) size(vbar_steps)
    write(unit,*) vbar_steps

    close(unit)
  endif



end subroutine dumpSimulation

!=======================================================================
! Loads the simulation progress.
!=======================================================================
subroutine loadSimulation(this)
  class (tDMCSim) :: this

  integer :: unit, n
  logical :: fileExists

  ! Check that file exists first!
  inquire(file='Progress.OUT',exist=fileExists)
  if (.not.fileExists) then
    print *, 'Progress.OUT file does not exist!'
    print *, 'Starting new simulation.'
    this%restarting = .false.
    this%QDMC%restart = .false.
    return
  endif

  open(newunit=unit,file='Progress.OUT',status='old')

  read(unit,*) this%equilibrated, this%Progress%Block
  read(unit,*) this%Progress%Vbar_avg, this%Progress%Vbar_avg_sq, this%Progress%Eref_avg, this%Progress%Eref_avg_sq

  ! Allocate energy arrays.
  allocate(this%Progress%V_block(1:this%Progress%Block))
  allocate(this%Progress%Eref_block(1:this%Progress%Block))

  read(unit,*) this%Progress%V_block(1:this%Progress%Block)
  read(unit,*) this%Progress%Eref_block(1:this%Progress%Block)

  ! Now load the walkers, at the same time storing the number of walkers in the QDMC object.
  this%QDMC%NumWalkers = this%Walkers%load()

  close(unit)

  ! And vbar at each step - only if equilibrated AND file exists.
  inquire(file='Vbar.OUT',exist=fileExists)
  if (this%equilibrated.and.fileExists) then
    open(newunit=unit,file='Vbar.OUT',status='old')
    read(unit,*) n

    allocate(this%Progress%vbar_steps(1:n))
    read(unit,*) this%Progress%vbar_steps
    close(unit)

  endif

  ! And the descendant weighting... only if equlibrated.
  if (this%equilibrated) call this%Generations%load()


end subroutine loadSimulation



!=======================================================================
! Loads descendant weighting data only, bins new histograms.
!=======================================================================
subroutine DWOnly(this,sys,numGens)
  class (tDMCSim) :: this
  type(molsysdat), intent(in) :: sys
  integer, intent(in), optional :: numGens
  integer :: unit1, n
  logical :: fileExists

  ! Check that files exist first.
  inquire(file='DWProg.OUT', exist=fileExists)
  if (.not.fileExists) stop 'DWProg.OUT not found!'
  inquire(file='DW.OUT', exist=fileExists)
  if (.not.fileExists) stop 'DW.OUT not found!'

  ! Read input data for QDMC.
  call this%QDMC%readInput(sys)

  ! Manually override numGens if we need to
  if (PRESENT(numGens)) this%QDMC%NumGenerations = numGens

  ! Check for badness regarding multi-step tracking
  if (this%Generations%multiStep.and.this%Generations%trackingSteps.gt.0) then
    if (this%Generations%trackingSteps.gt.this%QDMC%NumDescendantSteps) then
      stop 'Multi-step descendant tracking steps > QDMC%NumDescendantSteps!'
    endif

    if (MODULO(DLOG(real(this%Generations%trackingSteps,8))/DLOG(2.0d0),1.0d0).ne.0.0d0) then
      stop 'Need a descendant weighting tracking time, x, such that log2(x) is an integer.'
    endif

    n = int(LOG(real(this%Generations%trackingSteps))/LOG(2.0))

  else
    n = 1
  endif

  ! Set up the descendant weighting object.
  call this%Generations%setup(this%QDMC%NumGenerations,0,this%QDMC%NumDescendantSteps,sys%natom)

  ! Now load the generations
  call this%Generations%load()

  ! Load histogram details
  call SetupDW()



  print *, ''
  print *, '#  ================= Descendant Weighting results  ================  #'
  call DescendantWeighting(sys,this%QDMC,this%Generations%Generation,&
                            n,this%Generations%printXYZ)



end subroutine


!===================================================================================
! Writes all the current walkers to an xyz file. Why? Might be useful for something.
!===================================================================================
subroutine printWalkers(this,sys,step)
  class (tDMCSim) :: this
  type(molsysdat), intent(in) :: sys
  integer, intent(in) :: step
  type(tWalkerBox), dimension(1:this%QDMC%NumWalkers) :: Walkers
  integer :: i, j, k, unit, nAtoms

  ! Crack open the walkers.
  Walkers = this%Walkers%getWalkers()

  open(newunit=unit,file='Walkers.xyz',status='unknown',access='append')
  nAtoms = this%QDMC%NumWalkers*sys%natom

  write(unit,*) nAtoms
  write(unit,*) 'step', step

  ! Loop over walkers, write to Walkers.xyz.
  do i=1,this%QDMC%NumWalkers
    do j=1, sys%natom
      write(unit,*) sys%atom_label(j), (Walkers(i)%w%x(j,:)*au2ang)
    enddo
  enddo


  close(unit)



end subroutine printWalkers






!=======================================================================
! Calculates and returns the time the simulation has been running.
!=======================================================================
function calcWallTime(this) result(wallTime)
  class(tDMCSim) :: this

  integer(kind=4) :: today(3), now(3)
  real(kind=4) :: wallTime

  integer, dimension(1:12) :: months
  integer :: cYr, cMon, cDay, s1, s2, m1, m2, m3
  integer :: intMonths, i, j
  integer :: wall_sec



  months = (/ 31,28,31,30,31,30,31,31,30,31,30,31 /)
  ! Code not valid in leap years.

  ! Get the current time.
  call idate(today) ! today(1)=day, (2)=month, (3)=year
  call itime(now) ! now(1)=hour, (2)=minute, (3)=second

  cDay = today(1)
  cMon = today(2)
  cYr = today(3)

  ! Simple bit first:
  if ((this%sDay == cDay).and.(this%sMon == cMOn).and.(this%sYr == cYr)) then
    ! Return wall time in hours.
    s1 = this%sHour*3600 + this%sMin*60 + this%sSec
    s2 = now(1)*3600 + 60*now(2) + now(3)
    wallTime = real((s2-s1)/3600.0,4)
    return
  endif

  ! Time left to end of day that process started:
  s1 = (24-this%sHour)*3600-60*this%sMin-this%sSec
  ! Time since 0:00
  s2 = now(1)*3600 + 60*now(2) + now(3)

  ! If only day has rolled over:
  if ((this%sMon == cMOn).and.(this%sYr == cYr)) then
    wall_sec = s1 + (cDay-this%sDay-1)*86400 + s2
    wallTime = real(wall_sec/3600.0,4)
    return
  endif

  ! Calculate time left in start month
  m1 = (months(this%sMon)-this%sDay)*86400+s1

  ! Calculate time from beginning of this month, to now:
  m3 = (cDay-1)*86400+s2


  ! If month has rolled over:
  if (this%sYr == cYr) then

    ! Calculate time in intervening months, if any:
    m2 = 0
    do i=this%sMon+1,cMon-1
      m2 = m2 + months(i)*86400
    enddo


  else
    ! Finally, if year has rolled over...

    ! Calculate time from start to end of the year...
    m2 = 0
    do i=this%sMon+1,12
      m2 = m2 + months(i)*86400
    enddo

    ! And the time to the current month...
    do i=1,cMon-1
      m2 = m2 + months(i)*86400
    enddo

    ! And any intervening years...
    do i=1, cYr-this%sYr-1
      do j=1,12
        m2 = m2 + months(i)*86400
      enddo
    enddo
  endif

  wall_sec = m1+m2+m3
  wallTime = real(wall_sec/3600.0,4)

end function calcWallTime



end module DMC_mod
