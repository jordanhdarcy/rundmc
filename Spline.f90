module CubicSplines
    implicit none
    ! Contains subroutines to create and evaluate cubic splines.

    ! A Spline type, containing the tabulated function f(x), and the array of second derivatives f"(x).
    type tSpline
        integer :: nPoints
        real(kind=8), dimension(:), allocatable :: x, fx, f2x
        real(kind=8) :: mHi, bHi, mLo, bLo ! The gradients and constants of the straight lines at
        ! the ends of the spline - used for first order interpolation beyond the edges of the tabulated function.
        real(kind=8) :: interval

    contains

    procedure, pass(spline) :: load => setupSpline
    procedure, pass(spline) :: Splint


    end type tSpline

    contains

    ! insert subroutines here

    ! Reads the tabulated function from the given file, and sets up a new spline object.
    ! Will assume a natural spline for the moment, and set the second derivatives at the boundaries to 0.
    subroutine setupSpline(spline,fileName)
        character(*), intent(in) :: fileName
        character*30 :: comment
        integer :: i, error
        class(tSpline) :: spline

        ! these correspond to the cubic spline routine in Numerical Recipes (1992), p.109.
        real(kind=8) :: p, qn, sig, un, val1, val2, val3, val4, val5
        real(kind=8), allocatable, dimension(:) :: u

        integer :: IOStatus

        write(11,*) '==== Loading spline from ', fileName, ' ===='

        ! Open the POT file for reading
        open(unit=201,file=fileName,status='old')

        ! Read through the file once to count number of points.
        spline%nPoints = 0
        ! First line is a comment line.
        read(201,*,IOSTAT=IOStatus) comment
        do
            read(201,*,IOSTAT=IOStatus) comment
            ! Check IOStatus, break loop if end of file is reached.
            if (IOStatus.ne.0) exit
            ! Otherwise increment counter
            spline%nPoints = spline%nPoints+1
        enddo

        write(11,*) 'Function has ', spline%nPoints, ' tabulated values.'

        ! rewind, allocate, etc.
        allocate(spline%x(1:spline%nPoints),stat=error)
        allocate(spline%fx(1:spline%nPoints))
        allocate(spline%f2x(1:spline%nPoints))
        allocate(u(1:spline%nPoints))
        rewind(unit=201)

        ! First line is a comment line.
        read(201,*,IOSTAT=IOStatus) comment
        do i=1,spline%nPoints
            read(201,*,IOSTAT=IOStatus) spline%x(i), spline%fx(i)
        enddo

        write(11,*) 'Min x-value:', spline%x(1)
        write(11,*) 'Max x-value: ', spline%x(spline%nPoints)
        if (spline%x(1).gt.spline%x(spline%nPoints)) then
      print *, 'Minimum x value of spline PES is greater than maximum x value, you doofus.'
      stop
    endif


        ! Now form the spline:
        write(11,*) 'Reticulating spline...'

        ! Pinched from p. 109 of Numerical Recipes Vol 2, 1992:

        ! Use a natural spline - set f"(x) at the boundaries to 0.
        spline%f2x(1) = 0.0d0
        spline%f2x(spline%nPoints) = 0.0d0

        ! Decompositional loop of the tridiagonal algorithm. f2x and u used for temporary storage.
        do i=2,spline%nPoints-1
            sig = (spline%x(i)-spline%x(i-1))/(spline%x(i+1)-spline%x(i-1))
            p = sig*spline%f2x(i-1)+2.0d0
            spline%f2x(i) = (sig-1.0d0)/p

            ! To make the next part a bit nicer to write...
            val1 = spline%fx(i+1)-spline%fx(i)
            val2 = spline%x(i+1)-spline%x(i)
            val3 = spline%fx(i)-spline%fx(i-1)
            val4 = spline%x(i)-spline%x(i-1)
            val5 = spline%x(i+1)-spline%x(i-1)
            u(i) = (6.0d0 * (val1/val2 - val3/val4)/val5 - sig*u(i-1))/p
        enddo

        ! The backsubstitution loop of the tridiagonal algorithm
        do i=spline%nPoints-1, 1, -1
            spline%f2x(i) = spline%f2x(i)*spline%f2x(i+1) + u(i)
        enddo

        ! spline interval size
        spline%interval = spline%x(2) - spline%x(1)

        ! Calculate gradients and constants for lines at the edges of the spline.
        ! Use the first/last two points of the spline, perform first order extrapolation.
        ! If y1 = mx1 + b and y2 = mx2 + b, then m = (y1-y2)/(x1-x2) and b = y1-mx1.

        ! Do low, then high.
        val1 = spline%fx(1) - spline%fx(2)
        val2 = spline%x(1) - spline%x(2)
        spline%mLo = val1/val2
        spline%bLo = spline%fx(1) - spline%mLo*spline%x(1)


        val1 = spline%fx(spline%nPoints) - spline%fx(spline%nPoints-1)
        val2 = spline%x(spline%nPoints) - spline%x(spline%nPoints-1)
        spline%mHi = val1/val2
        spline%bHi = spline%fx(spline%nPoints) - spline%mHi*spline%x(spline%nPoints)

        ! Test spline here.
        !p = 0.0d0
        !do i=1, spline%nPoints
        !    val1 = Splint(spline%x(i)-0.001d0,spline)
        !    print *, spline%x(i), val1
        !enddo
        !do while (p.le.15.0d0)
        !    val1 = spline%Splint(p)
        !    print *, p, val1
        !    p = p+0.00005d0
        !enddo
        !stop



        write (11,*) 'Spline reticulated.'

    end subroutine

    ! Calculates f(x) using a cubic spline interpolation. All required values are passed in via the spline object.
    ! Spline evaluation taken from Numerical Recipes Vol 2 (1992), p. 110.
    real(kind=8) function Splint(spline,x)
        class(tSpline) :: spline
        real(kind=8), intent(in) :: x
        real(kind=8) :: A, B, C, D, h, dist
        integer :: ind ! index of nearest tabulated x value below the x value to interpolate.

        ! First, check if value to interpolate lies outside the bounds of the function.
        if (x.le.spline%x(1)) then
            ! First-order polynomial time.
            Splint = spline%mLo * x + spline%bLo

        elseif (x.ge.spline%x(spline%nPoints)) then
            ! First-order polynomial time.
            Splint = spline%mHi * x + spline%bHi

        else
            ! Cubic spline time.
            ! In this case, we don't need to search the table to find the bracketing values - we can be smarter.
            ! Calculate the distance of the given x value from the lowest x value.
            dist = x - spline%x(1)
            ! This distance is n intervals of x(i+1) - x(i) from the lowest x value, plus some remainder.
            ! The number of intervals is dist/interval, while the index of the array is one greater than that.
            ind = 1 + INT(dist/spline%interval)



            ! Now do the spline interpolation.
            h = spline%x(ind+1) - spline%x(ind)
            A = (spline%x(ind+1) - x)/h
            B = 1.0d0 - A
            C = 1.0d0/6.0d0*(A**3 - A)*h**2
            D = 1.0d0/6.0d0*(B**3 - B)*h**2

            ! Calculate Equation 3.3.3
            Splint = A*spline%fx(ind) + B*spline%fx(ind+1) + C*spline%f2x(ind) + D*spline%f2x(ind+1)
        endif

    end function Splint
    
end module CubicSplines
