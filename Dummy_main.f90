program RunDMC

  use molecule_specs, ONLY : molsysdat
  use interpolation!, ONLY : interp_params, read_interp, pot_file, neighbour_list, neighbour
  use fragment_mod, ONLY : frag_par, read_fragment_data

  implicit none

  type (molsysdat) :: system
  type (interp_params) :: interp
  type (frag_par), dimension(:), allocatable :: frag
  type (pot_file) :: POT, tempPOT1, tempPOT2
  type (neighbour_list), allocatable, dimension(:) :: neighbours
  type (neighbour_list) :: neighbourTest
  type (neighbour), pointer :: currNeigh
  type (pot_data_point), pointer :: dp
  type(datapoint_container), dimension(:), allocatable :: points


  integer :: i, j, k, count
  integer, dimension(1:500) :: fixedArray
  integer, dimension(:), allocatable :: allocArray
  real(kind=8) :: test

  !print *, 'Loading system'
  call system%readData('IN_SYSTEM')
  
  !print *, 'Loading IN_INTERP'
  call read_interp(interp,'IN_INTERP')

  !print *, 'Loading fragments'
  call read_fragment_data(system,frag,'IN_FRAG')

  !print *, 'Sorting out rigid bond lengths'
  call DetermineRigidBonds(system,frag)

  !print *, 'Bond permutations'
  call system%readPerms('IN_ATOMPERMS')

  !print *, 'Loading POT'
  call POT%loadPOT(system,interp,'POT')

  print *, 'Test inner neighbour listing'
  allocate(neighbours(1:10000))
  !allocate(points(1:interp%ndata))
  !allocate(tempPOT1%points(1:interp%ndata))
  !allocate(tempPOT2%points(1:interp%ndata))
  !allocate(allocArray(1:500))
  do i=1,10000
    call neighbours(i)%new(interp%ndata)
  enddo
  !call neighbourTest%destroy()
  !call neighbourTest%destroy()
  !stop
  do k=1,34000
  if (MODULO(k,100).eq.0) print *, 'block', k/100
  do j=1,10000
    !call destroyNeighbourList(neighbours(j))
    !call neighbours(j)%new()

    do i=1,interp%ndata

      dp => POT%points(i)%dp ! adds nothing
      neighbours(j)%n = i
      !neighbours(j)%points(i)%dp => dp
      call TestAssign(k,j)
      !dp%z
      !fixedArray(i) = i
      !allocArray(i) = i




      !POT%points(i)%dp => dp ! overhead

      !dp => tempPOT1%points(i)%dp
      !tempPOT%points(i)%dp => dp
      !tempPOT1%points(i)%dp => tempPOT2%points(i)%dp
      !points(i)%dp => dp
      !points(i)%dp => dp
      !call neighbours(j)%add(POT%points(i)%dp)
      !neighbours(j)%points(i)%dp => POT%points(i)%dp

    enddo


  enddo
  enddo


end program





!=============================================================================================
! Organise rigid fragments within the given system. Need to work out which of the bond lengths
! are rigid and will remain the same in the simulation, and which will need to be recalculated.
!=============================================================================================
subroutine DetermineRigidBonds(system,frag)
  use molecule_specs, ONLY : molsysdat
  use fragment_mod, ONLY : frag_par


  type(molsysdat), intent(inout) :: system
  type(frag_par), dimension(0:system%nfrag), intent(in) :: frag
  integer :: i, j, k, l, nNonRigid, ierr


  ! Calculate total number of rigid bonds - will be sum of intrafragment bonds.
  system%nRigidBonds = 0
  do i=1, system%nfrag
    system%nRigidBonds = system%nRigidBonds+frag(i)%natom*(frag(i)%natom-1)/2
  enddo
  nNonRigid = system%nbond - system%nRigidBonds
  system%nNRbonds = nNonRigid

  ! Now allocate array of non-rigid bonds.
  allocate(system%nonRigidBonds(1:nNonRigid),stat=ierr)
  if (ierr.ne.0) stop ' molsysdat allocation error: nonRigidBonds '

  ! Populate this array with atom indices. Loop through all atom-atom distances...
  nNonRigid = 0
  l=0
  do i=1, system%natom-1

    atomloop: do j=i+1, system%natom
      l = l+1
      ! Loop through rigid fragments. We'll see if the two atoms i,j are *within* a fragment.
      fragmentloop: do k=1, system%nfrag

        if ( (any(i.eq.frag(k)%indx(:))).and.(any(j.eq.frag(k)%indx(:))) ) then
          ! If they occur anywhere, then go to next atom combination.
          !print *, i, j, 'are in fragment', k
          !print *, 'move to next combo'
          cycle atomloop
        endif


      enddo fragmentloop

      ! If we're here, then this atom combination doesn't occur within any fragment.
      nNonRigid = nNonRigid+1
      system%nonRigidBonds(nNonRigid)%i = i
      system%nonRigidBonds(nNonRigid)%j = j
      system%nonRigidBonds(nNonRigid)%ind = l
    enddo atomloop
  enddo

  ! Now that we've done that, quickly zero all non-rigid bonds in system%bondlengths
  do i=1,system%nNRbonds
    j = system%nonRigidBonds(i)%ind
    system%bondLengths(j) = 0.0d0
  enddo

  ! Also square the array - we'll unsquare it later
  system%bondLengths = system%bondLengths**2

end subroutine DetermineRigidBonds

