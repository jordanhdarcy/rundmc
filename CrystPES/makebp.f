      subroutine makebp(pot,IN_ATOMPERMS,DoIP)

      use POTmodule
      use InterpTrajData
      implicit none
      type(POTobject)::pot
      character(len=*),intent(in)::IN_ATOMPERMS
      logical,intent(in)::DoIP

      character(80) :: icomm
      integer::idummy,n,i,j,k,ic

c  read in the atomic permutations into the nperm array

       open(unit=18, file=IN_ATOMPERMS)


cc read in a header

       read(18,80)icomm

80    format(a80)

c  read in a comment line

       read(18,80)icomm

c  read in the order of the group

       read(18,*)pot%ngroup
       call AllocatePOT2(pot)

c  read in the atomic perms

       do i=1,pot%ngroup

c  read in a comment line or separator
       read(18,80)icomm
       do k=1,pot%natom
       read(18,*)idummy,pot%nperm(i,k)
       enddo
c  idummy will be equal to k

       enddo

       close(unit=18)

       if (.not.DoIP) return

c  now set up the ip array of bond perms by looking at nperm

      do n=1,pot%ngroup

      ic=0
      do i=1,pot%natom-1
      do j=i+1,pot%natom
      ic=ic+1
      do k=1,pot%nbond
      if(pot%mb(k).eq.pot%nperm(n,i).and.pot%nb(k).eq.pot%nperm(n,j))pot%ip(k,n)=ic
      if(pot%nb(k).eq.pot%nperm(n,i).and.pot%mb(k).eq.pot%nperm(n,j))pot%ip(k,n)=ic
      enddo

      enddo
      enddo

        pot%nmrep(n,pot%nperm(n,:))=(/(k,k=1,pot%natom)/)
      enddo

      return
      end
