subroutine dvdir(mols,pot,j)

use InterpTrajData
use MPI_Data
implicit none
type(POTinstance)::mols
type(POTobject)::pot
integer,intent(in)::j

real(kind=8),dimension(pot%nbond,mols%nforc(j))::dvt,dtdr
real(kind=8),dimension(pot%natom,mols%nforc(j))::dvh,dtdh
real(kind=8),dimension(pot%natom,pot%nsu,mols%nforc(j))::dvsu,dtdsu
real(kind=8),dimension(pot%nint,mols%nforc(j))::z,dei
real(kind=8),dimension(pot%nbond)::sumdvt,rinv
real(kind=8),dimension(pot%natom)::sumdvh
real(kind=8),dimension(pot%natom,pot%nsu)::sumdvsu
real(kind=8),dimension(mols%nforc(j))::vt1,wt,ei,vk,vp1,vp2
real(kind=8)::temp,ect
integer,dimension(ranks)::np0,el0
integer::i,k,l,n,k1,nbh,j1
integer::m1,m2,e
logical::die

!  evaluate the energy and gradients

!  we will need to have some quantities for the gradients
!  of the weights
!  Note an approx here, due to neglect of small weights

if (NoForce) stop 'dvdir needs to have the force stuff switched off'

!  calc the raw wts and totsum
rinv=1/mols%r(1:pot%nbond,j)
nbh=pot%nbond
if (PeriodicDOF==2) nbh=nbh+pot%natom

if (.not.Fragmented.and.(ranks>1)) then
  np0=mols%nforc(j)/ranks
  np0(1:mols%nforc(j)-sum(np0))=np0(1)+1
  if (rank>0) then
    m1=sum(np0(1:rank))+1
  else
    m1=1
  endif
  m2=sum(np0(1:rank+1))
else
  m1=1
  m2=mols%nforc(j)
endif

vt1(m1:m2)=0
do i=1,pot%nbond
  do k=m1,m2
    temp=rinv(i)-pot%rda(mols%mda(k,j),pot%ip(i,mols%nda(k,j)))
    vt1(k)=vt1(k)+temp**2/pot%sumb(mols%mda(k,j),pot%ip(i,mols%nda(k,j)))
  enddo
enddo
if (TranslationalSym) then
  if (PeriodicDOF==2) then
    do i=1,pot%natom
      do k=m1,m2
        temp=mols%h(i,j)-pot%hda(mols%mda(k,j),pot%nmrep(mols%nda(k,j),i))
        temp=temp**2/pot%sumb(mols%mda(k,j),pot%nbond+pot%nmrep(mols%nda(k,j),i))
        vt1(k)= temp + vt1(k)
      enddo
    enddo
  endif
  do i=1,pot%natom
    do n=1,pot%nsu
      do k=m1,m2
        temp=mols%su(i,n,j) &
                -pot%suda(mols%mda(k,j),pot%nmrep(mols%nda(k,j),i), &
                                        pot%ips(n,mols%n2da(k,j))) &
                              *pot%isign(n,mols%n2da(k,j))
        temp=temp**2/pot%sumb(mols%mda(k,j), &
                        nbh+(pot%nmrep(mols%nda(k,j),i)-1)*pot%nsu &
                                        +pot%ips(n,mols%n2da(k,j)))
        vt1(k)= temp + vt1(k)
      enddo
    enddo
  enddo
endif

vp1(m1:m2)=vt1(m1:m2)**pot%ipow
vp2(m1:m2)=vt1(m1:m2)**pot%lowp
vk(m1:m2)=1/(vp1(m1:m2)+vp2(m1:m2))

if (.not.Fragmented.and.(ranks>1)) then
  el0(1)=0
  do i=1,ranks-1
    el0(i+1)=el0(i)+np0(i)
  enddo
  if (rank==0) then
    call MPI_Gatherv(MPI_IN_PLACE,0,MPI_DOUBLE_PRECISION, &
                vk,np0,el0,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
    mols%totsum(j)=sum(vk)
  else
    call MPI_Gatherv(vk(m1:m2),np0(rank+1),MPI_DOUBLE_PRECISION, &
                np0,np0,np0,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
  endif
  call MPI_Bcast(mols%totsum(j),1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
else
  mols%totsum(j)=sum(vk)
endif

wt(m1:m2)=vk(m1:m2)/mols%totsum(j)

!  calc the derivatives of the raw weights v

! dvt is actually the derivative of the primitive weight 
!       (w.r.t. the underlying r, h & su coords)
! divided by the sum of the primitive weights

do k=m1,m2
   ! dv'/dr
  do i=1,pot%nbond
    dvt(i,k)=-2*(1.d0/mols%r(i,j) &
           -pot%rda(mols%mda(k,j),pot%ip(i,mols%nda(k,j))))/mols%r(i,j)**2 &
                         /pot%sumb(mols%mda(k,j),pot%ip(i,mols%nda(k,j)))
  enddo
  if (TranslationalSym) then
    if (PeriodicDOF==2) then
      do i=1,pot%natom
         ! dv'/dh, h term
        dvh(i,k)=2*(mols%h(i,j) &
                        -pot%hda(mols%mda(k,j),pot%nmrep(mols%nda(k,j),i))) &
                /pot%sumb(mols%mda(k,j),pot%nbond+pot%nmrep(mols%nda(k,j),i))
        if (SurfaceCoordsIncludeHeight) then
           ! dv'/dh, h^2.s term
          dvh(i,k)=dvh(i,k)+4*sum((mols%su(i,:,j) &
                        -pot%suda(mols%mda(k,j),pot%nmrep(mols%nda(k,j),i), &
                                pot%ips(:,mols%n2da(k,j))) &
                     *pot%isign(:,mols%n2da(k,j)))*mols%su(i,:,j)/mols%h(i,j) &
                /pot%sumb(mols%mda(k,j), &
                        nbh+(pot%nmrep(mols%nda(k,j),i)-1)*pot%nsu &
                                        +pot%ips(:,mols%n2da(k,j))))
        endif
      enddo
    endif
    do i=1,pot%natom
!  This is the derivative w.r.t. the trig part of su only
      if (.not.SurfaceCoordsIncludeHeight) stop 'This looks like a bug.  Why h?'
      dvsu(i,:,k)=2*(mols%su(i,:,j) &
                     -pot%suda(mols%mda(k,j),pot%nmrep(mols%nda(k,j),i), &
                        pot%ips(:,mols%n2da(k,j)))*pot%isign(:,mols%n2da(k,j)))&
                                *mols%h(i,j)**2 &
                /pot%sumb(mols%mda(k,j), &
                        nbh+(pot%nmrep(mols%nda(k,j),i)-1)*pot%nsu &
                                        +pot%ips(:,mols%n2da(k,j)))
    enddo
  endif
enddo

 ! dv/dr = -1/v^2.(p.v'^(p-1)+q.v'^(q-1)).dv'/dr
dvt(:,m1:m2)=-dvt(:,m1:m2)*spread(wt(m1:m2)*vk(m1:m2) &
        *(pot%ipow*vp1(m1:m2)+pot%lowp*vp2(m1:m2))/vt1(m1:m2),1,pot%nbond)

if (.not.Fragmented.and.(ranks>1)) then
  if (rank==0) then
    call MPI_Gatherv(MPI_IN_PLACE,0,MPI_DOUBLE_PRECISION, &
                dvt,np0*pot%nbond,el0*pot%nbond,MPI_DOUBLE_PRECISION,&
                                0,MPI_COMM_WORLD,e)
  else
    call MPI_Gatherv(dvt(:,m1:m2),np0(rank+1)*pot%nbond,MPI_DOUBLE_PRECISION, &
                np0,np0,np0,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
  endif
endif

if (Fragmented.or.(rank==0)) sumdvt=sum(dvt,dim=2)
if (.not.Fragmented.and.(ranks>1)) &
        call MPI_Bcast(sumdvt,pot%nbond,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)

if (TranslationalSym) then
  if (PeriodicDOF==2) &
        dvh(:,m1:m2)=-dvh(:,m1:m2)*spread(wt(m1:m2)*vk(m1:m2) &
            *(pot%ipow*vp1(m1:m2)+pot%lowp*vp2(m1:m2))/vt1(m1:m2),1,pot%natom)
  dvsu(:,:,m1:m2)=-dvsu(:,:,m1:m2)*spread(spread(wt(m1:m2)*vk(m1:m2) &
        *(pot%ipow*vp1(m1:m2)+pot%lowp*vp2(m1:m2))/vt1(m1:m2),1,pot%natom), &
                                2,pot%nsu)

  if (.not.Fragmented.and.(ranks>1)) then
    if (rank==0) then
      call MPI_Gatherv(MPI_IN_PLACE,0,MPI_DOUBLE_PRECISION, &
                dvh,np0*pot%natom,el0*pot%natom,MPI_DOUBLE_PRECISION,&
                                0,MPI_COMM_WORLD,e)
      call MPI_Gatherv(MPI_IN_PLACE,0,MPI_DOUBLE_PRECISION, &
                dvsu,np0*pot%natom*pot%nsu,el0*pot%natom*pot%nsu, &
                        MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
    else
      call MPI_Gatherv(dvh(:,m1:m2),np0(rank+1)*pot%natom, &
                        MPI_DOUBLE_PRECISION, &
                np0,np0,np0,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
      call MPI_Gatherv(dvsu(:,:,m1:m2),np0(rank+1)*pot%natom*pot%nsu, &
                        MPI_DOUBLE_PRECISION, &
                np0,np0,np0,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
    endif
  endif

  if (Fragmented.or.(rank==0)) then
    if (PeriodicDOF==2) sumdvh=sum(dvh,dim=2)
    sumdvsu=sum(dvsu,dim=3)
  endif
  if (.not.Fragmented.and.(ranks>1)) then
    call MPI_Bcast(sumdvh,pot%natom,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
    call MPI_Bcast(sumdvsu,pot%natom*pot%nsu,MPI_DOUBLE_PRECISION,&
                                                        0,MPI_COMM_WORLD,e)
  endif
endif

!  calc the derivatives of the relative weights w

do k=m1,m2
  dvt(:,k)=dvt(:,k)-wt(k)*sumdvt
  if (PeriodicDOF==2) dvh(:,k)=dvh(:,k)-wt(k)*sumdvh
  dvsu(:,:,k)=dvsu(:,:,k)-wt(k)*sumdvsu
enddo

!  construct the normal local coords for each neighbour
!  recalling that intern calculates bond lengths NOT inverses

!  !ocl vi(k)
do k=m1,m2
  k1=mols%mda(k,j)
  do i=1,pot%nint
    z(i,k)=0.d0
    do l=1,pot%nbond
      z(i,k)=z(i,k)+pot%ut(k1,i,pot%ip(l,mols%nda(k,j)))/mols%r(l,j)
    enddo
    if (TranslationalSym) then
      if (PeriodicDOF==2) then
        do l=1,pot%natom
          z(i,k)=z(i,k) &
                +pot%ut(k1,i,pot%nbond+pot%nmrep(mols%nda(k,j),l))*mols%h(l,j)
        enddo
      endif
      do l=1,pot%natom
        do n=1,pot%nsu
          j1=pot%nsu*(pot%nmrep(mols%nda(k,j),l)-1)+pot%ips(n,mols%n2da(k,j))
          z(i,k)=z(i,k) &
                +pot%ut(k1,i,nbh+j1)*mols%su(l,n,j)*pot%isign(n,mols%n2da(k,j))
        enddo
      enddo
    endif
  enddo
enddo
do k=m1,m2
  z(:,k)=z(:,k)-pot%zda(mols%mda(k,j),:)
enddo

!  calculate the potential energy: scalar term 

ei(m1:m2)=pot%v0(mols%mda(m1:m2,j))

!  linear term in energy plus diagonal part of quadratic term
!  first term in derivative of taylor poly wrt local coords

do i=1,pot%nint
  do k=m1,m2
    ei(k) = z(i,k)*pot%v1(mols%mda(k,j),i) &
                   + 0.5d0*z(i,k)**2*pot%v2(mols%mda(k,j),i) + ei(k)
    dei(i,k) = z(i,k)*pot%v2(mols%mda(k,j),i) + pot%v1(mols%mda(k,j),i)
  enddo
enddo

!  the O(n^2) loop doesn't apply because we diagonalised d2v!!!

!  transform derivatives of taylor poly wrt normal local coords into 
!  derivs wrt bondlengths NOT inverses

do i=1,pot%nbond
  do k=m1,m2
    dtdr(i,k)=0.d0
    do l=1,pot%nint
      dtdr(i,k)=dei(l,k)*pot%ut(mols%mda(k,j),l,pot%ip(i,mols%nda(k,j))) &
                        + dtdr(i,k)
    enddo
  enddo
enddo
do k=m1,m2
  dtdr(:,k)=-dtdr(:,k)/mols%r(:,j)**2
enddo

if (TranslationalSym) then
  if (PeriodicDOF==2) then
    do i=1,pot%natom
      do k=m1,m2
        dtdh(i,k)=0.d0
        do l=1,pot%nint
          dtdh(i,k)=dtdh(i,k)+dei(l,k)*pot%ut(mols%mda(k,j),l, &
                                        pot%nbond+pot%nmrep(mols%nda(k,j),i))
          if (SurfaceCoordsIncludeHeight) &
                dtdh(i,k)=dtdh(i,k)+dei(l,k)*2*sum(pot%ut(mols%mda(k,j),l, &
                                nbh+(pot%nmrep(mols%nda(k,j),i)-1)*pot%nsu &
                                                +pot%ips(:,mols%n2da(k,j))) &
                                *pot%isign(:,mols%n2da(k,j)) &
                           *mols%su(i,:,j))/mols%h(i,j)
        enddo
      enddo
    enddo
  endif

  do i=1,pot%natom
    do n=1,pot%nsu
      do k=m1,m2
        dtdsu(i,n,k)=0.d0
        do l=1,pot%nint
          if (SurfaceCoordsIncludeHeight) then
            dtdsu(i,n,k)=dtdsu(i,n,k)+dei(l,k)*pot%ut(mols%mda(k,j),l, &
                                nbh+(pot%nmrep(mols%nda(k,j),i)-1)*pot%nsu &
                                                +pot%ips(n,mols%n2da(k,j))) &
                                *pot%isign(n,mols%n2da(k,j)) &
                           *mols%h(i,j)**2
          else
            dtdsu(i,n,k)=dtdsu(i,n,k)+dei(l,k)*pot%ut(mols%mda(k,j),l, &
                                nbh+(pot%nmrep(mols%nda(k,j),i)-1)*pot%nsu &
                                                +pot%ips(n,mols%n2da(k,j))) &
                                *pot%isign(n,mols%n2da(k,j))
          endif
        enddo
      enddo
    enddo
  enddo
endif

!  scale by relative weights, and their derivatives

mols%en(j)=sum(wt(m1:m2)*ei(m1:m2))

mols%dvdr(:,j)=0
do i=1,pot%nbond
  do k=m1,m2
    mols%dvdr(i,j)=ei(k)*dvt(i,k)+wt(k)*dtdr(i,k)+mols%dvdr(i,j)
  enddo
enddo

if (TranslationalSym) then
  if (PeriodicDOF==2) mols%dvdh(:,j)=matmul(dvh(:,m1:m2),ei(m1:m2))+matmul(dtdh(:,m1:m2),wt(m1:m2))
  mols%dvdsu(:,:,j)=0
  do i=1,pot%natom
    do n=1,pot%nsu
      do k=m1,m2
        mols%dvdsu(i,n,j)=ei(k)*dvsu(i,n,k)+wt(k)*dtdsu(i,n,k)+mols%dvdsu(i,n,j)
      enddo
    enddo
  enddo
endif

if (.not.Fragmented.and.(ranks>1)) then
  call MPI_Allreduce(MPI_IN_PLACE,mols%en(j),1,MPI_DOUBLE_PRECISION,&
                                                MPI_SUM,MPI_COMM_WORLD,e)
  if (rank==0) then
    call MPI_Reduce(MPI_IN_PLACE,mols%dvdr(:,j),pot%nbond,MPI_DOUBLE_PRECISION,&
                                                MPI_SUM,0,MPI_COMM_WORLD,e)
    if (TranslationalSym) then
      call MPI_Reduce(MPI_IN_PLACE,mols%dvdh(:,j),pot%natom, &
                        MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,e)
      call MPI_Reduce(MPI_IN_PLACE,mols%dvdsu(:,:,j),pot%natom*pot%nsu, &
                        MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,e)
    endif
  else
    call MPI_Reduce(mols%dvdr(:,j),np0,pot%nbond,MPI_DOUBLE_PRECISION,&
                                                MPI_SUM,0,MPI_COMM_WORLD,e)
    if (TranslationalSym) then
      call MPI_Reduce(mols%dvdh(:,j),np0,pot%natom, &
                        MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,e)
      call MPI_Reduce(mols%dvdsu(:,:,j),np0,pot%natom*pot%nsu, &
                        MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,e)
    endif
  endif
endif

mols%rms(j)=sum(wt(m1:m2)*(mols%en(j)-ei(m1:m2))**2)
if (.not.Fragmented.and.(ranks>1)) then
  if (rank==0) then
    call MPI_Reduce(MPI_IN_PLACE,mols%rms(j),1,MPI_DOUBLE_PRECISION,&
                                                MPI_SUM,0,MPI_COMM_WORLD,e)
  else
    call MPI_Reduce(mols%rms(j),np0,1,MPI_DOUBLE_PRECISION,&
                                                MPI_SUM,0,MPI_COMM_WORLD,e)
  endif
endif
mols%rms(j)=sqrt(mols%rms(j))

!  look out for loony geometries

if (nlowstop/=0) then
  ect=pot%vmin-0.0005d0
  die=(mols%en(j).lt.ect)
  if (TranslationalSym.and.(PeriodicDOF==2)) then
    die=die.or.(any(mols%h(:,j)>5))
  endif
  if (die) then
    if (nloop.gt.1) then
      do n=1,pot%natom
        write(8,*)mols%c(n,:,j)
      enddo
      write(8,'(3g24.15,i3)')mols%en(j),mols%rms(j),mols%totsum(j),3
    endif
    nfin(j)=3
    nstop=2
  endif
endif

end

