      subroutine readt(pot,IN_POT)

      use POTmodule
      use surface
      implicit none
      type(POTobject)::pot
      character(len=*),intent(in)::IN_POT

      character(80) :: comlin
      real(kind=8),dimension(pot%nredundant*(1+pot%nint)+CperUC*nc1*nc2)::dummy
      real(kind=8),dimension(pot%nredundant)::lr
      real(kind=8),dimension(nsurfc,nsurfc)::d2f
      real(kind=8), dimension(6) :: junk
      real(kind=8)::vv
      integer::i,j,k,badness,n

      open (unit=7,file=IN_POT)
 
      call lisym(11,'-')
      write(11,*) ' '
      write(11,*) ' *** subroutine readt:',
     .  ' read in potential energy surface parameters ***'
      write(11,*) ' '

! Because we're now using dynamic allocation, we need to 
! know how many datapoints
      k=0
      read(7,'(a20)')comlin
      do
        read(7,'(a20)',iostat=badness)comlin
        if (badness/=0) exit
!  Annoying, but index is case sensitive with most (all?) systems:
        if (index(comlin,'data')>0) then
          k=k+1
        elseif (index(comlin,'Data')>0) then
          k=k+1
        elseif (index(comlin,'DATA')>0) then
          k=k+1
        else
          cycle
        endif
!  we've just read a heading line
        if (JDHacks) then
          ! natom lines of fractional coordinates
          do i=1,pot%natom
            read(7,*) vv
          enddo
        endif
        read(7,*)dummy
        read(7,*)vv
        if (vv>pot%potmax) k=k-1
      enddo
      pot%ndata=k
      rewind(7)

      call AllocatePOT3(pot)

      if (nsurfa>0) then
        allocate(sad(CperUC,nc1,nc2,pot%ndata))
        allocate(dsad(CperUC,nc1,nc2,pot%ndata))
        allocate(d2sad(CperUC,nc1,nc2,CperUC,nc1,nc2,pot%ndata))
      endif

c   read comment/header

      read(7,80)comlin
   80 format(a80)
      write(11,81)comlin
   81 format(1x,a79)

c  read in the geometry (bond lengths, etc.),
c  the matrix defining the normal local coords, 
c  the potential energy, the gradients and diagonal force constants (wrt to 
c  the normal local coords) at all of the Ndata points.

      j=1
10    continue
       read(7,80,end=20)comlin
       if (pot%natom>0) then
         if (JDHacks) then
           ! Read fractional coordinates for data points
           do i=1,pot%natom
             read(7,*) pot%frac(j,i,:)
           enddo
         endif
         read(7,*)pot%rda(j,:)
         if (TranslationalSym) then
           if (PeriodicDOF==2) read(7,*)pot%hda(j,:)
           do i=1,pot%natom
             read(7,*)pot%suda(j,i,:)
           enddo
         endif
         do i=1,pot%nint
           read(7,*) (pot%ut(j,i,k),k=1,pot%nredundant)
         enddo
         read(7,*) pot%v0(j)
         read(7,*) (pot%v1(j,k),k=1,pot%nint)
         read(7,*) (pot%v2(j,k),k=1,pot%nint)
       else
         read(7,*)sad(:,:,:,j)
         read(7,*) pot%v0(j)
         read(7,*)dsad(:,:,:,j)
         do i=1,nsurfc
           read(7,*)d2f(i,1:i)
           d2f(1:i-1,i)=d2f(i,1:i-1)
         enddo
         d2sad(:,:,:,:,:,:,j)=reshape(d2f,(/CperUC,nc1,nc2,CperUC,nc1,nc2/))
       endif

         if (pot%v0(j).gt.pot%potmax) goto 10
            
         if (j==pot%ndata) goto 20
         j=j+1
         go to 10 ! OH MY GOD

20    continue
    
      write(11,*)' Number of PES data points = ',pot%ndata

c set vmin to be the minimum of the readin value in cntl and the
c minimum found in the POT file

      write(11,*)' lowest potential energy in the POT file   = ',minval(pot%v0)

      write(11,*)' lowest potential energy expected from IN_CNT  = '
     .             ,pot%vmin

      pot%vmin=min(pot%vmin,minval(pot%v0))

      write(11,*)' lowest potential energy   = ',pot%vmin

      close (unit=7)

c  construct the local coords for each data point
c  remembering that rda are read in as bond lengths NOT inverses 
!  invert bondlengths ALWAYS

      pot%zda=0
      do n=1,pot%ndata
        pot%rda(n,:)=1/pot%rda(n,:)
        lr(1:pot%nbond)=pot%rda(n,:)
        if (TranslationalSym) then
          if (PeriodicDOF==2) then
            lr(pot%nbond+1:pot%nbond+pot%natom)=pot%hda(n,:)
            lr(pot%nbond+pot%natom+1:pot%nredundant)
     +          =reshape(transpose(pot%suda(n,:,:)),(/pot%natom*pot%nsu/))
          else
            lr(pot%nbond+1:pot%nredundant)
     +          =reshape(transpose(pot%suda(n,:,:)),(/pot%natom*pot%nsu/))
          endif
        endif
        pot%zda(n,:)=matmul(pot%ut(n,:,:),lr)
      enddo


      !do n=1,pot%ndata
      !  do i=1,pot%ngsurf
      !    print *, 'perm', i
      !    do j=1,pot%natom
      !      do k=1,pot%nsu
      !        junk(k) = pot%suda(n,pot%nmrep(1,j),pot%ips(k,i))*pot%isign(k,i)
      !      enddo
      !      print *, junk(1:3)
      !      print *, junk(4:6)
      !    enddo
      !  enddo
      !enddo
      !stop

      return
      end

