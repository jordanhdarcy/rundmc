subroutine neighbourGrid(mols,pot,j)
  use POTmodule
  implicit none
  type(POTinstance) :: mols
  type(POTobject) :: pot
  integer, intent(in) :: j

  integer :: i, k, l, m, n, nOld, o, nDP, shift
  integer :: min, max
  integer :: nf
  integer :: dp, ap, sgp ! indexes: 'datapoint', 'atomic perm', 'space group perm'
  integer :: x, y, z, step, x1, y1, z1, x2, y2, z2
  integer :: xMin, xMax, yMin, yMax, zMin, zMax
  integer :: nCells
  logical, dimension(0:pot%nIntervals-1,0:pot%nIntervals-1,0:pot%nIntervals-1) :: checked
  real(kind=8), dimension(3) :: centroid, fracCoords
  real(kind=8), dimension(pot%maxDP) :: vt, wt
  integer, dimension(pot%maxDP) :: dpIndex, aPerm, sgPerm

  real(kind=8) :: sumNew, sumOld, t

  if (.not.TranslationalSym) stop "neighbourGrid: This won't work without periodic coordinates!"

  ! Calculate centroid, not centre-of-mass, of this geometry
  do i=1,3
    centroid(i) = SUM(mols%c(:,i,j))
  enddo
  centroid = centroid/pot%natom

  ! Calculate fractional coordinates, shift to unit cell.
  fracCoords(1) = DOT_PRODUCT(centroid,pot%RecipA)
  fracCoords(2) = DOT_PRODUCT(centroid,pot%RecipB)
  fracCoords(3) = DOT_PRODUCT(centroid,pot%RecipC)

  


  do i=1,3
    shift = 0
    if (fracCoords(i).gt.1.0) then
      shift = -int(fracCoords(i))
    elseif (fracCoords(i).lt.0.0) then
      shift = -int(fracCoords(i))+1
    endif
    fracCoords(i) = fracCoords(i) + real(shift,8)
  enddo

  ! Get location of this geometry in the grid.
  x = int(fracCoords(1)/pot%interval)
  y = int(fracCoords(2)/pot%interval)
  z = int(fracCoords(3)/pot%interval)


  ! Calculate weights of nearby data points, expand into nearby cells if needed.
  mols%totsum(j) = 0.d0
  n = 0
  nOld = 0
  nf = 0
  step = 0
  checked = .false.
  sumNew = 0.0d0
  nCells = 0

  do while (.true.)
    xMin = x-step
    xMax = x+step
    yMin = y-step
    yMax = y+step
    zMin = z-step
    zMax = z+step

    nOld = n

    do x1=xMin,xMax
      ! Ensure that x,y and z values are in the right range.
      if (x1.lt.0) then
        x2 = pot%nIntervals+x1
      elseif (x1.ge.pot%nIntervals) then
        x2 = x1-pot%nIntervals
      else
        x2 = x1
      endif

      do y1=yMin,yMax
        if (y1.lt.0) then
          y2 = pot%nIntervals+y1
        elseif (y1.ge.pot%nIntervals) then
          y2 = y1-pot%nIntervals
        else
          y2 = y1
        endif

        do z1=zMin,zMax
          if (z1.lt.0) then
            z2 = pot%nIntervals+z1
          elseif (z1.ge.pot%nIntervals) then
            z2 = z1-pot%nIntervals
          else
            z2 = z1
          endif

          if (.not.checked(x2,y2,z2)) then
            do i=1,pot%nDPs(x2,y2,z2)
              dp = pot%DPindex(x2,y2,z2,i)
              sgp = pot%DPperm(x2,y2,z2,i)
              do k=1,pot%ngroup
                n = n+1
                vt(n) = 0.0d0
                ! Store a few indexes - we'll need them later.
                dpIndex(n) = dp
                aPerm(n) = k
                sgPerm(n) = sgp
                ! Calculate primitive weights in the usual way.
                if (.not.FrozenMolecule) then
                  do l=1,pot%nbond
                    t = 1.0d0/mols%r(l,j)-pot%rda(dp,pot%ip(l,k))
                    if (TranslationalSym) t=t*pot%scaleR
                    vt(n) = vt(n) + t*t
                  enddo
                endif

                ! Do the periodic coordinates thing.
                do l=1,pot%natom
                    do m=1,pot%nsu ! Loop over periodic coordinates.
                      t = mols%su(l,m,j) - &
                       pot%suda(dp, pot%nmrep(k,l), pot%ips(m,sgp))*pot%isign(m,sgp)
                      ! note: pot%nmrep maps atom l to correct atom in permutation k
                      ! pot%ips maps periodic coordinate m to coodinate in space group perm spp
                      ! pot%isign is the sign of the coordinate in this space group perm.
                      t = t*pot%scaleS
                      vt(n) = vt(n) + t*t
                    enddo
                enddo
              enddo
            enddo
            checked(x2,y2,z2) = .true.
            nCells = nCells+1
          endif
        enddo
      enddo
    enddo

    !print *, n

    if (n.ne.0) then
      wt(nOld+1:n) = vt(nOld+1:n)**(-pot%ipow)
      sumOld = sumNew
      sumNew = SUM(wt(1:n))
      !print *, 'sumOld, sumNew:', sumOld, sumNew
      !print *, (sumNew-sumOld)/sumNew*100, nCells, n
      if ((nCells.eq.1).and.(n.gt.pot%minDP)) exit
      ! If we get lucky we can stop looping here.

      if ((sumNew-sumOld)/sumNew.lt.pot%totsumTol) exit

      ! OR: we've expanded across the whole cell...
      if (n.eq.pot%maxDP) exit
    endif
    step = step+1
  enddo


  mols%totsum(j) = sumNew
  wt(1:n) = wt(1:n)/mols%totsum(j)

  do i=1,n
    if (wt(i).gt.pot%wtol) then
      nf = nf+1
      mols%mda(nf,j) = dpIndex(i) ! Save this data point.
      mols%nda(nf,j) = aPerm(i) ! Save the atomic permuation
      mols%n2da(nf,j) = sgPerm(i) ! Save the space group perm
    endif
  enddo
  mols%nforc(j) = nf ! Save the number of neighbours.


end subroutine neighbourGrid
