subroutine expandPerms(pot)
  use POTmodule
  implicit none
  type(POTobject), intent(inout) :: pot
  logical :: fileExists
  real(kind=8) :: interval
  real(kind=8), dimension(1:3) :: centroid, permuted
  character(len=80) :: comment
  character(:), allocatable :: testStr
  integer :: nInt, i, j, k, l, x, y, z, n, min, max
  integer :: unit


  if (.not.JDHacks) stop "expandPerms should not be called when JDHacks is false."
  if (PeriodicDOF.ne.3) stop "expandPerms should only be called when PeriodicDOF == 3"
  
  call lisym(11,'-')
  write(11,*)
  write(11,*) "*** subroutine expandPerms: grid up the unit cell and expand the space group permutations ***"
  write(11,*)
  write(11,*) "Read parameters from IN_GRID"
  write(11,*)

  INQUIRE(file='IN_GRID',exist=fileExists)

  if (.not.fileExists) stop "IN_GRID not found!"

  open(newunit=unit,file='IN_GRID',status='unknown',action='read')

  ! Comment lines
  read(unit,*)
  read(unit,'(A)') comment
  read(unit,*) interval
  write(11,'(A)') comment
  write(11,*) interval
  read(unit,'(A)') comment
  read(unit,*) pot%minDP
  write(11,'(A)') comment
  write(11,*) pot%minDP
  read(unit,'(A)') comment
  read(unit,*) pot%totsumTol
  write(11,'(A)') comment
  write(11,*) pot%totsumTol
  if (pot%totsumTol.gt.100.0d0) stop "Totsum tolerance greater than 100 %. Seriously?"
  if (pot%minDP.gt.pot%ndata*pot%ngroup*pot%ngsurf) then
    print *, "pot%minDP is greater than effective number of data points!"
    print *, "minDP =", pot%minDP, "; effective number of data points =", pot%ndata*pot%ngroup*pot%ngsurf
    print *, "Try adjusting IN_GRID parameters."
    stop
  endif

  pot%totsumTol = pot%totsumTol/100.0d0

  if (pot%minDP.le.0) stop "You want pot%minDP to be <= 0? I don't think so."

  ! insert further reading here
  close(unit)

  if (interval.le.0.0.or.interval.gt.1) stop "Interval for unit cell gridding must be between 0 and 1."

  if (1.0d0-int(1.0/interval)*interval.gt.0.001d0) then
    ! Silly MOD function and floating point silliness, I'll do the MOD myself!
    print *, 'IN_GRID:'
    print *, '1/interval needs to be an integer, or very close to it.'
    print *, '1/interval =', 1.0d0/interval, '1.0 % interval =', 1.0d0-int(1.0/interval)*interval
    stop
  endif

  nInt = int(1.0d0/interval)
  pot%nIntervals = nInt

  write(11,'(A,I3,A,I6,A)'), "There will be ", nInt, " intervals, for a total of ", nInt**3, ' bins in the unit cell.'
  write(11,*) "Will consider a minimum of", pot%minDP, "effective data points for interpolation."

  pot%interval = interval
  min = 0
  max = nInt-1
  allocate(pot%nDPs(min:max,min:max,min:max))


  pot%nDPs = 0
  ! Now loop over data points, and permute their fractional coordinates according to the space group elements
  write(11,*)
  write(11,*) "Permuting fractional coordinates..."
  do l=1,2 ! Loop twice; first time so we can count permutations, then allocate and bin.
    pot%nDPs = 0
    do i=1,pot%ndata
      ! Calculate centroid of this data point.
      do j=1,3
        centroid(j) = sum(pot%frac(i,:,j))
      enddo
      centroid = centroid/pot%natom
      ! Loop over space group elements
      do j=1,pot%ngsurf
        ! Inspect element for x,y,z fractional coordinates
        do k=1,3
          select case (pot%SGPerm(j,k))
            case("x")
              permuted(k) = centroid(1)
            case("y")
              permuted(k) = centroid(2)
            case("z")
              permuted(k) = centroid(3)
            case("-x")
              permuted(k) = 1-centroid(1)
            case("-y")
              permuted(k) = 1-centroid(2)
            case("-z")
              permuted(k) = 1-centroid(3)
            case("x+1/2")
              permuted(k) = centroid(1)+0.5d0
            case("y+1/2")
              permuted(k) = centroid(2)+0.5d0
            case("z+1/2")
              permuted(k) = centroid(3)+0.5d0
            case("-x+1/2")
              permuted(k) = 1.5d0-centroid(1)
            case("-y+1/2")
              permuted(k) = 1.5d0-centroid(2)
            case("-z+1/2")
              permuted(k) = 1.5d0-centroid(3)
            case default
              print '(A,I3,A,I3)', 'Unrecognised input in IN_FRACPERMS at permutation', j, ', element ', k
              print *, 'Silly input: ', pot%SGPerm(j,k)
              stop
          end select
        enddo
        ! Correct for out-of-boundness
        where(permuted.lt.0.0d0)
          permuted = permuted+1.0d0
        elsewhere(permuted.gt.1.0d0)
          permuted = permuted-1.0d0
        endwhere



        ! Find bin for this permutation:
        x = int(permuted(1)/pot%interval)
        y = int(permuted(2)/pot%interval)
        z = int(permuted(3)/pot%interval)

        pot%nDPs(x,y,z) = pot%nDPs(x,y,z)+1
        if (l.eq.2) then
          n = pot%nDPs(x,y,z)
          pot%DPindex(x,y,z,n) = i
          pot%DPperm(x,y,z,n) = j
        endif
      enddo
    enddo
    if (l.eq.1) then
      ! Only allocate in the first loop.
      n = MAXVAL(pot%nDPs)
      allocate(pot%DPindex(min:max,min:max,min:max,1:n))
      allocate(pot%DPperm(min:max,min:max,min:max,1:n))
      pot%DPindex = 0
      pot%DPperm = 0
    endif
  enddo

  ! Guards against array out-of-bounds errors for silly choices of parameters in IN_GRID
  ! e.g. setting maximum number of steps to include the whole cell with a huge
  ! number for the minimum number of data points... may end up with more than
  ! nDP*nGroup*nSpacePerm data points to consider as possible neighbours.
  pot%maxDP = pot%ndata*pot%ngsurf*pot%ngroup


  write(11,*) "...finished permuting."
  write(11,*) "Effective data points:", pot%ndata*pot%ngsurf*pot%ngroup
  write(11,*) "Maximum number of data points in bin:", MAXVAL(pot%nDPs)
  write(11,*) "Minimum number of data points in bin (greater than 0):", MINVAL(pot%nDPs,MASK=(pot%nDPs > 0))
  write(11,*) "Number of bins with no data points:", COUNT(MASK=(pot%nDPs.eq.0))
  write(11,*)

end subroutine expandPerms