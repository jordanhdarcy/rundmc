module MPI_Data
implicit none
logical,parameter::MPI=.false.
include 'mpif.h'
save
integer::rank,ranks
end module MPI_Data

module POTmodule
use MPI_Data
implicit none

 ! for GJ style gas-surface potentials: TranslationalSym & GasSurface are true
 !              (even if there is a surface only)
 ! for fragmented gas-surface cluster models: GasSurface is true
 ! for fragmented slab: PBC is true, possibly with GasSurface
 ! for absorbates inside crystal lattices: PeriodicDOF=3 (2 otherwise)
 ! Fragmented potentials apply atommask
 ! PBC applies thermostat
 ! BinMin (energybinning.f90) requires GasSurface
logical,parameter::RecalculateCon=.false.
logical,parameter::TranslationalSym=.true.
logical,parameter::SurfaceCoordsIncludeHeight=.false.
logical,parameter::GasSurface=.false.
logical,parameter::PBC=.false.
logical,parameter::MinimumImage=.false.
logical,parameter::FrozenMolecule=.true.
logical,parameter::NoForce=.true.
logical,parameter::ModelPotential=.false.
logical,parameter::JDHacks=.true.
logical,parameter::SiliconHacks=.false.,H2Pt111Hacks=.false.,SiSlabHacks=.false.
integer,parameter::Nhien=5
integer,parameter::PeriodicDOF=3

type POTobject
  real(kind=8),dimension(:,:,:),pointer::ut,suda
  real(kind=8),dimension(:,:),pointer::v1,v2,rda,zda,sumb
  real(kind=8),dimension(:,:),pointer::hda
  real(kind=8),dimension(:,:),pointer::ceq
  real(kind=8),dimension(:,:),pointer::Xvar,XHdash
  real(kind=8),dimension(:,:,:),pointer::Xhien
  real(kind=8),dimension(:),pointer::v0,rads,amas,maxwt
  real(kind=8),dimension(:),pointer::smass
  real(kind=8),dimension(:),pointer::hien
  real(kind=8),dimension(2,2)::LatticeInv
  real(kind=8),dimension(PeriodicDOF)::RecipA,RecipB,RecipC
  real(kind=8),dimension(PeriodicDOF)::LattA,LattB,LattC
  real(kind=8),dimension(2)::SuperA,SuperB
  integer,dimension(:,:),pointer::ip,nperm,nmrep
  integer,dimension(:,:),pointer::ips,isign
  integer,dimension(:,:),pointer::rindex
  integer,dimension(:),pointer::mb,nb
  real(kind=8)::wtol,outer,avrads,potmax,vmin,sigma2,Z0depth,fragmax
  real(kind=8)::ClosestLatt,scaleR,scaleH,scaleS,refen
  real(kind=8)::MaxVar,MaxHdash,MaxVarEn,MaxHdashEn
  integer::natom,nbond,nredundant,nint,ngroup,ndata,nsdx
  integer::lowp,ipart,ipow,nneigh,mep,neighci
  integer::ngsurf,nsu
  integer::MaxVarN,MaxHdashN
  character(len=2),dimension(:),pointer::lab
  logical::surfdyn

  ! Jordan's junk follows
  real(kind=8),dimension(:,:,:),pointer::frac ! fractional coordinates for each data point
  character(len=6),dimension(:,:),pointer::SGPerm
  ! Jordan update: stores the space group permutation of x,y,z fractional coordinates
  ! in the format of the Bilbao Crystallographic Server permutations
  real(kind=8) :: interval, totsumTol
  integer,dimension(:,:,:),pointer::nDPs ! the total number of data points in each cube of the unit cell.
  integer,dimension(:,:,:,:),pointer :: DPindex, DPperm ! The index of each data point, and its permutation.
  integer :: nIntervals, minDP, maxDP
  logical :: degub ! debugging flag
end type POTobject

type POTinstance
   ! What would be dsu(j,i,n,i,m) is stored in dsu(j,i,n,m)
   ! What would be dh(j,i,i,3) is stored in dh(j,i)
  real(kind=8),dimension(:,:,:,:),pointer::dr,dsu
  real(kind=8),dimension(:,:,:),pointer::c,v,dv,c0,su,dvdsu
  real(kind=8),dimension(:,:),pointer::r,dvdr,h,dvdh,dh
  real(kind=8),dimension(:,:),pointer::sdx,sdv
  real(kind=8),dimension(:),pointer::totsum,en,rms,AsympEn,dvdsx
  integer,dimension(:,:),pointer::mda,nda,n2da,mdao,ndao,n2dao   ! molecule is second index
  integer,dimension(:,:),pointer::map
  integer,dimension(:),pointer::nforc,nforco,mdaol,ndaol,n2daol
  integer,dimension(:),pointer::nterms,term1
  integer::maxf,maxfo,nmax,nforcol              ! nmax ~ nmol
  integer::nstart      ! start of loop in dvdi
  logical,dimension(:,:),pointer::AsympMask
end type POTinstance

contains

subroutine AllocatePOT1(p)
 ! allocates stuff that depends only on natom, nbond
type(POTobject)::p
allocate(p%amas(p%natom))
allocate(p%mb(p%nbond))
allocate(p%nb(p%nbond) )
allocate(p%rindex(p%natom,p%natom))
allocate(p%lab(p%natom))
allocate(p%ceq(p%natom,3))
allocate(p%Xvar(p%natom,3))
allocate(p%XHdash(p%natom,3))
allocate(p%Xhien(p%natom,3,Nhien))
allocate(p%hien(Nhien))
p%MaxVar=-1
p%MaxHdash=huge(1.0_8)
p%hien=huge(1.0_8)
end subroutine AllocatePOT1

subroutine AllocatePOT2(p)
 ! allocates stuff that depends on ngroup
type(POTobject)::p
allocate(p%ip(p%nbond,p%ngroup))
allocate(p%nperm(p%ngroup,p%natom))
allocate(p%nmrep(p%ngroup,p%natom))
end subroutine AllocatePOT2

subroutine AllocatePOTs(p)
 ! allocates stuff that depends on surface symmetry and surface atoms
type(POTobject)::p
allocate(p%ips(p%nsu,p%ngsurf))
allocate(p%isign(p%nsu,p%ngsurf))
if (p%surfdyn) then
  allocate(p%smass(p%nsdx))
endif
if (JDHacks) allocate(p%SGPerm(1:p%ngsurf,1:3))

end subroutine AllocatePOTs

subroutine AllocatePOT3(p)
 ! Now we have ndata
type(POTobject)::p
allocate(p%v0(p%ndata))
allocate(p%v1(p%ndata,p%nint))
allocate(p%v2(p%ndata,p%nint))
allocate(p%ut(p%ndata,p%nint,p%nredundant))
allocate(p%rda(p%ndata,p%nbond))
p%v0=0
p%v1=0
p%v2=0
p%ut=0
p%rda=0
if (TranslationalSym) then
  if (PeriodicDOF==2) then
    allocate(p%hda(p%ndata,p%natom))
    p%hda=0
  endif
  allocate(p%suda(p%ndata,p%natom,p%nsu))
  p%suda=0
  if (JDHacks) allocate(p%frac(1:p%ndata,1:p%natom,1:3))
endif
allocate(p%maxwt(p%ndata))
p%maxwt=0
allocate(p%zda(p%ndata,p%nint))
allocate(p%rads(p%ndata))
allocate(p%sumb(p%ndata,p%nredundant))
p%zda=0
p%rads=0
p%sumb=0
end subroutine AllocatePOT3

subroutine ShrinkAllocatePOT3(p,n)
 ! We need to discard some data at the end
type(POTobject)::p
integer,intent(in)::n
real(kind=8),dimension(n)::t1
real(kind=8),dimension(n,p%nint)::t2
real(kind=8),dimension(n,p%nint,p%nredundant)::t3
real(kind=8),dimension(n,p%nbond)::t4
real(kind=8),dimension(n,p%natom)::t5
real(kind=8),dimension(n,p%natom,p%nsu)::t6
real(kind=8),dimension(n,p%nredundant)::t7
t1=p%v0(1:n)
deallocate(p%v0)
allocate(p%v0(n))
p%v0=t1
t2=p%v1(1:n,:)
deallocate(p%v1)
allocate(p%v1(n,p%nint))
p%v1=t2
t2=p%v2(1:n,:)
deallocate(p%v2)
allocate(p%v2(n,p%nint))
p%v2=t2
t3=p%ut(1:n,:,:)
deallocate(p%ut)
allocate(p%ut(n,p%nint,p%nredundant))
p%ut=t3
t4=p%rda(1:n,:)
deallocate(p%rda)
allocate(p%rda(n,p%nbond))
p%rda=t4
if (TranslationalSym) then
  if (PeriodicDOF==2) then
    t5=p%hda(1:n,:)
    deallocate(p%hda)
    allocate(p%hda(n,p%natom))
    p%hda=t5
  endif
  t6=p%suda(1:n,:,:)
  deallocate(p%suda)
  allocate(p%suda(n,p%natom,p%nsu))
  p%suda=t6
endif
deallocate(p%maxwt)
allocate(p%maxwt(n))
p%maxwt=0
t2=p%zda(1:n,:)
deallocate(p%zda)
allocate(p%zda(n,p%nint))
p%zda=t2
t1=p%rads(1:n)
deallocate(p%rads)
allocate(p%rads(n))
p%rads=t1
t7=p%sumb(1:n,:)
deallocate(p%sumb)
allocate(p%sumb(n,p%nredundant))
p%sumb=t7
p%ndata=n
end subroutine ShrinkAllocatePOT3

subroutine AllocateMols(p,n,mi,mo,pot,r)
 ! n is number of trajectories
type(POTinstance)::p
integer,intent(in)::n,mi,mo,r
type(POTobject)::pot
p%nmax=n
p%maxf=mi
p%maxfo=mo
allocate(p%c(pot%natom,3,n))
allocate(p%v(pot%natom,3,n))
allocate(p%r(pot%nbond,n))
if (TranslationalSym) then
  if (PeriodicDOF==2) then
    allocate(p%h(pot%natom,n))
    allocate(p%dh(pot%natom,n))
    allocate(p%dvdh(pot%natom,n))
  endif
  allocate(p%su(pot%natom,pot%nsu,n))
  allocate(p%dsu(pot%natom,pot%nsu,3,n))
  allocate(p%n2da(mi,n))
  allocate(p%n2dao(mo,n))
  allocate(p%n2daol(mo))
  allocate(p%dvdsu(pot%natom,pot%nsu,n))
  if (pot%surfdyn) then
    allocate(p%sdx(pot%nsdx,n))
    allocate(p%sdv(pot%nsdx,n))
    allocate(p%dvdsx(pot%nsdx))
  else
    nullify(p%sdx)
    nullify(p%sdv)
    nullify(p%dvdsx)
  endif
endif
allocate(p%c0(pot%natom,3,n))
allocate(p%dr(pot%nbond,pot%natom,3,n))
allocate(p%dv(pot%natom,3,n))
allocate(p%dvdr(pot%nbond,n))
allocate(p%totsum(n))
allocate(p%en(n))
allocate(p%rms(n))
allocate(p%AsympEn(n))
allocate(p%mda(mi,n))
allocate(p%nda(mi,n))
allocate(p%mdao(mo,n))
allocate(p%ndao(mo,n))
allocate(p%mdaol(mo))
allocate(p%ndaol(mo))
allocate(p%nforc(n))
allocate(p%nforco(n))
allocate(p%nterms(0:r-1))
allocate(p%term1(1:r-1))
allocate(p%map(pot%natom,pot%natom))
allocate(p%AsympMask(n,pot%nbond))
p%nstart=1
end subroutine AllocateMols

subroutine DeallocatePOT(p)
type(POTobject)::p
deallocate(p%amas)
deallocate(p%mb)
deallocate(p%nb)
deallocate(p%rindex)
deallocate(p%lab)
deallocate(p%ip)
deallocate(p%nperm)
deallocate(p%nmrep)
deallocate(p%v0)
deallocate(p%v1)
deallocate(p%v2)
deallocate(p%ut)
deallocate(p%rda)
if (TranslationalSym) then
  if (PeriodicDOF==2) deallocate(p%hda)
  deallocate(p%suda)
endif
deallocate(p%maxwt)
deallocate(p%zda)
deallocate(p%rads)
deallocate(p%sumb)
end subroutine DeallocatePOT

subroutine DeallocateMols(p)
type(POTinstance)::p
deallocate(p%c)
deallocate(p%v)
deallocate(p%r)
if (TranslationalSym) then
  if (PeriodicDOF==2) then
    deallocate(p%h)
    deallocate(p%dh)
    deallocate(p%dvdh)
  endif
  deallocate(p%su)
  deallocate(p%dsu)
  deallocate(p%n2da)
  deallocate(p%n2dao)
  deallocate(p%dvdsu)
  if (associated(p%sdx)) then
    deallocate(p%sdx)
    deallocate(p%sdv)
    deallocate(p%dvdsx)
  endif
endif
deallocate(p%c0)
deallocate(p%dr)
deallocate(p%dv)
deallocate(p%dvdr)
deallocate(p%totsum)
deallocate(p%en)
deallocate(p%AsympEn)
deallocate(p%rms)
deallocate(p%mda)
deallocate(p%nda)
deallocate(p%mdao)
deallocate(p%ndao)
deallocate(p%nforc)
deallocate(p%nforco)
deallocate(p%nterms)
deallocate(p%term1)
deallocate(p%map)
deallocate(p%AsympMask)
end subroutine DeallocateMols

subroutine SendPot(pot)
type(POTobject)::pot
integer,dimension(15)::bufi
real(kind=8),dimension(10)::bufr
integer::e
bufi(1:13)=(/pot%natom,pot%nint,pot%nbond,pot%ngroup,pot%ndata,&
        0,   pot%nredundant,pot%ipart,pot%lowp,pot%ipow,&
        pot%nneigh,pot%ngsurf,pot%nsu/)
if (pot%surfdyn) then
  bufi(14:15)=(/1,pot%nsdx/)
else
  bufi(14:15)=0
endif
call MPI_Bcast(bufi,15,MPI_INTEGER,0,MPI_COMM_WORLD,e)
bufr=(/pot%wtol,pot%outer,pot%potmax,pot%vmin,pot%sigma2,&
        pot%avrads,pot%scaleR,pot%scaleH,pot%scaleS,pot%Z0depth/)
call MPI_Bcast(bufr,10,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%amas,pot%natom,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%mb,pot%nbond,MPI_INTEGER,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%nb,pot%nbond,MPI_INTEGER,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%ip,pot%nbond*pot%ngroup,MPI_INTEGER,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%nperm,pot%natom*pot%ngroup,MPI_INTEGER,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%nmrep,pot%natom*pot%ngroup,MPI_INTEGER,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%v0,pot%ndata,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%v1,pot%ndata*pot%nint,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%v2,pot%ndata*pot%nint,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%ut,pot%ndata*pot%nint*pot%nredundant,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%rda,pot%ndata*pot%nbond,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%zda,pot%ndata*pot%nint,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%rads,pot%ndata,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%sumb,pot%ndata*pot%nredundant,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
if (TranslationalSym) then
  call MPI_Bcast(pot%ips,pot%nsu*pot%ngsurf,MPI_INTEGER,0,MPI_COMM_WORLD,e)
  call MPI_Bcast(pot%isign,pot%nsu*pot%ngsurf,MPI_INTEGER,0,MPI_COMM_WORLD,e)
  if (PeriodicDOF==2) call MPI_Bcast(pot%hda,pot%ndata*pot%natom,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
  call MPI_Bcast(pot%suda,pot%ndata*pot%natom*pot%nsu,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
  call MPI_Bcast(pot%RecipA,PeriodicDOF,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
  call MPI_Bcast(pot%RecipB,PeriodicDOF,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
  call MPI_Bcast(pot%RecipC,PeriodicDOF,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
endif
end subroutine SendPot

subroutine GetPot(pot)
type(POTobject)::pot
integer,dimension(15)::bufi
real(kind=8),dimension(10)::bufr
integer,dimension(MPI_STATUS_SIZE)::stat
integer::e
call MPI_Bcast(bufi,15,MPI_INTEGER,0,MPI_COMM_WORLD,e)
pot%natom=bufi(1)
pot%nint=bufi(2)
pot%nbond=bufi(3)
pot%ngroup=bufi(4)
pot%ndata=bufi(5)
pot%nredundant=bufi(7)
pot%ipart=bufi(8)
pot%lowp=bufi(9)
pot%ipow=bufi(10)
pot%nneigh=bufi(11)
pot%ngsurf=bufi(12)
pot%nsu=bufi(13)
pot%surfdyn=(bufi(14)==1)
pot%nsdx=bufi(15)
call AllocatePOT1(pot)
call AllocatePOT2(pot)
call AllocatePOT3(pot)
call MPI_Bcast(bufr,10,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
pot%wtol=bufr(1)
pot%outer=bufr(2)
pot%potmax=bufr(3)
pot%vmin=bufr(4)
pot%sigma2=bufr(5)
pot%avrads=bufr(6)
pot%scaleR=bufr(7)
pot%scaleH=bufr(8)
pot%scaleS=bufr(9)
pot%Z0depth=bufr(10)
call MPI_Bcast(pot%amas,pot%natom,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%mb,pot%nbond,MPI_INTEGER,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%nb,pot%nbond,MPI_INTEGER,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%ip,pot%nbond*pot%ngroup,MPI_INTEGER,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%nperm,pot%natom*pot%ngroup,MPI_INTEGER,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%nmrep,pot%natom*pot%ngroup,MPI_INTEGER,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%v0,pot%ndata,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%v1,pot%ndata*pot%nint,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%v2,pot%ndata*pot%nint,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%ut,pot%ndata*pot%nint*pot%nredundant,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%rda,pot%ndata*pot%nbond,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%zda,pot%ndata*pot%nint,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%rads,pot%ndata,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
call MPI_Bcast(pot%sumb,pot%ndata*pot%nredundant,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
if (TranslationalSym) then
  call AllocatePOTs(pot)
  call MPI_Bcast(pot%ips,pot%nsu*pot%ngsurf,MPI_INTEGER,0,MPI_COMM_WORLD,e)
  call MPI_Bcast(pot%isign,pot%nsu*pot%ngsurf,MPI_INTEGER,0,MPI_COMM_WORLD,e)
  if (PeriodicDOF==2) call MPI_Bcast(pot%hda,pot%ndata*pot%natom,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
  call MPI_Bcast(pot%suda,pot%ndata*pot%natom*pot%nsu,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
  call MPI_Bcast(pot%RecipA,PeriodicDOF,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
  call MPI_Bcast(pot%RecipB,PeriodicDOF,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
  call MPI_Bcast(pot%RecipC,PeriodicDOF,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,e)
endif
end subroutine GetPot

end module POTmodule




module InterpTrajData
use POTmodule
implicit none
integer,parameter::InnerListSize=30000
integer,parameter::OuterListSize=30000
save
 ! for single PES interptraj:
real(kind=8)::tups,heutu,delta,pi
real(kind=8)::sample,bipmax,distab,econ,etrans,echeck,eshift,efraga
real(kind=8),dimension(:,:),pointer::rmin
integer,dimension(:,:),pointer::ichansave
integer,dimension(:),pointer::nfin,ntest,mfrag,ifraga,ifragb,kf
integer::nloop,nstep,nprint,ir,nstop,neighci,neighco,neighc,nstepad
integer::nsel,nlowstop,nmol,neighci2,neighco2,neighreassess,neighmax
integer::nfraga,nfragb,nfrag
logical,dimension(:),pointer::fragad
 ! for fragmented PESs:
type(POTobject),dimension(:),allocatable::pots
type(POTinstance),dimension(:),allocatable::inst
integer,dimension(:),allocatable::InstPerPot
integer::npots
logical::Fragmented,AdaptNeigh,WarnedOuter,OutpCalled
end module InterpTrajData

module surface
implicit none
save
integer::nsurfa,nsurfc,nc1,nc2,CperUC
real(kind=8),dimension(:,:,:,:,:,:,:),allocatable::d2sad
real(kind=8),dimension(:,:,:,:),allocatable::sad,dsad
integer,dimension(:),allocatable::surfcount

contains

function Translate1(c,n1,n2) result (T)
 ! shifts coordinates (n1,n2) left/down
real(kind=8),dimension(CperUC,nc1,nc2),intent(in)::c
integer,intent(in)::n1,n2
real(kind=8),dimension(CperUC,nc1,nc2)::T
integer::i,j,m,n
 ! assumes n1,n2>=0
if (n1+n2==0) then
  T=c
  return
endif
do j=1,nc2
  n=mod(j+n2-1,nc2)+1
  do i=1,nc1
    m=mod(i+n1-1,nc1)+1
    T(:,i,j)=c(:,m,n)
  enddo
enddo
end function Translate1

function Translate2(c,n1,n2) result (T)
real(kind=8),dimension(CperUC,nc1,nc2,CperUC,nc1,nc2),intent(in)::c
integer,intent(in)::n1,n2
real(kind=8),dimension(CperUC,nc1,nc2,CperUC,nc1,nc2)::T
integer::ia,ib,ja,jb,ma,mb,na,nb
if (n1+n2==0) then
  T=c
  return
endif
do jb=1,nc2
  nb=mod(jb+n2-1,nc2)+1
  do ib=1,nc1
    mb=mod(ib+n1-1,nc1)+1
    do ja=1,nc2
      na=mod(ja+n2-1,nc2)+1
      do ia=1,nc1
        ma=mod(ia+n1-1,nc1)+1
        T(:,ia,ja,:,ib,jb)=c(:,ma,na,:,mb,nb)
      enddo
    enddo
  enddo
enddo
end function Translate2

end module surface

