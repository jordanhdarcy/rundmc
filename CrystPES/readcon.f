      subroutine readcon(pot,IN_CON)

      use POTmodule
      implicit none
      type(POTobject)::pot
      character(len=*),intent(in)::IN_CON
      integer::j,badness

      open(unit=16,file=IN_CON)

      do j=1,pot%ndata
        read(16,*,iostat=badness)pot%rads(j),pot%sumb(j,:)
        if (badness/=0) exit
      enddo

      if (j<pot%ndata+1) then
        j=j-1
        write(*,'(''Ndata being reduced from '',i5,'' to '',i5)')pot%ndata,j
        if (j<int(0.9*pot%ndata)) stop 'This seems unreasonable!'
        call ShrinkAllocatePOT3(pot,j)
      endif

      close(unit=16)

      pot%avrads=sum(pot%rads)/real(pot%ndata,8)

      return
      end
