subroutine crossproduct3(v,a,b)
real(kind=8),dimension(3),intent(in)::a,b
real(kind=8),dimension(3),intent(out)::v
v=(/a(2)*b(3)-a(3)*b(2),a(3)*b(1)-a(1)*b(3),a(1)*b(2)-a(2)*b(1)/)
end subroutine crossproduct3

