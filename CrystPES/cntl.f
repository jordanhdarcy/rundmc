      subroutine cntl(pot,IN_CNT,KeepTrajData)

      use InterpTrajData
      implicit none
      type(POTobject)::pot
      character(len=*),intent(in)::IN_CNT
      logical,intent(in)::KeepTrajData

c   this subroutine reads in run control parameters from file on unit 5

      character(20) :: msunit,enunit,lnunit
      character(80) :: title,comlin
      real(kind=8)::efragb,amsc,esc,eukjpm,hkjpms
      real(kind=8)::rsc,scalef,tsc,avanum,hjs
      real(kind=8)::t1,t2,t3
      integer::nunits,i,k,i1,i2,i3,i4


      call lisym(11,'-')
      write(11,*) ' *** subroutine cntl:',
     .  ' read in run control parameters ***'

      open (unit=7,file=IN_CNT)

c   read in title of run

      read(7,80) title
   80 format(a80)

c  read in the number 1...7 defining the units

      nunits=5
c---------------------------------------------------------------------
c
c   calculate and scale model parameters
c   units for inputing  masses,energies and lengths:
c   if (nunits) then (mass  energy   length     scale factor) are:
c         1           g/mol kcal/mol angstroms  2.045482828d13
c         2           g/mol kj/mol   angstroms  1.0d13
c         3           g/mol cm-1     angstroms  1.093739383d12
c         4           g/mol eV       angstroms  9.822693571d13
c         5           g/mol hartrees bohr       9.682886569d14
c         6           g/mol aJ           angstroms  9.124087591d13
c         7           g/mol 1.d+05 j/mol angstroms  1.0d15
c
c on output masses, energies, lengths and times are scaled by
c amsc, esc, rsc and tsc (in seconds), respectively and
c must be multiplied by these scale factors to be converted
c to the stated units.

      amsc=1.0d0
      esc=1.0d0
      rsc=1.0d0
      scalef=1.d0
      if(nunits.eq.1)then
        scalef=2.045482828d13
        msunit=' g/mol'
        enunit=' kcal/mol'
        lnunit=' angstroms'
        eukjpm=4.184d0
      elseif(nunits.eq.2)then
        scalef=1.0d13
        msunit=' g/mol'
        enunit=' kJ/mol'
        lnunit=' angstroms'
        eukjpm=1.00d0
      elseif(nunits.eq.3)then
        scalef=1.093739383d12
        msunit=' g/mol'
        enunit=' cm-1'
        lnunit=' angstroms'
        eukjpm=1.196265839d-02
      elseif(nunits.eq.4)then
        scalef=9.822693571d13
        msunit=' g/mol'
        enunit=' eV'
        lnunit=' angstroms'
        eukjpm=96.48530899d0
      elseif(nunits.eq.5)then
        scalef=9.682886569d14
        msunit=' g/mol'
        enunit=' hartree'
        lnunit=' bohr'
        eukjpm=2625.499964d0
      elseif(nunits.eq.6)then
        scalef=9.124087591d13
        msunit=' g/mol'
        enunit=' 0.1382 aJ'
        lnunit=' angstroms'
        eukjpm=83.24897441d0
      elseif(nunits.eq.7)then
        scalef=1.0d14
        msunit=' g/mol'
        enunit=' 1.0d+05 J/mol'
        lnunit=' angstroms'
        eukjpm=1.00d+02
      endif
      tsc=dsqrt(amsc/esc)*rsc/scalef
      tups=tsc*1.0d12

c   Avagadro's number

      avanum=6.0221367d23

c   speed of light in cm s-1 and cm tu-1

!      ccmps=2.99792458d10
!      ccmptu=ccmps*tsc

c   Planck's constant in J s, kJ/mol s and eu tu

      hjs=6.6260755d-34
      hkjpms=hjs*avanum*1.0d-03
      heutu=(hkjpms/tsc)/eukjpm

c---------------------------------------------------------------------
c   echo some parameters

      write(11,*) title

c echo info from readccm

      write(11,*) 'The number of atoms = ',pot%natom
      if (TranslationalSym) then
        if (PeriodicDOF==2) then
          write(11,*)'This is a 2D-PERIODIC GAS-SURFACE potential'
        else
          write(11,*)'This is a 3D-PERIODIC potential'
        endif
      endif
      write(11,*)
      write(11,*) 'The atomic labels and masses are:'
      do i=1,pot%natom
      write(11,*) pot%lab(i), pot%amas(i)
      enddo
      write(11,*)
      write(11,*) 'The bonded atoms'
      do i=1,pot%nbond
      write(11,*) pot%mb(i), pot%nb(i)
      enddo
      write(11,*)
      write(11,*) 'The number of group elements = ',pot%ngroup
      write(11,*)

c      write(11,*) 'The permutations of atoms in the group are:'
c      write(11,*)
c      do i=1,ngroup
c       do k=1,natom
c       write(11,*)k,nperm(i,k)
c       enddo
c       write(11,*)
c      enddo
      write(11,*) 'The permutations of bonds in the group are:'
      write(11,*)
      do i=1,pot%ngroup
       write(11,*)
       write(11,8769)' Reordered atoms ',(pot%nperm(i,k),k=1,pot%natom)
       do k=1,pot%nbond
       write(11,*)k, pot%ip(k,i)
       enddo
       write(11,*)
      enddo
8769  format(a18,20i3)

      write(11,*) 'The units are defined as atomic units in cntl.f'
      write(11,*)

      write(11,40)esc,enunit,rsc,lnunit,amsc,msunit,
     .  tsc,tups,scalef
   40 format(
     ./,'  unit scaling parameters',
     ./,'  esc=',g12.5,a10,'  rsc=',g12.5,a10,'  amsc=',g12.5,a10,
     ./,'  tsc=',g17.10,' s =',g17.10,' ps  scalef=',g17.10,' s-1')

!      write(11,45)avanum,ccmps,ccmptu,hjs,hkjpms,heutu,eukjpm
!   45 format(
!     ./,'  Avagadro''s number   =',g17.10,
!     ./,'  Speed of light       =',g17.10,' cm s-1  =',
!     .g17.10,' cm tu-1',
!     ./,'  Planck''s constant   =',g17.10,' J s     =',
!     .g17.10,' kJ/mol s',
!     ./,'                                           =',g17.10,' eu tu',
!     ./,'  1 eu = 1 energy unit = ',g17.10,' kJ/mol')

      call lisym(11,'-')


c     call lisym(11,'-')
      write(11,81) title
   81 format(1x,a79)
c     call lisym(11,'-')

c   read in time step,no. of traj. step, nprint,no. of molecule etc.

      read(7,80) comlin
      write(11,81) comlin
      if (GasSurface.and..not.SiliconHacks) then
        read(7,*) t1,i1,i2,i3,i4
      else
        read(7,*) t1,i1,i2,i3
      endif
      write(11,100) t1,i1,i2,i3
  100 format(1x,g18.10,3i10)
      if (KeepTrajData) then
        delta=t1
        nstep=i1
        nprint=i2
        nmol=i3
        if (GasSurface.and..not.SiliconHacks) nstepad=i4/i2
      endif

      read(7,80) comlin
      write(11,81) comlin
      read(7,*) t1,t2,t3
      write(11,140) t1,t2,t3
      if (KeepTrajData) then
        efraga=t1
        efragb=t2
        etrans=t3
      endif

      read(7,80) comlin
      write(11,81) comlin
      read(7,*)t1
      write(11,140)t1
      if (KeepTrajData) econ=t1

  140 format(1x,3g18.10)

      read(7,80) comlin
      write(11,81) comlin
      read(7,*) t1,t2
      write(11,140) t1,t2
      if (KeepTrajData) then
        bipmax=t1
        distab=t2
        if(bipmax.gt.distab)then
          write(11,*) ' WARNING: bipmax =',bipmax,'.gt.distab=',distab
          write(11,*) ' RESET distab = bipmax = ',bipmax
          distab=bipmax
          write(11,81) comlin
          write(11,140) bipmax,distab
        endif
      endif


c  determine one or twp part weight function is used, ipart=1,2

       read(7,80) comlin
       write(11,81)comlin
       read(7,*)pot%ipart
       write(11,91)pot%ipart
91     format(2x,'we will use the ',i1,' part weight function')

      if(pot%ipart.eq.1.or.pot%ipart.eq.2)go to 92
      write(11,*)' incorrect value for 1 or 2 part weight function'
      stop
92    continue

c  read in the parameters defining the weight function, the neighbour
c  list and the number of timesteps between calls to neighbour

       read(7,80) comlin
       write(11,81)comlin
       read(7,*)pot%lowp,pot%ipow
       write(11,*)pot%lowp,pot%ipow

       read(7,80) comlin
       write(11,81)comlin
       read(7,*)pot%wtol,pot%outer
       write(11,*)pot%wtol,pot%outer

       read(7,80) comlin
       write(11,81)comlin
! This will accept either the old two integers (adaptive switched off implied)
! or int,int,logical,int,int
       read(7,80)comlin
       write(11,81)comlin
       ! JORDAN: I don't want to update the inner neighbour list every step...
       if (scan(comlin,'Tt')==0) then
         read(comlin,*)neighco,pot%neighci
      endif

       if (KeepTrajData) then
         if (scan(comlin,'Tt')==0) then
           read(comlin,*)neighco,neighci
           AdaptNeigh=.false.
         else
           read(comlin,*)neighco,neighci,AdaptNeigh,neighco2,neighci2
!  AdaptNeigh will be true
           if (Fragmented) then
             if (mod(neighco,neighco2)/=0) stop 'Outer updates mismatch'
             if (mod(neighco,nprint)/=0) stop 'print-update mismatch'
           else
             if (MinimumImage) stop 'Adaptive neighbour frequency not fixed for MinimumImage'
             neighreassess=neighco2
! Next lowest power of 2
             neighco2=2**int(log(real(neighco,8))/log(2.0_8))
             if (neighco2==neighco) neighco2=max(neighco2/2,1)
           endif
         endif
         if (.not.AdaptNeigh) then
           neighco2=neighco
           neighci2=neighci
         endif
         neighmax=neighco
c  to allow one part neighbour to work
         neighc=neighci
       endif

c  read in the sampling fraction used in outp to determine number
c  of trajectory points output in file TOUT, and number of
c  data points chosen

       read(7,80) comlin
       write(11,81)comlin
       read(7,*) t1
       write(11,*) t1
       if (KeepTrajData) sample=t1

       read(7,80) comlin
       write(11,81)comlin
       read(7,*) i1
       write(11,*) i1
       if (KeepTrajData) nsel=i1

c  read in energy maximum and minimum with which to screen data

       read(7,80)comlin
       write(11,81)comlin
       if (Fragmented) then
         read(7,*) pot%potmax,pot%vmin,pot%fragmax
         write(11,*) pot%potmax,pot%vmin,pot%fragmax
       else
         read(7,*) pot%potmax,pot%vmin
         write(11,*) pot%potmax,pot%vmin
       endif

c  read in flag to determine whether low energy trajectories are stopped

       read(7,80)comlin
       write(11,81)comlin
       read(7,*) i1
       write(11,*) i1
       if (KeepTrajData) nlowstop=i1

c read in the multipicitive factor used in radius.f, confidrad.f

       read(7,80)comlin
       write(11,81)comlin
       read(7,*) pot%nneigh
       write(11,*) pot%nneigh

c  Enter the energy error tolerance for confidrad.f

       read(7,80)comlin
       write(11,81)comlin
       read(7,*) pot%sigma2
       write(11,*) pot%sigma2
       pot%sigma2=pot%sigma2**2

c The scale factors for the different classes of coordinates
       if (TranslationalSym) then
         read(7,80)comlin
         write(11,81)comlin
         read(7,*)pot%scaleR,pot%scaleH,pot%scaleS
         write(11,*)pot%scaleR,pot%scaleH,pot%scaleS
       endif

c calculate pi, conversion factors

      pi=2.d0*dacos(0.d0)


c  scale atomic masses

      do 150 i=1,pot%natom
        pot%amas(i)=pot%amas(i)/amsc
  150 continue


      close(unit=7)

      return
      end

c  --------------------------------------------------------------

      subroutine readz(pot,IN_ZMA)
      use InterpTrajData
      implicit none
      type(POTobject)::pot
      character(len=*),intent(in)::IN_ZMA

      character(len=80)::comlin
      integer,dimension(pot%nbond)::ibfraga,ibfragb
      integer::i,j,k,nbfraga,nbfragb
 
      call lisym(11,'-')
      write(11,*) ' *** subroutine readz: creat mb,nb read in fragment data'
      write(11,*) ' '

c  given that we need all the atom-atom bonds, we just assign the indirect
c  addresses of the bonds, rather than the traditional read from ZMA

      if (pot%natom>0) then
        k=1
        do i=1,pot%natom-1
        pot%rindex(i,i)=-1
        do j=i+1,pot%natom

           pot%mb(k)=i
           pot%nb(k)=j
           pot%rindex(i,j)=k
           pot%rindex(j,i)=k
           k=k+1

        enddo
        enddo
        pot%rindex(i,i)=-1
      endif

c   read fragment atom numbers

      if (IN_ZMA=='') goto 4100

      open (unit=7,file=IN_ZMA)

80    format(a80)
81    format(1x,a79)

      read(7,80)comlin
      write(11,81)comlin

      read(7,*)nfraga,nfragb
      write(11,*)nfraga,nfragb

      if(nfraga+nfragb.ne.pot%natom)then
        write(11,*) 'ABORT: nfraga+nfragb.ne.natom'
        write(11,*) nfraga,'+',nfragb,'.ne.',pot%natom
        stop
      endif

      read(7,80)comlin
      write(11,81)comlin

      if(nfraga.gt.0)then
      read(7,*)(ifraga(i),i=1,nfraga)
      write(11,*)(ifraga(i),i=1,nfraga)
      endif

      if(nfragb.gt.0)then
        read(7,80)comlin
        write(11,81)comlin
        read(7,*)(ifragb(i),i=1,nfragb)
        write(11,*)(ifragb(i),i=1,nfragb)
      endif
      if (nfraga .le. 1) go to 3100

      nbfraga=0
      do 3000 i=1,pot%nbond
	do 2900 j=1,nfraga
	  do 2800 k=1,nfraga
            if ((pot%mb(i).eq.ifraga(j)).and.(pot%nb(i).eq.ifraga(k))) then
              nbfraga=nbfraga+1
              ibfraga(nbfraga)=i
            endif
 2800     continue 
 2900   continue
 3000 continue
	
      write (11,*) 'no. of bonds in fragment a is', nbfraga
      write (11,*) 'bonds in fragment a are'
      write (11,*) (ibfraga(i),i=1,nbfraga)

 3100 continue

      if (nfragb .le. 1) go to 4100

      nbfragb=0
      do 4000 i=1,pot%nbond
	do 3900 j=1,nfragb
	  do 3800 k=1,nfragb
            if ((pot%mb(i).eq.ifragb(j)).and.(pot%nb(i).eq.ifragb(k))) then
              nbfragb=nbfragb+1
              ibfragb(nbfragb)=i
              endif
 3800     continue 
 3900   continue
 4000 continue

      write (11,*) 'no. of bonds in fragment b is', nbfragb
      write (11,*) 'bonds in fragment b are'
      write (11,*) (ibfragb(i),i=1,nbfragb)
      
 4100 continue

      close(unit=7)

      return
      end

