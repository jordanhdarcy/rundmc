
	subroutine readccm(pot,IN_SYSTEM,rrmin,ntot,rccm)
!	include 'traj.inc'

        use InterpTrajData
        implicit none
        type(POTobject)::pot
        real(kind=8)::cx
        character(len=*),intent(in)::IN_SYSTEM
        logical,intent(in)::rrmin,rccm
        integer,intent(inout)::ntot
! if ntot>0 read a second number from the natoms line and return it

	character(80) :: icomm
        integer::i,j

        open (unit=7,file=IN_SYSTEM)

c  read comment ; a header for the SYSTEM file

        read(7,80)icomm
80      format(a80)

c  read in the actual number of atoms

        read(7,80)icomm

        if (ntot>0) then
          read(7,*) pot%natom,ntot
        else
          read(7,*) pot%natom
        endif

        if (TranslationalSym) then
          pot%nint=3*pot%natom
        elseif (pot%natom==1) then
          pot%nint=0
        elseif (pot%natom==2) then
          pot%nint=1
        else
          pot%nint=3*pot%natom-6
        endif

        pot%nbond=pot%natom*(pot%natom-1)/2
        pot%nredundant=pot%nbond

        call AllocatePOT1(pot)
        if (pot%natom==0) return

c  read in the element labels for the atoms

        read(7,80)icomm

        read(7,*)pot%lab

c  read in the atomic masses

        read(7,80)icomm
        read(7,*)(pot%amas(i),i=1,pot%natom)

        if (rrmin) then
c  read in the cutoff lengths rmin(i,j)
          allocate(rmin(pot%natom,pot%natom+1))
          read(7,80)icomm
          read(7,80)icomm
          do i=1,pot%natom-1
            do j=i+1,pot%natom
              read(7,*) rmin(i,j)
            enddo
          enddo
          if (GasSurface) then
            read(7,80)icomm
            do i=1,pot%natom
              read(7,*)rmin(i,pot%natom+1)
            enddo
          endif
        endif

        close (unit=7)

        if (rccm) then
          open(unit=7,file='IN_CCM')
          read(7,*)
          do i=1,pot%natom
            read(7,*)pot%ceq(i,:)
          enddo
          close(7)
          if (.not.PBC) then
            cx=sum(pot%ceq(:,3)*pot%amas)/sum(pot%amas)
            pot%ceq(:,3)=pot%ceq(:,3)-cx
          endif
        endif

        return
        end

