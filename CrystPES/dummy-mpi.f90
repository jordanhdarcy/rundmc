subroutine MPI_Init(i)
integer,intent(in)::i
stop 'Why are you calling dummy MPI routines?'
end subroutine MPI_Init

subroutine MPI_Finalize
end subroutine MPI_Finalize

subroutine MPI_Comm_rank
end subroutine MPI_Comm_rank

subroutine MPI_Comm_size
end subroutine MPI_Comm_size

subroutine MPI_Bcast
end subroutine MPI_Bcast

subroutine MPI_send
end subroutine MPI_send

subroutine MPI_recv
end subroutine MPI_recv

subroutine MPI_scatter
end subroutine MPI_scatter

subroutine MPI_Reduce
end subroutine MPI_Reduce

subroutine MPI_Allreduce
end subroutine MPI_Allreduce

subroutine MPI_Abort
end subroutine MPI_Abort

subroutine MPI_Gather
end subroutine MPI_Gather

subroutine MPI_Gatherv
end subroutine MPI_Gatherv

