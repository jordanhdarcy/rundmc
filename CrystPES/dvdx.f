
      subroutine dvdx(mols,pot,j)

      use POTmodule
      implicit none
      type(POTinstance)::mols
      type(POTobject)::pot
      integer,intent(in)::j

      integer::i,k,n

c
c   calculates dv/dx from dv/dr*dr/dx
c
      if (NoForce) stop 'Why call dvdx with NoForce?'

      mols%dv(:,:,j)=0

      do i=1,pot%nbond
         mols%dv(pot%mb(i),:,j)=mols%dv(pot%mb(i),:,j)
     +                  +mols%dvdr(i,j)*mols%dr(i,pot%mb(i),:,j)
         mols%dv(pot%nb(i),:,j)=mols%dv(pot%nb(i),:,j)
     +                  +mols%dvdr(i,j)*mols%dr(i,pot%nb(i),:,j)
      enddo

      if (TranslationalSym) then
        k=pot%nbond+pot%natom
        do i=1,pot%natom
          mols%dv(i,3,j)=mols%dv(i,3,j)
     +             +mols%dvdh(i,j)*mols%dh(i,j)
          do n=1,pot%nsu
            mols%dv(i,:,j)=mols%dv(i,:,j)
     +             +mols%dvdsu(i,n,j)*mols%dsu(i,n,:,j)
          enddo
        enddo
      endif

!        write(*,*)mols%en(j)
!        write(*,*)mols%dv(1:2,:,j)
!        stop

      return                                              
      end                                                 
