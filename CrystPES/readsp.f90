subroutine readsp(pot,IN_SURFPERMS)
use InterpTrajData
implicit none
type(POTobject)::pot
character(len=*),intent(in)::IN_SURFPERMS
real(kind=8),dimension(:),allocatable::sm,sma
integer,dimension(3)::dof
integer::i,j,k,ssa,ssb,nau,l1,l2,n
logical::ll
character(len=100) :: line
character(len=6) :: str1
 ! Get size of surface perm group from unit 8
open(unit=8, file=IN_SURFPERMS)
  read(8,*)
  read(8,*)
  read(8,*)pot%ngsurf,pot%nsu
 ! get info about moving surface atoms (unit 10)
  inquire(file='IN_SURFDYN',exist=ll)
  if (ll) then
    open(unit=10,file='IN_SURFDYN')
    read(10,*)
    read(10,*)i
    pot%surfdyn=(i==1)
    if (pot%surfdyn) then
      read(10,*)
      read(10,*)ssa,ssb
      read(10,*)
      read(10,*)nau
      allocate(sm(nau),sma(nau*ssa*ssb*3))
      read(10,*)
      read(10,*)sm
      read(10,*)
      pot%nsdx=0
      do j=1,nau
        read(10,*)dof
        k=pot%nsdx+sum(dof)
        sma(pot%nsdx+1:k)=sm(j)
        pot%nsdx=k
      enddo
      do j=2,ssa
        sma((j-1)*pot%nsdx+1:j*pot%nsdx)=sma(1:pot%nsdx)
      enddo
      pot%nsdx=pot%nsdx*ssa
      do j=2,ssb
        sma((j-1)*pot%nsdx+1:j*pot%nsdx)=sma(1:pot%nsdx)
      enddo
      pot%nsdx=pot%nsdx*ssb
    endif
    close(10)
  else
    pot%surfdyn=.false.
  endif
 ! Now we have the surface info we need to...
  call AllocatePOTs(pot)
  if (pot%surfdyn) then
    pot%smass=sma(1:pot%nsdx)
    deallocate(sm,sma)
  endif
 !  read in the permutations for the surface coords
  do i=1,pot%ngsurf
    read(8,*)
    read(8,*)pot%ips(:,i)
    read(8,*)pot%isign(:,i)
  enddo
  close(8)
   ! bondlengths & periodic ccords:
  pot%nredundant=pot%nbond+pot%natom*pot%nsu
   ! Maybe heights:
  if (PeriodicDOF==2) pot%nredundant=pot%nredundant+pot%natom

  ! Jordan addition: read in permutations of fractional coordinates, not periodic coordinates
  if (JDHacks) then
    inquire(file='IN_FRACPERMS',exist=ll)
    if (.not.ll) stop "IN_FRACPERMS not found. Perhaps set JDHacks = .false.?"
    open(unit=8,file='IN_FRACPERMS',action='read')
    read(8,*) ! comment line
    pot%SGPerm = '     '
    do i=1,pot%ngsurf
      read(8,'(A)') line

      ! Fortran is bad at parsing strings, so I have to do it myself
      line = ADJUSTL(line)
      l1 = LEN_TRIM(line)
      l2 = 0
      n = 1

      do j=1,l1
        if (line(j:j).ne.' ') then
          l2 = l2+1
          pot%SGPerm(i,n)(l2:l2) = line(j:j)
          ! Check if the next character is blank...
          if (line(j+1:j+1).eq.' ') then
            ! Also check if the three coordinates have been read yet
            if (n.eq.3) then
              ! Finished reading this line, can break this loop
              exit
            else
              n = n+1
              l2 = 0
            endif
          endif
        endif
      enddo
    enddo
  endif
end
