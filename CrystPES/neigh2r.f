
      subroutine neighbour2(mols,pot,j)

      use POTmodule
      use MPI_Data
      use InterpTrajData, only: Fragmented
      implicit none
      type(POTinstance)::mols
      type(POTobject)::pot
      integer,intent(in)::j

c  modified 19-7-96 by kt to approximate totsum by totsum from previous dvdi
c  and to calculate an outer neighbour list

      real(kind=8),dimension(pot%nbond)::rinv
      real(kind=8)::test,wt1,t
      integer,dimension(ranks)::np0,el0
      integer::i,m,n,npow,i1,i2,n1,n2,nbh,k
      integer::m1,m2,np,e

      rinv=1/mols%r(:,j)

c  we set up the outer neighbour list
c  outer is the factor by which the outer wtol is less than the inner wtol
c  wtol is the cutoff for allowing points into the neibour list
c  nforco is the number of outer neighbours
c  mdao identifies the outer data point in POT that neighbour k comes from
c  ndao identifies which permutation outer neighbour k is

      mols%nforco(j)=0
      test=pot%wtol*pot%outer*mols%totsum(j)
      if(test.gt.1.d0)then
        npow=pot%lowp
      else
        npow=pot%ipow
      endif
      test=1.d0/test**(1.d0/npow)
      nbh=pot%nbond
      if (PeriodicDOF==2) nbh=nbh+pot%natom
55    continue
      if (Fragmented.or.(ranks==1)) then
        m1=1
        m2=pot%ndata
      else
        np=(pot%ndata-1)/ranks+1
        m1=rank*np+1
        m2=min((rank+1)*np,pot%ndata)
      endif
      do m=m1,m2
        do n1=1,pot%ngroup
          do n2=1,pot%ngsurf
            wt1=0
            n=(n1-1)*pot%ngsurf+n2
            if (.not.FrozenMolecule) then
              do i=1,pot%nbond
                i1=pot%ip(i,n1)
                t=rinv(i)-pot%rda(m,i1)
                wt1=t*t/pot%sumb(m,i1)+wt1
              enddo
            endif
            if (TranslationalSym) then
              if (PeriodicDOF==2) then
                do i=1,pot%natom
                  i1=pot%nmrep(n1,i)
                  t=mols%h(i,j)-pot%hda(m,i1)
                  wt1=wt1+t*t/pot%sumb(m,pot%nbond+i1)
                enddo
              endif
              do i=1,pot%natom
                do k=1,pot%nsu
                  i1=pot%nmrep(n1,i)
                  i2=pot%ips(k,n2)
                  t=mols%su(i,k,j)-pot%suda(m,i1,i2)*pot%isign(k,n2)
                  wt1=wt1+t*t/pot%sumb(m,nbh+(i1-1)*pot%nsu+i2)
                enddo
              enddo
            endif
            if (wt1<test) then
              mols%nforco(j)=mols%nforco(j)+1
              if (mols%nforco(j).gt.mols%maxfo) then
                if (.not.Fragmented.and.(ranks>1)) then
                  write(*,*)'nforco test failed, rank',rank
                  call MPI_Abort(MPI_COMM_WORLD,0)
                endif
                mols%nforco(j)=0
                test=0.5d0*test
                go to 55
              endif
              mols%mdao(mols%nforco(j),j)=m
              mols%ndao(mols%nforco(j),j)=n1
              if (TranslationalSym) mols%n2dao(mols%nforco(j),j)=n2
            endif
          enddo
        enddo
      enddo

      if (.not.Fragmented.and.(ranks>1)) then
!  This should probably be a series of MPI_allgatherv
        call MPI_Gather(mols%nforco(j),1,MPI_INTEGER,
     +                          np0,1,MPI_INTEGER,0,MPI_COMM_WORLD,e)
        if (rank==0) np=sum(np0)
        call MPI_Bcast(np,1,MPI_INTEGER,0,MPI_COMM_WORLD,e)
        if (np>mols%maxfo) then
          mols%nforco(j)=0
          test=0.5d0*test
          go to 55
        endif
        if (rank==0) then
          el0(1)=0
          do i=2,ranks
            el0(i)=el0(i-1)+np0(i-1)
          enddo
          call MPI_Gatherv(MPI_IN_PLACE,0,MPI_INTEGER,
     +                  mols%mdao(:,j),np0,el0,MPI_INTEGER,0,MPI_COMM_WORLD,e)
          call MPI_Gatherv(MPI_IN_PLACE,0,MPI_INTEGER,
     +                  mols%ndao(:,j),np0,el0,MPI_INTEGER,0,MPI_COMM_WORLD,e)
          if (TranslationalSym)
     +           call MPI_Gatherv(MPI_IN_PLACE,0,MPI_INTEGER,
     +                  mols%n2dao(:,j),np0,el0,MPI_INTEGER,0,MPI_COMM_WORLD,e)
          mols%nforco(j)=sum(np0)
        else
          call MPI_Gatherv(mols%mdao(:,j),mols%nforco(j),MPI_INTEGER,
     +                  np0,np0,np0,MPI_INTEGER,0,MPI_COMM_WORLD,e)
          call MPI_Gatherv(mols%ndao(:,j),mols%nforco(j),MPI_INTEGER,
     +                  np0,np0,np0,MPI_INTEGER,0,MPI_COMM_WORLD,e)
          if (TranslationalSym)
     +           call MPI_Gatherv(mols%n2dao(:,j),mols%nforco(j),MPI_INTEGER,
     +                  np0,np0,np0,MPI_INTEGER,0,MPI_COMM_WORLD,e)
        endif
        call MPI_Bcast(mols%nforco(j),1,MPI_INTEGER,0,MPI_COMM_WORLD,e)
        call MPI_Bcast(mols%mdao(:,j),mols%nforco(j),
     +                                       MPI_INTEGER,0,MPI_COMM_WORLD,e)
        call MPI_Bcast(mols%ndao(:,j),mols%nforco(j),
     +                                       MPI_INTEGER,0,MPI_COMM_WORLD,e)
        if (TranslationalSym)
     +          call MPI_Bcast(mols%n2dao(:,j),mols%nforco(j),
     +                                       MPI_INTEGER,0,MPI_COMM_WORLD,e)
      endif

      end

