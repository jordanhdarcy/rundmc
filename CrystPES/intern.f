
      subroutine intern(mols,pot,j)

      use POTmodule
      implicit none
      real(kind=8),parameter::Pi=3.1415926535897932384626433_8
      type(POTinstance)::mols
      type(POTobject)::pot
      integer,intent(in)::j

      real(kind=8),dimension(pot%natom)::y1,y2,y3
      integer::i
      logical,parameter::TwoD=(PeriodicDOF==2)

c  bond lengths

      do i=1,pot%nbond
         mols%r(i,j)=sqrt(sum((mols%c(pot%mb(i),:,j)-mols%c(pot%nb(i),:,j))**2))
      enddo

c  bond length derivatives

      mols%dr(:,:,:,j)=0
      do i=1,pot%nbond
        mols%dr(i,pot%mb(i),:,j)=(mols%c(pot%mb(i),:,j)-mols%c(pot%nb(i),:,j))/mols%r(i,j)
        mols%dr(i,pot%nb(i),:,j)=-mols%dr(i,pot%mb(i),:,j)
      enddo

      if (TranslationalSym) then
        mols%dsu(:,:,:,j)=0
!  heights
        if (TwoD) mols%h(:,j)=1/(mols%c(:,3,j)+pot%Z0depth)
!  in plane periodics
        y1=matmul(mols%c(:,1:PeriodicDOF,j),pot%RecipA(1:PeriodicDOF))
        y2=matmul(mols%c(:,1:PeriodicDOF,j),pot%RecipB(1:PeriodicDOF))
        mols%su(:,1,j)=sin(2*Pi*y1)
        mols%su(:,2,j)=cos(2*Pi*y1)
        mols%su(:,3,j)=sin(2*Pi*y2)
        mols%su(:,4,j)=cos(2*Pi*y2)
        if (pot%nsu==6) then
          y3=matmul(mols%c(:,1:PeriodicDOF,j),pot%RecipC(1:PeriodicDOF))
          mols%su(:,5,j)=sin(2*Pi*y3)
          mols%su(:,6,j)=cos(2*Pi*y3)
        endif
        if (SurfaceCoordsIncludeHeight) then
          do i=1,pot%natom
            mols%su(i,:,j)=mols%su(i,:,j)*mols%h(i,j)**2
          enddo
        endif
!  derivatives
        if (.not.NoForce) then
          do i=1,pot%natom
            if (TwoD) mols%dh(i,j)=-mols%h(i,j)**2
            mols%dsu(i,1,1:PeriodicDOF,j)=2*Pi*pot%RecipA(1:PeriodicDOF)*cos(2*Pi*y1(i))
            mols%dsu(i,2,1:PeriodicDOF,j)=-2*Pi*pot%RecipA(1:PeriodicDOF)*sin(2*Pi*y1(i))
            mols%dsu(i,3,1:PeriodicDOF,j)=2*Pi*pot%RecipB(1:PeriodicDOF)*cos(2*Pi*y2(i))
            mols%dsu(i,4,1:PeriodicDOF,j)=-2*Pi*pot%RecipB(1:PeriodicDOF)*sin(2*Pi*y2(i))
            if (pot%nsu==6) then
              mols%dsu(i,5,1:PeriodicDOF,j)=2*Pi*pot%RecipC(1:PeriodicDOF)*cos(2*Pi*y3(i))
              mols%dsu(i,6,1:PeriodicDOF,j)=-2*Pi*pot%RecipC(1:PeriodicDOF)*sin(2*Pi*y3(i))
            endif
          enddo
          if (.not.TwoD) then
             mols%dsu(:,:,:,j)=mols%dsu(:,(/1,3,5,2,4,6/),:,j)
          endif
        endif
        if (.not.TwoD) mols%su(:,:,j)=mols%su(:,(/1,3,5,2,4,6/),j)

      endif

      return
      end

