
      subroutine neighbour1(mols,pot,j)

      use POTmodule
      use InterpTrajData, only: Fragmented
      implicit none
      type(POTinstance)::mols
      type(POTobject)::pot
      integer,intent(in)::j

c  the original neighbour subroutine which calculates totsum from scratch

      real(kind=8),dimension(pot%ndata,pot%ngroup*pot%ngsurf)::vt
      real(kind=8),dimension(pot%nbond)::rinv
      real(kind=8)::test,wt1,t
      integer::i,k,i1,i2,m,n,n1,n2,nbh

c    assume a weighting function of 1/r**(2*ipow)

      mols%totsum(j)=0.d0
      rinv=1/mols%r(:,j)
      vt=0
      nbh=pot%nbond
      if (PeriodicDOF==2) nbh=nbh+pot%natom

      do m=1,pot%ndata
        do n1=1,pot%ngroup
          do n2=1,pot%ngsurf
            n=(n1-1)*pot%ngsurf+n2
            if (.not.FrozenMolecule) then
              do i=1,pot%nbond
                i1=pot%ip(i,n1)
                t=rinv(i)-pot%rda(m,i1)
                vt(m,n)=t*t/pot%sumb(m,i1)+vt(m,n)
              enddo
            endif
            if (TranslationalSym) then
              if (PeriodicDOF==2) then
                do i=1,pot%natom
                  i1=pot%nmrep(n1,i)
                  t=mols%h(i,j)-pot%hda(m,i1)
                  vt(m,n)=t*t/pot%sumb(m,pot%nbond+i1)+vt(m,n)
                enddo
              endif
              do i=1,pot%natom
                do k=1,pot%nsu
                  i1=pot%nmrep(n1,i)
                  i2=pot%ips(k,n2)
                  t=mols%su(i,k,j)-pot%suda(m,i1,i2)*pot%isign(k,n2)
                  vt(m,n)=t*t/pot%sumb(m,nbh+(i1-1)*pot%nsu+i2)+vt(m,n)
                enddo
              enddo
            endif
            vt(m,n)=1.d0/(vt(m,n)**pot%lowp+vt(m,n)**pot%ipow)
            mols%totsum(j)=mols%totsum(j)+vt(m,n)
          enddo
        enddo
      enddo

c  we set up the massive neighbour list
c  wtol is passed in common/shepdata
c  nforc is the number of neighbours
c  mda identifies the data point in POT that neighbour k comes from
c  nda identifies which permutation neighbour k is

      mols%nforco(j)=0
      test=pot%wtol*pot%outer

55    continue
      do m=1,pot%ndata
        do n1=1,pot%ngroup
          do n2=1,pot%ngsurf
            n=(n1-1)*pot%ngsurf+n2
            wt1=vt(m,n)/mols%totsum(j)
            if (wt1>test) then
              mols%nforco(j)=mols%nforco(j)+1
              if (mols%nforco(j)>mols%maxfo) then
                mols%nforco(j)=0
                test=2.d0*test
                go to 55
              endif
              mols%mdao(mols%nforco(j),j)=m
              mols%ndao(mols%nforco(j),j)=n1
              if (TranslationalSym) mols%n2dao(mols%nforco(j),j)=n2
            endif
          enddo
        enddo
      enddo

      end

