subroutine dvdi(mols,pot,j)

use InterpTrajData
use MPI_Data
implicit none
type(POTinstance)::mols
type(POTobject)::pot
integer,intent(in)::j

real(kind=8),dimension(pot%nbond,mols%nforc(j))::dvt,dtdr
real(kind=8),dimension(pot%natom,mols%nforc(j))::dvh,dtdh
real(kind=8),dimension(pot%natom,pot%nsu,mols%nforc(j))::dvsu,dtdsu
real(kind=8),dimension(pot%nint,mols%nforc(j))::z,dei
real(kind=8),dimension(pot%nbond)::sumdvt
real(kind=8),dimension(pot%natom)::sumdvh
real(kind=8),dimension(pot%natom,pot%nsu)::sumdvsu
real(kind=8),dimension(mols%nforc(j))::vt1,wt,ei
real(kind=8)::temp,ect
integer,dimension(MPI_STATUS_SIZE)::stat
integer::i,k,l,n,k1,nbh,j1,pp,e
logical::die

!  evaluate the energy and gradients

!  we will need to have some quantities for the gradients
!  of the weights
!  Note an approx here, due to neglect of small weights

!  calc the raw wts and totsum

vt1=0
do i=1,pot%nbond
  do k=mols%nstart,mols%nforc(j)
    temp=1.d0/mols%r(i,j)-pot%rda(mols%mda(k,j),pot%ip(i,mols%nda(k,j)))
    if (TranslationalSym) temp=temp*pot%scaleR
    vt1(k)= temp*temp + vt1(k)
  enddo
enddo
if (TranslationalSym) then
  if (PeriodicDOF==2) then
    do i=1,pot%natom
      do k=mols%nstart,mols%nforc(j)
        temp=mols%h(i,j)-pot%hda(mols%mda(k,j),pot%nmrep(mols%nda(k,j),i))
        temp=temp*pot%scaleH
        vt1(k)= temp*temp + vt1(k)
      enddo
    enddo
  endif
  do i=1,pot%natom
    do n=1,pot%nsu
      do k=mols%nstart,mols%nforc(j)
        temp=mols%su(i,n,j) &
                -pot%suda(mols%mda(k,j),pot%nmrep(mols%nda(k,j),i), &
                                        pot%ips(n,mols%n2da(k,j))) &
                              *pot%isign(n,mols%n2da(k,j))
        temp=temp*pot%scaleS
        vt1(k)= temp*temp + vt1(k)
      enddo
    enddo
  enddo
endif

if (.not.Fragmented.and.(ranks>1)) then
  if (rank==0) then
    do pp=1,ranks-1
      if (mols%nterms(pp)>0) &
        call MPI_recv(vt1(mols%term1(pp):mols%term1(pp)+mols%nterms(pp)-1), &
                        mols%nterms(pp), &
                MPI_DOUBLE_PRECISION,pp,MPI_ANY_TAG,MPI_COMM_WORLD,stat,e)
    enddo
  else
    call MPI_send(vt1,mols%nforc(j),MPI_DOUBLE_PRECISION,0,0,MPI_COMM_WORLD,e)
  endif
endif

!do i=1,mols%nforc(j)
!  print *, 'dp:', mols%mda(i,j), 'aperm:', mols%nda(i,j), 'spperm:', mols%n2da(i,j)
!enddo

!print *, 'nNeigh:', mols%nforc(j)


 ! huge(1.0_8)**(-1.0_8/real(pot%ipow,8))
if (any(vt1<1.0e-10_8)) vt1=vt1/minval(vt1)*1.0e-10_8
wt=vt1**(-pot%ipow)
mols%totsum(j)=sum(wt)
wt=wt/mols%totsum(j)

if (.not.Fragmented.and.(ranks>1)) then
  if (rank==0) then
    do pp=1,ranks-1
      if (mols%nterms(pp)>0) &
        call MPI_send(wt(mols%term1(pp):mols%term1(pp)+mols%nterms(pp)-1), &
                        mols%nterms(pp), &
                MPI_DOUBLE_PRECISION,pp,0,MPI_COMM_WORLD,e)
    enddo
  else
    call MPI_recv(wt,mols%nforc(j),MPI_DOUBLE_PRECISION,0,MPI_ANY_TAG,&
                                                   MPI_COMM_WORLD,stat,e)
  endif
endif

if (.not.NoForce) then
!  calc the derivatives of the raw weights v
! dvt is actually the derivative of the primitive weight 
!       (w.r.t. the underlying r, h & su coords)
! divided by the sum of the primitive weights

  do k=mols%nstart,mols%nforc(j)
    do i=1,pot%nbond
      dvt(i,k)=-2*(1.d0/mols%r(i,j) &
           -pot%rda(mols%mda(k,j),pot%ip(i,mols%nda(k,j))))/mols%r(i,j)**2
    enddo
    dvt(:,k)=-dvt(:,k)*pot%ipow*wt(k)/vt1(k)
    if (TranslationalSym) dvt(:,k)=dvt(:,k)*pot%scaleR**2
    if (TranslationalSym) then
      if (PeriodicDOF==2) then
        do i=1,pot%natom
          dvh(i,k)=2*(mols%h(i,j) &
                        -pot%hda(mols%mda(k,j),pot%nmrep(mols%nda(k,j),i)))&
                                        *pot%scaleH**2
          if (SurfaceCoordsIncludeHeight) then
            dvh(i,k)=dvh(i,k)+4*sum((mols%su(i,:,j) &
                        -pot%suda(mols%mda(k,j),pot%nmrep(mols%nda(k,j),i), &
                                pot%ips(:,mols%n2da(k,j))) &
                     *pot%isign(:,mols%n2da(k,j)))*mols%su(i,:,j)/mols%h(i,j)) &
                                        *pot%scaleS**2
          endif
        enddo
        dvh(:,k)=-dvh(:,k)*pot%ipow*wt(k)/vt1(k)
      endif
      do i=1,pot%natom
!  This is the derivative w.r.t. the trig part of su only
        if (.not.SurfaceCoordsIncludeHeight) stop 'This looks like a bug.  Why h?'
        dvsu(i,:,k)=2*(mols%su(i,:,j) &
                     -pot%suda(mols%mda(k,j),pot%nmrep(mols%nda(k,j),i), &
                        pot%ips(:,mols%n2da(k,j)))*pot%isign(:,mols%n2da(k,j)))&
                                *mols%h(i,j)**2*pot%scaleS**2
      enddo
      dvsu(:,:,k)=-dvsu(:,:,k)*pot%ipow*wt(k)/vt1(k)
    endif
  enddo

  if (.not.Fragmented.and.(ranks>1)) then
    if (rank==0) then
      do pp=1,ranks-1
        if (mols%nterms(pp)>0)  then
          call MPI_recv(dvt(:,mols%term1(pp):mols%term1(pp)+mols%nterms(pp)-1), &
                pot%nbond*mols%nterms(pp), &
                MPI_DOUBLE_PRECISION,pp,MPI_ANY_TAG,MPI_COMM_WORLD,stat,e)
          call MPI_recv(dvh(:,mols%term1(pp):mols%term1(pp)+mols%nterms(pp)-1), &
                pot%natom*mols%nterms(pp), &
                MPI_DOUBLE_PRECISION,pp,MPI_ANY_TAG,MPI_COMM_WORLD,stat,e)
          call MPI_recv(dvsu(:,:,mols%term1(pp):mols%term1(pp)+mols%nterms(pp)-1), &
                pot%natom*pot%nsu*mols%nterms(pp), &
                MPI_DOUBLE_PRECISION,pp,MPI_ANY_TAG,MPI_COMM_WORLD,stat,e)
        endif
      enddo
    else
      call MPI_send(dvt,pot%nbond*mols%nforc(j),MPI_DOUBLE_PRECISION,0,0,&
                                                        MPI_COMM_WORLD,e)
      call MPI_send(dvh,pot%natom*mols%nforc(j),MPI_DOUBLE_PRECISION,0,0,&
                                                        MPI_COMM_WORLD,e)
      call MPI_send(dvsu,pot%natom*pot%nsu*mols%nforc(j), &
                        MPI_DOUBLE_PRECISION,0,0,MPI_COMM_WORLD,e)
    endif
  endif

  sumdvt=sum(dvt,dim=2)
  if (PeriodicDOF==2) sumdvh=sum(dvh,dim=2)
  sumdvsu=sum(dvsu,dim=3)

  if (.not.Fragmented.and.(ranks>1)) then
    if (rank==0) then
      do pp=1,ranks-1
        if (mols%nterms(pp)>0)  then
          call MPI_send(sumdvt,pot%nbond,MPI_DOUBLE_PRECISION,pp,0,MPI_COMM_WORLD,e)
          call MPI_send(sumdvh,pot%natom,MPI_DOUBLE_PRECISION,pp,0,MPI_COMM_WORLD,e)
          call MPI_send(sumdvsu,pot%natom*pot%nsu,MPI_DOUBLE_PRECISION,pp,0, &
                        MPI_COMM_WORLD,e)
        endif
      enddo
    else
      call MPI_recv(sumdvt,pot%nbond,MPI_DOUBLE_PRECISION, &
                0,MPI_ANY_TAG,MPI_COMM_WORLD,e)
      call MPI_recv(sumdvh,pot%natom,MPI_DOUBLE_PRECISION, &
                0,MPI_ANY_TAG,MPI_COMM_WORLD,e)
      call MPI_recv(sumdvsu,pot%natom*pot%nsu,MPI_DOUBLE_PRECISION, &
                0,MPI_ANY_TAG,MPI_COMM_WORLD,e)
    endif
  endif

!  calc the derivatives of the relative weights w
 ! for convenience (I'm getting sick of writing MPI code) we do all on rank 0 as well
!  do k=mols%nstart,mols%nforc(j)
  do k=1,mols%nforc(j)
    dvt(:,k)=dvt(:,k)-wt(k)*sumdvt
    if (PeriodicDOF==2) dvh(:,k)=dvh(:,k)-wt(k)*sumdvh
    dvsu(:,:,k)=dvsu(:,:,k)-wt(k)*sumdvsu
  enddo
endif

!  construct the normal local coords for each neighbour
!  recalling that intern calculates bond lengths NOT inverses

!  !ocl vi(k)
nbh=pot%nbond
if (PeriodicDOF==2) nbh=nbh+pot%natom
do k=mols%nstart,mols%nforc(j)
  k1=mols%mda(k,j)
  do i=1,pot%nint
    z(i,k)=0.d0
    do l=1,pot%nbond
      z(i,k)=z(i,k)+pot%ut(k1,i,pot%ip(l,mols%nda(k,j)))/mols%r(l,j)
    enddo
    if (TranslationalSym) then
      if (PeriodicDOF==2) then
        do l=1,pot%natom
          z(i,k)=z(i,k) &
                +pot%ut(k1,i,pot%nbond+pot%nmrep(mols%nda(k,j),l))*mols%h(l,j)
        enddo
      endif
      do l=1,pot%natom
        do n=1,pot%nsu
          j1=pot%nsu*(pot%nmrep(mols%nda(k,j),l)-1)+pot%ips(n,mols%n2da(k,j))
          z(i,k)=z(i,k) &
                +pot%ut(k1,i,nbh+j1)*mols%su(l,n,j)*pot%isign(n,mols%n2da(k,j))
        enddo
      enddo
    endif
  enddo
enddo
do k=mols%nstart,mols%nforc(j)
  z(:,k)=z(:,k)-pot%zda(mols%mda(k,j),:)
enddo

!  calculate the potential energy: scalar term 

ei(mols%nstart:mols%nforc(j))=pot%v0(mols%mda(mols%nstart:mols%nforc(j),j))

!  linear term in energy plus diagonal part of quadratic term
!  first term in derivative of taylor poly wrt local coords

do i=1,pot%nint
  do k=mols%nstart,mols%nforc(j)
    ei(k) = z(i,k)*pot%v1(mols%mda(k,j),i) &
                   + 0.5d0*z(i,k)**2*pot%v2(mols%mda(k,j),i) + ei(k)
  enddo
enddo

if (.not.NoForce) then
  do i=1,pot%nint
    do k=mols%nstart,mols%nforc(j)
      dei(i,k) = z(i,k)*pot%v2(mols%mda(k,j),i) + pot%v1(mols%mda(k,j),i)
    enddo
  enddo


  !  the O(n^2) loop doesn't apply because we diagonalised d2v!!!
  
  !  transform derivatives of taylor poly wrt normal local coords into 
  !  derivs wrt bondlengths NOT inverses
  
  do i=1,pot%nbond
    do k=mols%nstart,mols%nforc(j)
      dtdr(i,k)=0.d0
      do l=1,pot%nint
        dtdr(i,k)=dei(l,k)*pot%ut(mols%mda(k,j),l,pot%ip(i,mols%nda(k,j))) &
                          + dtdr(i,k)
      enddo
    enddo
    dtdr(i,mols%nstart:mols%nforc(j))= &
                  -dtdr(i,mols%nstart:mols%nforc(j))/mols%r(i,j)**2
  enddo
  
  if (TranslationalSym) then
    if (PeriodicDOF==2) then
      do i=1,pot%natom
        do k=mols%nstart,mols%nforc(j)
          dtdh(i,k)=0.d0
          do l=1,pot%nint
            dtdh(i,k)=dtdh(i,k)+dei(l,k)*pot%ut(mols%mda(k,j),l, &
                                          pot%nbond+pot%nmrep(mols%nda(k,j),i))
            if (SurfaceCoordsIncludeHeight) &
                  dtdh(i,k)=dtdh(i,k)+dei(l,k)*2*sum(pot%ut(mols%mda(k,j),l, &
                                  nbh+(pot%nmrep(mols%nda(k,j),i)-1)*pot%nsu &
                                                  +pot%ips(:,mols%n2da(k,j))) &
                                  *pot%isign(:,mols%n2da(k,j)) &
                             *mols%su(i,:,j))/mols%h(i,j)
          enddo
        enddo
      enddo
    endif
  
    do i=1,pot%natom
      do n=1,pot%nsu
        do k=mols%nstart,mols%nforc(j)
          dtdsu(i,n,k)=0.d0
          do l=1,pot%nint
            if (SurfaceCoordsIncludeHeight) then
              dtdsu(i,n,k)=dtdsu(i,n,k)+dei(l,k)*pot%ut(mols%mda(k,j),l, &
                                  nbh+(pot%nmrep(mols%nda(k,j),i)-1)*pot%nsu &
                                                  +pot%ips(n,mols%n2da(k,j))) &
                                  *pot%isign(n,mols%n2da(k,j)) &
                             *mols%h(i,j)**2
            else
              dtdsu(i,n,k)=dtdsu(i,n,k)+dei(l,k)*pot%ut(mols%mda(k,j),l, &
                                  nbh+(pot%nmrep(mols%nda(k,j),i)-1)*pot%nsu &
                                                  +pot%ips(n,mols%n2da(k,j))) &
                                  *pot%isign(n,mols%n2da(k,j))
            endif
          enddo
        enddo
      enddo
    enddo
  endif
endif

if (.not.Fragmented.and.(ranks>1)) then
  if (rank==0) then
    do pp=1,ranks-1
      if (mols%nterms(pp)>0)  then
        call MPI_recv(ei(mols%term1(pp):mols%term1(pp)+mols%nterms(pp)-1), &
                mols%nterms(pp), &
                MPI_DOUBLE_PRECISION,pp,MPI_ANY_TAG,MPI_COMM_WORLD,stat,e)
        if (.not.NoForce) then
          call MPI_recv(dtdr(:,mols%term1(pp):mols%term1(pp)+mols%nterms(pp)-1), &
                pot%nbond*mols%nterms(pp), &
                MPI_DOUBLE_PRECISION,pp,MPI_ANY_TAG,MPI_COMM_WORLD,stat,e)
          call MPI_recv(dtdh(:,mols%term1(pp):mols%term1(pp)+mols%nterms(pp)-1), &
                pot%natom*mols%nterms(pp), &
                MPI_DOUBLE_PRECISION,pp,MPI_ANY_TAG,MPI_COMM_WORLD,stat,e)
          call MPI_recv(dtdsu(:,:,mols%term1(pp):mols%term1(pp)+mols%nterms(pp)-1),&
                pot%natom*pot%nsu*mols%nterms(pp), &
                MPI_DOUBLE_PRECISION,pp,MPI_ANY_TAG,MPI_COMM_WORLD,stat,e)
        endif
      endif
    enddo
  else
    call MPI_send(ei,mols%nforc(j),MPI_DOUBLE_PRECISION,0,0,MPI_COMM_WORLD,e)
    if (.not.NoForce) then
      call MPI_send(dtdr,pot%nbond*mols%nforc(j),MPI_DOUBLE_PRECISION,0,0,&
                                                        MPI_COMM_WORLD,e)
      call MPI_send(dtdh,pot%natom*mols%nforc(j),MPI_DOUBLE_PRECISION,0,0,&
                                                        MPI_COMM_WORLD,e)
      call MPI_send(dtdsu,pot%natom*pot%nsu*mols%nforc(j), &
                        MPI_DOUBLE_PRECISION,0,0,MPI_COMM_WORLD,e)
    endif
    return
  endif
endif

!  scale by relative weights, and their derivatives

mols%en(j)=sum(wt*ei)

if (.not.NoForce) then
  mols%dvdr(:,j)=0
  do i=1,pot%nbond
    do k=1,mols%nforc(j)
      mols%dvdr(i,j)=ei(k)*dvt(i,k)+wt(k)*dtdr(i,k)+mols%dvdr(i,j)
    enddo
  enddo

  if (TranslationalSym) then
    if (PeriodicDOF==2) mols%dvdh(:,j)=matmul(dvh,ei)+matmul(dtdh,wt)
    mols%dvdsu(:,:,j)=0
    do i=1,pot%natom
      do n=1,pot%nsu
        do k=1,mols%nforc(j)
          mols%dvdsu(i,n,j)=ei(k)*dvsu(i,n,k)+wt(k)*dtdsu(i,n,k)+mols%dvdsu(i,n,j)
        enddo
      enddo
    enddo
  endif
endif

mols%rms(j)=sqrt(sum(wt*(mols%en(j)-ei)**2))

!  look out for loony geometries

if (nlowstop/=0) then
  ect=pot%vmin-0.0005d0
  die=(mols%en(j).lt.ect)
  if (TranslationalSym.and.(PeriodicDOF==2)) then
    die=die.or.(any(mols%h(:,j)>5))
  endif
  if (die) then
    if (nloop.gt.1) then
      do n=1,pot%natom
        write(8,*)mols%c(n,:,j)
      enddo
      write(8,'(3g24.15,i3)')mols%en(j),mols%rms(j),mols%totsum(j),3
    endif
    nfin(j)=3
    nstop=2
  endif
endif

end

subroutine dvdis(mols,pot,t)
use InterpTrajData
use surface
implicit none
type(POTinstance)::mols
type(POTobject)::pot
integer,intent(in)::t
real(kind=8),dimension(CperUC*nc1*nc2,CperUC*nc1*nc2)::d2t
real(kind=8),dimension(CperUC*nc1*nc2,mols%nforc(t))::dvt,dx,dei
real(kind=8),dimension(mols%nforc(t))::vt1,wt,ei
real(kind=8),dimension(CperUC*nc1*nc2)::sumdvt
integer::i,j,k
logical::NotDataPoint
do i=1,mols%nforc(t)
  dx(:,i)=mols%sdx(:,t)-reshape(Translate1(sad(:,:,:,mols%mda(i,t)), &
                                                mols%nda(i,t),mols%n2da(i,t)),&
                                     (/CperUC*nc1*nc2/))
enddo
vt1=sum(dx**2,dim=1)
NotDataPoint=all(vt1>1.0e-12_8)
if (NotDataPoint) then
  wt=1.d0/(vt1**pot%ipow)
else
  where (vt1<=1.0e-12_8)
    wt=1
  elsewhere
    wt=0
  endwhere
endif
mols%totsum(t)=sum(wt)
wt=wt/mols%totsum(t)
if (NotDataPoint) then
   ! dvt is derivative of primitive wt divided by the sum of the primitive weights
  do i=1,mols%nforc(t)
    dvt(:,i)=-2*pot%ipow*dx(:,i)*wt(i)/vt1(i)
  enddo
  sumdvt=sum(dvt,dim=2)
   ! derivs of rel weight
  do i=1,mols%nforc(t)
    dvt(:,i)=dvt(:,i)-wt(i)*sumdvt
  enddo
endif
 ! put it together
ei=pot%v0(mols%mda(1:mols%nforc(t),t))
do i=1,mols%nforc(t)
  dei(:,i)=reshape(Translate1(dsad(:,:,:,mols%mda(i,t)),&
                         mols%nda(i,t),mols%n2da(i,t)),(/CperUC*nc1*nc2/))
  ei(i)=ei(i)+sum(dei(:,i)*dx(:,i))
  d2t=reshape(Translate2(d2sad(:,:,:,:,:,:,mols%mda(i,t)),&
                                        mols%nda(i,t),mols%n2da(i,t)), &
                     (/CperUC*nc1*nc2,CperUC*nc1*nc2/))
  do j=1,CperUC*nc1*nc2
    ei(i)=ei(i)+0.5*d2t(j,j)*dx(j,i)**2
    do k=1,j-1
      ei(i)=ei(i)+d2t(k,j)*dx(j,i)*dx(k,i)
    enddo
  enddo
  dei(:,i)=dei(:,i)+matmul(d2t,dx(:,i))
enddo
do i=1,CperUC*nc1*nc2
  mols%dvdsx(i)=sum(ei*dvt(i,:))
enddo
if (NotDataPoint) then
  do i=1,CperUC*nc1*nc2
    mols%dvdsx(i)=mols%dvdsx(i)+sum(wt*dei(i,:))
  enddo
endif
mols%en(t)=sum(wt*ei)
mols%rms(t)=sqrt(sum(wt*(mols%en(t)-ei)**2))
!write(*,*)mols%en(t)
!write(*,*)real(ei)
!write(*,*)real(wt)
!write(*,*)real(mols%dvdsx)
!stop
end subroutine dvdis
