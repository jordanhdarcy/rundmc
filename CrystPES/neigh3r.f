
      subroutine neighbour3(mols,pot,j)

      use POTmodule
      use MPI_Data
      use InterpTrajData, only: Fragmented
      implicit none
      type(POTinstance)::mols
      type(POTobject)::pot
      integer,intent(in)::j

c  modified 19-7-96 by kt to approximate totsum by totsum from previous dvdi
c  and to calculate an inner neighbour list from the outer one

      real(kind=8),dimension(pot%nbond)::rinv
      real(kind=8)::test,wt1,t
      integer,dimension(ranks)::np0,el0
      integer::i,k,i1,i2,npow,nbh,kk,m,n2
      integer::m1,m2,np,e

      rinv=1/mols%r(:,j)

c  we set up the inner neighbour list
c  wtol is the cutoff
c  nforc is the number of neighbours
c  mda identifies the data point in POT that neighbour k comes from
c  nda identifies which permutation neighbour k is

      mols%nforc(j)=0
      test=pot%wtol*mols%totsum(j)
      if(test.gt.1.d0)then
        npow=pot%lowp
      else
        npow=pot%ipow
      endif
      test=1.d0/test**(1.d0/npow)
      nbh=pot%nbond
      if (PeriodicDOF==2) nbh=nbh+pot%natom
      if (Fragmented.or.(ranks==1)) then
        m1=1
        m2=mols%nforco(j)
      else
        np=(mols%nforco(j)-1)/ranks+1
        m1=rank*np+1
        m2=min((rank+1)*np,mols%nforco(j))
      endif
!      do k=1,mols%nforco(j)
      do k=m1,m2
        wt1=0
        m=mols%mdao(k,j)
        if (.not.FrozenMolecule) then
          do i=1,pot%nbond
            i2=pot%ip(i,mols%ndao(k,j))
            t=rinv(i)-pot%rda(m,i2)
            wt1=t*t/pot%sumb(m,i2)+wt1
          enddo
        endif
        if (TranslationalSym) then
          if (PeriodicDOF==2) then
            do i=1,pot%natom
              i2=pot%nmrep(mols%ndao(k,j),i)
              t=mols%h(i,j)-pot%hda(m,i2)
              wt1=wt1+t*t/pot%sumb(m,pot%nbond+i2)
            enddo
          endif
          do i=1,pot%natom
            do kk=1,pot%nsu
              n2=mols%n2dao(k,j)
              i1=pot%nmrep(mols%ndao(k,j),i)
              i2=pot%ips(kk,n2)
              t=mols%su(i,kk,j)-pot%suda(m,i1,i2)*pot%isign(kk,n2)
              wt1=wt1+t*t/pot%sumb(m,nbh+(i1-1)*pot%nsu+i2)
            enddo
          enddo
        endif

        if (wt1.lt.test) then
           mols%nforc(j)=mols%nforc(j)+1
           if (mols%nforc(j).gt.mols%maxf) then
             write(*,*)' *** ABORT ***'
             write(*,*)' nforc.gt.maxf'
             write(*,*) ' neigh3: nforc =',mols%nforc(j)
             write(10,*)' *** ABORT ***'
             write(10,*)' nforc.gt.maxf'
             write(10,*) ' neigh3: nforc =',mols%nforc(j)
                if (ranks>1) call MPI_Abort(MPI_COMM_WORLD,0)
             stop
           endif
           mols%mda(mols%nforc(j),j)=mols%mdao(k,j)
           mols%nda(mols%nforc(j),j)=mols%ndao(k,j)
           if (TranslationalSym) mols%n2da(mols%nforc(j),j)=mols%n2dao(k,j)
         endif

      enddo

      if (.not.Fragmented.and.(ranks>1)) then
!  This should probably be a series of MPI_allgatherv
        call MPI_Gather(mols%nforc(j),1,MPI_INTEGER,
     +                          np0,1,MPI_INTEGER,0,MPI_COMM_WORLD,e)
        if (rank==0) then
          if (sum(np0)>mols%maxf) then
            write(*,*)np0,'=',sum(np0),'>',mols%maxf
            call MPI_Abort(MPI_COMM_WORLD,0)
          endif
          el0(1)=0
          do i=2,ranks
            el0(i)=el0(i-1)+np0(i-1)
          enddo
          call MPI_Gatherv(MPI_IN_PLACE,0,MPI_INTEGER,
     +                  mols%mda(:,j),np0,el0,MPI_INTEGER,0,MPI_COMM_WORLD,e)
          call MPI_Gatherv(MPI_IN_PLACE,0,MPI_INTEGER,
     +                  mols%nda(:,j),np0,el0,MPI_INTEGER,0,MPI_COMM_WORLD,e)
          if (TranslationalSym)
     +           call MPI_Gatherv(MPI_IN_PLACE,0,MPI_INTEGER,
     +                  mols%n2da(:,j),np0,el0,MPI_INTEGER,0,MPI_COMM_WORLD,e)
          mols%nforc(j)=sum(np0)
        else
          call MPI_Gatherv(mols%mda(:,j),mols%nforc(j),MPI_INTEGER,
     +                  np0,np0,np0,MPI_INTEGER,0,MPI_COMM_WORLD,e)
          call MPI_Gatherv(mols%nda(:,j),mols%nforc(j),MPI_INTEGER,
     +                  np0,np0,np0,MPI_INTEGER,0,MPI_COMM_WORLD,e)
          if (TranslationalSym)
     +           call MPI_Gatherv(mols%n2da(:,j),mols%nforc(j),MPI_INTEGER,
     +                  np0,np0,np0,MPI_INTEGER,0,MPI_COMM_WORLD,e)
        endif
        call MPI_Bcast(mols%nforc(j),1,MPI_INTEGER,0,MPI_COMM_WORLD,e)
        call MPI_Bcast(mols%mda(:,j),mols%nforc(j),
     +                                       MPI_INTEGER,0,MPI_COMM_WORLD,e)
        call MPI_Bcast(mols%nda(:,j),mols%nforc(j),
     +                                       MPI_INTEGER,0,MPI_COMM_WORLD,e)
        if (TranslationalSym)
     +          call MPI_Bcast(mols%n2da(:,j),mols%nforc(j),
     +                                       MPI_INTEGER,0,MPI_COMM_WORLD,e)
      endif

      return
      end

