
      subroutine neighbour(mols,pot,j)

      use POTmodule
      use InterpTrajData, only: Fragmented
      implicit none
      type(POTinstance)::mols
      type(POTobject)::pot
      integer,intent(in)::j

      real(kind=8),dimension(pot%ndata,pot%ngroup*pot%ngsurf)::vt
      real(kind=8)::t,wt1
      integer::i,m,n1,n2,nf,n,k

c    assume a weighting function of 1/r**(2*pot%ipow)

      mols%totsum(j)=0.d0
      vt=0
c  !ocl vi(k)
      !call grid(x,data)
      do m=1,pot%ndata
      do n1=1,pot%ngroup ! Loop over bond permutations (ngroup = 2 for H2)
      do n2=1,pot%ngsurf ! Loop over space group permutations (ngsurf = 192 for MOF5/Fm-3m)
         n=(n1-1)*pot%ngsurf+n2 ! An array index... runs from 1 ... ngroup*ngsurf I think
         if (.not.FrozenMolecule) then
           do i=1,pot%nbond !  Loop over inverse bond length coordinates, if molecule isn't frozen.
            t=1.d0/mols%r(i,j)-pot%rda(m,pot%ip(i,n1))
            if (TranslationalSym) t=t*pot%scaleR
            vt(m,n)=t*t+vt(m,n)
           enddo
         endif
         if (TranslationalSym) then
           if (PeriodicDOF==2) then
             do i=1,pot%natom
                t=mols%h(i,j)-pot%hda(m,pot%nmrep(n1,i))
                t=t*pot%scaleH
                vt(m,n)=t*t+vt(m,n)
             enddo
           endif
           do i=1,pot%natom
             do k=1,pot%nsu ! Loop over periodic coordinates - nsu should be 6.
               ! i, the atom, k is periodic coordinate (sin/cos, etc.), j the given
               ! walker/geometry/whatever
               ! m is data point, n1 is bond/atomic perm, n2 is space group perm
               ! pot%nmrep maps i to the correct atom in this atomic permutation (n1), I think
               ! pot%ips maps this periodic coordinate (k) in this space group permutation (n2)
               ! pot%isign gives the sign of this coordinate in this permutation (±1)
               ! Hey, I think I'm getting it
               t=mols%su(i,k,j)-pot%suda(m,pot%nmrep(n1,i),pot%ips(k,n2))
     +                                           *pot%isign(k,n2)
               t=t*pot%scaleS
               vt(m,n)=t*t+vt(m,n) ! Update raw weight
             enddo
           enddo
         endif
      enddo
      enddo
      enddo

      if (any(vt<1.0e-12_8)) then
        where (vt<1.0e-12_8)
          vt=1
        elsewhere
          vt=0
        endwhere
      else
        vt=vt**(-pot%ipow)
      endif
      mols%totsum(j)=sum(vt)


c  we set up the massive neighbour list
c  wtol is passed in common/shepdata
c  nforc is the number of neighbours
c  mda identifies the data point in POT that neighbour k comes from
c  nda identifies which permutation neighbour k is

      nf=0
!      t=0

      do m=1,pot%ndata ! Loop over everything as before
      do n1=1,pot%ngroup
      do n2=1,pot%ngsurf

        n=(n1-1)*pot%ngsurf+n2
         wt1=vt(m,n)/mols%totsum(j)
!      if (wt1>t) then
!        t=wt1
!        i=m
!        k=n2
!      endif

         if (wt1.gt.pot%wtol) then
             nf=nf+1
             if (nf>mols%maxf) then
               write(*,*)' *** ABORT ***'
               write(*,*)' nforc.gt.maxf'
               write(*,*)j,nf,mols%maxf
               write(10,*)' *** ABORT ***',j
               write(10,*)' nforc.gt.maxf'
               stop
             endif
             mols%mda(nf,j)=m ! Save the data point
             mols%nda(nf,j)=n1 ! Save the atomic permutation of this data point
             if (TranslationalSym) mols%n2da(nf,j)=n2 ! Save the space group perm of this data point
         endif

      enddo
      enddo
      enddo
      mols%nforc(j)=nf ! Save number of neighbours

!      write(*,*)t,i,k
!      write(*,*)maxval(vt(1:pot%ndata,1:pot%ngroup*pot%ngsurf)),mols%totsum(j)
!      write(*,*)real(vt(15,1:pot%ngroup*pot%ngsurf))
!      stop

      return
      end

      subroutine neighbours(mols,pot,t)
! Only called for natom==0 surface dynamics
      use POTmodule
      use surface
      implicit none
      type(POTinstance)::mols
      type(POTobject)::pot
      integer,intent(in)::t
      real(kind=8),dimension(pot%ndata,nc1,nc2)::vtmn
      real(kind=8)::rat
      integer::i,j,k,nf,m
      do i=1,pot%ndata
        do k=1,nc2
          do j=1,nc1
            vtmn(i,j,k)=sum((mols%sdx(:,t)
     +                  -reshape(Translate1(sad(:,:,:,i),j-1,k-1),
     +                                        (/CperUC*nc1*nc2/)))**2)
          enddo
        enddo
      enddo
      if (any(vtmn<1.0e-12_8)) then
        where (vtmn<1.0e-12_8)
          vtmn=1
        elsewhere
          vtmn=0
        endwhere
      else
        vtmn=1.d0/(vtmn**pot%ipow)
      endif
      mols%totsum(t)=sum(vtmn)
      nf=0
      do m=1,pot%ndata
        do k=1,nc2
          do j=1,nc1
            rat=vtmn(m,j,k)/mols%totsum(t)
            if(rat>pot%wtol)then
              nf=nf+1
              mols%mda(nf,t)=m
              mols%nda(nf,t)=j-1
              mols%n2da(nf,t)=k-1
            endif
          enddo
        enddo
      enddo
      mols%nforc(t)=nf
!      mols%nforc(t)=1
!      write(*,*)mols%nforc(t),mols%mda(1,t),mols%nda(1,t),mols%n2da(1,t)
      end subroutine neighbours
