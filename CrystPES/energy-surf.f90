program fred
use InterpTrajData
use surface
implicit none
type(POTobject)::potfile
type(POTinstance)::mols
integer::i,badness

 ! setup
if (MPI) stop 'Energy-surf needs to be built without MPI in interptraj'
if (SurfaceCoordsIncludeHeight.and.(PeriodicDOF==3)) stop 'There is no height in a 3D pot'
ranks=1
rank=0
Fragmented=.false.
i=0

!if (potfile%ipart==2) call readccm(potfile,'IN_SYSTEM',.false.,i,.false.) ! Terry, why?

call readccm(potfile,'IN_SYSTEM',.false.,i,.false.)

if ((PeriodicDOF==3).and..not.(FrozenMolecule.or.NoForce)) write(*,*)'Jordan, is that you?  Check your flags!'
nsurfa=0
nsurfc=0
CperUC=0
nc1=0
nc2=0
call readz(potfile,'')
if (PeriodicDOF==2) then
  call readsp(potfile,'IN_SURFPERMS')
else
  call readsp(potfile,'IN_LATTICEPERMS')
endif
call makebp(potfile,'IN_ATOMPERMS',.true.)
call cntl(potfile,'IN_CNT',.false.)
call AllocateMols(mols,1,InnerListSize,OuterListSize,potfile,1)
call readlatt(potfile)
call readt(potfile,'POT')
!call readcon(potfile,'CON')

 ! Now get some coordinates and calculate the energy
do
   ! silly element ordering
  do i=1,potfile%natom
    read(*,*,iostat=badness)mols%c(i,:,1)
  enddo
  if (badness/=0) exit
   ! Start the energy evaluation here
  call intern(mols,potfile,1)
  if (potfile%ipart==1) then
    call neighbour(mols,potfile,1)
    call dvdi(mols,potfile,1)
  else
    call neighbour1(mols,potfile,1)
    call neighbour2(mols,potfile,1)
    call neighbour3(mols,potfile,1)
    call dvdir(mols,potfile,1)
  endif
   ! for forces (might need modifying for 3D periodic):
!  call dvdx(mols,potfile,1)
  write(*,*)mols%en(1),mols%nforc(1),mols%rms(1),mols%totsum(1)
enddo


end
