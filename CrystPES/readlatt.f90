subroutine readlatt(pot)
use POTmodule
implicit none
type(POTobject)::pot
real(kind=8),dimension(PeriodicDOF)::a,b,c
real(kind=8)::vv,i,j
!         this file contains a header, then the x and y components
!         of the real space surface cell vectors, 1 per line
!         then another comment, and the depth of the height origin
open(unit=7,file='IN_LATTICE')
read(7,*)
read(7,*)a
read(7,*)b



if (PeriodicDOF==2) then
  read(7,*)
  read(7,*)pot%Z0depth
  read(7,*)
  read(7,*)
  read(7,*)
  read(7,*)i,j
  pot%SuperA=i*a(1:2)+j*b(1:2)
  read(7,*)i,j
  pot%SuperB=i*a(1:2)+j*b(1:2)
  vv=a(1)*b(2)-a(2)*b(1)
  pot%RecipA(1:2)=(/b(2),-b(1)/)/vv
  pot%RecipB(1:2)=(/-a(2),a(1)/)/vv
  pot%RecipC(1:2)=(/-b(2),-b(1)/)/vv
  pot%LattA(1:2)=a(1:2)
  pot%LattB(1:2)=b(1:2)
  pot%ClosestLatt=sqrt(minval((/sum(a**2),sum(b**2), &
                                sum((a+b)**2),sum((a-b)**2)/)))
  pot%LatticeInv(1,:)=pot%RecipA(1:2)
  pot%LatticeInv(2,:)=pot%RecipB(1:2)
else     ! PeriodicDOF==3
  read(7,*)c
  call crossproduct3(pot%RecipA,b,c)
  call crossproduct3(pot%RecipB,c,a)
  call crossproduct3(pot%RecipC,a,b)
  vv=sum(a*pot%RecipA)
  pot%LattA(1:3)=a(1:3)
  pot%LattB(1:3)=b(1:3)
  pot%LattC(1:3)=c(1:3)
  pot%RecipA=pot%RecipA/vv
  pot%RecipB=pot%RecipB/vv
  pot%RecipC=pot%RecipC/vv
endif
close(7)
end subroutine readlatt
