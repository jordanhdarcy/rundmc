module qdmc_structures

  implicit none
  !-----------------------
  ! qdmc parameters 
  !-----------------------
  type qdmc_par
    integer :: NumGeoms, WalkersPerGeom
    integer :: MinWalkers, MaxWalkers, Normwalkers, NumWalkers
    integer :: NumBlocks, StepsPerBlock,step
    integer :: NumGenerations, DelayBetweenGens, NumDescendantSteps
    real(kind=8) :: tau, feedback,Etrial, StepSize, sample
    integer :: BlocksToEquil
    real(kind=8), dimension(:,:,:), pointer :: EquilibriumGeom
    character(len=256) :: stem   ! used for file names
    logical :: restart
    logical :: lowe
    integer :: lowe_count
    logical :: printVectors, printObservables ! degubbing in descendant weighting.
    logical :: printDetails ! Whether to print Eref, Vbar, etc. for each step.
    logical :: dumpDWData ! set to 'true' when it's time to update DW save data.
    logical :: multiStep ! Track descendant weighting generations for multiple steps
    integer :: genToSave ! Descendant weighting generation to add to the DW output file.
  integer :: startHour, startMin, currHour, currMin ! Keeping track of number of hours/mins simulation has been running.
  real(kind=4) :: totHours, wallCutOff
  logical :: trackTime

  contains

    procedure, pass(this) :: new => qdmc_par_init
    procedure, pass(qdmc) :: readInput => read_qdmc
  end type qdmc_par
  !-----------------------
  !  a bunch of energies
  !-----------------------
  type qdmc_energies
    real(kind=8) :: trial,growth,block,average,av_sq
  end type qdmc_energies

  !------------------------
  !  a doodad for fragments
  !------------------------
   type dmc_frag
     real(kind=8), dimension(3) :: com  ! centre of mass
     real(kind=8), dimension(3,3) :: rm ! current rotation matrix
     real(kind=8), dimension(3) :: i ! current rotation matrix
   end type dmc_frag

contains
  !-------------------------
  !  qdmc_par constructor
  !-------------------------
  subroutine qdmc_par_init(this,n,ng)
    class (qdmc_par) :: this
    integer, intent(in) :: n
    integer, intent(in) :: ng
    integer :: ierr
    allocate(this%EquilibriumGeom(ng,n,3),stat=ierr)
    if (ierr.ne.0) stop 'Error allocating qdmc_par%EquilibriumGeom'
    this%NumGeoms = ng
    this%WalkersPerGeom = 0
    this%MinWalkers = 0
    this%MaxWalkers = 0
    this%NormWalkers = 0
    this%NumWalkers = 0
    this%NumBlocks = 0
    this%StepsPerBlock = 0
    this%step = 0
    this%NumGenerations = 0
    this%DelayBetweenGens = 0
    this%NumDescendantSteps = 0
    this%lowe_count = 0
    this%dumpDWData = .false.
    this%printDetails = .false.
    this%genToSave = -1
  this%startHour = -1
  this%startMin = -1
  this%currHour = -1
  this%currMin = -1
  this%totHours = 0.0
  this%wallCutOff = 0.0
  this%trackTime = .false.
  this%restart = .false.
    return
  end subroutine qdmc_par_init







subroutine read_qdmc(qdmc,sys)

!  this subroutine reads in the parameters needed for the
!  quantum diffusion Monte Carlo simulation on the 
!  interpolated surface
!  written 25/2/2003 by dlc

  use molecule_specs, ONLY : molsysdat
  CLASS(qdmc_par) :: qdmc
  character(len=80) icomm
  type (molsysdat), intent(in) :: sys

  integer :: i,j,k, ng
  integer :: total_steps, total_descendant_steps

  write(11,*) '----------------------------------------------------'
  write(11,*) ''
  write(11,*) ' read diffusion monte carlo parameters from IN_QDMC '
  write(11,*) ''

  open (unit=7,file='IN_QDMC',status='old')

! read comment; a header for the IN_QDMC file

  read(7,80) icomm
  write (11,80) icomm
80      format(a80)

! read in the number of seed geometries

  read(7,80) icomm
  write(11,80) icomm
  read(7,*) ng
  write(11,*) ng

! create a qdmc_par object

  call qdmc%new(sys%natom,ng)

!  read in the details of numbers of replicas

  read(7,80)icomm
  write(11,80) icomm
  read(7,*) qdmc%MinWalkers, qdmc%MaxWalkers, qdmc%WalkersPerGeom
  write(11,*) qdmc%MinWalkers, qdmc%MaxWalkers, qdmc%WalkersPerGeom

!  read in the number of blocks, and number until equilibration

  read(7,80)icomm
  write(11,80) icomm
  read(7,*) qdmc%NumBlocks, qdmc%BlocksToEquil, qdmc%StepsPerBlock
  write(11,*) qdmc%NumBlocks, qdmc%BlocksToEquil, qdmc%StepsPerBlock

!  read in the descendant weighting parameters

  read(7,80)icomm
  write(11,80)icomm
  read(7,*) qdmc%NumGenerations, qdmc%DelayBetweenGens, qdmc%NumDescendantSteps
  write(11,*) qdmc%NumGenerations, qdmc%DelayBetweenGens, qdmc%NumDescendantSteps

!  read in the time step

  read(7,80)icomm
  write(11,80) icomm
  read(7,*) qdmc%tau
  write(11,*) qdmc%tau

!  read in the feedback parameter alpha

  read(7,80)icomm
  write(11,80) icomm
  read(7,*) qdmc%feedback
  write(11,*) qdmc%feedback

!  read in the maximum step size for initial displacements

  read(7,80)icomm
  write(11,80) icomm
  read(7,*) qdmc%StepSize
  write(11,*) qdmc%StepSize

!  read in the probability of writing a walker to TOUT

  read(7,80)icomm
  write(11,80) icomm
  read(7,*) qdmc%sample
  write(11,*) qdmc%sample


8 format(a1)



!  read in equilibrium or near-equilibrium geometry

  do j=1,qdmc%NumGeoms
    read(7,80)icomm
    write(11,80) icomm
    do i=1,sys%natom
      read(7,*)(qdmc%EquilibriumGeom(j,i,k),k=1,3)
      write(11,*)(qdmc%EquilibriumGeom(j,i,k),k=1,3)
    enddo
  enddo

  close (unit=7)

!  setting the normal number of walkers to be number of seed geometries
!  times the number of walkers per geometry

   qdmc%NormWalkers=qdmc%NumGeoms*qdmc%WalkersPerGeom

!  sanity checks

  if (qdmc%NormWalkers > qdmc%MaxWalkers) then
      print *, ' ERROR: NormWalkers > MaxWalkers '
      print *, '      : qdmc%NormWalkers ', qdmc%NormWalkers
      print *, '      : qdmc%MaxWalkers  ', qdmc%MaxWalkers
      call exit(1)
  endif

  if (qdmc%NormWalkers < qdmc%MinWalkers) then
      print *, ' ERROR: NormWalkers < MinWalkers '
      print *, '      : qdmc%NormWalkers ', qdmc%NormWalkers
      print *, '      : qdmc%MinWalkers  ', qdmc%MinWalkers
      call exit(1)
  endif

  if (qdmc%MinWalkers > qdmc%MaxWalkers) then
      print *, ' ERROR: MinWalkers > MaxWalkers '
      print *, '      : qdmc%MaxWalkers  ', qdmc%MaxWalkers
      print *, '      : qdmc%MinWalkers  ', qdmc%MinWalkers
      call exit(1)
  endif

  if (qdmc%BlocksToEquil > qdmc%NumBlocks) then
      print *, ' ERROR: BlocksToEquil > NumBlocks '
      print *, '      : qdmc%BlocksToEquil  ', qdmc%BlocksToEquil
      print *, '      : qdmc%NumBlocks      ', qdmc%NumBlocks
      call exit(1)
  endif

  total_steps = (qdmc%NumBlocks - qdmc%BlocksToEquil)*qdmc%StepsPerBlock
  total_descendant_steps = (qdmc%NumGenerations-1)*qdmc%DelayBetweenGens + qdmc%NumDescendantSteps+1
  !total_descendant_steps = (1+qdmc%NumGenerations)*qdmc%DelayBetweenGens +&
  !                        qdmc%NumDescendantSteps
  
  if (total_steps < total_descendant_steps) then
      print *, 'ERROR: total_steps < total_descendant_steps '
      print *, '     : total_steps            ', total_steps
      print *, '     : total_descendant_steps ', total_descendant_steps
      call exit(1)
  endif

  ! Check that, if we're tracking generations for multiple steps, that the number of tracking
  ! steps is a power of 2 - that is, log2(steps) is an integer.
  if (qdmc%multiStep) then
    !if (LOG(qdmc%NumDescendantSteps)/LOG(2))
    if (MODULO(DLOG(real(qdmc%NumDescendantSteps,8))/DLOG(2.0d0),1.0d0).ne.0.0d0) then
      print *, 'Need a descendant weighting tracking time, x, such that log2(x) is an integer.'
      stop
    endif
  endif

  return
end subroutine read_qdmc



end module qdmc_structures

