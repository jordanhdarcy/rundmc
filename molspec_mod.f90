!  parameters read in from IN_SYSTEM
module molecule_specs
   implicit none
   !private ! temporary public!

   public :: molsysdat
   public :: amu2au, bohr2m, twopi, kJ, kCal, au2ang, pi

   real(kind=8), parameter :: amu2au = 1822.888481
   real(kind=8), parameter :: bohr2m = 5.29177d-11
   real(kind=8), parameter :: twopi = 6.28318530717959
   real(kind=8), parameter :: kJ = 2625.4973
   real(kind=8), parameter :: kCal = kJ/4.184
   real(kind=8), parameter :: au2ang = 0.52917720859d0
   real(kind=8), parameter :: pi = 2.d0*dacos(0.d0)
   integer, parameter :: max_frag = 2

    !===========================================================
    ! A type to store details about non-rigid bonds in the system.
    !===========================================================
    type, private :: tNonRigid
        integer :: i, j ! index of bond, atom i to j.
    integer :: ind ! index of this bond in bonds array.
    end type tNonRigid

   ! define the type
   type molsysdat
     integer natom,nbond,nint,ngroup,nRigidBonds,nNRbonds
   integer, dimension(1:624) :: iseed
     integer nfrag
     character(len=70) :: title
     character(len=3), dimension(:), pointer ::  atom_label
     real(kind=8), dimension(:), pointer :: mass
     integer, dimension(:), pointer :: nb,mb
     integer, dimension(:,:), pointer :: aperm,bperm, nonRigidbperm
     real(kind=8), dimension(:), allocatable :: bondLengths
   type(tNonRigid), dimension(:), allocatable :: nonRigidBonds


  ! These are all specific to MOF-5 and can be safely ignored otherwise
   logical :: MOF5Hacks = .false.
   logical :: halfCell = .false.
   logical :: heatMap = .false.
   logical :: pbc = .false.
   real(kind=8), dimension(3,3) :: uVectors, lVectors, recipV
   real(kind=8) :: length
   logical :: RDF = .false.
   logical :: QH2Hacks = .false.

   contains


    procedure, pass(this) :: new => MolSysDat_init
    procedure, pass(this) :: perms => MolSysDat_perms
    procedure, pass(sys) :: readData => read_system_data
    procedure, pass(sys) :: readPerms => bondperm
    procedure, pass(this) :: readUnitVectors => readUnitVectors
    procedure, pass(this) :: cartToFrac => cartToFrac
    procedure, pass(this) :: fracToCart => fracToCart

   end type molsysdat
   ! declare the constructor



 contains
   ! the allocation routine
   subroutine MolSysDat_init(this,n,title)
     class(molsysdat) :: this
     integer, intent(in) :: n
     character(len=70) :: title
     integer :: ierr, i
     this%natom = n
     this%nfrag = 0
     this%nbond = n*(n-1)/2
     if (n > 2) then
       this%nint  = 3*n-6
     else
       this%nint  = 3*n-5
     endif
     this%title = title
     allocate(this%atom_label(n),stat=ierr)
     if (ierr.ne.0) stop ' molsysdat allocation error: atom_label '
     allocate(this%mass(n),stat=ierr)
     if (ierr.ne.0) stop ' molsysdat allocation error: mass '
     allocate(this%nb(this%nbond),stat=ierr)
     if (ierr.ne.0) stop ' molsysdat allocation error: nb '
     allocate(this%mb(this%nbond),stat=ierr)
     if (ierr.ne.0) stop ' molsysdat allocation error: mb '
     allocate(this%bondLengths(1:this%nbond),stat=ierr)
     if (ierr.ne.0) stop ' molsysdat allocation error: bondLengths '
     return


   ! Set unit vectors to cartesian unit vectors - they'll be overwritten if necessary.
   this%uVectors = 0.0d0
   do i=1,3
    this%uVectors(i,i) = 1.0d0
   enddo

   end subroutine MolSysDat_init

   ! routine to allocate pertumation structures
   subroutine MolSysDat_perms(this,n)
     class(molsysdat) :: this
     integer, intent(in) :: n
     integer :: ierr
     this%ngroup=n
     allocate(this%aperm(this%natom,this%ngroup),stat=ierr)
     if (ierr.ne.0) stop ' molsysdat allocation error: aperm '
     allocate(this%bperm(this%nbond,this%ngroup),stat=ierr)
     if (ierr.ne.0) stop ' molsysdat allocation error: bperm '
   allocate(this%nonRigidbperm(this%nNRbonds,this%ngroup),stat=ierr)
   if (ierr.ne.0) stop ' molsysdat allocation error: nonRigidbperm '
     return
   end subroutine MolSysDat_perms



   !=============================================
   ! Read system data from given IN_SYSTEM file.
   !=============================================
   subroutine read_system_data(sys,sysFile)
      implicit none

      class(molsysdat) :: sys
      character(*), intent(in) :: sysFile
      integer n,i,j,k
      integer length, ifin

      character*79 title,comment_line
      character*800 elements
      character*1400 masses

!  announce out intentions

      write(11,*) '---------------------------------------------------'
      write(11,*) ''
      write(11,*) '   read in molecule specifications from ', trim(sysFile)
      write(11,*) ''

      open(unit=7,file=trim(sysFile),status='old')

!  read comment, which is a header for the SYSTEM file

      read(7,82) title
      write(11,82) title
80    format(a800)
81    format(a200)
82    format(a80)

!  read in the actual number of atoms

      read(7,82) comment_line
      read(7,*)  n

      write(11,82) comment_line
      write(11,*)  n

!  read in the element labels for the atoms

      read(7,82) comment_line
      read(7,80) elements

      write(11,82) comment_line
      write(11,*) trim(elements)

!  read in masses of atoms into a string for now

      read(7,82) comment_line
      read(7,161) masses

      write(11,82) comment_line
      write(11,160) masses
160   format(a200)
161   format(a1400)

!  create a new molsysdat object

      call sys%new(n,title)

!  extract the elemental labels from ielements

      do i=1,sys%natom-1
         length = len(elements)
         ifin   = index(elements,',')
         if (ifin.gt.3) then
           write(6,*)' atomic label in SYSTEM has too many characters'
           write(6,*) elements
           call exit(1)
         endif
         sys%atom_label(i) = elements(1:ifin-1)
         elements = elements(ifin+1:length)
      enddo
      ifin = index(elements,' ')
      sys%atom_label(sys%natom) = elements(1:ifin-1)

!  read in the atomic masses from masses string saved earlier

      do i=1,sys%natom-1
         length = len(masses)
         ifin   = index(masses,',')
         read(masses(1:ifin-1),*) sys%mass(i)
         masses = masses(ifin+1:length)
      enddo
      ifin = index(masses,' ')
      read(masses(1:ifin-1),*) sys%mass(sys%natom)

      ! convert mass in amu to atomic units
      sys%mass = sys%mass*amu2au

      write(11,*) 'atomic masses in atomic units (which are 1822*amu) '
      write(11,*) (sys%mass(i),i=1,sys%natom)

!  given that we need all the atom-atom bonds,
!  we just assign the indirect addresses of the bonds

      k=1
      do i=1,sys%natom-1
      do j=i+1,sys%natom
         sys%mb(k)=i
         sys%nb(k)=j
         k=k+1
      enddo
      enddo

      sys%bondLengths = 0.0d0

!  for QDMC that's all we're interested in.

      close(unit=7)

      return
      end subroutine read_system_data






      !=========================================================
      ! Reads in atom permutations from given IN_ATOMPERMS file.
      !=========================================================
      subroutine bondperm(sys,permsFile)

      class(molsysdat) :: sys
      character(*), intent(in) :: permsFile

      character(len=80) icomm
      integer idummy
      integer i,j,k,n,ic

!  read atomic permutations into the aperm array

      open(unit=8,file=trim(permsFile),status='old')

      read(8,80)icomm
80    format(a80)

!  read in a comment line and the order of the group

      read(8,80)icomm
      read(8,*) n

!  allocate sys%aperm and sys%bperm

      call sys%perms(n)

!  read in the atomic perm

      do n=1,sys%ngroup
        read(8,80)icomm
        do k=1,sys%natom
          read(8,*)idummy,sys%aperm(k,n)   ! idummy will equal k
        enddo
      enddo

      close(unit=8)

!  now set up the bperm array of bond permutations by looking at aperm

      do n=1,sys%ngroup

    ! Do all the bond permutations.
        ic=0
        do i=1,sys%natom-1
        do j=i+1,sys%natom

          ic=ic+1
          do k=1,sys%nbond
            if (sys%mb(k).eq.sys%aperm(i,n).and.sys%nb(k).eq.sys%aperm(j,n)) then
              sys%bperm(k,n)=ic
              !print *, 'mb(k):', sys%mb(k), 'nb(k):', sys%nb(k)
              !print *, 'bond no:', k, 'perm:', n, 'index:', sys%bperm(k,n)
            endif

            if (sys%nb(k).eq.sys%aperm(i,n).and.sys%mb(k).eq.sys%aperm(j,n)) then
               sys%bperm(k,n)=ic
               !print *, 'mb(k):', sys%mb(k), 'nb(k):', sys%nb(k)
               !print *, 'bond no:', k, 'perm:', n, 'index:', sys%bperm(k,n)
            endif
          enddo
        enddo
        enddo

        ic=1
        do i=1,sys%natom-1
        do j=i+1,sys%natom
        ! Do just the non-rigid permutations.

          do k=1,sys%nNRbonds

             if (sys%nonRigidBonds(k)%i.eq.sys%aperm(i,n).and.sys%nonRigidBonds(k)%j.eq.sys%aperm(j,n)) then
                sys%nonRigidbperm(k,n)=ic
      !          print *, 'atom i:', sys%nonRigidBonds(k)%i, 'atom j:', sys%nonRigidBonds(k)%j
      !          print *, 'bond no:', k, 'perm:', n, 'ic:', ic
                ic=ic+1
             endif
             if (sys%nonRigidBonds(k)%j.eq.sys%aperm(i,n).and.sys%nonRigidBonds(k)%i.eq.sys%aperm(j,n)) then
                sys%nonRigidbperm(k,n)=ic
      !          print *, 'atom i:', sys%nonRigidBonds(k)%i, 'atom j:', sys%nonRigidBonds(k)%j
      !          print *, 'bond no:', k, 'perm:', n, 'ic:', ic
                ic=ic+1
             endif
          enddo

        enddo
        enddo


      enddo

      return
      end subroutine bondperm


    !=========================================================
    ! Reads in unit vectors from IN_LATTICE file.
    !=========================================================
    subroutine readUnitVectors(this)
     use vec_mod
     class(molsysdat) :: this
     integer :: unit, i, j
     real(kind=8) :: sumV

     open(newunit=unit,file='IN_LATTICE',status='unknown',action='read')

     read(unit,*)

     ! Unit vectors (in bohr) are on the next three lines
     do i=1,3
      read(unit,*) (this%uVectors(i,j),j=1,3)
      this%lVectors(i,:) = this%uVectors(i,:)
      ! Now normalise
      this%uVectors(i,1:3) = normalise(this%uVectors(i,1:3))
      !print *, dsqrt(DOT_PRODUCT(this%uVectors(i,1:3),this%uVectors(i,1:3)))
     enddo

     this%length = DSQRT(DOT_PRODUCT(this%lVectors(1,:),this%lVectors(1,:)))

     ! Get reciprocal lattice vectors, in case we need them for periodic boundary conditions
     this%recipV(1,:) = cross(this%lVectors(2,:),this%lVectors(3,:))
     this%recipV(2,:) = cross(this%lVectors(3,:),this%lVectors(1,:))
     this%recipV(3,:) = cross(this%lVectors(1,:),this%lVectors(2,:))

     sumV = DOT_PRODUCT(this%recipV(1,:),this%lVectors(1,:))

     this%recipV(:,:) = this%recipV(:,:)/sumV


     ! That's all
     close(unit)

    end subroutine readUnitVectors

    !=========================================================
    ! Converts the given cartesian coordinates into fractional
    ! coordinates.
    !=========================================================
    function cartToFrac(this,n,carts) result(frac)
      class(molsysdat) :: this
      integer, intent(in) :: n
      real(kind=8), dimension(n,3), intent(in) :: carts
      real(kind=8), dimension(n,3) :: frac
      integer :: i

      do i=1,n
        frac(i,:) = MATMUL(this%recipV,carts(i,:))
      enddo
    end function cartToFrac


    !=========================================================
    ! Converts the given fractional coordinates into cartesian
    ! coordinates.
    !=========================================================
    function fracToCart(this,n,frac) result(carts)
      class(molsysdat) :: this
      integer, intent(in) :: n
      real(kind=8), dimension(n,3), intent(in) :: frac
      real(kind=8), dimension(n,3) :: carts
      integer :: i, j


      !do j=1,3
      !  carts(:,j) = frac(:,1)*this%lVectors(1,j)+frac(:,2)*this%lVectors(2,j)+frac(:,3)*this%lVectors(3,j)
      !enddo
      do i=1,n
        carts(i,:) = MATMUL(frac(i,:),this%lVectors(:,:))
      enddo


    end function fracToCart


end module molecule_specs
