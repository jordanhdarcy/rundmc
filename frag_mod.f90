! parameters read in from IN_FRAG
module fragment_mod
   implicit none
   ! define fragment data type
   ! indx is an array containing references to atoms in the full molecule
   type frag_par
     integer natom
     real(kind=8) :: mass
     character(len=80) :: title
     integer, dimension(:), pointer :: indx
     real(kind=8), dimension(3) :: i ! principle moments of inertia
     real(kind=8), dimension(3) :: centre ! centre of mass at initial geom
     real(kind=8), dimension(3,3) :: ax ! principal axes
   logical :: fixed
   end type frag_par

   interface frag_init
      module procedure frag_init
   end interface


contains

   subroutine frag_init(this,f_natom,f_indx,title)
      type (frag_par), intent(out) :: this
      integer, dimension(:), intent(in) :: f_indx
      integer, intent(in) :: f_natom
      character(len=70), intent(in) :: title
      integer :: j,ierr
      this%title = title
      this%natom = f_natom
      this%mass = 0.d0
      allocate(this%indx(f_natom),stat=ierr)
      if (ierr.ne.0) stop ' fragment_mod allocation error: frag%indx '
      do j=1,f_natom
        this%indx(j) = f_indx(j)
      enddo
    this%fixed = .false.
   end subroutine frag_init



  subroutine read_fragment_data(sys,frag,fragFile)
      use molecule_specs

      type (molsysdat), intent(inout) :: sys
      type (frag_par), dimension(:), allocatable, intent(out) :: frag
      character(*), intent(in) :: fragFile
      
      integer, dimension(:), allocatable :: f_indx,n_indx
      integer n,i,ierr,k
      integer f_natom
      integer nonfrag

      character*80 title,comment_line
      

  !  announce out intentions

      write(11,*) '---------------------------------------------------'
      write(11,*) ''
      write(11,*) '   read in fragment specifications from IN_FRAG '
      write(11,*) ''

      open(unit=7,file=trim(fragFile),status='old')

  !  read comment, which is a header for the FRAG file

      read(7,80) title
      write(11,81) title
  80    format(a79)
  81    format(a80)

  !  read in number of fragments

      read(7,80) comment_line
      read(7,*) sys%nfrag
      
      write(11,81) comment_line
      write(11,*) sys%nfrag

  ! allocate space for fragment data type and atom index array

      allocate(frag(0:sys%nfrag),stat=ierr)
      if (ierr.ne.0) stop ' Error allocating frag array '

      allocate(f_indx(sys%natom),n_indx(sys%natom),stat=ierr)
      if (ierr.ne.0) stop ' Error allocating f_indx or n_indx array '


  ! for each fragment...
      do k=1,sys%nfrag

  !  read in number of atoms

      read(7,80) comment_line
      read(7,*) f_natom

      write(11,81) comment_line
      write(11,*) f_natom

  !  read in fragment label

      read(7,80) comment_line 
      write(11,81) comment_line 
   
      read(7,*) title
      write(11,*) title

  !  read atom indexes of fragment
   
      read(7,80)   comment_line
      read(7,*)   (f_indx(i),i=1,f_natom)
      write(11,80) comment_line
      write(11,*) (f_indx(i),i=1,f_natom)





  !  assign to fragment object

      call frag_init(frag(k),f_natom,f_indx,title)

  ! read in whether to fix these atoms ('yes'), or treat them as rigid rotors.
      read(7,80)   comment_line
      write(11,80) comment_line
      read(7,80)   comment_line
      comment_line = ADJUSTL(comment_line)
      write(11,80) comment_line

      if (comment_line == 'yes') then
        frag(k)%fixed = .true.
        write(11,*) 'fragment', k, 'will be fixed in space.'
      endif

      enddo




  ! create a list containing all of the atoms that belong to a fragment

      f_natom = 0
      do n=1,sys%natom 
      do k=1,sys%nfrag 
        if ( any(n.eq.frag(k)%indx(:))) then
        f_natom = f_natom + 1
        f_indx(f_natom) = n
        endif
      enddo
      enddo

  !  now create a 3rd fragment object which contains atoms not in any fragment

      nonfrag = 0
      do n=1,sys%natom
      if (all(n.ne.f_indx(:))) then
        nonfrag = nonfrag + 1
        n_indx(nonfrag) = n
      endif
      enddo

  ! assign leftover atoms to fragment object
      title = 'leftovers'

      call frag_init(frag(0),nonfrag,n_indx,title)


  ! debug: check fragment assignment
  !      write(*,*) 'fragment indicies'
  !      do k=0,sys%nfrag
  !        write(*,*) k
  !        write(*,*) frag(k)%indx(:)
  !      enddo

  !  we're done!
       
      close(unit=7)

      deallocate(f_indx,n_indx)
      return
  end subroutine read_fragment_data

end module fragment_mod
