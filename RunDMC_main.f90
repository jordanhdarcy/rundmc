!=======================================================================
! Diffusion Monte Carlo on spline, modified Shepard or any
! combination of the two PESs. A vastly renovated version of qdmc90,
! brought kicking and screaming into the 21st century by Jordan D'Arcy.
!
! Available on the App store.
!=======================================================================

program RunDMC

  use molecule_specs, ONLY : molsysdat
  use PES_mod, ONLY : tPES
  use gasPES_mod, ONLY : tGasPES
  use crystPES_mod, ONLY : tCrystPES
  use DMC_mod, ONLY : tDMCSim
  use Utilities, ONLY : read_iseed
  use Args_mod, ONLY : tArgs

  implicit none

  ! Objects kept by the main program.
  type (molsysdat), target :: system
  class (tPES), allocatable :: PES
  type (tDMCSim) :: simulation

  type (tArgs) :: args

  ! Timing:
  real :: t1,t2,hours,minutes,seconds
  integer today(3), now(3)


  ! Get command line arguments.
  call args%get()

  if (args%DMC) then

    call idate(today)   ! today(1)=day, (2)=month, (3)=year
    call itime(now)     ! now(1)=hour, (2)=minute, (3)=second
    write ( *, 124 )  today(2), today(1), today(3), now
    124    format ( 'Date ', i2.2, '/', i2.2, '/', i4.4, '; time ',  &
          i2.2, ':', i2.2, ':', i2.2 )

    !---------------------------------------------------------------
    ! Start timing CPU time
    !---------------------------------------------------------------

    call cpu_time(t1)

    ! Initialise random number generator.
    call read_iseed(system%iseed)

    ! Read system data from IN_SYSTEM.
    call system%readData('IN_SYSTEM')

    ! Check if we need a gas phase or crystal PES.
    if (args%crystal) then
      allocate(tCrystPES :: PES)

      system%MOF5Hacks = args%MOF5Hacks
      system%halfCell = args%halfCell
      system%heatMap = args%heatMap
      system%pbc = args%pbc

      call system%readUnitVectors()
    else
      allocate(tGasPES :: PES)
      system%QH2Hacks = args%QH2Hacks
    endif



    ! Load the PES.
    call PES%harvestArgs(args)
    call PES%load(system)

    ! Set up the simulation itself.
    call simulation%harvestArgs(args)
    call simulation%setup(system)

    ! Now equilibrate.
    call simulation%equilibrate(system,PES)

    ! And sample.
    call simulation%sample(system,PES)

    !----------------------------------------------------------------
    ! stop timer and print CPU time used
    !----------------------------------------------------------------
    call cpu_time(t2)
    print *,' '
    hours = (t2-t1)/3600.0d0
    minutes = (hours - int(hours))*60
    seconds =  (minutes - int(minutes))*60
    print *,'CPU time: ',int(hours),'hours',int(minutes),'minutes',int(seconds),'seconds'
    call idate(today)   ! today(1)=day, (2)=month, (3)=year
    call itime(now)     ! now(1)=hour, (2)=minute, (3)=second
    write ( *, 124 )  today(2), today(1), today(3), now

    call exit(0)

  endif

  if (args%calcEnergies) then
    ! Initialise random number generator
    call read_iseed(system%iseed)

    ! Read system data from IN_SYSTEM.
    call system%readData('IN_SYSTEM')

    ! Check if we need a gas phase or crystal PES.
    if (args%crystal) then
      allocate(tCrystPES :: PES)
      system%MOF5Hacks = args%MOF5Hacks
      call system%readUnitVectors()
    else
      allocate(tGasPES :: PES)
    endif

    ! Load the PES.
    call PES%harvestArgs(args)
    call PES%load(system)

    call PES%LoadXYZ(args%XYZfile)

    call exit(0)

  endif

  if (args%DWonly) then
    ! Read system data from IN_SYSTEM.
    call system%readData('IN_SYSTEM')

    system%MOF5Hacks = args%MOF5Hacks
    system%halfCell = args%halfCell
    system%heatMap = args%heatMap
    system%RDF = args%RDF
    system%pbc = args%pbc
    system%QH2Hacks = args%QH2Hacks


    call simulation%harvestArgs(args)

    call simulation%DWOnly(system)

    call exit(0)



  endif






end program RunDMC
