!****************************************************************
! Contains absolutely everything required for a gas phase PES. Uses
! bits and pieces from molecule_specs and interpolation modules,
! but is completely standalone from anything related to DMC.
!
! Last edited by Jordan, 25/03/15.
!****************************************************************
module gasPES_mod

use PES_mod, ONLY : tPES, tFragment, tSplineFrag
use interpolation
use molecule_specs, ONLY : molsysdat, au2ang
use CubicSplines
use fragment_mod, ONLY : frag_par, read_fragment_data
use MSIEnergy_mod, ONLY : Potential, intern_rigid, updateNeighbours, addToTOUT

implicit none

private
logical :: tout = .false.
public :: tGasPES


!=================================================================================
! A type to store all the PES information. This is what will be carried by the
! simulation above. Only a limited number of things will actually be visible.
!================================================================================
type, extends(tPES) :: tGasPES
  ! An array of individual fragment PESs.
  !class(tFragArray), dimension(:), allocatable :: FragContainer
  integer :: nMSI, nFrags
  integer :: nSplines
  integer :: nSPEs ! Number of fragments where only a single-point energy is added/subtracted.
  real(kind=8) :: SPE ! The total single-point-energy to add to the total energy.

  logical :: calcRigidBonds = .false.
  logical :: printInterp = .false.

    contains

    procedure, pass(this) :: harvestArgs => harvestArgs

    ! Procedure will load the PES, passing in this object. Will be seen by main program as
    ! PES%load, will be subroutine loadPES here.
    procedure, pass(PES) :: load => loadPES
    procedure, pass(this) :: calcEnergy => calcEnergy
    procedure, pass(this) :: setupTOUT => setupTOUT
    procedure, pass(this) :: writeTOUT => writeTOUT
    procedure, pass(this) :: LoadXYZ => LoadXYZ
	  procedure, pass(this) :: checkNeighs => checkNeighs
    procedure, pass(this) :: allocateIDs => allocateIDs
    procedure, pass(this) :: allocateNeighbours => allocateNeighbours

end type tGasPES





!=====================================================================
! Doodads for Mad Mick's Modified Shepard Madness.
!=====================================================================
type, extends(tFragment) :: tMSIFrag
  integer, dimension(:), allocatable :: atomIndex ! The index that atom i in this fragment corresponds to.
  type (molsysdat) :: system ! All the relevant system information.
  type (interp_params) :: interp ! All interpolation parameters for this fragment.
  type (pot_file) :: POT
  type (frag_par), dimension(:), allocatable :: rigidFragments ! Rigid fragments within this fragment, if any.
  type (neighbour_list), dimension(:), allocatable :: neighbours
  type (tMSIFrag), pointer :: identFrag
  logical :: updateNeighbours = .false.
  logical :: kill = .true.
  logical :: printLowE = .true.

  contains

    procedure, pass(this) :: calcE => calcMSI

end type tMSIFrag

contains


!======================================================================
! Harvests relevant command-line arguments from the given Arguments
! object. Must be called before 'loadPES' for maximum effectiveness.
!======================================================================
subroutine harvestArgs(this,args)
  use Args_mod, ONLY : tArgs

  class(tGasPES) :: this
  type (tArgs), intent(in) :: args

  this%calcRigidBonds = args%calcRigidBonds
  this%printInterp = args%printInterp
  this%absEnergy = args%absEnergy
  this%printDetails = args%printDetails
  this%CPUtime = args%CPUtime
  this%killLowE = args%killWalkers
  this%printLowE = args%printLowE

end subroutine harvestArgs

!=====================================================================
! Calculates the potential energy V of the given geometry. If
! provided, an array of the individual fragment energies is returned.
!=====================================================================
function calcEnergy(this,coords,ID,step,lowE,fragEnergies) result(V)


  class(tGasPES) :: this
  real(kind=8), dimension(1:this%nAtoms,1:3), intent(in) :: coords
  integer, dimension(:), intent(in) :: ID
  integer, intent(in) :: step
  logical, optional, intent(out) :: lowE
  real(kind=8), dimension(:), optional, intent(out) :: fragEnergies
  real(kind=8), dimension(1:this%nAtoms,1:3) :: fragCoords
  real(kind=8) :: V, energy, dist
  integer :: threadID
  logical :: dLowE ! 'dummy' low energy flag - avoids seg faults!
  integer :: i, j, n, ind

  dLowE = .false.



  V = 0.0d0

  ! Loop over PES fragments
  do i=1, this%nFrags

    energy = this%FragContainer(i)%fragment%calcE(coords,ID(i),step,dLowE)
    !print *, i, energy

    ! Should we stop the energy calculation here? If lowE is
    ! present AND is true - i.e. we care about whether a geometry
    ! has a low energy - we will.
    if (PRESENT(lowE).and.dLowE) then
      lowE = .true.
      return
    endif

    if (PRESENT(fragEnergies)) fragEnergies(i) = energy

    ! Add the energy * multiplier to the total for this walker.
    V = V + this%FragContainer(i)%fragment%multiplier * energy

  enddo

  ! Add the single point energies.
  V = V + this%SPE

  ! Add the global minimum.
  V = V - this%Vmin

  if (PRESENT(lowE)) lowE = .false.


end function calcEnergy

!=====================================================================
! Checks if it's time to update the neighbour lists on the modified
! Shepard PESs.
!=====================================================================
subroutine checkNeighs(this,step,check)
  class(tGasPES) :: this
  integer, intent(in) :: step
  type(tMSIFrag), pointer :: identMSI
  logical, dimension(:), allocatable, intent(out) :: check
  integer :: i

  allocate(check(1:this%nFrags))
  check = .false.

  do i=1,this%nFrags
    ! Only check if this is a modified Shepard fragment.
	select type (frag => this%FragContainer(i)%fragment)
	  type is (tMSIFrag)
      ! So I don't need to explicitly reset this to false AFTER setting it to true, I'll just
      ! keep setting this to false first.
      frag%updateNeighbours = .false.
	    identMSI => frag%identFrag
      if ((MODULO(step,identMSI%interp%update_inner).eq.0).or. &
        step.eq.1) then
        check(i) = .true.
        frag%updateNeighbours = .true.
      endif
	end select
  enddo

end subroutine checkNeighs

!=====================================================================
! Calculates the potential energy of the given geometry on a modified
! Sherpard PES.
!=====================================================================
function calcMSI(this,coords,ID,step,lowE) result(V)

  class(tMSIFrag) :: this
  real(kind=8), dimension(:,:), intent(in) :: coords
  integer, intent(in) :: ID, step ! The ID of this walker
  logical, intent(out) :: lowE
  real(kind=8), dimension(1:this%nAtoms,1:3) :: fragCoords
  real(kind=8), dimension(1:this%identFrag%system%nbond) :: r
  real(kind=8), dimension(1:this%identFrag%system%nNRbonds) :: nonRigidr
  real(kind=8) :: V
  type(tMSIFrag), pointer :: identMSI
  integer :: i, j
  real(kind=8) :: rms, totsum

  V = 0.0d0
  r = 0.0d0 ! atom-atom distances
  nonRigidr = 0.0d0 ! atom-atom distances which aren't rigid
  lowE = .false.

  ! First, get the coordinates of this fragment from the full geometry.
  do i=1, this%nAtoms
    j = this%atomIndex(i)
    fragCoords(i,:) = coords(j,:)
  enddo

  ! Get pointer to fragment that contains the actual PES (may be itself - that's OK).
  identMSI => this%identFrag

  call intern_rigid(identMSI%system,fragCoords,r,nonRigidr)

  ! Do we need to update the inner neighbour list?
  if (this%updateNeighbours) call updateNeighbours(identMSI%system, &
      identMSI%POT,identMSI%interp,this%neighbours(ID),nonRigidr)

  ! Pass to energy calculation.
  V = Potential(identMSI%system,identMSI%interp, &
     identMSI%POT,this%neighbours(ID), r, nonRigidr, rms, totsum)

  if (identMSI%interp%printTOUT) then
    call addToTOUT(fragCoords,V,rms,totsum,identMSI%interp)
  endif


  ! Kill the walker if energy < 2 kJ mol-1 below minimum
  if (this%kill.and.(V - identMSI%interp%vmin < -0.00076 )) then
    lowE = .true.
    ! Add this to TOUT file, even if we're not currently writing
    if (tout) call addToTOUT(fragCoords,V,rms,totsum,identMSI%interp)
    if (this%printLowE) then
      !$OMP CRITICAL (lowE1)
      write(11,*) '--------------------------------------------------'
      write(11,*) ' Walker killed: energy more than 2.0 kJmol < Vmin '
      write(11,*) '         : PES          ', trim(this%name)
      write(11,*) '         : interp%vmin  =',identMSI%interp%vmin
      write(11,*) '         : energy       =',V
      write(11,*)

      do j=1, this%nAtoms
        write(11,*) fragCoords(j,:)
      enddo
      !$OMP END CRITICAL (lowE1)
    endif


  endif

end function calcMSI





!===========================================================
! Creates new TOUT files for the fragment MSI PESs.
!===========================================================
subroutine setupTOUT(this)
  class(tGasPES) :: this
  integer :: i, unit
  character*50 :: fileName

  tout = .true.
  do i=1, this%nFrags
    ! Only do this for MSI fragments.
    select type (frag => this%FragContainer(i)%fragment)
    type is (tMSIFrag)
      ! And only do this for 'unique' fragments.
      if (frag%identicalTo == i) then
        if (this%nFrags == 1) then
          fileName = 'TOUT.1'
        else
          fileName = trim(frag%interp%name)//'.TOUT'
        endif
        ! Open a new TOUT file.
        open(newunit=unit,file=trim(fileName),status='replace')
        write(unit,*) ' File contains sampled qdmc configurations'
        close(unit)
      endif
    end select
  enddo
end subroutine setupTOUT

!===========================================================
! Doesn't really write to TOUT, but sets flags to write
! to TOUT next time the the energy is calculated.
!===========================================================
subroutine writeTOUT(this,status)
  class(tGasPES) :: this
  logical, intent(in) :: status
  integer :: i

  do i=1, this%nFrags
    ! Only do this for MSI fragments.
    select type (frag => this%FragContainer(i)%fragment)
    type is (tMSIFrag)
      ! And only do this for 'unique' fragments.
      if (frag%identicalTo == i) frag%interp%printTOUT = status
    end select
  enddo

end subroutine writeTOUT

!===========================================================
! Loads the PES from the IN_PES file. Now bound to PES%load.
!===========================================================
subroutine LoadPES(PES,sys)

    use molecule_specs
    use interpolation
    use omp_lib, ONLY : OMP_get_max_threads

    implicit none

    class(tGasPES) :: PES
    type(molsysdat), target, intent(in) :: sys

    character(100) :: comment, name, fragType
    character(100) :: sysFile, permsFile, interpFile, potFile, fragFile

    integer :: nTotal, i, j, k, MSICount, SPECount, splineCount, fragCount, n, mult, n1,n2
    integer :: maxThreads


    integer :: error, IOstatus

    logical :: fileExists

  type(interp_params) :: tempInterp

  real(kind=8) :: tempSPE

  PES%nAtoms = sys%natom

    ! Check that PES file exists.
    INQUIRE(file='IN_PES',exist=fileExists)
    if (.NOT.fileExists) then
        print *, 'IN_PES not found!'
        call exit(1)
    endif

    ! Open file, etc.
    open(unit=200,file='IN_PES',status='old')

111     format(a70)

    write(11,*) '-----------------------------------'
    write(11,*) '  Reading PES details from IN_PES'
    write(11,*) '-----------------------------------'

    write(11,*)

    read(200,111,IOSTAT=IOstatus) comment ! First two headers
    write(11,111) comment
    read(200,111,IOSTAT=IOstatus) comment
    write(11,111) comment

    ! Number of modified Shepard and SPE fragments:
    read(200,*,IOSTAT=IOstatus) PES%nMSI, PES%nSplines, PES%nSPEs
    write(11,*) PES%nMSI, PES%nSplines, PES%nSPEs

    PES%nFrags = PES%nMSI + PES%nSplines
  PES%SPE = 0.0d0

    ! Sanity check, then allocation.
    if ((PES%nFrags.le.0)) then
        print *, "There is no PES! Need at least one spline PES to run DMC on. Exiting."
        call exit(1)
    endif

    allocate(PES%FragContainer(1:PES%nFrags),stat=error)
    if (error.ne.0) stop 'Error allocating PES fragments array!'

    ! Get max number of threads:
    maxThreads = OMP_get_max_threads()


    ! If there's only one fragment - i.e. one MSI surface - then load the surface from the usual IN_* files.
    if ((PES%nMSI == 1).and.(PES%nSplines == 0)) then

    ! Allocate the first element of the polymorphic array to an MSI fragment.
    allocate(tMSIFrag :: PES%FragContainer(1)%fragment)
    ! Have to do this so compiler knows I want all an MSI fragment:
    select type (tempMSIFrag => PES%FragContainer(1)%fragment)
      type is (tMSIFrag)

        tempMSIFrag%identicalTo = -1
        write(11,*) 'Only one modified Shepard PES. Loading from regular IN_* files.'
        call tempMSIFrag%system%readData('IN_SYSTEM')
        tempMSIFrag%nAtoms = tempMSIFrag%system%natom
        call read_fragment_data(tempMSIFrag%system,tempMSIFrag%rigidFragments,'IN_FRAG')

        if (PES%calcRigidBonds) tempMSIFrag%system%nfrag = 0

        call DetermineRigidBonds(tempMSIFrag%system,tempMSIFrag%rigidFragments)
        call read_interp(tempMSIFrag%interp,'IN_INTERP')
        tempMSIFrag%interp%name = 'MSI'
        call tempMSIFrag%system%readPerms('IN_ATOMPERMS')

        ! POT file.
        call tempMSIFrag%POT%loadPOT(tempMSIFrag%system, &
          tempMSIFrag%interp, 'POT')


        !call read_pot(tempMSIFrag%system, &
        !  tempMSIFrag%interp, &
        !  tempMSIFrag%pot,'POT')





          allocate(tempMSIFrag%atomIndex(1:tempMSIFrag%system%nAtom),stat=error)
          if (error.ne.0) stop 'Error allocating fragment PES atom indices array!'
          ! Fill atomIndex array with 1 to 1 mapping.
          do i=1,tempMSIFrag%system%nAtom
          tempMSIFrag%atomIndex(i) = i
          enddo

          tempMSIFrag%multiplier = 1
          tempMSIFrag%name = 'MSI'

          PES%vmin = tempMSIFrag%interp%vmin

          tempMSIFrag%identFrag => tempMSIFrag
          tempMSIFrag%identicalTo = 1

          tempMSIFrag%interp%printDetails = PES%printDetails
          tempMSIFrag%kill = PES%killLowE
          ! Print low energy walkers?
          tempMSIFrag%printLowE = PES%printLowE

      end select
      return

    endif

    ! Alrighty, time to loop.
    nTotal = PES%nFrags+PES%nSPEs
    SPECount = 0
    MSICount = 0
    splineCount = 0
  fragCount = 0

    do i=1, nTotal
        read(200,111,IOSTAT=IOstatus) comment ! A '========' separator.
        write(11,111) comment
        read(200,111,IOSTAT=IOstatus) comment ! 'name of frag' header
        write(11,111) comment

        read(200,*,IOSTAT=IOstatus) name
        write(11,111) name
        read(200,111,IOSTAT=IOstatus) comment ! 'fragment type' header
        write(11,111) comment
        read(200,*,IOSTAT=IOstatus) fragType
        write(11,111) fragType

        ! check type of fragment we're about to read.
        ! (trim any leading spaces)
        fragType = ADJUSTL(fragType)


    if (fragType == 'MSI') then

      fragCount = fragCount+1
      MSICount = MSICount+1
      ! Create new MSI fragment in polymorphic fragments array.
      allocate(tMSIFrag :: PES%FragContainer(fragCount)%fragment)

    elseif (fragType == 'SPLINE') then

      fragCount = fragCount + 1
      ! Create new spline fragment in polymorphic fragments array.
      allocate(tSplineFrag :: PES%FragContainer(fragCount)%fragment)

    elseif (fragType == 'SPE') then

      write(11,*)
            write(11,*) '----PES fragment is a single-point energy.----'
            write(11,*)

            ! Load single-point energy.
            SPECount = SPECount+1

            read(200,111,IOSTAT=IOstatus) comment
            write(11,111) comment

            read(200,*,IOSTAT=IOstatus) tempSPE
            write(11,*) tempSPE

            ! Multiplier
            read(200,111,IOSTAT=IOstatus) comment
            write(11,111) comment

            read(200,*,IOSTAT=IOstatus) mult
            write(11,*) mult


            PES%SPE = PES%SPE + mult*tempSPE

      ! Nothing more to do in this loop, cycle.
      cycle

    else
            print *, TRIM(fragType), ' is not recognised as a type of fragment PES.'
            print *, 'Fragment name: ', trim(name)
            print *, 'Valid types are:'
            print *, "'MSI' for modified Shepard surface."
            print *, "'SPLINE' for a 1D cubic spline surface."
            print *, "'SPE' for a single-point energy."
            call exit(1)
        endif


    ! Now load MSI or spline fragment, depending on the type that's just been allocated.
    select type (frag => PES%FragContainer(fragCount)%fragment)
    type is (tMSIFrag)

      write(11,*)
            write(11,*) '----PES fragment is a modified Shepard interpolation.----'
            write(11,*)



            frag%name = name
            frag%indList = i ! where fragment is in list.
            ! Load modified Shepard surfaces.
            read(200,111,IOSTAT=IOstatus) comment ! 'no. atoms' header
            write(11,111) comment
            read(200,*,IOSTAT=IOstatus) n ! save to n to make things a bit neater.
            write(11,*) n

            frag%nAtoms = n

            allocate(frag%atomIndex(1:n),stat=error)
            if (error.ne.0) stop 'Error allocating fragment PES atom indices array!'

            ! Read indices of the atoms in this fragment.
            read(200,111,IOSTAT=IOstatus) comment ! 'indices' header
            write(11,111) comment
            read(200,*,IOSTAT=IOstatus) (frag%atomIndex(j),j=1,n)
            write(11,*) (frag%atomIndex(j),j=1,n)

            ! Fragment sameness:
            read(200,111,IOSTAT=IOstatus) comment ! 'sameness' header
            write(11,111) comment
            read(200,*,IOSTAT=IOstatus) frag%identicalTo
            write(11,*) frag%identicalTo

            ! Check for sameness:
            if (frag%identicalTo.ne.-1) then
                if ((frag%identicalTo.gt.nTotal).OR.(frag%identicalTo.le.0)) then
                        print *, 'Fragment ', TRIM(name), " is identical to fragment &
                            that doesn't exist!"
                        call exit(1)
                endif

                write(11,*) 'Fragment ', i, 'is identical to fragment ', frag%identicalTo

                ! Can skip next two lines, and not load system, interp, pot etc.
                read(200,111,IOSTAT=IOstatus) comment
                read(200,111,IOSTAT=IOstatus) comment

                ! Pretend there's sanity checks here for things such as nAtoms, etc.

                ! When all fragments are loaded, the 'identical to' indices will be updated so they point to index in Fragments array, because they may not initially if there's a good mix of MSI and SPE energies, and other reasons.
            else
                ! Load the modified Shepard surface!
                read(200,111,IOSTAT=IOstatus) comment ! 'name of input files'
                write(11,111) comment
                read(200,*,IOSTAT=IOstatus) sysFile, permsFile, interpFile, potFile, fragFile
                write(11,111) sysFile, permsFile, interpFile, potFile, fragFile

                ! Read fragment's IN_SYSTEM file.
                call frag%system%readData(trim(sysFile))

                ! Read fragment's rigid fragments file (IN_FRAG)
                call read_fragment_data(frag%system,frag%rigidFragments,trim(fragFile))

                if (PES%calcRigidBonds) frag%system%nfrag = 0

                ! Set up rigid bonds.
                call DetermineRigidBonds(frag%system,frag%rigidFragments)

                ! Read interpolation details.
                call read_interp(frag%interp,trim(interpFile))

                frag%interp%name = frag%name

                ! Do atom permutations.
                call frag%system%readPerms(trim(permsFile))

                ! POT file.
                call frag%POT%loadPOT(frag%system, &
                   frag%interp,trim(potFile))




              ! Print interpolation details?
              frag%interp%printDetails = PES%printDetails

              ! Kill walkers?
              frag%kill = PES%killLowE
              ! Print killed walkers?
              frag%printLowE = PES%printLowE
      endif

            ! Addition or subtraction?
            read(200,111,IOSTAT=IOstatus) comment
            write(11,111) comment

            read(200,*,IOSTAT=IOstatus) frag%multiplier
            write(11,*) frag%multiplier

            ! All there is for MSI fragments.


    ! Or a spline PES...
    type is (tSplineFrag)

      write(11,*)
      write(11,*) '----PES fragment is a 1D cubic spline.----'
      write(11,*)

      splineCount = splineCount+1

      frag%name = name
      frag%indList = i ! where fragment is in list

      read(200,111,IOSTAT=IOstatus) comment ! 'no. atoms' header
      write(11,111) comment
      read(200,*,IOSTAT=IOstatus) n ! save to n to make things a bit neater.
      write(11,*) n

      frag%nAtoms = n

      ! read number of centre-of-mass atoms required
      read(200,111,IOSTAT=IOstatus) comment ! 'no. atoms' header
      write(11,111) comment
      read(200,*,IOSTAT=IOstatus) n1, n2

      allocate(frag%CoMs(2))

      frag%CoMs(1)%nAtoms = n1
      frag%CoMs(2)%nAtoms = n2

      write(11,*) frag%CoMs(1)%nAtoms, frag%CoMs(2)%nAtoms

      allocate(frag%CoMs(1)%indices(1:n1),stat=error)
      allocate(frag%CoMs(2)%indices(1:n2),stat=error)
      if (error.ne.0) stop 'Error allocating PES spline indices array!'

      allocate(frag%CoMs(1)%mass(1:n1))
      allocate(frag%CoMs(2)%mass(1:n2))

      ! read centre-of-mass atom indexes
      read(200,111,IOSTAT=IOstatus) comment
      write(11,111) comment
      read(200,*,IOSTAT=IOstatus) (frag%CoMs(1)%indices(j),j=1,n1)
      read(200,*,IOSTAT=IOstatus) (frag%CoMs(2)%indices(j),j=1,n2)
      write(11,*) (frag%CoMs(1)%indices(j),j=1,n1)
      write(11,*) (frag%CoMs(2)%indices(j),j=1,n2)

      ! Sum the masses:
      do j=1,2
        frag%CoMs(j)%sumMasses = 0.d0
        do k=1,frag%CoMs(j)%nAtoms
          n = frag%CoMs(j)%indices(k)
          frag%CoMs(j)%sumMasses = frag%CoMs(j)%sumMasses + sys%mass(n)
          ! Save the mass of this atom while we're at it.
          frag%CoMs(j)%mass(k) = sys%mass(n)
        enddo
      enddo

      ! Fragment sameness:
      read(200,111,IOSTAT=IOstatus) comment ! 'sameness' header
      write(11,111) comment
      read(200,*,IOSTAT=IOstatus) frag%identicalTo
      write(11,*) frag%identicalTo

      if (frag%identicalTo.ne.-1) then
        if (frag%identicalTo.gt.nTotal) then
          print *, 'Spline PES fragment ', trim(frag%name), " is &
            identical to fragment that doesn't exist!"
          call exit(1)
        endif

        write(11,*) 'Fragment ', i, 'is identical to fragment ', frag%identicalTo

        ! Can skip next two lines, and not load system, interp, pot etc.
        read(200,111,IOSTAT=IOstatus) comment
        read(200,111,IOSTAT=IOstatus) comment

        ! Pretend there's sanity checks here for things such as nAtoms, etc.

        ! When all fragments are loaded, the 'identical to' indices will be updated so they point to index in Fragments array, because they may not initially if there's a good mix of MSI, spline and SPE energies, and other reasons.
      else
        ! Load the spline PES
        ! Get the file name of POT file
        read(200,111,IOSTAT=IOstatus) comment
        write(11,111) comment
        read(200,*,IOSTAT=IOstatus) potFile
        write(11,111) potFile

        call frag%spline%load(trim(potFile))

        frag%identicalTo = fragCount


      endif

      ! Addition or subtraction?
      read(200,111,IOSTAT=IOstatus) comment
      write(11,111) comment

      read(200,*,IOSTAT=IOstatus) frag%multiplier
      write(11,*) frag%multiplier

      write(11,*) '==== Finished loading spline. ===='


    end select


    enddo

  !!!!! Apologies for the stupid select type -> select type bullshit.

    ! A quick check:
    do i=1, PES%nFrags
        if (PES%FragContainer(i)%fragment%identicalTo.ne.-1) then
        ! Ensure index points to right fragment in array, not fragment in list in IN_PES.
        ! Find other fragment in list.
            do j=1, PES%nFrags
                if (PES%FragContainer(i)%fragment%identicalTo == PES%FragContainer(j)%fragment%indList) then
                    PES%FragContainer(i)%fragment%identicalTo = j
          select type (frag => PES%FragContainer(i)%fragment)
          type is (tMSIFrag)
            select type (ident => PES%FragContainer(j)%fragment)
            type is (tMSIFrag) ! Fuck you, Fortran.
              frag%identFrag => ident
            end select
          type is (tSplineFrag)
          select type (ident => PES%FragContainer(j)%fragment)
            type is (tSplineFrag) ! Fuck you, Fortran.
              frag%identFrag => ident
            end select
          end select
                    exit ! break this loop.
                endif
            enddo
        else
            ! Set it so it points to itself. I'm doing this so I don't have to check for identical
            ! fragments when looping over fragments in the potential subroutine; it sorts itself out!
            PES%FragContainer(i)%fragment%identicalTo = i
      select type (frag => PES%FragContainer(i)%fragment)
      type is (tMSIFrag)
        select type (ident => PES%FragContainer(i)%fragment)
        type is (tMSIFrag)
          frag%identFrag => ident ! Once again, fuck you Fortran for making me go through this nonsense
          ! with polymorphic objects.
        end select
      type is (tSplineFrag)
        select type (ident => PES%FragContainer(i)%fragment)
        type is (tSplineFrag)
          frag%identFrag => ident ! Once again, fuck you Fortran for making me go through this nonsense
          ! with polymorphic objects.
        end select
      end select
        endif
    enddo

  ! Finally, read IN_INTERP again to get Vmin for the whole surface.
  write(11,*) '-------------------------------------------'
  write(11,*) '        Read IN_INTERP to get Vmin'
  write(11,*) '-------------------------------------------'

  call read_interp(tempInterp,'IN_INTERP')
  PES%Vmin = tempInterp%vmin


    close(unit=200)

end subroutine LoadPES



!=============================================================================================
! Organise rigid fragments within the given system. Need to work out which of the bond lengths
! are rigid and will remain the same in the simulation, and which will need to be recalculated.
!=============================================================================================
subroutine DetermineRigidBonds(system,frag)
  use fragment_mod, ONLY : frag_par


  type(molsysdat), intent(inout) :: system
  type(frag_par), dimension(0:system%nfrag), intent(in) :: frag
  integer :: i, j, k, l, nNonRigid, ierr


  ! Calculate total number of rigid bonds - will be sum of intrafragment bonds.
  system%nRigidBonds = 0
  do i=1, system%nfrag
    system%nRigidBonds = system%nRigidBonds+frag(i)%natom*(frag(i)%natom-1)/2
  enddo
  nNonRigid = system%nbond - system%nRigidBonds
  system%nNRbonds = nNonRigid

  ! Now allocate array of non-rigid bonds.
  allocate(system%nonRigidBonds(1:nNonRigid),stat=ierr)
  if (ierr.ne.0) stop ' molsysdat allocation error: nonRigidBonds '

  ! Populate this array with atom indices. Loop through all atom-atom distances...
  nNonRigid = 0
  l=0
  do i=1, system%natom-1

    atomloop: do j=i+1, system%natom
      l = l+1
      ! Loop through rigid fragments. We'll see if the two atoms i,j are *within* a fragment.
      fragmentloop: do k=1, system%nfrag

        if ( (any(i.eq.frag(k)%indx(:))).and.(any(j.eq.frag(k)%indx(:))) ) then
          ! If they occur anywhere, then go to next atom combination.
          !print *, i, j, 'are in fragment', k
          !print *, 'move to next combo'
          cycle atomloop
        endif


      enddo fragmentloop

      ! If we're here, then this atom combination doesn't occur within any fragment.
      nNonRigid = nNonRigid+1
      system%nonRigidBonds(nNonRigid)%i = i
      system%nonRigidBonds(nNonRigid)%j = j
      system%nonRigidBonds(nNonRigid)%ind = l
    enddo atomloop
  enddo

  ! Now that we've done that, quickly zero all non-rigid bonds in system%bondlengths
  do i=1,system%nNRbonds
    j = system%nonRigidBonds(i)%ind
    system%bondLengths(j) = 0.0d0
  enddo

  ! Also square the array - we'll unsquare it later
  system%bondLengths = system%bondLengths**2

end subroutine DetermineRigidBonds



!================================================================
! Loads geometries from a .xyz file and prints their energies.
!================================================================
subroutine LoadXYZ(this,fileName)
    use omp_lib, ONLY : OMP_get_max_threads, OMP_get_thread_num
    use Utilities, ONLY : readXYZ

    class (tGasPES) :: this
    character(30), intent(in) :: fileName
    character(30) :: outFormat

    integer :: geomCount, maxThreads, threadID
    integer :: n, i, j, k

    real(kind=8), dimension(:,:,:), allocatable :: Geoms
    real(kind=8), dimension(:), allocatable :: Energies, tempFragE
    real(kind=8), dimension(:,:), allocatable :: fragEnergies
    integer, dimension(:), allocatable :: IDs
    logical, dimension(:), allocatable :: updateNeighbours

    real :: t1, t2

    geomCount = 0

    maxThreads = OMP_get_max_threads()


    Geoms = readXYZ(this%nAtoms,fileName,geomCount)
    allocate(Energies(1:geomCount))
    allocate(tempFragE(1:this%nFrags))
    allocate(fragEnergies(1:geomCount,1:this%nFrags))

    call this%allocateNeighbours(maxThreads)
    call this%allocateIDs(IDs)
    call this%checkNeighs(1,updateNeighbours)

    ! Now loop through and calculate the energies
  call CPU_TIME(t1)
!$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(IDs,tempFragE) SCHEDULE(guided)
    do i=1, geomCount
        IDs = OMP_get_thread_num()+1
        Energies(i) = this%calcEnergy(Geoms(i,1:this%nAtoms,1:3),IDs,0,fragEnergies=tempFragE)
        fragEnergies(i,:) = tempFragE(:)
    enddo
!$OMP END PARALLEL DO
  call CPU_TIME(t2)

  if (this%absEnergy) Energies = Energies + this%vmin

  if (this%CPUtime) then
    print *, t2-t1
    call exit(0)
  endif

    write(outFormat,'(A11,I2,A7)') "(I4,F20.12,",this%nFrags,"E15.7)"

    if (this%printDetails) then
      ! Print energies in less plottable format
      do i=1, geomCount
        print *, "==== geom #", i, "===="
        print *, "frag #, name, energy, energy above PES min (kJ mol-1), mult"
        do j=1,this%nFrags
          print *, j, trim(this%FragContainer(j)%fragment%name), fragEnergies(i,j),this%FragContainer(j)%fragment%multiplier
        enddo
        if (.not.this%absEnergy) Energies(i) = Energies(i) + this%vmin
        print *, "absolute energy, energy above min (kJ mol-1):", Energies(i), (Energies(i)-this%vmin)*2625.5d0
      enddo

    else

      print *, '# geom number |  energy |  frag #1 energy  |  frag #2 energy,  etc.'

      do i=1, geomCount
          print outFormat, i, Energies(i), (fragEnergies(i,j),j=1,this%nFrags)
      enddo

    endif


end subroutine LoadXYZ










!==========================================================================================
! Allocates and returns an array for storing IDs for a walker on the modified Shepard PESs.
!===========================================================================================
subroutine allocateIDs(this,idPES)
  class (tGasPES) :: this
  integer, dimension(:), intent(out), allocatable :: idPES

  allocate(idPES(1:this%nFrags))

  idPES = 0

end subroutine allocateIDs

!=========================================================================
! Allocates the neighbourlists on the modified Shepard PESs.
!=========================================================================
subroutine allocateNeighbours(this,max)
  class (tGasPES) :: this
  integer, intent(in) :: max
  integer :: i, j, badness

  ! Loop over all the fragments on this PES.
  do i=1, this%nFrags
    select type (frag => this%FragContainer(i)%fragment)
      type is (tMSIFrag)
        ! Only allocate modified Shepard fragments, of course.
        allocate(frag%neighbours(1:max),stat=badness)
        if (badness.ne.0) stop 'Error allocating neighbourlist!'
        do j=1,max
          call frag%neighbours(j)%new(frag%identFrag%interp%ndata)
        enddo
    end select
  enddo
end subroutine allocateNeighbours

end module gasPES_mod
