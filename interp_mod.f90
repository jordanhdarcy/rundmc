
module interpolation
  implicit none
  !---------------------------------------------
  ! parameters which are read in from IN_INTERP
  !---------------------------------------------
  type interp_params
    integer :: ipart,update_inner,nneigh,ncluster
    integer :: ipow
    integer :: ndata,nforc
    real(kind=8) :: wtol,vmax, vmin
    real(kind=8) :: avrads
    integer :: neigh_max
    logical :: printDetails
  logical :: printTOUT
  character(100) :: name
  end type interp_params



  type pot_file
    ! r: inverse bond lengths for datapoint i (both rigid and non-rigid)
    ! nonRigidr: inverse bond lengths that are non-rigid
    ! z: zetas for datapoint i
    ! v1 & v2: first and second derivatives
    real(kind=8), allocatable, dimension(:,:) :: r, nonRigidr, z, v1, v2
    ! v0: datapoint energy
    real(kind=8), allocatable, dimension(:) :: v0
    ! ut: the Utrans matrix for each datapoint
    ! NOTE: all arrays above will be allocated for best column-major performance,
    ! where applicable.
    real(kind=8), allocatable, dimension(:,:,:) :: ut
    integer, allocatable, dimension(:) :: ID, perm
    contains
      procedure, pass(this) :: loadPOT => read_pot
      procedure, pass(this) :: new => allocatePOT
      procedure, pass(this) :: destroy => deallocatePOT
  end type pot_file



  !=================================================================
  ! Contains the inner neighbours.
  !==============================================================
  type neighbour_list
    integer, allocatable, dimension(:) :: points
    integer :: n ! Number of neighbours

    contains
      procedure, pass(this) :: new => newNeighbourList
      procedure, pass(this) :: add => addNeighbour

  end type neighbour_list


contains

  !--------------------------------------
  ! Euclidean distance
  !--------------------------------------
  real(kind=8) function dist(a,b)
    real(kind=8), dimension(:), intent(in) :: a,b
    if (size(a) /= size(b)) then
       print *, ' Error in dist, arguments must be the same size '
       call exit(1)
    endif
    dist = sqrt(dist_sq(a,b))
    return
  end function dist
  real(kind=8) function dist_sq(a,b)
    real(kind=8), dimension(:), intent(in) :: a,b
    integer :: i
    dist_sq = 0
    do i=1,size(a)
       dist_sq = (a(i) - b(i))**2 + dist_sq
    enddo
    return
  end function dist_sq


!==============================================
! Reads interpolation parameters from given IN_INTERP file.
!==============================================
subroutine read_interp(interp,interpFile)

   implicit none
   type (interp_params), intent(out) :: interp
   character(*), intent(in) :: interpFile

   character(len=80) comment_line

80    format(a80)
81    format(1x,a79)

!----------------------------------------------------------------
! read in the parameters for the interpolation
!----------------------------------------------------------------

   open(unit=7,file=trim(interpFile),status='old')
   write(11,*) '----------------------------------------------------'
   write(11,*) ''
   write(11,*) ' read interpolation parameters from IN_INTERP '
   write(11,*) ''

!  read title line

   read(7,80)   comment_line
   write(11,81) comment_line

!  determine one or twp part weight function is used, ipart=1,2

   interp%ipart = 1
   write(11,91) interp%ipart
91    format(4x,'we will use the ',i1,' part weight function')

!  read in the parameters defining the weight function, the neighbour
!  list and the number of timesteps between calls to neighbour

   read(7,80)   comment_line
   write(11,81) comment_line
   read(7,*)    interp%ipow
   write(11,*)  interp%ipow

   interp%ipow = interp%ipow/2

   read(7,80)   comment_line
   write(11,81) comment_line
   read(7,*)    interp%wtol
   write(11,*)  interp%wtol

   read(7,80)   comment_line
   write(11,81) comment_line
   read(7,*)    interp%update_inner
   write(11,*)  interp%update_inner

!  read in energy maximum and minimum with which to screen data

   read(7,80)   comment_line
   write(11,81) comment_line
   read(7,*)    interp%vmax, interp%vmin
   write(11,*)  interp%vmax, interp%vmin



! close IN_INTERP

   close(unit=7)

   interp%printDetails = .false.


   return
   end subroutine read_interp





   subroutine read_pot(this,sys,interp,potFile)

      use molecule_specs

      implicit none
      class(pot_file) :: this
      type (molsysdat), intent(inout) :: sys
      type (interp_params), intent(inout) :: interp
      character(*), intent(in) :: potFile
	    type(pot_file) :: tempPOT

      integer  :: ierr,i,j,k,n,reject,ndata
      real(kind=8) :: vmin_pot
      character(len=80) :: comment_line

      integer :: counter
      real(kind=8) :: bondDiff

80    format(a80)
81    format(1x,a79)

!----------------------------------------------------------------
! read in the derivative data from which the PES is interpolated
!----------------------------------------------------------------

      write(11,*) '----------------------------------------------------'
      write(11,*) ' '
      write(11,*) '  read potential energy data from POT  '
      write(11,*) ' '

      open(unit=3,file=trim(potFile),status='old')

!  read through the file to find out how many data points it contains

      interp%ndata = 0
      read(3,80) comment_line
      do
        read(3,80,iostat=ierr,end=11) comment_line
        if (index(comment_line,'data') /= 0) then
           interp%ndata = interp%ndata + 1
        endif
      enddo

10    if (ierr.ne.0) stop ' Error reading from POT file '
11    continue

!  allocate space
      call tempPOT%new(sys,interp)

!  rewind and do a formatted read

      rewind(unit=3,iostat=ierr)
      if (ierr /= 0) stop ' Error rewinding POT file '

      read(3,80) comment_line
      reject = 0
      i=1
      do while (i <= interp%ndata)

        ! read in the potential energy data, including the
        ! definitions of the normal local coordinates
        ! Note: for efficiency, all arrays will be stored so they can be
        ! accessed in column-major order
        read(3,80,err=20,end=21) comment_line
        read(3,*)  (tempPOT%r(j,i),j=1,sys%nbond)

        ! Copy non-rigid bonds into array for later use.
        do j=1, sys%nNRbonds
          k = sys%nonRigidBonds(j)%ind
          !print *, j, k
          tempPOT%nonRigidr(j,i) = tempPOT%r(k,i)
        enddo

        ! Now, in case of rigid bonds, I'm just going to save a copy of r from the first data point...
        if (i.eq.1) then
            sys%bondLengths(:) = tempPOT%r(:,i)
            ! We'll worry about which of the rs are actually rigid later. Carry on...
        endif
        ! Read the Utrans matrix
        do j=1,sys%nint
           read(3,*) (tempPOT%ut(j,k,i),k=1,sys%nbond)
        enddo
        read(3,*)  tempPOT%v0(i)
        read(3,*)  (tempPOT%v1(j,i),j=1,sys%nint)
        read(3,*)  (tempPOT%v2(j,i),j=1,sys%nint)
        ! if the energy is too high skip this point
        if (tempPOT%v0(i) > interp%vmax) then
           i = i - 1
           interp%ndata = interp%ndata - 1
           reject = reject + 1
        endif
        i=i+1
      enddo

20    if (ierr.ne.0) stop ' Error reading from POT file '
21    continue

      close(unit=3)

      write(11,*) ' Read in',interp%ndata,' data points from POT'
      write(11,*) ' rejected ',reject,' data points as higher than vmax'

! set vmin to be the minimum of the value read in from IN_INTERP and the
! minimum found in the POT file
      vmin_pot = MINVAL(tempPOT%v0,1)


      write(11,*)' lowest potential energy in the POT file          = ',vmin_pot
      write(11,*)' lowest potential energy expected from IN_INTERP  = ',interp%vmin

      if (vmin_pot < interp%vmin) then
         interp%vmin = vmin_pot
      endif

      write(11,*)' lowest potential energy used (ie value of vmin)  = ',interp%vmin

!  invert data bonds once and for all
      tempPOT%r = 1.d0/tempPOT%r
      tempPOT%nonRigidr = 1.d0/tempPOT%nonRigidr

      !  construct the local coords for each data point
      !  remembering that rda are read in as bond lengths NOT inverses
      !  and that we have inverted them
      tempPOT%z = 0.0d0


      do i=1,interp%ndata
        tempPOT%z(:,i) = MATMUL(tempPOT%ut(:,:,i),tempPOT%r(:,i))
      enddo

    ! Expand permutations into distinct data points, because I hate looking at arrays in array indices.
	  ! Curse you, Deb.
    write(11,*) 'Permuting data points...'

    ! Store a copy of ndata, since I'm about to overwrite it.
    ndata = interp%ndata
    interp%ndata = interp%ndata*sys%ngroup

    ! Allocate the actual POT file.
    call this%new(sys,interp)

	  ! Copy permutated data into final POT object, check for and remove identical geometries.
	  counter = 0
	  do i=1,ndata! For each data point...
	    do n=1,sys%ngroup ! For each permutation of that data point...
        counter = counter+1
        ! Copy atom-atom distances
        do j=1,sys%nbond
          this%r(j,counter) = tempPOT%r(sys%bperm(j,n),i)
        enddo
        do j=1,sys%nNRbonds
          this%nonRigidr(j,counter) = tempPOT%nonRigidr(sys%nonRigidbperm(j,n),i)
        enddo
        ! Copy the uT matrix
        do j=1,sys%nbond
          do k=1,sys%nint
            this%ut(k,j,counter) = tempPOT%ut(k,sys%bperm(j,n),i)
          enddo
        enddo
        ! Copy, zetas, v0, dV/dZ, etc.
        ! (all of these are invariant under permutation)
        this%v0(counter) = tempPOT%v0(i)
        this%v1(:,counter) = tempPOT%v1(:,i)
        this%v2(:,counter) = tempPOT%v2(:,i)
        this%z(:,counter) = tempPOT%z(:,i)
        this%id(counter) = i ! not sure if I'll need this ID, but I'll keep it for now.
        this%perm(counter) = n ! not sure if I care about storing this, we'll see.
      enddo
	  enddo

    ! Check for identical data points.
    i=1
    do while (i.le.interp%ndata-1)
      j = i+1
      do while (j.le.interp%ndata)
        bondDiff = dist(this%nonRigidr(:,i),this%nonRigidr(:,j))
        if (bondDiff.lt.1.d-10) then
          this%r(:,j:interp%ndata-1) = this%r(:,j+1:interp%ndata)
          this%nonRigidr(:,j:interp%ndata-1) = this%nonRigidr(:,j+1:interp%ndata)
          this%ut(:,:,j:interp%ndata-1) = this%ut(:,:,j+1:interp%ndata)
          this%z(:,j:interp%ndata-1) = this%z(:,j+1:interp%ndata)
          this%v0(j:interp%ndata-1) = this%v0(j+1:interp%ndata)
          this%v1(:,j:interp%ndata-1) = this%v1(:,j+1:interp%ndata)
          this%v2(:,j:interp%ndata-1) = this%v2(:,j+1:interp%ndata)
          this%id(j:interp%ndata-1) = this%id(j+1:interp%ndata)
          this%perm(j:interp%ndata-1) = this%perm(j+1:interp%ndata)
          interp%ndata = interp%ndata-1
          cycle
        endif
        j=j+1
      enddo
      i=i+1
    enddo

    write(11,*) 'Number of data points used for interpolation:', interp%ndata

    ! deallocate tempPOT now that we're finished with it.
    call tempPOT%destroy()

    return
end subroutine read_pot

!===========================================================
! Allocates everything for the POT file.
!===========================================================
subroutine allocatePOT(this,sys,interp)
  use molecule_specs

  implicit none
  class(pot_file) :: this
  type (molsysdat), intent(inout) :: sys
  type (interp_params), intent(inout) :: interp

  allocate(this%r(sys%nbond,interp%ndata))
  allocate(this%nonRigidr(sys%nNRbonds,interp%ndata))
  allocate(this%ut(sys%nint,sys%nbond,interp%ndata))
  allocate(this%z(sys%nint,interp%ndata))
  allocate(this%v0(interp%ndata))
  allocate(this%v1(sys%nint,interp%ndata))
  allocate(this%v2(sys%nint,interp%ndata))
  allocate(this%id(interp%ndata))
  allocate(this%perm(interp%ndata))

end subroutine allocatePOT

!===========================================================
! Deallocates everything for the POT file.
!===========================================================
subroutine deallocatePOT(this)
  implicit none
  class(pot_file) :: this

  deallocate(this%r,this%nonRigidr,this%z,this%ut)
  deallocate(this%v0,this%v1,this%v2)
  deallocate(this%id,this%perm)

end subroutine deallocatePOT

!===========================================================
! Creates a new inner neighbour list, ready for data points.
!===========================================================
subroutine newNeighbourList(this,nData)
  class(neighbour_list) :: this
  integer, intent(in) :: nData
  this%n = 0
  allocate(this%points(1:nData))
  this%points = -1
end subroutine


!========================================================
! Adds a new data point to the neighbour list.
!========================================================
subroutine addNeighbour(this,DP)
  class(neighbour_list) :: this
  integer, intent(in) :: DP
  this%n = this%n+1
  this%points(this%n) = DP
end subroutine addNeighbour


end module interpolation
