!=======================================
! Produces |psi|**2 histograms defined by user in IN_DWCOORDS.
!=======================================
module Histograms_mod

implicit none

  private

  public :: SetupDW, DescendantWeighting

    ! A descendant weighting coordinate, defining a bond distance, angle or dihedral.
    ! Contains file name, histogram min, max and bin width, and indices of the relevant atoms.
    type Coordinate
        character(7) :: coordType ! 'Dist' 'Angle' 'Dihed'
        character(100) :: fileName
        character(100) :: name
        real(kind=8) :: min,max,binWidth
        real(kind=8) :: Average
        integer, dimension(:), allocatable :: indices
        real(kind=8), dimension(:), allocatable :: Histogram
        integer :: intMin, intMax
        integer :: fileUnit
    end type Coordinate

    ! Defines an observable of interest from descendant weighting.
    ! Can be distance, angle or dihedral (for the moment).
    type Observable
      character(7) :: ObsType ! 'Dist' 'Angle' 'Dihed'
      integer, dimension(:), allocatable :: vecInd ! Indexes of vectors
      real(kind=8) :: obs

      contains
        procedure, pass(this) :: calc => calcObservable
    end type Observable

    ! Defines a 2D histogram.
    type Histogram2D
      character(50) :: name
      real(kind=8) :: min1,max1,binWidth1,factor
      real(kind=8) :: min2,max2,binWidth2
      real(kind=8), dimension(:,:), allocatable :: hist
      integer :: n, intMin1, intMax1
      integer :: intMin2, intMax2
      integer, dimension(:,:), allocatable :: obsInd
      logical :: continuous = .false.

      contains
        procedure, pass(this) :: new => newHistogram2D
        procedure, pass(this) :: bin => binHistogram2D
        procedure, pass(this) :: output => writeHistogram2D

    end type Histogram2D

    ! Defines a 3D histogram (or volume/density map)
    type Histogram3D
      character(50) :: name
      real(kind=8) :: min1,max1,binWidth1,factor
      real(kind=8) :: min2,max2,binWidth2
      real(kind=8) :: min3,max3,binWidth3
      real(kind=8), dimension(:,:,:), allocatable :: hist
      integer :: n, intMin1, intMax1
      integer :: intMin2, intMax2
      integer :: intMin3, intMax3
      integer, dimension(:,:), allocatable :: obsInd
      logical :: continuous = .false.

      contains
        procedure, pass(this) :: new => newHistogram3D
        procedure, pass(this) :: bin => binHistogram3D
        procedure, pass(this) :: output => writeHistogram3D

    end type Histogram3D


    ! Defines a 1D histogram, with subroutines to bin and write to
    ! file.
    type Histogram
      character(50) :: name
      real(kind=8) :: min,max,binWidth,Avg,factor
      real(kind=8), dimension(:), allocatable :: hist
      integer :: n, intMin, intMax
      integer, dimension(:), allocatable :: obsInd
      logical :: continuous = .false.

      contains
        procedure, pass(this) :: new => newHistogram
        procedure, pass(this) :: bin => binHistogram
        procedure, pass(this) :: output => writeHistogram
    end type Histogram




    ! Defines a centre-of-mass used in histogram coordinates.
    type CoM
        integer, dimension(:), allocatable :: indices
        integer :: nAtoms
        real(kind=8) :: sumMasses
    end type CoM

    ! Defines a vector - whether it's a vector between two atoms/CoMs, or the addition/subtraction/cross product
    ! of two other vectors.
    type Vector
        integer, dimension(2) :: indices
        character(4) :: vecType
        real(kind=8) :: length ! if this vector is a cross product.
        real(kind=8), dimension(3) :: elements
        logical :: defined = .false.
    end type Vector


    type(Coordinate), dimension(:), allocatable, private, save :: Coordinates
    type(Coordinate), dimension(:), allocatable, private, save :: VecCoordinates
    type(Observable), dimension(:,:), allocatable, private, save :: Observables
    type(Histogram), dimension(:,:), allocatable, private, save :: Histograms
    type(Histogram2D), dimension(:,:), allocatable, private, save :: Histograms2D
    type(Histogram3D), dimension(:,:), allocatable, private, save :: Histograms3D
    type(CoM), dimension(:), allocatable, private, save :: CoMs
    type(Vector), dimension(:,:), allocatable, private, save :: Vectors
    integer, private, save :: nHists, nCoMs, nGhosts, nVectors, nVecHists, nObs
    integer, private, save :: nHists2D, nHists3D
    integer, private, save :: nThreads
    real(kind=8), dimension(:,:), allocatable, private, save :: ghostAtoms

    contains


!====================================================================================
! Reads IN_DWCOORDS, establishes coordinates in preparation for descendant weighting.
!====================================================================================
subroutine SetupDW()
  use omp_lib, ONLY : OMP_get_max_threads

  character(2) :: comment
  integer :: IOStatus
  integer :: i,j,k,n, unit
  logical :: fileExists

  INQUIRE(file='IN_DWCOORDS',exist=fileExists)

  if (.not.fileExists) stop 'IN_DWCOORDS not found!'

  nThreads = OMP_get_max_threads()

  open(newunit=unit,file='IN_DWCOORDS',status='old')

  read(unit,*,IOSTAT=IOStatus) comment

  ! Number of centre-of-masses first:
  read(unit,*,IOSTAT=IOStatus) nCoMs
  allocate(CoMs(1:nCoMs))

  ! Read the centre-of-masses.
  read(unit,*,IOSTAT=IOStatus) comment
  do i=1,nCoMs
    ! Read number of atoms first
    read(unit,*,IOSTAT=IOStatus) CoMs(i)%nAtoms
    backspace(unit) ! Back up one to read again

    ! Allocate and read indexes
    allocate(CoMs(i)%indices(1:CoMs(i)%nAtoms))
    read(unit,*,IOSTAT=IOStatus) CoMs(i)%nAtoms, (CoMs(i)%indices(j),j=1,CoMs(i)%nAtoms)

  enddo

  ! Number of vectors:
  read(unit,*,IOSTAT=IOStatus) comment
  read(unit,*,IOSTAT=IOStatus) nVectors
  allocate(Vectors(1:nVectors,1:nThreads))
  do i=1,7
    read(unit,*,IOSTAT=IOStatus) comment
  enddo

  ! Read all the vectors
  do i=1,nVectors
    read(unit,*,IOSTAT=IOStatus) Vectors(i,1)%vecType, (Vectors(i,1)%indices(n),n=1,2)

    if (trim(Vectors(i,1)%vecType).eq.'X') then
      ! Back up and get length of cross product.
      backspace(unit)
      read(unit,*,IOSTAT=IOStatus) Vectors(i,1)%vecType, (Vectors(i,1)%indices(n),n=1,2), Vectors(i,1)%length
    endif

    ! A few checks:
    if((trim(Vectors(i,1)%vecType).ne.'Vec') &
      .and.(trim(Vectors(i,1)%vecType).ne.'Add') &
      .and.(trim(Vectors(i,1)%vecType).ne.'Sub') &
      .and.(trim(Vectors(i,1)%vecType).ne.'X') &
      .and.(trim(Vectors(i,1)%vecType).ne.'Proj')) then
          print *, 'Unrecognised vector type in IN_DWCOORDS.'
          print *, 'Type:  ', trim(Vectors(i,1)%vecType)
          stop
    endif

    if (Vectors(i,1)%vecType.ne.'Vec') then

      if (ANY(Vectors(i,1)%indices.le.0).or.ANY(Vectors(i,1)%indices.gt.nVectors)) then
        print *, 'IN_DWCOORDS:'
        print *, 'Vector #', i, "references vector that doesn't exist."
        stop
      endif

      do j=1,2
        n = Vectors(i,1)%indices(j)
        if (.not.Vectors(n,1)%defined) then
          print *, 'IN_DWCOORDS:'
          print *, 'Vector #', n, 'referenced by vector #', i, 'is not defined yet!'
          print *, 'Check ordering in IN_DWCOORDS.'
          stop
        endif
      enddo
    endif
    Vectors(i,1)%defined = .true.
    ! Copy this vector for the rest of the threads.
    do j=2,nThreads
      Vectors(i,j) = Vectors(i,1)
    enddo

  enddo

  ! Read in observables.
  read(unit,*,IOSTAT=IOStatus) comment
  read(unit,*,IOSTAT=IOStatus) nObs
  allocate(Observables(1:nObs,1:nThreads))
  do i=1,6 ! Junk lines
    read(unit,*,IOSTAT=IOStatus) comment
  enddo
  do i=1,nObs
    ! Get type of observable first
    read(unit,*,IOSTAT=IOStatus) Observables(i,1)%ObsType

    select case (trim(Observables(i,1)%ObsType))
        case('Length') !
            n = 1
        case('Angle') ! Angle
            n = 2
        case('Dihed') ! Dihedral
            n = 3
        case('Comp') ! the component of one vector along another
            n = 2
        case default
            stop 'Unrecognised coordinate type in IN_DWCOORDS'
    end select

    allocate(Observables(i,1)%vecInd(1:n))


    backspace(unit)
    read(unit,*,IOSTAT=IOStatus) Observables(i,1)%ObsType,(Observables(i,1)%vecInd(j),j=1,n)

    ! Copy this observable for the rest of the threads.
    do j=2,nThreads
      Observables(i,j) = Observables(i,1)
    enddo

  enddo

  ! Read in the histogram details.
  read(unit,*,IOSTAT=IOStatus) comment
  read(unit,*,IOSTAT=IOStatus) nHists
  allocate(Histograms(1:nHists,1:nThreads))
  read(unit,*,IOSTAT=IOStatus) comment

  do i=1,nHists
    read(unit,*,IOSTAT=IOStatus) n
    backspace(unit)
    allocate(Histograms(i,1)%obsInd(1:n))
    read(unit,*,IOSTAT=IOStatus) Histograms(i,1)%n, (Histograms(i,1)%obsInd(j), j=1,n), &
      Histograms(i,1)%min, Histograms(i,1)%max, Histograms(i,1)%binWidth, Histograms(i,1)%name

    ! Quick check for badness:
    if (ANY(Histograms(i,1)%obsInd.le.0).or.ANY(Histograms(i,1)%obsInd.gt.nObs)) then
      print *, 'IN_DWCOORDS:'
      print *, 'Histogram #', i, "references observable that doesn't exist."
      stop
    endif

    if (Histograms(i,1)%binWidth.le.0) then
      print *, 'IN_DWCOORDS'
      print *, 'Histogram #', i, "has bin width <= 0."
      stop
    endif


    ! Flag dihedral histograms for continuity.
    do j=1,Histograms(i,1)%n
      n = Histograms(i,1)%obsInd(j)
      if (Observables(n,1)%ObsType.eq.'Dihed') then
        ! Assume continuity if min = 0 deg, max = 360 deg.
        if ((Histograms(i,1)%min.eq.0).and.(Histograms(i,1)%max.eq.360)) then
          Histograms(i,1)%continuous = .true.
          exit;
        endif
      endif
    enddo
    call Histograms(i,1)%new()

    ! Copy histogram for the rest of the threads
    do j=2,nThreads
      Histograms(i,j) = Histograms(i,1)
    enddo
  enddo

  ! Read details for 2D histograms.
  read(unit,*,IOSTAT=IOStatus) comment
  read(unit,*,IOSTAT=IOStatus) nHists2D
  ! Three lines of comments
  read(unit,*,IOSTAT=IOStatus) comment
  read(unit,*,IOSTAT=IOStatus) comment
  read(unit,*,IOSTAT=IOStatus) comment
  if (nHists2D.gt.0) then
    allocate(Histograms2D(1:nHists2D,1:nThreads))
    do i=1,nHists2D
      ! Read number of observables to bin in 2D histogram.
      read(unit,*,IOSTAT=IOStatus) n
      backspace(unit)

      allocate(Histograms2D(i,1)%obsInd(2,1:n))
      read(unit,*,IOSTAT=IOStatus) Histograms2D(i,1)%n, (Histograms2D(i,1)%obsInd(1,j), j=1,n), &
      Histograms2D(i,1)%min1, Histograms2D(i,1)%max1, Histograms2D(i,1)%binWidth1

      read(unit,*,IOSTAT=IOStatus) (Histograms2D(i,1)%obsInd(2,j), j=1,n), &
      Histograms2D(i,1)%min2, Histograms2D(i,1)%max2, Histograms2D(i,1)%binWidth2, Histograms2D(i,1)%name

      call Histograms2D(i,1)%new()

      ! Copy histogram for the rest of the threads
      do j=2,nThreads
        Histograms2D(i,j) = Histograms2D(i,1)
      enddo

    enddo
  endif

  ! Read details for 3D Histograms
  read(unit,*,IOSTAT=IOStatus) comment
  read(unit,*,IOSTAT=IOStatus) nHists3D

  if (nHists3D.gt.0) then
    allocate(Histograms3D(1:nHists3D,1:nThreads))
    ! Four lines of comments
    read(unit,*,IOSTAT=IOStatus) comment
    read(unit,*,IOSTAT=IOStatus) comment
    read(unit,*,IOSTAT=IOStatus) comment
    read(unit,*,IOSTAT=IOStatus) comment


    do i=1,nHists3D
      ! Read number of observables to bin in 2D histogram.
      read(unit,*,IOSTAT=IOStatus) n
      backspace(unit)

      allocate(Histograms3D(i,1)%obsInd(3,1:n))
      read(unit,*,IOSTAT=IOStatus) Histograms3D(i,1)%n, (Histograms3D(i,1)%obsInd(1,j), j=1,n), &
      Histograms3D(i,1)%min1, Histograms3D(i,1)%max1, Histograms3D(i,1)%binWidth1

      read(unit,*,IOSTAT=IOStatus) (Histograms3D(i,1)%obsInd(2,j), j=1,n), &
      Histograms3D(i,1)%min2, Histograms3D(i,1)%max2, Histograms3D(i,1)%binWidth2

      read(unit,*,IOSTAT=IOStatus) (Histograms3D(i,1)%obsInd(3,j), j=1,n), &
      Histograms3D(i,1)%min3, Histograms3D(i,1)%max3, Histograms3D(i,1)%binWidth3, Histograms3D(i,1)%name

      call Histograms3D(i,1)%new()

      ! Copy histogram for the rest of the threads
      do j=2,nThreads
        Histograms3D(i,j) = Histograms3D(i,1)
      enddo

    enddo
  endif



  close(unit)

! Read in ghost atoms
  nGhosts = 0

  ! Check that file exists first.
  INQUIRE(file='IN_GHOSTS',exist=fileExists)

  if (.not.fileExists) return ! no worries, no need to do anything.

  open(newunit=unit,file='IN_GHOSTS',status='unknown',action='read')

  ! comment line
  read(unit,*)
  ! read number of ghost atoms
  read(unit,*) nGhosts
  allocate(ghostAtoms(1:nGhosts,1:3))
  ! comment line
  read(unit,*)
  do i=1,nGhosts
    read(unit,*) (ghostAtoms(i,j),j=1,3)
  enddo

  close(unit)

end subroutine SetupDW



!=============================================================================
! Set up the histogram by calculating number of bins, etc. and allocate array.
!=============================================================================
subroutine newHistogram(this)
  class(Histogram) :: this

  this%intMin = int(anint(this%min/this%binWidth))
  this%intMax = int(anint(this%max/this%binWidth))
  allocate(this%hist(this%intMin:this%intMax))
  this%hist = 0.0d0
  this%Avg = 0.0d0
  ! A multiplicative factor, to account for multiple bins in single histogram
  this%factor = 1.0d0/real(this%n,8)

end subroutine

!=============================================================================
! Set up the histogram by calculating number of bins, etc. and allocate array.
!=============================================================================
subroutine newHistogram2D(this)
  class(Histogram2D) :: this

  this%intMin1 = int(anint(this%min1/this%binWidth1))
  this%intMax1 = int(anint(this%max1/this%binWidth1))
  this%intMin2 = int(anint(this%min2/this%binWidth2))
  this%intMax2 = int(anint(this%max2/this%binWidth2))
  allocate(this%hist(this%intMin1:this%intMax1,this%intMin2:this%intMax2))
  this%hist = 0.0d0
  ! A multiplicative factor, to account for multiple bins in single histogram
  this%factor = 1.0d0/real(this%n,8)

end subroutine

!=============================================================================
! Set up the histogram by calculating number of bins, etc. and allocate array.
!=============================================================================
subroutine newHistogram3D(this)
  class(Histogram3D) :: this

  this%intMin1 = int(anint(this%min1/this%binWidth1))
  this%intMax1 = int(anint(this%max1/this%binWidth1))
  this%intMin2 = int(anint(this%min2/this%binWidth2))
  this%intMax2 = int(anint(this%max2/this%binWidth2))
  this%intMin3 = int(anint(this%min3/this%binWidth3))
  this%intMax3 = int(anint(this%max3/this%binWidth3))
  allocate(this%hist(this%intMin1:this%intMax1,this%intMin2:this%intMax2,this%intMin3:this%intMax3))
  this%hist = 0.0d0
  ! A multiplicative factor, to account for multiple bins in single histogram
  this%factor = 1.0d0/real(this%n,8)

end subroutine


!------------------------------------------------------
! descendant weighting routine
! - called after monte carlo run is finished
! - calculates observables as averages over |psi|**2
!------------------------------------------------------
! ^ Heritage-listed banner

subroutine DescendantWeighting(sys,qdmc,Generations,mInd,writeXYZ)
  use molecule_specs, ONLY : molsysdat, pi, au2ang
  use qdmc_structures, ONLY : qdmc_par
  use DW_mod, ONLY : tGeneration
  use Utilities, ONLY : intern
  use vec_mod
  use Geometry_mod
  use omp_lib, ONLY : OMP_get_thread_num


  !========== Variables ===========
  type (molsysdat), intent(in)  :: sys
  type (qdmc_par), intent(in) :: qdmc
  type (tGeneration), intent(inout), dimension(:) :: Generations
  integer, intent(in) :: mInd
  logical, intent(in) :: writeXYZ

  ! Variables for descendant weighting itself:
  real(kind=8), dimension(1:qdmc%NumGenerations) :: denominator
  real(kind=8) :: weight

  ! Atom-atom distances
  real(kind=8), dimension(1:sys%nbond) :: walkerR
  real(kind=8), dimension(1:nThreads,1:sys%nbond) :: avr

  real(kind=8), dimension(1:sys%natom+nCoMs+nGhosts,1:3) :: walkerCoords

  ! Indexes
  integer :: i1, i2

  integer :: i, j, k, l, m, n, o, id ! stock up on loop counters
  integer :: unit
  !========== End variables =========





  ! Write XYZ first.
  if (writeXYZ) then
    open(newunit=unit,file='Generations.xyz',status='replace',action='write')

    do i=1,qdmc%NumGenerations
      write(unit,*) Generations(i)%NumWalkers*sys%natom
      write(unit,*) 'generation', i

      do j=1,Generations(i)%NumWalkers
      !  if (Generations(i)%parents(j)%descendants(mInd).ge.1) then
          do k=1,sys%natom
            write(unit,*) sys%atom_label(k), (Generations(i)%parents(j)%w%x(k,l)*au2ang, l=1,3)
          enddo
      !  endif
      enddo

    enddo

    close(unit)

  endif

  !--------------------------------------------
  ! check on descendants, make denominator
  !--------------------------------------------
  denominator = 0.0d0
  do i=1,qdmc%NumGenerations
    do j=1,Generations(i)%NumWalkers
      denominator(i) = denominator(i) + Generations(i)%parents(j)%descendants(mInd)
    enddo
  enddo

  ! Sum masses for each CoM defined by user.
  do i=1,nCoMs
    CoMs(i)%sumMasses = 0.0d0
    do j=1,CoMs(i)%nAtoms
      k = CoMs(i)%indices(j)
      CoMs(i)%sumMasses = CoMs(i)%sumMasses + sys%mass(k)
    enddo
  enddo

  if (sys%MOF5Hacks) call MOF5Hacks(sys,qdmc,walkerCoords,1,1)

  ! Start the loop over generations and walkers to calculate observables
  avr = 0.0d0
  !$OMP PARALLEL DO DEFAULT(SHARED) &
  !$OMP PRIVATE(weight,id,walkerR,walkerCoords,j,k,l,m,n,i1,i2)
  do i=1,qdmc%NumGenerations
    id = OMP_get_thread_num()+1

    do j=1,Generations(i)%NumWalkers
      weight = Generations(i)%parents(j)%descendants(mInd)/denominator(i)

      ! Calculate expectation values <r>
      call intern(sys,Generations(i)%parents(j)%w%x,walkerR)
      avr(id,:) = avr(id,:) + 1.0d0/walkerR(:) * weight

      ! Copy walker coordinates into array with centre-of-masses and ghost atoms.
      walkerCoords(1:sys%natom,:) = Generations(i)%parents(j)%w%x

      if (nGhosts.gt.0) then
        k = sys%natom + nCoMs + 1
        l = k + nGhosts - 1
        walkerCoords(k:l,:) = ghostAtoms(1:nGhosts,:)
      endif

      ! Calculate centre-of-masses
      do k=1,nCoMs
        l = sys%nAtom+k
        walkerCoords(l,:) = 0.0d0

        do m=1,CoMs(k)%nAtoms
          n = CoMs(k)%indices(m)
          walkerCoords(l,:) = walkerCoords(l,:)+sys%mass(n)*walkerCoords(n,:)
        enddo

        walkerCoords(l,:) = walkerCoords(l,:)/CoMs(k)%sumMasses
      enddo ! End loop over centre-of-masses

      ! (H2)4-Li+-benzene hacks - will just switch H2s and centre-of-masses around.
      if (sys%QH2Hacks) call QH2Hacks(sys,walkerCoords)

      ! Print details, if necessary
      if (qdmc%printVectors.or.qdmc%printObservables) then
        print *, '=============================================='
        print *, 'Walker', j, 'Gen', i
        print *, 'descendants', Generations(i)%parents(j)%descendants(mInd), 'weight', weight
        print *, ''
        do k=1,sys%natom+nCoMs
          if (k.le.sys%natom) then
            print *, sys%atom_label(k), (walkerCoords(k,l)*au2ang,l=1,3)
          else
            print *, 'Bq', (walkerCoords(k,l)*au2ang,l=1,3)
          endif
        enddo
        print *, ''
        if (qdmc%printVectors) print *, '--Vectors--'
      endif

      if (sys%MOF5Hacks) call MOF5Hacks(sys,qdmc,walkerCoords,2,id)

      ! Calculate all required vectors.
      do k=1,nVectors
        i1 = Vectors(k,id)%indices(1)
        i2 = Vectors(k,id)%indices(2)

        select case(trim(Vectors(k,id)%vecType))
          case('Vec') ! Define the vector from walker coordinates.
            Vectors(k,id)%elements = walkerCoords(i2,:) - walkerCoords(i1,:)
          case('Add') ! Add two vectors together.
            Vectors(k,id)%elements = Vectors(i1,id)%elements + Vectors(i2,id)%elements
          case('Sub') ! Subtract i2 vector from i1.
            Vectors(k,id)%elements = Vectors(i1,id)%elements - Vectors(i2,id)%elements
          case('X') ! Cross product * some length.
            Vectors(k,id)%elements = normalise(cross(Vectors(i1,id)%elements,Vectors(i2,id)%elements))*Vectors(k,id)%length
          case('Proj') ! Project vector 1 along vector 2.
            ! Here, use the property that if a1 is the vector projection of a along b,
            ! then a1 = (a.b)/(b.b) * b.
            Vectors(k,id)%elements = Vectors(i2,id)%elements * &
                DOT_PRODUCT(Vectors(i1,id)%elements,Vectors(i2,id)%elements) / &
                DOT_PRODUCT(Vectors(i2,id)%elements,Vectors(i2,id)%elements)
        end select
        if (qdmc%printVectors) print *, 'Bq', (Vectors(k,id)%elements(l)*au2ang,l=1,3)
      enddo

      if (qdmc%printObservables) print *, '--Observables--'

      ! Calculate all observables.
      do k=1,nObs
        call Observables(k,id)%calc(Vectors(:,id))
        if (qdmc%printObservables) print *, k, trim(Observables(k,id)%ObsType), ':', Observables(k,id)%obs
      enddo

      ! Bin the histograms.
      do k=1,nHists
        call Histograms(k,id)%bin(Observables(:,id),weight)
      enddo
      ! Bin 2D histograms
      do k=1,nHists2D
        call Histograms2D(k,id)%bin(Observables(:,id),weight)
      enddo
      ! Bin 3D histograms
      do k=1,nHists3D
        call Histograms3D(k,id)%bin(Observables(:,id),weight)
      enddo
    enddo ! End loop over walkers
  enddo ! End loop over generations
  !$OMP END PARALLEL DO

  ! Combine individual thread histograms into one. Will do this by summing them
  ! into the first copy of the histogram.
  do i=2,nThreads
    do j=1,nHists
      Histograms(j,1)%hist = Histograms(j,1)%hist + Histograms(j,i)%hist
      Histograms(j,1)%Avg = Histograms(j,1)%Avg + Histograms(j,i)%Avg
    enddo
    do j=1,nHists2D
      Histograms2D(j,1)%hist = Histograms2D(j,1)%hist + Histograms2D(j,i)%hist
    enddo
    do j=1,nHists3D
      Histograms3D(j,1)%hist = Histograms3D(j,1)%hist + Histograms3D(j,i)%hist
    enddo

    ! And the bond averages, too
    avr(1,:) = avr(1,:) + avr(i,:)
  enddo

  ! Loop over histograms, write files.
  do i=1,nHists
    call Histograms(i,1)%output(qdmc%NumGenerations,qdmc%stem)
  enddo
  do i=1,nHists2D
    call Histograms2D(i,1)%output(qdmc%NumGenerations,qdmc%stem)
  enddo
  do i=1,nHists3D
    call Histograms3D(i,1)%output(qdmc%NumGenerations,qdmc%stem)
  enddo

  ! Final MOF-5 hacks, if necessary.
  if (sys%MOF5Hacks) call MOF5Hacks(sys,qdmc,walkerCoords,3,1)

  avr = avr/real(qdmc%numGenerations,8)
  print *, ' i   <r>'
  12 format(2x,i4,2x,g12.5)
  do i=1,sys%nbond
    print 12, i, avr(1,i)
  enddo
  print *, ' '
  print *, '---------------------Expectation values----------------------'
  do i=1,nHists
    print *, '<',trim(Histograms(i,1)%name),'>', Histograms(i,1)%Avg
  enddo
  print *, '-------------------------------------------------------------'





end subroutine DescendantWeighting

!===================================================================
! Perform the various MOF-5 hacks for histograms, heat maps, etc.
!===================================================================
subroutine MOF5Hacks(sys,qdmc,coords,stage,id)
  use molecule_specs, ONLY : molsysdat, pi, au2ang
  use qdmc_structures, ONLY : qdmc_par
  use DW_mod, ONLY : tGeneration
  use Utilities, ONLY : intern
  use vec_mod
  use Geometry_mod


  !========== Variables ===========
  type (molsysdat), intent(in)  :: sys
  type (qdmc_par), intent(in) :: qdmc
  real(kind=8), dimension(sys%natom+nCoMs+nGhosts,3), intent(inout) :: coords
  integer, intent(in) :: stage ! 1=setup, 2=vector indices, 3=something
  integer, intent(in) :: id ! thread number so we don't ruin things

  logical, save :: setup = .false.

  real(kind=8), dimension(3,3), save :: latticeVec, recipVec

  integer :: i, j, k, l, m, unit
  integer, save :: aMin, aMax, bMin, bMax, oMin, oMax, ZnMin, ZnMax
  real(kind=8) :: closest, distance
  real(kind=8), dimension(sys%natom+nCoMs,3) :: fracCoords
  real(kind=8), dimension(4) :: Zns
  integer, dimension(4) :: ZnInds
  integer, dimension(3) :: shift

  ! Things for radial distribution function
  real(kind=8), dimension(3) :: dx
  real(kind=8), allocatable, dimension(:,:), save :: gr
  real(kind=8), save :: delG
  real(kind=8) :: rho, nIdeal, vol
  integer, parameter :: nbins = 100
  integer :: binInt
  integer, allocatable, dimension(:), save :: ngr
  character(100) :: fileName


  select case(stage)
  case(1) ! Setup everything required for MOF-5 hacks

    if (nGhosts.ne.67) then
      print *, 'IN_GHOSTS not appropriate for MOF-5. Need nGhosts = 67.'
      print *, 'nGhosts = ', nGhosts
      call exit(1)
    endif
    ! Indexes of oxygens, cell centres and zinc atoms in IN_GHOSTS
    j = sys%natom+nCoMs
    oMin = 1+j
    oMax = 8+j
    aMin = 9+j
    aMax = 35+j
    bMin = 10+j
    bMax = 34+j
    ZnMin = 36+j
    ZnMax = 67+j

    call sys%readUnitVectors()
    latticeVec = sys%lVectors ! store a local copy of the lattice vectors

    ! Calculate reciprocal vectors
    recipVec(1,:) = cross(latticeVec(2,:),latticeVec(3,:))
    recipVec(2,:) = cross(latticeVec(3,:),latticeVec(1,:))
    recipVec(3,:) = cross(latticeVec(1,:),latticeVec(2,:))

    recipVec(:,:) = recipVec(:,:)/DOT_PRODUCT(recipVec(1,:),latticeVec(1,:))

    ! Setup+allocate radial distribution function
    if (sys%natom.gt.2) then
      ! Set interval size based on length of box
      delG = sys%length/(2.d0*real(nbins,8))
      allocate(gr(nThreads,0:nbins))
      gr = 0.d0
      allocate(ngr(nThreads))
      ngr = 0
    endif


    !!! Insert heat map stuff here !!!

    setup = .true.
  case(2) ! Find nearest framework atoms, update vector indexes
    if (.not.setup) stop "MOF5 hacks have not been set up."

    ! Calculate fractional coordinates, shift to unit cell if required.
    j = sys%natom+nCoMs
    fracCoords = sys%cartToFrac(j,coords(1:j,:))

    ! Loop over H2 centre-of-masses
    do i=sys%natom+1,sys%natom+nCoMs
      shift = 0
      do j=1,3
        if (fracCoords(i,j).gt.1.d0) then
          shift(j) = -int(fracCoords(i,j))
        else if (fracCoords(i,j).lt.0d0) then
          shift(j) = -int(fracCoords(i,j))+1
        endif
      enddo

      ! Calculate CoM-H vectors
      k =(i-sys%natom-1)*2+1 ! this will always give the index of the
      ! first H atom
      ! This is CoM-H for both H atoms
      do j=k,k+1
        fracCoords(j,:) = fracCoords(j,:) - fracCoords(i,:)
      enddo
      ! Shift the centre-of-mass
      fracCoords(i,:) = fracCoords(i,:) + shift(:)
      ! Calculate new H positions
      do j=k,k+1
        fracCoords(j,:) = fracCoords(j,:) + fracCoords(i,:)
      enddo
    enddo

    ! Convert fractional coordinates to cartesians
    j = sys%natom+nCoMs
    coords(1:j,:) = sys%fracToCart(j,fracCoords(1:j,:))

    ! Loop over centre-of-masses again
    do i=sys%natom+1,sys%natom+nCoMs
      ! Find nearest oxygen atom to the H2
      closest=HUGE(closest)
      k = i-sys%natom ! index of O-H2CoM vector
      l = i-sys%natom+nCoMs ! index of z axis vector
      m = i-sys%natom+2*nCoMs ! index of O-Zn vector
      do j=oMin,oMax
        call distt(coords(i,:),coords(j,:),distance)
        if (distance.lt.closest) then
          closest = distance
          Vectors(k,id)%indices(1) = j
          Vectors(l,id)%indices(1) = j
          Vectors(m,id)%indices(1) = j
        endif
      enddo
      ! Find nearest sub-cell centre
      closest=HUGE(closest)
      do j=aMin,aMax
        call distt(coords(i,:),coords(j,:),distance)
        if (distance.lt.closest) then
          closest = distance
          Vectors(l,id)%indices(2) = j
        endif
      enddo

      ! Find four nearest Zn atoms to H2 centre-of-mass
      Zns = HUGE(distance)
      ZnInds = -1
      do j=ZnMin,ZnMax
        call distt(coords(j,:),coords(i,:),distance)
        if (ANY(distance.le.Zns)) then
          ! Add this distance to the array of Zn distances
          l = 1
          do while(distance.gt.Zns(l))
            l = l+1
          enddo
          if (l.lt.4) then
            Zns(l+1:4) = Zns(l:3)
            ZnInds(l+1:4) = ZnInds(l:3)
          endif
          Zns(l) = distance
          ZnInds(l) = j
        endif
      enddo
      ! Now, the largest H2-Zn distance will be the Zn atom that points
      ! out of the sub-cell. For the x-axis, exclude the fourth
      ! Zn atom and use the one with the lowest index. This will keep the x-axis
      ! consistent for this alpha site.
      l = i-sys%natom+2*nCoMs
      Vectors(m,id)%indices(2) = MINVAL(ZnInds(1:3))

      ! Update H-Zn vectors
      l = 7*nCoMs+i-sys%natom
      Vectors(l,id)%indices(2) = ZnInds(1)
      Vectors(l+nCoMs,id)%indices(2) = ZnInds(1)
    enddo

    ! Loop over the centre-of-masses one last time, bin radial distribution function.
    if (sys%natom.gt.2) then
      ngr(id) = ngr(id)+1
      do i=sys%natom+1,sys%natom+nCoMs-1
        do j=i+1,sys%natom+nCoMs
          ! Calculate difference between x,y,z fractional coordinates for the
          ! centre-of-masses
          dx = fracCoords(i,:) - fracCoords(j,:)

          ! Apply periodic boundary conditions: if dx > 0.5 (or < -0.5) for any x,y,z coordinate,
          ! then anint(dx) = ±1 and subtracting anint(dx) maps dx to the nearest periodic
          ! image
          dx = dx - anint(dx)

          ! Calculate the distance
          distance = DSQRT(DOT_PRODUCT(dx,dx))*sys%length

          ! Only bin distances within half the box length
          if (distance.lt.0.5d0*sys%length) then
            binInt = int(distance/delG)
            gr(id,binInt) = gr(id,binInt)+2
          endif
        enddo
      enddo
    endif
  case(3) ! Write out radial distribution function
    if (sys%natom.gt.2) then
      ! The ideal gas density = n_particles/volume
      rho = nCoMs/(sys%length**3)
      fileName = 'Radial_Hist.'//trim(qdmc%stem)
      open(newunit=unit,file=fileName,status='unknown',action='write')
      ngr(1) = SUM(ngr)
      do i=1,nbins
        distance = delG*(i+1) ! the distance to the next bin
        ! the volume between this and the next bin
        vol = ((i+1)**3 - i**3)*delG**3

        ! The number of ideal gas particles in this volume
        nIdeal = 4.d0/3.d0*pi*vol*rho
        gr(1,i) = SUM(gr(:,i))/real(nCoMs*nIdeal*ngr(1),8)

        write(unit,*) distance, gr(1,i)
      enddo
      close(unit)

    endif
  end select

end subroutine MOF5Hacks

subroutine QH2Hacks(sys,coords)
  use molecule_specs, ONLY : molsysdat, pi, au2ang
  use vec_mod
  use Geometry_mod

  !========== Variables ===========
  type (molsysdat), intent(in)  :: sys
  real(kind=8), dimension(sys%natom+nCoMs+nGhosts,3), intent(inout) :: coords
  real(kind=8) :: maxDist, dist
  real(kind=8), dimension(2,3) :: H2Coords
  integer :: CoMFurthest, i, indFurthest

  ! Calculate furthest H2 from Li+.
  maxDist = 0.d0
  CoMFurthest = -1
  do i=23,26
    call distt(coords(1,:),coords(i,:),dist)
    if (dist.gt.maxDist) then
      maxDist = dist
      CoMFurthest = i
    endif
  enddo
  indFurthest = 14+(CoMFurthest-23)*2

  ! Swap furthest H2 with fourth one
  H2Coords(:,:) = coords(indFurthest:indFurthest+1,:)
  coords(indFurthest:indFurthest+1,:) = coords(20:21,:)
  coords(20:21,:) = H2Coords(:,:)
  H2Coords(1,:) = coords(CoMFurthest,:)
  coords(CoMFurthest,:) = coords(26,:)
  coords(26,:) = H2Coords(1,:)

end subroutine QH2Hacks



!===================================================================
! Calculate and return observable. Segregates a bit of code from
! descendant weighting routine. Even though Vectors array is accessible
! to whole module, I'm making it read-only here.
!===================================================================
subroutine calcObservable(this,Vectors)
  use molecule_specs, ONLY : pi
  use vec_mod

  class(Observable) :: this
  type(Vector), dimension(nVectors), intent(in) :: Vectors
  integer :: i1, i2, i3

  this%obs = 0.0d0
  select case(this%ObsType)
    case('Length')
      i1 = this%vecInd(1)
      this%obs = DSQRT(DOT_PRODUCT(Vectors(i1)%elements,Vectors(i1)%elements))
    case('Angle')
      i1 = this%vecInd(1)
      i2 = this%vecInd(2)
      this%obs = ANGVEC(Vectors(i1)%elements,Vectors(i2)%elements)*180.0d0/pi
    case('Dihed')
      i1 = this%vecInd(1)
      i2 = this%vecInd(2)
      i3 = this%vecInd(3)
      call dihedralVec(Vectors(i1)%elements,Vectors(i2)%elements,Vectors(i3)%elements,this%obs)
    case('Comp')
      i1 = this%vecInd(1)
      i2 = this%vecInd(2)

      ! Calculate component of first vector along second one.
      ! The component of the vector a along b is given by (a.b)/|b|.
      this%obs = DOT_PRODUCT(Vectors(i1)%elements,Vectors(i2)%elements)
      this%obs = this%obs/DSQRT(DOT_PRODUCT(Vectors(i2)%elements,Vectors(i2)%elements))

  end select

end subroutine

!===========================================================================
! Bins the given histogram, taking the observables and weight of the walker.
!===========================================================================
subroutine binHistogram(this,Observables,weight)
  class(Histogram) :: this
  type(Observable), dimension(1:nObs), intent(in) :: Observables
  real(kind=8), intent(in) :: weight
  real(kind=8) :: obs
  integer :: i, j, binInt

  ! Loop over all observables to be binned into this histogram.
  do i=1,this%n
    j=this%obsInd(i)
    obs = Observables(j)%obs

    if (obs.gt.this%max) then
      obs = this%max
    else if (obs.lt.this%min) then
      obs = this%min
    endif

    j = int(anint(obs/this%binWidth))
    this%hist(j) = this%hist(j) + this%factor*weight
    this%Avg = this%Avg + obs*this%factor*weight
  enddo
end subroutine binHistogram

!===========================================================================
! Bins the given histogram, taking the observables and weight of the walker.
!===========================================================================
subroutine binHistogram2D(this,Observables,weight)
  class(Histogram2D) :: this
  type(Observable), dimension(1:nObs), intent(in) :: Observables
  real(kind=8), intent(in) :: weight
  real(kind=8) :: obs1, obs2
  integer :: i, j, k, binInt

  ! Loop over all observables to be binned into this histogram.
  do i=1,this%n
    j=this%obsInd(1,i)
    k=this%obsInd(2,i)
    obs1 = Observables(j)%obs
    obs2 = Observables(k)%obs

    if (obs1.gt.this%max1) then
      obs1 = this%max1
    else if (obs1.lt.this%min1) then
      obs1 = this%min1
    endif

    if (obs2.gt.this%max2) then
      obs2 = this%max2
    else if (obs2.lt.this%min2) then
      obs2 = this%min2
    endif

    j = int(anint(obs1/this%binWidth1))
    k = int(anint(obs2/this%binWidth2))

    this%hist(j,k) = this%hist(j,k) + this%factor*weight
  enddo
end subroutine binHistogram2D

!===========================================================================
! Bins the given 3D histogram, taking the observables and weight of the walker.
!===========================================================================
subroutine binHistogram3D(this,Observables,weight)
  class(Histogram3D) :: this
  type(Observable), dimension(1:nObs), intent(in) :: Observables
  real(kind=8), intent(in) :: weight
  real(kind=8) :: obs1, obs2, obs3
  integer :: i, j, k, l, binInt

  ! Loop over all observables to be binned into this histogram.
  do i=1,this%n
    j=this%obsInd(1,i)
    k=this%obsInd(2,i)
    l=this%obsInd(3,i)
    obs1 = Observables(j)%obs
    obs2 = Observables(k)%obs
    obs3 = Observables(l)%obs

    if (obs1.gt.this%max1) then
      obs1 = this%max1
    else if (obs1.lt.this%min1) then
      obs1 = this%min1
    endif

    if (obs2.gt.this%max2) then
      obs2 = this%max2
    else if (obs2.lt.this%min2) then
      obs2 = this%min2
    endif

    if (obs3.gt.this%max3) then
      obs3 = this%max3
    else if (obs3.lt.this%min3) then
      obs3 = this%min3
    endif

    j = int(anint(obs1/this%binWidth1))
    k = int(anint(obs2/this%binWidth2))
    l = int(anint(obs3/this%binWidth3))

    this%hist(j,k,l) = this%hist(j,k,l) + this%factor*weight
  enddo
end subroutine binHistogram3D

!===========================================================================
! Writes the given histogram to its file.
!===========================================================================
subroutine writeHistogram(this,nGens,stem)
  class(Histogram) :: this
  integer, intent(in) :: nGens
  character(256), intent(in) :: stem
  character(100) :: fileName
  integer :: unit, i
  real(kind=8) :: x

  ! Ensure histogram is continuous, if applicable
  if (this%continuous) then
    this%hist(this%intMin) = this%hist(this%intMin) + this%hist(this%intMax)
    this%hist(this%intMax) = this%hist(this%intMin)
  endif

  this%hist = this%hist/real(nGens,8)
  this%Avg = this%Avg/real(nGens,8)

  fileName = trim(this%name)//'_Hist.'//trim(stem)

  ! Write histogram to file!
  open(newunit=unit,file=trim(fileName),status='unknown')
  x = this%min
  do i=this%intMin, this%intMax
    write(unit,*) x, this%hist(i)
    x = x+this%binWidth
  enddo
  close(unit)
end subroutine writeHistogram

!===========================================================================
! Writes the given 2D histogram to its file.
!===========================================================================
subroutine writeHistogram2D(this,nGens,stem)
  class(Histogram2D) :: this
  integer, intent(in) :: nGens
  character(256), intent(in) :: stem
  character(100) :: fileName
  integer :: unit, i, j
  real(kind=8) :: x, y


  this%hist = this%hist/real(nGens,8)

  fileName = trim(this%name)//'_Hist.'//trim(stem)

  ! Write histogram to file!
  open(newunit=unit,file=trim(fileName),status='unknown')
  x = this%min1
  do i=this%intMin1, this%intMax1
    y = this%min2
    do j=this%intMin2, this%intMax2
      write(unit,*) x, y, this%hist(i,j)
      y = y + this%binWidth2
    enddo
    x = x+this%binWidth1
    write(unit,*) ''
  enddo
  close(unit)
end subroutine writeHistogram2D

!===========================================================================
! Writes the given 3D histogram to its file.
!===========================================================================
subroutine writeHistogram3D(this,nGens,stem)
  class(Histogram3D) :: this
  integer, intent(in) :: nGens
  character(256), intent(in) :: stem
  character(100) :: fileName, bovName
  integer :: unit, i, j, n1,n2,n3
  real(kind=4) :: x, y, z


  this%hist = this%hist/real(nGens,8)

  fileName = trim(this%name)//'.dat'//trim(stem)
  bovName = trim(this%name)//'.bov'//trim(stem)


  ! First write the control details to a .bov file for VisIt.
  open(newunit=unit,file=trim(bovName),status='unknown')
  write(unit,*) 'TIME: 0.0000'
  write(unit,*) 'DATA_FILE: ', trim(fileName)
  n1 = this%intMax1-this%intMin1+1
  n2 = this%intMax2-this%intMin2+1
  n3 = this%intMax3-this%intMin3+1
  write(unit,*) 'DATA_SIZE:', n1, n2, n3
  write(unit,*) 'DATA_FORMAT: DOUBLE'
  write(unit,*) 'VARIABLE: ProbabilityDensity'
  write(unit,*) 'DATA_ENDIAN: LITTLE'
  write(unit,*) 'CENTERING: zonal'
  x = this%min1*0.52917720859
  y = this%min2*0.52917720859
  z = this%min3*0.52917720859
  write(unit,*) 'BRICK_ORIGIN:', x, y, z
  x = (this%max1-this%min1)*0.52917720859
  y = (this%max2-this%min2)*0.52917720859
  z = (this%max3-this%min3)*0.52917720859
  write(unit,*) 'BRICK_SIZE:', x, y, z
  write(unit,*) 'BYTE_OFFSET: 4'
  close(unit)


  ! Write volume data to file.
  open(newunit=unit,file=trim(fileName),form='unformatted',access='sequential')
  write(unit) this%hist
  close(unit)
end subroutine writeHistogram3D



    !------------------------------------------------------
    ! descendant weighting routine
    ! - called after monte carlo run is finished
    ! - calculates observables as averages over |psi|**2
    !------------------------------------------------------

!ubroutine OldDescendantWeighting(sys,qdmc,Generations,mInd,writeXYZ)

!    use molecule_specs, ONLY : molsysdat, pi, au2ang
!    use qdmc_structures, ONLY : qdmc_par
!    use DW_mod, ONLY : tGeneration
!    use Utilities, ONLY : intern
!    use vec_mod
!    use Geometry_mod

!    implicit none

!    type (molsysdat), intent(in)  :: sys
!    type (qdmc_par), intent(in) :: qdmc
!    type (tGeneration), intent(inout), dimension(:) :: Generations
!    integer, intent(in) :: mInd
!    logical, intent(in) :: writeXYZ

!    real(kind=8) :: e, sumMasses, observable, denom
!    real(kind=8), dimension(:), allocatable :: denominator, avr, avrsq, avinvrsq
!    real(kind=8), dimension(:,:), allocatable :: WalkerCoords
!    real(kind=8), dimension(1:nVectors,3) :: VectorCarts ! cartesian coordinates for vectors.
!    real(kind=8), dimension(1:sys%nbond) :: walkerR

!    integer :: error, i, j, k, l, m, n, CoMInd, atomInd

!    integer :: d, binInt

!    integer :: i1, i2, i3, i4, startInd, endInd

!    ! Junk needed for writing histogram to file.
!    character(100) :: fileName
!    integer :: u
!    real(kind=8) :: x

!    ! Some things for MOF-5 only, won't affect anything that's non-MOF-5...
!    real(kind=8), dimension(1:3,1:3) :: latticeVec, recipVec
!    real(kind=8) :: sumV, b1, b2, dist, Zcomp
!    real(kind=8), dimension(1:sys%natom,1:3) :: shiftedCoords
!    real(kind=8), dimension(1:2,1:3) :: CoMH
!    real(kind=8), dimension(1:3) :: H2CoM, shiftedCoM, fracCoords, tempVec, proj
!    integer, dimension(1:3) :: shift
!    real(kind=8) :: shortest
!    real(kind=8), dimension(1:8) :: nearestOxides
!    real(kind=8), dimension(1:4) :: nearestZn
!    integer, dimension(1:4) :: Zn
!    ! Rubbish for heat-map plot.
!    real(kind=8), dimension(:,:), allocatable :: heatMap
!    real(kind=4) :: mapWidth
!    integer :: aMin, aMax, bMin, bMax, oMin, oMax, ZnMin, ZnMax
!    integer, dimension(1:8,1:2) :: aZind ! indexes of fixed alpha z axes - in form of oxide -> centre
!    integer, dimension(1:8,1:2) :: bZind ! indexes of fixed beta z axes
!    real(kind=8), dimension(1:8,1:3,1:3) :: alphaFrame
!    real(kind=8), dimension(1:8,1:3,1:3) :: betaFrame
!    real(kind=8), dimension(1:3,1:3) :: nearestFrame
!    real(kind=8), dimension(1:3) :: xyzComp
!    logical :: alpha
!    integer :: ZnInd, Hind, hUnit, binX, binY, max, Xind, CentInd, OInd, cellInd
!    logical :: fileExists

!    real(kind=8), dimension(:), allocatable :: gr
!    real(kind=8) :: box, delG, rho, vol, nIdeal
!    real(kind=8), dimension(3) :: dx
!    integer :: grMax, totWalkers



!    ! Set up radial distribution function, if applicable.
!    if (sys%RDF) then
!      ! Let the lattice vectors define the box for the radial distribution function
!      call sys%readUnitVectors()

!      ! Get the total number of walkers.
!      totWalkers = 0
!      do i=1,qdmc%NumGenerations
!        totWalkers = totWalkers + Generations(i)%NumWalkers
!      enddo

!      e = 100 ! number of bins
!      if (sys%pbc) then
!        ! Set interval size based on length of the box
!        delG = sys%length/(2.0d0*e)
!        grMax = int(e)
!      else
!        ! Otherwise I'll set the max to 100 bohr.
!        delG = 100.0d0/e
!        grMax = int(e)
!      endif
!      ! Allocate g(r) array:
!      allocate(gr(0:grMax))

!      gr = 0.0d0

!    endif



!    !================== MOF-5 hacks first! ========================!
!    if (sys%MOF5Hacks) then

!      ! Assign lattice vectors (in bohr)
!      latticeVec(1,1:3) = (/-47.1602326565432,4.59870187374042,-10.3348570324944/)
!      latticeVec(2,1:3) = (/11.1980138674596,12.6773936715671,-45.4525508413023/)
!      latticeVec(3,1:3) = (/-1.60826516066104,-46.5847807150840,-13.3896264284622/)

!      ! Calculate reciprocal vectors
!      recipVec(1,:) = cross(latticeVec(2,:),latticeVec(3,:))
!      recipVec(2,:) = cross(latticeVec(3,:),latticeVec(1,:))
!      recipVec(3,:) = cross(latticeVec(1,:),latticeVec(2,:))

!      sumV = 0.0d0

!      do i=1,3
!        sumV = sumV + recipVec(1,i)*latticeVec(1,i)
!      enddo

!      recipVec(:,:) = recipVec(:,:)/sumV

!      if (sys%heatMap) then
!        if (nGhosts.ne.67) stop 'IN_GHOSTS not appropriate for heat map. nGhosts should be 67.'
!        ! Get bin width - this is so I don't need to keep recompiling this thing.
!        INQUIRE(file='IN_MAP',exist=fileExists)
!        if (.not.fileExists) stop 'IN_MAP not found!'
!        open(newunit=hUnit,file='IN_MAP',status='unknown',action='read')

!        read(hUnit,*) ! comment line
!        read(hUnit,*) mapWidth
!        close(hUnit)
!        ! Get max integer bin in array, allocate.
!        max = int(1/mapWidth)
!        allocate(heatMap(0:max+1,0:max+1))
!        heatMap = 0.0d0

!        oMin = 1
!        oMax = 8
!        aMin = 9
!        aMax = 35
!        bMin = 10
!        bMax = 34
!        ZnMin = 36
!        ZnMax = 67

!        ! Assign indexes of fixed alpha z axes. Oxygen atom first, then subcell centre.
!        aZind(1,1:2) = (/1,13/)
!        aZind(2,1:2) = (/1,19/)
!        aZind(3,1:2) = (/2,25/)
!        aZind(4,1:2) = (/2,31/)
!        aZind(5,1:2) = (/3,13/)
!        aZind(6,1:2) = (/3,25/)
!        aZind(7,1:2) = (/4,19/)
!        aZind(8,1:2) = (/4,31/)
!        ! Indexes of fixed beta z axes.
!        bZind(1,1:2) = (/1,22/)
!        bZind(2,1:2) = (/2,22/)
!        bZind(3,1:2) = (/3,22/)
!        bZind(4,1:2) = (/4,22/)
!        bZind(5,1:2) = (/1,10/)
!        bZind(6,1:2) = (/2,34/)
!        bZind(7,1:2) = (/3,16/)
!        bZind(8,1:2) = (/4,28/)

!        alphaFrame = 0.0d0
!        betaFrame = 0.0d0

!        ! Calculate axes vectors for fixed alpha frames.
!        do i=1,8
!          OInd = aZind(i,1)
!          CentInd = aZind(i,2)

!          ! Z axis is just the oxygen-centre vector.
!          alphaFrame(i,3,:) = normalise(ghostAtoms(CentInd,:)-ghostAtoms(OInd,:))

!          ! Get the three zincs that form the alpha site in this subcell:
!          Zn = 0
!          m = 0
!          do j=ZnMin,ZnMax
!            ! Calculate the O-Zn-centre distance (this will give three Zincs in "cup" of this site).
!            ! This should be 25ish bohr.
!            tempVec(:) = ghostAtoms(j,:) - ghostAtoms(OInd,:) ! O-Zn vector
!            dist = dsqrt(DOT_PRODUCT(tempVec,tempVec))
!            tempVec(:) = ghostAtoms(CentInd,:) - ghostAtoms(j,:)
!            dist = dist + dsqrt(DOT_PRODUCT(tempVec,tempVec))
!            if (dist.lt.25.0d0) then
!              m = m+1
!              Zn(m) = j
!            endif
!          enddo

!          ! For a consistent x-axis here, I'll use the Zn atom with lowest index.
!          ZnInd = MINVAL(Zn(1:3))

!          ! X-axis is the component of the O-Zn vector perpendicular to the Z-axis:
!          tempVec(:) = ghostAtoms(ZnInd,:) - ghostAtoms(OInd,:)

!          ! Project O-Zn along the Z-axis:
!          ! First find the component of O-Zn along Z - this is the dot product of O-Zn and Z.
!          Zcomp = DOT_PRODUCT(tempVec(:),alphaFrame(i,3,:))

!          ! Then multiply z by this component.
!          proj(:) = Zcomp * alphaFrame(i,3,:)

!          ! Calculate x-axis by subtracting this projection from the O-Zn vector:
!           alphaFrame(i,1,:) = normalise(tempVec(:) - proj(:))

!          ! The y-axis is just the cross product of these
!           alphaFrame(i,2,:) = cross(alphaFrame(i,1,:),alphaFrame(i,3,:))

!        enddo

!        ! Calculate axes vectors for fixed beta frames.
!        do i=1,8
!          OInd = bZind(i,1)
!          CentInd = bZind(i,2)

!          ! As before, Z axis is just the oxygen-centre vector.
!          betaFrame(i,3,:) = normalise(ghostAtoms(CentInd,:)-ghostAtoms(OInd,:))

!          ! Find nearest Zn tetrahedral again
!          Zn = 0
!          m = 0
!          do j=ZnMin,ZnMax
!            ! Calculate O-Zn distance.
!            tempVec(:) = ghostAtoms(j,:) - ghostAtoms(OInd,:)
!            dist = dsqrt(DOT_PRODUCT(tempVec,tempVec))
!            if (dist.lt.6.0d0) then
!              m = m+1
!              Zn(m) = j
!            endif
!          enddo

!          ! Discard one O-Zn vector - the one collinear with the z axis - by checking cross products.
!          do j=1,4
!            tempVec(:) = ghostAtoms(Zn(j),:) - ghostAtoms(OInd,:)
!            tempVec(:) = cross(tempVec(:),betaFrame(i,3,:))
!            if (DOT_PRODUCT(tempVec,tempVec).lt.1.0d-10) then
!              Zn(j) = -1
!              ZnInd = MINVAL(Zn,Zn.gt.0)
!              exit
!            endif
!          enddo

!          ! ZnInd has been set to the smallest index of the three appropriate Zn atoms, define x- and y-axes
!          ! in a similar manner as before.
!          tempVec(:) = ghostAtoms(ZnInd,:) - ghostAtoms(OInd,:)

!          ! Component of this vector along the z-axis:
!          Zcomp = DOT_PRODUCT(tempVec(:),betaFrame(i,3,:))

!          ! Project along the z axis
!          proj(:) = Zcomp * betaFrame(i,3,:)

!          ! Subtract this projection from O-Zn:
!          betaFrame(i,1,:) = normalise(tempVec(:)-proj(:))

!          ! Cross product for y axis:
!          betaFrame(i,2,:) = cross(betaFrame(i,1,:),betaFrame(i,3,:))


!        enddo

!        ! Shift starting indices now.
!        oMin = oMin+sys%nAtom+nCoMs
!        oMax = oMax+sys%nAtom+nCoMs
!        aMin = aMin+sys%nAtom+nCoMs
!        aMax = aMax+sys%nAtom+nCoMs
!        bMin = bMin+sys%nAtom+nCoMs
!        bMax = bMax+sys%nAtom+nCoMs
!        ZnMin = ZnMin+sys%nAtom+nCoMs
!        ZnMax = ZnMax+sys%nAtom+nCoMs
!        aZind = aZind+sys%nAtom+nCoMs
!        bZind = bZind+sys%nAtom+nCoMs

!      endif

!      ! Loop through walkers, get fractional coordinates and shift to
!      ! unit cell.
!      do i=1,qdmc%NumGenerations
!        do j=1, Generations(i)%NumWalkers
!          do m=1,sys%natom,2 ! Loop over H2 molecules
!            fracCoords = 0.0d0
!            CoMH = 0.0d0
!            call distt(Generations(i)%parents(j)%w%x(m,:),Generations(i)%parents(j)%w%x(m+1,:),b1)
!            !print *, 'coords before:'

!            ! Calculate H2 centre-of-mass
!            H2CoM = 0.0d0
!            H2CoM(:) = 0.5d0*(Generations(i)%parents(j)%w%x(m,:)+Generations(i)%parents(j)%w%x(m+1,:))

!            ! Now calculate fractional coords, and shift if necessary.
!            fracCoords = getFractionalCoords(H2CoM,recipVec)
!            shift = 0
!            do k=1,3
!              if (fracCoords(k).gt.1) then
!                shift(k) = -int(fracCoords(k))
!              else if (fracCoords(k).lt.0) then
!                shift(k) = -int(fracCoords(k))+1
!              endif
!              !print *, fracCoords(k), int(fracCoords(k)), shift(k)

!            enddo

!            !print *, (fracCoords(k)+shift(k), k=1,3)

!            ! Shift CoM to unit cell.
!            shiftedCoM = 0.0d0

!            ! Addition: shift to half cell along lattice vector a
!            if ((fracCoords(1)+shift(1).gt.0.5).and.sys%halfCell) then
!              fracCoords(1) = 1-(fracCoords(1)+shift(1))
!              shift(1) = 0
!            endif
!            do k=1,3
!              do l=1,3
!                shiftedCoM(k) = shiftedCoM(k) + (fracCoords(l)+shift(l))*latticeVec(l,k)
!              enddo


!            enddo

!            !print *, 'CoM before:'
!            !print *, (H2CoM(k)*au2ang,k=1,3)
!            !print *, (shift(k),k=1,3)
!            !print *, 'CoM after:'
!            !print *, (shiftedCoM(k)*au2ang,k=1,3)

!            !print *, 'coords before:'
!            !do k=1,2
!            !  print *, (Generations(i)%parents(j)%w%x(k,l)*au2ang,l=1,3)
!            !enddo

!            ! Calculate CoM-H vectors, and add vector to shifted coords.
!            !print *, 'coords after:'

!            !print *, (fracCoords(l)+shift(l),l=1,3)
!            do k=1,2
!              CoMH(k,:) = Generations(i)%parents(j)%w%x(m+k-1,:) - H2CoM(:)
!              shiftedCoords(k,:) = shiftedCoM(:) + CoMH(k,:)
!            enddo


!            ! Save the new walker coordinates.
!            Generations(i)%parents(j)%w%x(m:m+1,1:3) = shiftedCoords(1:2,1:3)

!            !call distt(Generations(i)%parents(j)%w%x(1,:),Generations(i)%parents(j)%w%x(2,:),b2)
!            call distt(shiftedCoords(1,:),shiftedCoords(2,:),b2)




!            ! Bond length check. Leave uncommented for immediate degubbing.
!            if (abs(b1-b2).gt.0.00001) then
!              print *, 'orig coords:'
!              do k=1,2
!                print *, (Generations(i)%parents(j)%w%x(k,l)*au2ang,l=1,3)
!              enddo
!              print *, 'shifted coords:'
!              do k=1,2
!                print *, (shiftedCoords(k,l)*au2ang,l=1,3)
!              enddo
!              print *, 'b1, b2', b1, b2
!            endif
!          enddo
!        enddo
!      enddo



!    endif



!    !================== End MOF-5 hacks, normal descendant weighting follows
!    if (writeXYZ) then
!      open(newunit=u,file='Generations.xyz',status='replace',action='write')

!      do i=1,qdmc%NumGenerations
!        write(u,*) Generations(i)%NumWalkers*sys%natom
!        write(u,*) 'generation', i

!        do j=1,Generations(i)%NumWalkers
!        !  if (Generations(i)%parents(j)%descendants(mInd).ge.1) then
!            do k=1,sys%natom
!              write(u,*) sys%atom_label(k), (Generations(i)%parents(j)%w%x(k,l)*au2ang, l=1,3)
!            enddo
!        !  endif
!        enddo

!      enddo

!      close(u)

!    endif




!        !if (.not.allocated(Generations)) stop 'Generations not allocated'
!        if (.not.allocated(Coordinates)) stop 'Coordinates not allocated'



!        allocate(denominator(1:qdmc%NumGenerations),stat=error)
!        if (error.ne.0) stop 'DW: Error allocating denominator array.'

!        allocate(avr(sys%nbond),stat=error)
!        if (error.ne.0) stop ' Error allocating avr'

!        allocate(avrsq(sys%nbond),stat=error)
!        if (error.ne.0) stop ' Error allocating avrsq'

!        allocate(avinvrsq(sys%nbond),stat=error)
!        if (error.ne.0) stop ' Error allocating avinvrsq'


!        !--------------------------------------------
!        ! check on descendants, make denominator
!        !--------------------------------------------
!        do i=1,qdmc%NumGenerations
!            e = 0.0d0
!            denominator(i) = 0.0
!            do j=1,Generations(i)%NumWalkers
!                d = Generations(i)%parents(j)%descendants(mInd)
!                e = e + Generations(i)%parents(j)%energy * d
!                denominator(i) = denominator(i) + d
!            enddo
!        enddo

!        11  format(2x,2(2x,i6),3(2x,g16.9))

!        ! calculate expectation values <r>, <r**2>

!        avr = 0.0d0
!        avrsq = 0.0d0
!        avinvrsq = 0.d0
!        !  do igrp=1,sys%ngroup

!        do k=1,qdmc%NumGenerations
!            do n=1,Generations(k)%NumWalkers
!                d = Generations(k)%parents(n)%descendants(mInd)
!                call intern(sys,Generations(k)%parents(n)%w%x,walkerR)
!                !        ibond = sys%bperm(i,igrp)
!                avr(:) = avr(:) + 1.0d0/walkerR(:)*d/denominator(k)
!                avrsq(:) = avrsq(:) + (1.0d0/walkerR(:)**2)*d/denominator(k)
!                avinvrsq(:) = avinvrsq(:) + walkerR(:)**2 * d/denominator(k)
!            enddo
!        enddo

!        ! Sum masses for each CoM defined by user.
!        do i=1,nCoMs
!            CoMs(i)%sumMasses = 0.0d0
!            do l=1,CoMs(i)%nAtoms
!                atomInd = Coms(i)%indices(l)
!                CoMs(i)%sumMasses = CoMs(i)%sumMasses + sys%mass(atomInd)
!            enddo
!        enddo




!        allocate(WalkerCoords(sys%nAtom+nComs+nGhosts,3),stat=error)
!        if (error.ne.0) stop 'DW: Error allocating walker coordinates.'

!        ! Loop over generations and walkers.

!        do k=1, qdmc%NumGenerations
!            denom = denominator(k)

!            do j=1, Generations(k)%NumWalkers

!                d = Generations(k)%parents(j)%descendants(mInd)

!                WalkerCoords = 0.0d0
!                WalkerCoords(1:sys%nAtom,:) = Generations(k)%parents(j)%w%x

!                ! Shove ghost atoms into WalkerCoords (note temporary usage of loop counters for indexes)
!                i=sys%nAtom+nCoMs+1
!                m=i+nGhosts-1
!                if (nGhosts.gt.0) WalkerCoords(i:m,:) = ghostAtoms(1:nGhosts,:)

!                ! Calculate centre-of-masses
!                do i=1,nCoMs

!                    CoMInd = sys%nAtom+i

!                    do m=1,3
!                        sumV = 0.0d0

!                        do l=1,CoMs(i)%nAtoms
!                            atomInd = CoMs(i)%indices(l)
!                            !print *, atomInd, sys%atom_label(atomInd), sys%mass(atomInd)
!                            sumV = sumV + sys%mass(atomInd)*WalkerCoords(atomInd,m)
!                        enddo

!                        WalkerCoords(CoMInd,m) = sumV/CoMs(i)%sumMasses
!                    enddo
!                ! End loop over centre-of-masses
!                enddo




!        !!!!========================================
!        !!!! Begin some more yucky MOF-5 hacks here
!        !!!! I'm sorry, future Jordan
!        !!!!========================================
!        if (sys%MOF5Hacks) then
!          ! Do things for histograms here.

!        endif


!        if (qdmc%printVectors.or.qdmc%printObservables) print *, '============='

!                ! Now calculate all required vectors.
!                do i=1, nVectors
!                    i1 = Vectors(i)%indices(1)
!                    i2 = Vectors(i)%indices(2)
!                    select case(trim(Vectors(i)%vecType))
!                        case('Vec')
!                            VectorCarts(i,:) = WalkerCoords(i2,:)-WalkerCoords(i1,:)
!                        case('Add')
!                            VectorCarts(i,:) = VectorCarts(i1,:) + VectorCarts(i2,:)
!                        case('Sub')
!                            VectorCarts(i,:) = VectorCarts(i1,:) - VectorCarts(i2,:)
!                        case('X')
!                            VectorCarts(i,:) = normalise(cross(VectorCarts(i1,:),VectorCarts(i2,:)))*Vectors(i)%length
!                        case('Proj') ! Project vector 1 along vector 2.

!              ! Here, use the property that if a1 is the vector projection of a along b,
!              ! then a1 = (a.b)/(b.b) * b.
!              VectorCarts(i,:) = DOT_PRODUCT(VectorCarts(i1,:),VectorCarts(i2,:)) / &
!                 DOT_PRODUCT(VectorCarts(i2,:),VectorCarts(i2,:))*VectorCarts(i2,:)

!                    end select
!                    if (qdmc%printVectors) print *, Vectors(i)%vecType, 'Bq', VectorCarts(i,:)

!                enddo

!                ! Loop through coordinates and gather observables.


!                if ((qdmc%printVectors).or.(qdmc%printObservables)) then
!                    do i=1,sys%nAtom+nCoMs
!                        if (i.le.sys%nAtom) then
!                            print *, sys%atom_label(i), (WalkerCoords(i,m),m=1,3)
!                        else
!                            print *, 'Bq', (WalkerCoords(i,m),m=1,3)
!                        endif
!                    enddo
!                endif


!                ! Bin atom-based histograms.
!                do i=1,nHists
!                    observable = 0.0d0
!                    select case(trim(Coordinates(i)%coordType))
!                        case('Dist')
!                            ! Calculate bond length.
!                            i1 = Coordinates(i)%indices(1)
!                            i2 = Coordinates(i)%indices(2)
!                            call distt(WalkerCoords(i1,:),WalkerCoords(i2,:),observable)
!                        case('Angle')
!                            ! Calculate angle.
!                            i1 = Coordinates(i)%indices(1)
!                            i2 = Coordinates(i)%indices(2)
!                            i3 = Coordinates(i)%indices(3)
!                            call angle(WalkerCoords(i1,:),WalkerCoords(i2,:),WalkerCoords(i3,:),observable)
!                        case('Dihed')
!                            ! Calculate dihedral.
!                            i1 = Coordinates(i)%indices(1)
!                            i2 = Coordinates(i)%indices(2)
!                            i3 = Coordinates(i)%indices(3)
!                            i4 = Coordinates(i)%indices(4)
!                            call dihedral(WalkerCoords(i1,:),WalkerCoords(i2,:),WalkerCoords(i3,:), &
!                                WalkerCoords(i4,:),observable)
!                        case default
!                            stop 'Bad input in DW coordinate type. This should have been caught earlier!'
!                    end select
!
!                    ! If this observable is outside of the histogram bounds...
!                    if (observable.gt.Coordinates(i)%max) then
!                        observable = Coordinates(i)%max
!                    else if (observable.lt.Coordinates(i)%min) then
!                        observable = Coordinates(i)%min
!                    endif
!
!                    ! Now bin this value.
!
!                    binInt = int(anint(observable/Coordinates(i)%binWidth))
!                    !print *, Coordinates(i)%fileName, observable, binInt, d/denom
!                    Coordinates(i)%Histogram(binInt) = Coordinates(i)%Histogram(binInt) + d/denom
!                    !print *, Coordinates(i)%Histogram(binInt)
!
!                    ! Update coordinate average:
!                    Coordinates(i)%Average = Coordinates(i)%Average + observable * d/denom
!
!                    if (qdmc%printObservables) then
!                        print *, trim(Coordinates(i)%name), observable
!                    endif
!
!                ! End loop over histogram coordinates.
!                enddo
!
!
!
!
!        ! Continue normally...
!
!                ! Bin vector-based histograms.
!                do i=1,nVecHists
!                    observable = 0.0d0
!                    select case(trim(VecCoordinates(i)%coordType))
!                        case('Length')
!                            i1 = VecCoordinates(i)%indices(1)
!                            ! Calculate the length of this vector - square root of dot product with itself.
!                            observable = dsqrt(DOT_PRODUCT(VectorCarts(i1,:),VectorCarts(i1,:)))
!                        case('Angle')
!                            i1 = VecCoordinates(i)%indices(1)
!                            i2 = VecCoordinates(i)%indices(2)
!                            observable = ANGVEC(VectorCarts(i1,:),VectorCarts(i2,:))*180.0d0/pi
!                        case('Dihed')
!                            i1 = VecCoordinates(i)%indices(1)
!                            i2 = VecCoordinates(i)%indices(2)
!                            i3 = VecCoordinates(i)%indices(3)
!                            call dihedralVec(VectorCarts(i1,:),VectorCarts(i2,:),VectorCarts(i3,:),observable)
!                    end select
!
!                    ! If this observable is outside of the histogram bounds...
!                    if (observable.gt.VecCoordinates(i)%max) then
!                        observable = VecCoordinates(i)%max
!                    else if (observable.lt.VecCoordinates(i)%min) then
!                        observable = VecCoordinates(i)%min
!                    endif
!                    !print *, observable
!
!                    ! Now bin this value.
!
!                    binInt = int(anint(observable/VecCoordinates(i)%binWidth))
!                    !print *, VecCoordinates(i)%fileName, observable, binInt, d/denom
!                    VecCoordinates(i)%Histogram(binInt) = VecCoordinates(i)%Histogram(binInt) + d/denom
!                    !print *, VecCoordinates(i)%Histogram(binInt)
!
!                    ! Update coordinate average:
!                    VecCoordinates(i)%Average = VecCoordinates(i)%Average + observable * d/denom
!
!
!
!                    if (qdmc%printObservables) then
!                        print *, trim(VecCoordinates(i)%name), observable
!                    endif
!                ! End loop over vector-based histogram coordinates.
!                enddo
!
!
!                !!!!!!!!!!!!!!!!!!! MOF-5 heat map hack here.
!                if (sys%MOF5Hacks.and.sys%heatMap) then
!
!                  ! Loop over all atoms...
!                  do i=1,sys%natom
!                    alpha = .false.
!                    ! Determine whether this atom is in an alpha cell or beta cell.
!                    shortest = 100.0d0
!                    do l=aMin,aMax
!                      ! Vector between atom and cell centre.
!                      tempVec(:) = WalkerCoords(l,:) - WalkerCoords(i,:)
!                      dist = dsqrt(DOT_PRODUCT(tempVec,tempVec))
!                      if (dist.lt.shortest) then
!                        shortest = dist
!                        CentInd = l
!                      endif
!                    enddo
!                    ! Map index back to original list
!                    centInd = centInd - sys%natom - nCoMs
!                    ! Odd indices are alpha, even are beta
!                    if (MOD(centInd,2).ne.0) alpha = .true.
!
!                    !print *, alpha
!
!                    centInd = centInd + sys%natom + nCoMs
!
!                    ! Find nearest oxide oxygen:
!                    shortest = 100.0d0
!                    do l=oMin,oMax
!                      ! Vector between oxygen at this atom:
!                      tempVec(:) = WalkerCoords(i,:) - WalkerCoords(l,:)
!                      dist = dsqrt(DOT_PRODUCT(tempVec,tempVec))
!                      if (dist.lt.shortest) then
!                        shortest = dist
!                        OInd = l
!                      endif
!                    enddo
!
!                    ! Get Zn tetrahedral around this oxygen:
!                    Zn = 0
!                    m = 0
!                    do l=ZnMin,ZnMax
!                      ! O-Zn vector:
!                      tempVec(:) = WalkerCoords(l,:) - WalkerCoords(OInd,:)
!                      dist = dsqrt(DOT_PRODUCT(tempVec,tempVec))
!                      if (dist.lt.6.0d0) then
!                        m = m+1
!                        Zn(m) = l
!                      endif
!                    enddo
!
!                    ! Calculate z axis, discard Zn atom collinear to z
!                    nearestFrame(3,:) = normalise(WalkerCoords(CentInd,:)-WalkerCoords(OInd,:))
!
!                    do l=1,4
!                      tempVec(:) = WalkerCoords(Zn(l),:) - WalkerCoords(OInd,:)
!                      tempVec(:) = cross(nearestFrame(3,:),tempVec(:))
!                      if (DOT_PRODUCT(tempVec,tempVec).lt.1.0d-10) then
!                        Zn(l) = -1
!                        ZnInd = MINVAL(Zn,Zn.gt.0)
!                        exit
!                      endif
!                    enddo
!
!                    ! ZnInd has been set to the smallest index of the three appropriate Zn atoms, define x- and y-axes:
!                    ! X-axis is the component of the O-Zn vector perpendicular to the Z-axis:
!                    tempVec(:) = WalkerCoords(ZnInd,:) - WalkerCoords(OInd,:)
!
!                    ! Project O-Zn along the Z-axis:
!                    ! First find the component of O-Zn along Z - this is the dot product of O-Zn and Z.
!                    Zcomp = DOT_PRODUCT(tempVec(:),nearestFrame(3,:))
!
!                    ! Then multiply z by this component.
!                    proj(:) = Zcomp * nearestFrame(3,:)
!
!                    ! Calculate x-axis by subtracting this projection from the O-Zn vector:
!                    nearestFrame(1,:) = normalise(tempVec(:) - proj(:))
!
!                    ! The y-axis is just the cross product of these
!                    nearestFrame(2,:) = cross(nearestFrame(1,:),nearestFrame(3,:))
!
!                    ! FINALLY, find components of the O-H vector along x,y,z:
!                    tempVec(:) = WalkerCoords(i,:) - WalkerCoords(OInd,:)
!                    ! Component of vector A along vector B is given by (A.B)/(B.B).
!                    ! Since B is a unit vector, then component is just A.B.
!                    do l=1,3
!                      xyzComp(l) = DOT_PRODUCT(tempVec,nearestFrame(l,:))
!                    enddo
!
!
!                    ! Now we have components along x,y and z, can now bin this atom at appropriate
!                    ! locations within unit cell; that is, in alpha OR beta cells
!                    do l=1,8
!                      ! Calculate vector in appropriate frame
!                      if (alpha) then
!                        tempVec(:) = MATMUL(xyzComp,alphaFrame(l,:,:))
!                        OInd = aZind(l,1)
!                      else
!                        tempVec(:) = MATMUL(xyzComp,betaFrame(l,:,:))
!                        OInd = bZind(l,1)
!                      endif
!
!                      ! Translate vector to appropriate cell:
!                      tempVec(:) = tempVec(:) + WalkerCoords(OInd,:)
!                      fracCoords = getFractionalCoords(tempVec(:),recipVec)
!
!                      !print *, fracCoords(1:2)
!
!                      where (fracCoords.gt.1.0)
!                        fracCoords = fracCoords-1.0d0
!                      elsewhere (fracCoords.lt.0.0)
!                        fracCoords = fracCoords + 1.0d0
!                      endwhere
!
!                      ! Finally, bin histogram.
!                      binX = int(anint(fracCoords(1)/mapWidth))
!                      binY = int(anint(fracCoords(2)/mapWidth))
!                      heatMap(binX,binY) = heatMap(binX,binY)+real(d/denom*0.0625*1.0d0/sys%natom,8)
!                    enddo
!                  enddo
!
!                endif

                ! Add to the radial distribution function.
!                if (sys%RDF.and..not.sys%MOF5Hacks) then
!                  ! Assuming single atoms for the moment, will update for molecular H2.
!                  ! (Use shiftedCoords array for fractional coordinates, since it has the correct
!                  ! dimensionality and why needlessly define extra arrays?)
!                  shiftedCoords = sys%cartToFrac(sys%natom,WalkerCoords(1:sys%natom,:))
!                  do i=1,sys%natom-1
!                    do l=i+1,sys%natom
!                      ! Calculate difference between x,y,z fractional coordinates for this pair.
!                      dx = shiftedCoords(i,:)-shiftedCoords(l,:)
!
!                      ! Apply periodic boundary conditions
!                      if (sys%pbc) dx = dx - anint(dx)
!                      ! If dx > 0.5 (or < -0.5), anint(dx) = ± 1 and subtracting anint(dx) maps
!                      ! dx to the nearest periodic image.
!
!                      ! Finally, calculate the distance.
!                      dist = DSQRT(DOT_PRODUCT(dx,dx))*sys%length
!
!                      if (.not.sys%pbc.or.dist.lt.0.5*sys%length) then ! Only bin distances within half the box length (or any distance if we don't care about the box)
!                        binInt = int(dist/delG)
!                        gr(binInt) = gr(binInt) + 2
!                      endif
!                    enddo
!                  enddo
!                else if (sys%RDF.and.sys%MOF5Hacks) then
!                  ! Radial distribution function for H2 in MOF-5. Sorry.
!                  shiftedCoords = sys%cartToFrac(sys%natom,WalkerCoords(1:sys%natom,:))
!                  do i=1,sys%natom-3,2
!                    do l=i+2,sys%natom-1,2
!                      ! Calculate difference between x,y,z fractional coordinates for this pair.
!                      ! Calculate the centre-of-mass at the same time.
!                      dx = 0.5d0*(shiftedCoords(i,:)+shiftedCoords(i+1,:)-shiftedCoords(l,:)-shiftedCoords(l+1,:))
!
!                      ! Apply periodic boundary conditions
!                      if (sys%pbc) dx = dx - anint(dx)
!                      ! If dx > 0.5 (or < -0.5), anint(dx) = ± 1 and subtracting anint(dx) maps
!                      ! dx to the nearest periodic image.
!
!                      ! Finally, calculate the distance.
!                      dist = DSQRT(DOT_PRODUCT(dx,dx))*sys%length
!
!                      if (.not.sys%pbc.or.dist.lt.0.5*sys%length) then ! Only bin distances within half the box length (or any distance if we don't care about the box)
!                        binInt = int(dist/delG)
!                        gr(binInt) = gr(binInt) + 2
!                      endif
!                    enddo
!                  enddo
!                endif
!
!
!
!            ! End loop over walkers
!            enddo
!        ! End loop over generations
!        enddo
!
!        ! Now loop through coordinates and print histograms
!        do i=1, nHists
!            ! A little bit of bureaucracy before printing:
!
!            ! Ensure dihedrals are continuous.
!            if ((Coordinates(i)%coordType == 'Dihed').and.(abs(Coordinates(i)%max-360d0).le.0.0001)) then
!                j = Coordinates(i)%intMin
!                k = Coordinates(i)%intMax
!                ! Only if maximum value of dihedral is 360 degrees.
!                Coordinates(i)%Histogram(j) = Coordinates(i)%Histogram(j) + Coordinates(i)%Histogram(k)
!                Coordinates(i)%Histogram(k) = Coordinates(i)%Histogram(j)
!            endif
!
!            Coordinates(i)%Histogram = Coordinates(i)%Histogram/qdmc%NumGenerations
!            Coordinates(i)%Average = Coordinates(i)%Average/qdmc%NumGenerations
!
!            fileName = trim(Coordinates(i)%fileName)//trim(qdmc%stem)
!            u = Coordinates(i)%fileUnit
!
!            open(unit=u,file=trim(fileName),status='unknown')
!
!            ! Write the histogram to file!
!
!            x = Coordinates(i)%min
!            do j=Coordinates(i)%intMin, Coordinates(i)%intMax
!                !print *, x, Coordinates(i)%Histogram(j)
!                write(u,*) x, Coordinates(i)%Histogram(j)
!                x = x + Coordinates(i)%binWidth
!            enddo
!
!            close(u)
!
!
!
!        enddo
!
!        do i=1, nVecHists
!            ! A little bit of bureaucracy before printing:
!
!            ! Ensure dihedrals are continuous.
!            if ((VecCoordinates(i)%coordType == 'Dihed').and.(abs(VecCoordinates(i)%max-360d0).le.0.0001)) then
!                ! Only if maximum value of dihedral is 360 degrees.
!                VecCoordinates(i)%Histogram(VecCoordinates(i)%intMin) =  &
!                    VecCoordinates(i)%Histogram(VecCoordinates(i)%intMin) + VecCoordinates(i)%Histogram(VecCoordinates(i)%intMax)
!                VecCoordinates(i)%Histogram(VecCoordinates(i)%intMax) = VecCoordinates(i)%Histogram(VecCoordinates(i)%intMin)
!            endif
!
!            VecCoordinates(i)%Histogram = VecCoordinates(i)%Histogram/qdmc%NumGenerations
!            VecCoordinates(i)%Average = VecCoordinates(i)%Average/qdmc%NumGenerations
!
!            fileName = trim(VecCoordinates(i)%fileName)//trim(qdmc%stem)
!            u = VecCoordinates(i)%fileUnit
!
!            open(unit=u,file=trim(fileName),status='unknown')
!
!            ! Write the histogram to file!
!            x = VecCoordinates(i)%min
!            do j=VecCoordinates(i)%intMin, VecCoordinates(i)%intMax
!                !print *, x, VecCoordinates(i)%Histogram(j)
!                write(u,*) x, VecCoordinates(i)%Histogram(j)
!                x = x + VecCoordinates(i)%binWidth
!            enddo
!
!            close(u)
!
!        enddo
!
!
!
!        avr = avr/qdmc%NumGenerations
!        avrsq = avrsq/qdmc%NumGenerations
!        avinvrsq = avinvrsq/qdmc%NumGenerations
!
!          print *,' i   <r>   <r**2> <1/r**2> '
!          do i=1,sys%nbond
!             print 12,i,avr(i),avrsq(i),avinvrsq(i)
!          enddo
!
!        12 format(2x,i4,3(2x,g12.5))
!
!        print *, ' '
!
!        print *, '---------------------Expectation values----------------------'
!
!        ! print averages/expectation values for defined coordinates.
!        do i=1, nHists
!            print *, '<',trim(Coordinates(i)%name),'>', Coordinates(i)%Average
!        enddo
!
!        do i=1, nVecHists
!            print *, '<',trim(VecCoordinates(i)%name),'>', VecCoordinates(i)%Average
!        enddo
!
!        print *, '-------------------------------------------------------------'
!
!
!
!    !!!!!!!! Final MOF-5 hack here.
!    if (sys%MOF5Hacks.and.sys%heatMap) then
!      open (newunit=hUnit,file='Map.dat',status='replace')
!      heatMap = heatMap/qdmc%NumGenerations
!      do i=0,max
!        do j=0,max
!            write(hUnit,*) i*mapWidth, j*mapWidth, heatMap(i,j)
!        enddo
!        write(hUnit,*)
!      enddo
!
!      close(hUnit)
!    endif
!
!    ! Print radial distribution function.
!    if (sys%RDF) then
!      ! The ideal gas density = n_particles/volume
!      rho = sys%natom/(sys%length**3)
!
!      ! MOF-5: density is actually half the above, since we treat H2 as a diatomic and not a spherical cow.
!      if (sys%MOF5Hacks) rho = 0.5d0*rho
!
!      sumV = SUM(gr)*delG
!
!      open (newunit=hUnit,file='RadialDist.OUT',status='replace')
!      do i=1,grMax
!        dist = delG*(i+1) ! The distance
!
!        ! The volume between this and the next bin.
!        vol = ((i+1)**3 - i**3)*delG**3
!
!        ! The number of ideal gas particles in this volume.
!        nIdeal = 1
!        if (sys%pbc) then
!          nIdeal = (4.0d0/3.0d0)*pi*vol*rho
!          gr(i) = gr(i)/(sys%natom*nIdeal*totWalkers)
!        else
!          gr(i) = gr(i)/sumV
!        endif
!
!        write(hUnit,*) dist, gr(i)
!      enddo
!      close(hUnit)
!
!    endif
!
!
!
!
!
!
!
!    end subroutine OldDescendantWeighting

!===================================================================
! Adds a number to the correct place in an array, sorted from lowest
! to highest.
!===================================================================
function addToSortedArray(newVal,array) result(newArray)
  real(kind=8), intent(in) :: newVal
  real(kind=8), dimension(:), intent(in) :: array
  real(kind=8), dimension(1:size(array)) :: newArray
  integer :: i,n

  n = size(array)
  newArray = array

  ! Find where the new value should go.
  do i=1,n
    if (newVal.le.array(i)) then
      ! Shift all the array elements after this one.
      if (i.ne.n) then
        newArray(i+1:n) = array(i:n-1)
      endif
      newArray(i) = newVal
      return
    endif
  enddo

  stop "addToSortedArray: we shouldn't be here, Jordan."


end function addToSortedArray

!===================================================================
! Takes the given cartesian coordinates and reciprocal lattice
! vectors, and returns fractional coordinates.
!===================================================================
function getFractionalCoords(x,recip) result(frac)
  real(kind=8), dimension(1:3), intent(in) :: x
  real(kind=8), dimension(1:3,1:3), intent(in) :: recip
  real(kind=8), dimension(1:3) :: frac
  integer :: i, j

  frac = 0.0d0

  do i=1,3
    do j=1,3
      frac(i) = frac(i) + x(j)*recip(i,j)
    enddo
  enddo
  return
end function getFractionalCoords



end module Histograms_mod
