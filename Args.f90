module Args_mod
implicit none
!=====================================================
! Flags and command line input for the program itself.
!=====================================================
type tArgs
  ! Simulation stuff
  logical :: DMC = .true.
  logical :: printDMCsteps = .false.
  logical :: trackTime = .false.
  logical :: crystal = .false.
  logical :: pbc = .false.
  logical :: writeXYZ = .false.
  logical :: killWalkers = .true.
  logical :: printLowE = .true.
  real(kind=4) :: cutTime

  ! Related to energy calculations
  logical :: calcEnergies = .false.
  logical :: absEnergy = .false.
  logical :: calcRigidBonds = .false.
  logical :: printInterp = .false.
  logical :: CPUtime = .false.
  logical :: printDetails = .false.
  character(30) :: xyzFile

  ! Restart-related things.
  logical :: saveProg = .false.
  logical :: restarting = .false.

  ! Histogram suffix
  character(30) :: suffix

  ! Descendant weighting
  logical :: DWonly = .false.
  logical :: printObs = .false.
  logical :: printVectors = .false.
  logical :: DWtoXYZ = .false.
  logical :: multiStep = .false.
  integer :: steps = 0

  ! Misc
  logical :: writeTOUT = .false.
  integer :: nTOUT = 1
  logical :: MOF5Hacks = .false.
  logical :: halfCell = .false.
  logical :: heatMap = .false.
  logical :: degub = .false.
  logical :: bigMem = .false.
  logical :: rdf = .false.
  logical :: QH2Hacks = .false.

  contains

    procedure, pass(this) :: get => getArguments
end type tArgs


contains

!=====================================================
! Reads the arguments from the command line.
!=====================================================
subroutine getArguments(this)
  implicit none
  class (tArgs) :: this

  integer :: nArgs, indArg
  character(256) :: cArg, charCutTime
  character(3) :: cTOUT

  nArgs = command_argument_count()
  indArg = 1
  do while (indArg.le.nArgs)
    ! Get this argument.
    call get_command_argument(indArg,cArg)
    select case (ADJUSTL(cArg))
      case('--crystal')
        ! This is a crystal simulation, instead of gas phase.
        ! Will be using Terry's code for all energy evaluations.
        this%crystal = .true.
      case('--PBC') ! Switch on periodic boundary conditions
        this%pbc = .true.

      case('--RDF') ! Calculates radial distribution function between atoms.
      ! Will update for fragments later.
        this%rdf = .true.

      case('--bigmem')
        this%bigMem = .true.

      case('--wallCutOff')
        this%trackTime = .true.
        call get_command_argument(indArg+1,charCutTime)
        read(charCutTime,*) this%cutTime

        indArg = indArg+2
        cycle

      case('--printDMCsteps')
        this%printDMCsteps = .true.

      case('--restartable')
        this%saveProg = .true.

      case('--restart')
        this%restarting = .true.
        ! restarting also implies simulation should be restartable.
        this%saveProg = .true.

      case('--suffix')
        call get_command_argument(indArg+1,this%suffix)
        indArg = indArg+2
        cycle
      case('--dontKill')
        this%killWalkers = .false. ! Don't kill walkers if their energy is below the global minimum, at all.

      case('--dontPrintLowE')
        this%printLowE = .false.

      case('--energy')
        ! No DMC will be performed.
        this%DMC = .false.
        this%calcEnergies = .true.
        ! Get name of .xyz file.
        call get_command_argument(indArg+1,this%xyzFile)
        indArg = indArg+2
        cycle

      case('--absenergy') ! Same as above, but prints the absolute energy.
        ! No DMC will be performed.
        this%DMC = .false.
        this%calcEnergies = .true.
        this%absEnergy = .true.
        ! Get name of .xyz file.
        call get_command_argument(indArg+1,this%xyzFile)
        indArg = indArg+2
        cycle

      case('--printDetails') ! Prints extra details for energy calculations.
        this%printDetails = .true.

      case('--calcRigidBonds')  ! Ignores the IN_FRAG file for all MSI fragments and calculates rigid atom-atom distances during interpolation
        this%calcRigidBonds = .true.

      case('--printInterp')
        this%printInterp = .true.

      case('--CPUtime')
        this%CPUtime = .true.

      case('--DWonly')
        this%DWonly = .true.
        this%DMC = .false.

      case('--DWtoXYZ')
        this%DWtoXYZ = .true.

      case('--multiStep') ! Tracks descendant weighting generations for multiple steps
        this%multiStep = .true.
        print *, 'Descendants will be tracked for multiple numbers of steps:'
        print *, '2, 4, 8, 16, ... max'
        print *, 'Log2(max) must be an integer, specified in IN_QDMC.'

      case('--step')
       if (.not.this%multiStep) stop 'Specify --multiStep before using --step.'

       call get_command_argument(indArg+1,cArg)
       read(cArg,*) this%steps
       indArg = indArg+2
       cycle

      case('--printvectors')
        this%printVectors = .true.

      case('--printobs')
        this%printObs = .true.

      case('--writeTOUT')
        this%writeTOUT = .true.


      case('--writeXYZ')
        this%writeXYZ = .true.
        print *, 'Will write all walkers to .xyz file... hope your hard drive is big enough.'
        call execute_command_line('rm -f Walkers.xyz')

      case('--MOF5') ! Activates any MOF-5 hacks in the code.
        this%MOF5Hacks = .true.
        print *, 'Caution: MOF5-specific code hacks activated.'
        print *, 'Will do the following:'
        print *, 'Shift descendant weighting walkers to unit cell.'
        print *, 'Bin special histograms.'
        print *, 'If specified with --halfcell, shift DW walkers to half of the unit cell.'
        print *, '--heatmap will make a heat-map-style plot out of descendant weighting walkers.'

      case('--halfcell')
        this%halfCell = .true.

      case('--heatmap')
        this%heatMap = .true.

      case('--QH2')
        this%QH2Hacks = .true.
        print *, '(H2)4-Li+-benzene code hacks activated.'


      case('--help')
        print *, "*~ We can't help everyone, but everyone can help someone. ~*"
        print *, '-- Ronald Reagan'
        call exit

      case('--degub')
        this%degub = .true.


      case default
        print *, 'Option ', trim(cArg), ' not recognised.'
        print *, 'Valid options are:'
        print *, '--help'
        print *, '--crystal'
        print *, '--restartable'
        print *, '--restart'
        print *, '--suffix <histogram suffix>'
        print *, '--printDMCsteps'
        print *, '--printInterp'
        print *, '--calcRigidBonds'
        print *, '--dontKill'
        print *, '--dontPrintLowE'
        print *, '--energy <.xyz file>'
        print *, '--absenergy <.xyz file>'
        print *, '--writeTOUT'
        print *, '--nTOUT <no. geometries to add to TOUT>'
        print *, '--writeXYZ'
        print *, '--CPUtime'
        print *, '--wallCutOff <time, hours>'
        print *, '=== Descendant-weighting-only options: ==='
        print *, '--DWonly'
        print *, '--printobs'
        print *, '--printvectors'
        print *, '--DWtoXYZ'
        print *, '--multiStep'
        print *, '--step <tracking steps>, only if using --DWonly and --multiStep'
        print *, '--QH2'
        print *, '=== MOF5/crystal-only options: ==='
        print *, '--MOF5'
        print *, '--PBC'
        print *, '--RDF'
        print *, '--halfcell'
        print *, '--heatmap'
        call exit(0)


      end select


    indArg = indArg+1
  enddo


end subroutine getArguments

end module Args_mod
