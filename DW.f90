!======================================================================
! A module for all the descendant weighting happy funtimes.
!======================================================================
module DW_mod

use Walkers_mod, ONLY : tWalker, tWalkers, tWalkerBox
use qdmc_structures, ONLY : qdmc_par

implicit none

private

public :: tGenerations, tGeneration

!=====================================================================
! A parent walker in a descendant weighting generation.
!=====================================================================
type tParent
  type(tWalker) :: w ! The walker object representing this parent.
  integer, dimension(:), allocatable :: descendants ! The number of descendants.
  integer :: ID ! The ID of this parent in this generation.
  real(kind=8) :: energy
end type tParent

!=====================================================================
! A single descendant weighting generation. Contains details about
! the start and finish times, tracking, and an array of parents.
!=====================================================================
type tGeneration
  type(tParent), dimension(:), allocatable :: parents
  integer numwalkers
  integer start_time
  integer finish_time
  logical started
  logical finished
  logical dumpToFile

  contains
    !procedure, pass(this) :: startGen => startGeneration
    !procedure, pass(this) :: endGen => endGeneration

end type tGeneration



!=====================================================================
! Finally, a single type to hold all the generations. This is what
! the DMC simulation above will handle.
!=====================================================================
type tGenerations
  type (tGeneration), dimension(:), allocatable :: Generation
  integer :: numGens, delay, trackingSteps
  integer :: activeGens, started, finished
  integer :: nAtoms
  integer :: nMultiStep ! The number of multi-step tracking blocks
  logical :: dumpDWdata
  logical :: printXYZ
  logical :: multiStep

  contains
    procedure, pass(this) :: setup => setupGenerations
    procedure, pass(this) :: update => updateGenerations
    procedure, pass(this) :: dump => dumpGenerations
    procedure, pass(this) :: load => loadGenerations
end type tGenerations

contains

!==================================================================
! A few bits and pieces to set up the array of generations.
!==================================================================
subroutine setupGenerations(this,numGens,delay,trackingSteps,nAtoms)
  class (tGenerations) :: this
  integer, intent(in) :: numGens, delay, trackingSteps, nAtoms
  integer :: i, j, error
  this%numGens = numGens
  this%delay = delay
  this%trackingSteps = trackingSteps
  this%nAtoms = nAtoms
  this%dumpDWdata = .false.

  ! Allocate array of generations.
  allocate(this%Generation(1:numGens),stat=error)
  if (error.ne.0) stop 'Error allocating generations array.'

  this%activeGens = 0
  this%started = 1
  this%finished = 1

  if (this%multiStep) then
    this%nMultiStep = int(LOG(real(this%trackingSteps))/LOG(2.0))
  else
    this%nMultiStep = 1
  endif


  ! Loop through generations, set some numbers.
  do i=1, numGens
    this%Generation(i)%numwalkers = 0
    this%Generation(i)%started = .false.
    this%Generation(i)%finished = .false.
    this%Generation(i)%dumpToFile = .false.

    !if (i == 1) then
    !  this%Generation(i)%start_time = 1
    !else
      this%Generation(i)%start_time = (i-1)*delay+1
    !endif

    this%Generation(i)%finish_time = this%Generation(i)%start_time + trackingSteps

  enddo
end subroutine setupGenerations

!==================================================================
! Updates the generations object:
! 1. Starts a new generation if required.
! 2. Ends a generation if required.
!==================================================================
subroutine updateGenerations(this,WalkerObj,nWalkers,step)
  class (tGenerations) :: this
  type (tWalkers), intent(inout) :: WalkerObj ! The overall object that contains the walkers.
  type (tWalkerBox), dimension(1:nWalkers) :: Walkers ! Pointers to the walkers themselves.
  integer, intent(in) :: nWalkers, step
  integer :: i, j, k, l, n, error

  ! Loop through generations, checking for unstarted ones.
  do i=this%started, this%numGens
    if (.not.this%Generation(i)%started) then
      if (this%Generation(i)%start_time == step) then
        ! Time to start this generation.
        this%Generation(i)%numwalkers = nWalkers

        this%activeGens = this%activeGens+1
        this%started = this%started+1

        ! Crack open the walkers...
        Walkers = WalkerObj%getWalkers()

        ! Make copies of the walkers.
        allocate(this%Generation(i)%parents(1:nWalkers),stat=error)

        if (error.ne.0) stop 'Error allocating parents array in updateGenerations.'
        do j=1, nWalkers
          ! Create the new parents, passing in the geometry of the current walker.
          call this%Generation(i)%parents(j)%w%create(0,0,Walkers(j)%w)

          ! Deallocate r array to avoid memory badness
          deallocate(this%Generation(i)%parents(j)%w%r)

          ! Give this parent an ID, add it to the current walker.
          this%Generation(i)%parents(j)%ID = j
          call Walkers(j)%w%addParent(j,this%activeGens)

          allocate(this%Generation(i)%parents(j)%descendants(1:this%nMultiStep))
          this%Generation(i)%parents(j)%descendants = 0
          this%Generation(i)%parents(j)%energy = Walkers(j)%w%Vcurr

        enddo

        print *, '     started generation ',i,' at step ',step
        this%Generation(i)%started = .true.

        this%Generation(i)%dumpToFile = .true.
        this%dumpDWdata = .true. ! flip the switches to save DW data.

        ! Since we don't want to start more than one generation at a time, we can exit this loop.
        exit
      endif
    endif
  enddo

  ! Update multi-step tracking, if applicable
  if (this%multiStep) then
    n=0
    ! Loop over active generations (note: this%finished contains active generations for some reason)
    do i=this%finished, this%finished+this%activeGens-1
      ! Now loop over multi-step intervals
      do j=1,this%nMultiStep-1
        if (step.eq.this%Generation(i)%start_time+2**j) then
          !print *, 'update generation', i, 'step', step
          n=i-this%finished+1 ! the index of this generation in the walker's ID array.
          ! Count descendants... crack open the walkers
          Walkers = WalkerObj%getWalkers()
          ! Loop through and count descendants.
          !print *, 'update gen', i, 'at step', step
          do l=1, nWalkers
            k = Walkers(l)%w%getParent(n,.false.)
            !print *, n, k, this%Generation(i)%numwalkers
            ! The first argument above is the index of this generation in the walker's ID array.
            this%Generation(i)%parents(k)%descendants(j) = this%Generation(i)%parents(k)%descendants(j)+1
          enddo

          ! We can now exit this loop.
          exit


        endif
      enddo
    enddo
  endif

  ! Now loop through generations and check for finished ones.
  do i=this%finished, this%numGens
    if (.not.this%Generation(i)%finished) then
      if (this%Generation(i)%finish_time == step) then
        ! Time to finish this generation.
        this%Generation(i)%finished = .true.
        this%finished = this%finished+1

        ! Crack open the walkers once more...
        Walkers = WalkerObj%getWalkers()


        ! Loop through and count descendants.
        n = this%nMultiStep
        do j=1, nWalkers
          k = Walkers(j)%w%getParent(this%activeGens,.true.)

          this%Generation(i)%parents(k)%descendants(n) = this%Generation(i)%parents(k)%descendants(n)+1
        enddo

        ! I think that's it.
        this%activeGens = this%activeGens-1

        print *, '     finished generation ',i,' at step ',step

        this%dumpDWdata = .true.

        exit

      endif
    endif
  enddo

end subroutine updateGenerations

!==================================================================
! Dumps the descendant weighting generations to various files.
!==================================================================
subroutine dumpGenerations(this)
  class (tGenerations) :: this
  integer :: unit1, unit2, unit3, unit
  integer :: i, j, k, l, nAtoms
  logical :: fileExists

  ! Will be writing to three files: DWProg.OUT, DW.OUT, CurrGen.OUT
  ! DWProg.OUT contains details about the generations themselves - start/end time, number of walkers,
  ! etc.
  ! DW.OUT contains the final descendant weighting data, and a generation is only added
  ! when it's finished being tracked.
  ! CurrGen.OUT contains generations that are still being tracked.

  if (.not.this%dumpDWdata) return

  ! Backup old files, if they exist.
  inquire(file='DWProg.OUT',exist=fileExists)
  if (fileExists) call System('cp DWProg.OUT DWProg.Backup')

  !inquire(file='DW.OUT',exist=fileExists)
  !if (fileExists) call System('cp DW.OUT DW.Backup')

  ! Open the three files.
  open(newunit=unit1,file='DWProg.OUT',status='replace',access='sequential')
  open(newunit=unit2,file='DW.OUT',status='unknown',access='append')
  open(newunit=unit3,file='CurrGen.OUT',status='replace',access='sequential')

  write(unit1,*) this%numGens

  ! Loop through generations
  do i=1, this%numGens

    if (this%Generation(i)%dumpToFile) then
      ! Check which file this generation needs to be saved to.
      if (.not.this%Generation(i)%finished) then
        unit = unit3 ! Save in CurrGen.OUT
      else
        unit = unit2 ! Save in DW.OUT
        this%Generation(i)%dumpToFile = .false. ! Won't need to write this generation again.
      endif

      ! Loop over walkers in generation, etc.
      do j=1, this%Generation(i)%numwalkers
        do k=1,SIZE(this%Generation(i)%parents(j)%w%x,1)
          write(unit,*) (this%Generation(i)%parents(j)%w%x(k,l),l=1,3)
        enddo

        write(unit,*) this%Generation(i)%parents(j)%ID,&
          this%Generation(i)%parents(j)%energy

        write(unit,*) (this%Generation(i)%parents(j)%descendants(k),k=1,this%nMultiStep)
      enddo
    endif

    ! Write some details to DWProg.OUT.
    write(unit1,*) this%Generation(i)%numwalkers,&
      this%Generation(i)%started,&
      this%Generation(i)%finished,&
      this%Generation(i)%dumpToFile

  enddo ! End the loop over generations

  ! Close files.
  close(unit1)
  close(unit2)
  close(unit3)

  this%dumpDWdata = .false.



end subroutine dumpGenerations

!==================================================================
! Load the descendant weighting data from various files.
!==================================================================
subroutine loadGenerations(this)
  use Utilities, ONLY : ProgressBar

  class (tGenerations) :: this
  type (tWalker) :: tempWalker
  logical :: fileExists
  integer :: i, j, unit1, unit2, unit3, unit4, unit
  integer :: numGens, k, l, error

  type(ProgressBar) :: progress

  if (this%numGens == 0) return

  ! Check that DWProg.OUT exists.
  inquire(file='DWProg.OUT',exist=fileExists)
  if (.not.fileExists) stop 'DWProg.OUT not found!'

  ! Open the three files.
  open(newunit=unit1,file='DWProg.OUT',status='unknown',action='read')
  open(newunit=unit2,file='DW.OUT',status='unknown',action='read')
  open(newunit=unit3,file='CurrGen.OUT',status='unknown',action='read')


  read(unit1,*) numGens

  call progress%new(0.0,real(this%numGens,4),"  Loading generations... ")

  ! Create temporary walker
  call tempWalker%create(this%nAtoms,1)

  print *



  ! Loop through the generations
  do i=1, this%numGens
    read(unit1,*) this%Generation(i)%numwalkers,&
      this%Generation(i)%started,&
      this%Generation(i)%finished,&
      this%Generation(i)%dumpToFile

    if (this%Generation(i)%numwalkers.gt.0) then
      ! Decide where to read this generation's walker data from, if anywhere.
      if (this%Generation(i)%dumpToFile) then
        ! This means generation is being saved in CurrGen.OUT.
        unit = unit3
      else
        unit = unit2 ! DW.OUT
      endif

      allocate(this%Generation(i)%parents(1:this%Generation(i)%numwalkers),stat=error)
      if (error.ne.0) stop 'Error allocating parents array in loadGenerations.'

      ! Now we can loop over walkers/parents in this generation.
      do j=1, this%Generation(i)%numwalkers
        ! Get coordinates
        do k=1,this%nAtoms
          read(unit,*) (tempWalker%x(k,l),l=1,3)
        enddo
          !read(unit,*) tempWalker%x(:,:)
        ! Other things
        read(unit,*) this%Generation(i)%parents(j)%ID,&
          this%Generation(i)%parents(j)%energy

        allocate(this%Generation(i)%parents(j)%descendants(1:this%nMultiStep))

        read(unit,*) (this%Generation(i)%parents(j)%descendants(k),k=1,this%nMultiStep)

        ! Copy temporary walker.
        call this%Generation(i)%parents(j)%w%create(0,0,tempWalker)

        ! Deallocate r array to avoid memory badness
        deallocate(this%Generation(i)%parents(j)%w%r)



      enddo

      call progress%update(real(i,4))

      this%started = this%started+1

      ! Some quick checks:
      if (this%Generation(i)%started.and..not.this%Generation(i)%finished) then
        this%activeGens = this%activeGens+1
      endif

      if (this%Generation(i)%finished) this%finished = this%finished+1

    endif
  enddo ! End the loop over generations.

  ! Cleanup
  call tempWalker%kill()

  ! Close files.
  close(unit1)
  close(unit2)
  close(unit3)

  print *

end subroutine loadGenerations

end module DW_mod
